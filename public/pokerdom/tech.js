// routines to get vw height and width


        <script type="text/javascript">
            $(window).click(function(e) {
                $("#wvw").text($(window).width());
                $("#wvh").text($(window).height());
                var ratio = $(window).width() / $(window).height();
                $("#wvr").text(ratio);

                var onevw = $(window).width() / 100;
                console.log(onevw)
                var id = e.target.id;
                $("#oid").text(id);

                console.log($("#" + id).width())
                var ovw = $("#" + id).width() / onevw;
                ovw = Math.ceil(ovw * 100) / 100;
                $("#ovw").text(ovw);

                var ovh = ovw / ($("#" + id).width() / $("#" + id).height());
                $("#ovh").text(Math.ceil(ovh * 100) / 100);
            });
            // width: 17.42vw; height: 11.01vh;
        </script>


        <div id="viewportblock" style="position: absolute; border: 1px solid red; top: 400px; color: #FFF;">
            <p>Window viewport width: <span id="wvw"></span></p>
            <p>Window viewport height: <span id="wvh"></span></p>
            <p>Window viewport ratio: <span id="wvr"></span></p>
            <p>Object id: <span id="oid"></span></p>
            <p>Object viewport width: <span id="ovw"></span></p>
            <p>Object viewport height: <span id="ovh"></span></p>
        </div>
