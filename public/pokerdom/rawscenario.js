﻿scenarioRaw = JSON.parse('{"name": "Test",\
    "flop": ["ca", "sj", "da"],\
    "dealer_id": 1, \
    "blind_size": 10,\
    "turn": "c3",\
    "river": "dq",\
    "gotoUrl": "https://www.kamagames.com/pokerist",\
    "actionUrl": "https://apps.apple.com/app/texas-holdem-poker-pokerist/id370339723",\
    "actionUrlText": "Испытай УДУЧУ",\
    "soundtrack" : "data/file_example_MP3_2MG.mp3",\
    "winDelaySec": 1,\
    "winnerText": "Привет наш промокод ВОТ",\
    "players": [\
	{ "id": 0, "active": true, "playable": "no", "name": "polyubomu", "balance": 150500, "image": null, "cards": ["c2", "c6"], "strategy": "callblinds" },\
    { "id": 1, "active": true, "playable": "no", "name": "geka", "balance": 3000, "image": null, "cards": ["s2", "s6"], "strategy": "alwayscall" },\
    { "id": 2, "active": true, "playable": "no", "name": "sasha", "balance": 1200, "image": null, "cards": ["sk", "hk"], "strategy": "callblinds" },\
    { "id": 3, "active": true, "playable": "no", "name": "player999", "balance": 1000, "image": "avatar-1.jpg", "cards": ["sq", "h9"], "strategy": "callblinds" },\
    { "id": 4, "active": true, "playable": "yes", "name": "Андрей", "balance": 2200, "image": "avatar-andrey.jpg", "cards": ["ha", "hj"], "strategy": "alwayscall" },\
    { "id": 5, "active": true, "playable": "no", "name": "circus", "balance": 1800, "image": "avatar2.jpg", "cards": ["d5", "d6"], "strategy": "callblinds" } ],\
    "rounds": [ \
    [{ "id": 0, "action": "check", "bet": 0 }, { "id": 1, "action": "check", "bet": 0 }, { "id": 2, "action": "check", "bet": 0 }, { "id": 3, "action": "check", "bet": 0 }, { "id": 4, "action": "call", "bet": 0 }, { "id": 5, "action": "call", "bet": 0 }],\
    [{ "id": 0, "action": "raise", "bet": 50 }, { "id": 1, "action": "raise", "bet": 600 }, { "id": 2, "action": "call", "bet": 0 }, { "id": 3, "action": "call", "bet": 0 }, { "id": 4, "action": "check", "bet": 0 }, { "id": 5, "action": "raise", "bet": 1200 }],\
    [{ "id": 0, "action": "check", "bet": 0 }, { "id": 1, "action": "check", "bet": 0 }, { "id": 2, "action": "check", "bet": 0 }, { "id": 3, "action": "check", "bet": 0 }, { "id": 4, "action": "check", "bet": 0 }, { "id": 5, "action": "check", "bet": 0 }],\
    [{ "id": 0, "action": "check", "bet": 0 }, { "id": 1, "action": "raise", "bet": 200 }, { "id": 2, "action": "call", "bet": 0 }, { "id": 3, "action": "call", "bet": 0 }, { "id": 4, "action": "call", "bet": 0 }, { "id": 5, "action": "call", "bet": 0 }] ]\ }');