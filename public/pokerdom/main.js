﻿// Poker simulator
// Developed for Bazelevs production
// Not perfect, but very cool


/* CHECK THAT http://visionmedia.github.io/move.js/ */
var diamond_prefix = "d"; // ♦
var hearts_prefix = "h"; // ♥
var spades_prefix = "s"; // ♠
var clubs_prefix = "c"; // ♣

var cards_prefix = "Assets/cards/";
var cards_ext = ".png";
var player_prefix = "player_";
var data_folder = "data/";
var balanceSuffix = " р.";
var slider_object = "slider-bet";

var actions = ["check", "fold", "raise", "call", "reraise"];
var player_strategy = ["alwayscall", "alwaysfold", "callblinds"];
var actionsRU = []; actionsRU["smallblind"] = "Чек"; actionsRU["blind"] = "Чек"; actionsRU["check"] = "Чек"; actionsRU["fold"] = "Сброс"; actionsRU["raise"] = "Ставка"; actionsRU["call"] = "Принято"; actionsRU["reraise"] = "Увеличение";
// player strategy works as follows:
// - alwayscall - calls every raise that another player maked
// - alwaysfold - folds everything another player raises
// - callblinds - callblinds but do not call raises

// SO ♣2 is c2, ♣K is ck, ♣A is ca

var scenario = null; // loaded via raw file or by Ajax

var pokersim = {
    rounds: [],
    flopline: null,
    round: "setup",
    table: null,
    roundStep: 0,
    blindSize: 0,
    totalBank: 0,
    roundData: null,
    statusFadeTimeout: 1500,
    playerSecondsToReact: 3,
    cardAnimateDelay: 100,
    gameStartDelay: 1000,
    dealerPlayerId: null,
    players: [],
    playerIds: [],
    activePlayerIds: [],
    roundBets: [],
    currentRoundBet: 0,
    tableCenterCoords: {},
    cardImageCash: [],
    soundtrackFile: null,
    gotoUrl: null,
    winDelaySec: 0,
    winnerText: null,

    // params
    mode: "regular", // regular or debug
    source: "file", // file or local
    scenarioDataFile: "",

    // Technical
    enableDebug: function (timesFaster) {
        pokersim.statusFadeTimeout /= timesFaster; //500
        pokersim.playerSecondsToReact /= timesFaster; // 0.5
        pokersim.cardAnimateDelay /= timesFaster; // 0.5
        pokersim.trackDraggable();
        //$("#coordinates_panel").show();
        //$("#systems_panel").show();
    },
    trackDraggable: function () {
        $(".draggable").draggable();
        $(".draggable").mousemove(function () {
            var top = $(this).css("top");
            var left = $(this).css("left");
            var fixedTop = 120;
            var fixedLeft = 528;
            $("#coordinates_panel").html("Top: " + top + "<br /> Left: " + left + "<br/>Mrgn Top: " + (parseInt(top) - fixedTop) + "<br/>Mrgn Left: " + (parseInt(left) - fixedLeft));
        });
    },

    parseParameters: function () {
        var url = location.href;
        var hasParams = url.indexOf("?") > 0;
        if (hasParams == false) return;
        url = url.substring(url.indexOf("?") + 1, url.length);
        var pairs = url.split("&");
        for (var i = 0; i < pairs.length; i++) {
            var data = pairs[i].split("=");
            if (data[0] == "scenario") pokersim.scenarioDataFile = data[1];
            if (data[0] == "debug") {
                pokersim.mode = "debug";
                pokersim.enableDebug(data[1]);
            }
        }
    },
    setupCenter: function () {
        var offset = $(pokersim.table).offset();
        // top, left
        pokersim.tableCenterCoords = {
            top: parseInt(offset.top + $(pokersim.table).height() / 2),
            left: parseInt(offset.left + $(pokersim.table).width() / 2)
        };
    },

    // INITIALIZATION
    initialize: function () {
        pokersim.parseParameters();
        
        if (pokersim.source != "local" && location.href.indexOf("http") != -1) pokersim.loadScenario();
        else {
            scenario = scenarioRaw;
            pokersim.setupEnviroment();
        }
    },
    setupEnviroment: function () {
        pokersim.rounds["preflop"] = 0; pokersim.rounds["flop"] = 1; pokersim.rounds["turn"] = 2; pokersim.rounds["river"] = 3;

        document.getElementById("start").removeAttribute("disabled");
        document.getElementById("start").innerHTML = "Начать";

        pokersim.table = document.getElementById("table");
        pokersim.setupCenter();
        pokersim.setupPlayers();
        pokersim.blindSize = scenario.blind_size;
        pokersim.flopline = document.getElementById("flopline");
        pokersim.setDealer(scenario.dealer_id);
        pokersim.soundtrackFile = scenario.soundtrack;
        pokersim.gotoUrl = scenario.gotoUrl;
        pokersim.actionUrl = scenario.actionUrl;
        pokersim.winDelaySec = scenario.winDelaySec;
        pokersim.winnerText = scenario.winnerText;

        pokersim.setupBlinds();

        document.getElementById("start").onclick = function () { document.getElementById("start").style.display = "none"; setTimeout(pokersim.continue, pokersim.gameStartDelay); pokersim.launchSoundtrack(); };
        pokersim.round = "start";

        pokersim.setUrlAndTexts(scenario.actionUrlText);

        $("#betButtons button").click(function () { pokersim.playerAction(this.value); });
        // bet multipliers disabled
        //$("#betMultipliers button").click(function () { pokersim.playerSelectBetMultuplier(this.value); });
        $("#playerControls #playerBet").change(function () { pokersim.playerUpdateRaiseBtnText(this.value); });
        $("#playerControls #playerBet").keyup(function () { pokersim.playerUpdateRaiseBtnText(this.value); });
        $("#betMinus").click(function () { pokersim.betStepChange(-1); });
        $("#betPlus").click(function () { pokersim.betStepChange(1); });

        console.log("Game start from " + pokersim.players[scenario.dealer_id].name);

        window.addEventListener("resize", pokersim.windowCheckOrientation);
        pokersim.windowCheckOrientation();
    },
    windowCheckOrientation: function () {
        if (window.screen.orientation.angle == 90 || window.screen.orientation.angle == 270) {
            // we need to ask to rotate phone?
        }
        //console.log("The orientation of the screen is: " + window.screen.orientation.angle);
    },
    setUrlAndTexts: function (btnCaption) {
        if (btnCaption != null && btnCaption != "")
            $("#getLucky").text(btnCaption);

        if (pokersim.gotoUrl != null && pokersim.gotoUrl != "")
        {
            $(".gotoblock").wrap("<a target='_blank' href='"+pokersim.gotoUrl+"' style='display: block'></a>");
        }

        if (pokersim.actionUrl != null && pokersim.actionUrl != "")
        {
            $(".actionButton").attr("href", pokersim.actionUrl);
        }
	},
    launchSoundtrack: function(){
        if (pokersim.soundtrackFile == null) return;
        var x = document.createElement("AUDIO");
        x.setAttribute("id", "myVideo");
       //x.setAttribute("controls", "controls");

        var z = document.createElement("SOURCE");
        z.setAttribute("src", pokersim.soundtrackFile);
        z.setAttribute("type", "audio/mpeg");
        x.appendChild(z);

        x.autoplay = true;
        x.loop = true;
        
        document.body.appendChild(x);
	},
    loadScenario: function () {
        var xobj = new XMLHttpRequest();
        xobj.overrideMimeType("application/json");
        xobj.open('GET', "data/" + pokersim.scenarioDataFile, true);
        xobj.onreadystatechange = function () {
            if (xobj.readyState == 4 && xobj.status == "200") {
                pokersim.scenarioLoadCallback(xobj.responseText);
            }
        }
        xobj.send(null);
    },
    scenarioLoadCallback: function (jsonText) {
        if (jsonText != null)
            scenario = JSON.parse(jsonText.toString('utf8'));

        pokersim.setupEnviroment();
    },
    setDealer: function (id) {
        pokersim.dealerPlayerId = id;
        $("#dealer-chip").removeClass("dealer_x").addClass("dealer_player_" + id);
    },
    nextActivePlayer: function (pid) {
        for (var i = 0; i < pokersim.activePlayerIds.length; i++) {
            if (pokersim.activePlayerIds[i] == pid) {
                var next = i + 1;
                if (next >= pokersim.activePlayerIds.length)
                    return pokersim.activePlayerIds[0];
                else
                    return pokersim.activePlayerIds[next];
            }
        }
    },
    smallBlindPlayerId: null, bigBlindPlayerId: null,
    setupBlinds: function () {
        pokersim.resetRoundBets();
        pokersim.smallBlindPlayerId = pokersim.nextActivePlayer(pokersim.dealerPlayerId);
        pokersim.bigBlindPlayerId = pokersim.nextActivePlayer(pokersim.smallBlindPlayerId);
        //console.log("Dealer: " + pokersim.dealerPlayerId + "; small: " + pokersim.smallBlindPlayerId + "; big: " + pokersim.bigBlindPlayerId);
        pokersim.makeBet(pokersim.smallBlindPlayerId, pokersim.blindSize / 2);
        pokersim.makeBet(pokersim.bigBlindPlayerId, pokersim.blindSize);
    },
    setupPlayers: function () {
        var playerCount = scenario.players.length, html = '', card1 = '', card2 = '';
        for (var i = 0; i < playerCount; i++) {
            if (scenario.players[i].active) {
                pokersim.activePlayerIds.push(scenario.players[i].id);

                pokersim.players[scenario.players[i].id] = scenario.players[i];
                pokersim.players[scenario.players[i].id].isActive = true;
                pokersim.players[scenario.players[i].id].roundBet = 0;

                if (scenario.players[i].playable == "no" && pokersim.mode != "debug") {
                    card1 = "back"; card2 = "back"; // hide cards if not playable
                    // cashing cards
                    pokersim.cardImageCash.push(new Image(cards_prefix + scenario.players[i].cards[0] + cards_ext));
                    pokersim.cardImageCash.push(new Image(cards_prefix + scenario.players[i].cards[1] + cards_ext));
                }
                else {
                    card1 = scenario.players[i].cards[0]; card2 = scenario.players[i].cards[1]; // show cards if faka or playable
                }

                html = '<div id="' + player_prefix + scenario.players[i].id + '" class="player">\
                    <img src="./' + cards_prefix + card1 + cards_ext + '" class="card card1 ' + scenario.players[i].cards[0] + '"  />\
                    <img src="./' + cards_prefix + card2 + cards_ext + '" class="card card2 ' + scenario.players[i].cards[1] + '" />\
                    <div>';
                if (pokersim.players[scenario.players[i].id].image != null)
                    html += '<img class="avatar" src="./' + data_folder + pokersim.players[scenario.players[i].id].image + '" alt="' + scenario.players[i].name + '" /><label class="hasimage">';
                else
                    html += '<label>';

                html += '<span>' + scenario.players[i].name + '</span><strong>' + scenario.players[i].balance + ' р.</strong><p id="status_' + scenario.players[i].id + '" class="status">STATUS</p></label></div></div>';

                pokersim.table.innerHTML = html + pokersim.table.innerHTML;
            }
            else {
                pokersim.players[scenario.players[i].id] = { id: scenario.players[i].id, isActive: false, roundBet: 0 };
                pokersim.table.innerHTML = '<div id="' + player_prefix + scenario.players[i].id + '" class="player"><div></div></div>' + pokersim.table.innerHTML;
            }

            pokersim.playerIds.push(scenario.players[i].id);
        }
    },

    // ANIMATION
    cardWidth: null, cardHeight: null, cardDefaultOffset: null,
    animatePlayerCards: function (callback) {
        var card = null, offset = null, width = null, height = null;
        for (var i = 0; i < pokersim.playerIds.length; i++) {
            card = $("#" + player_prefix + pokersim.playerIds[i] + " .card1");
            if (pokersim.cardWidth == null) {
                pokersim.cardWidth = card.width(); pokersim.cardHeight = card.height();
                pokersim.cardDefaultOffset = { top: pokersim.tableCenterCoords.top - pokersim.cardHeight / 2, left: pokersim.tableCenterCoords.left - pokersim.cardWidth / 2 };
            }
            pokersim.animateCard(card);
        }

        for (var i = 0; i < pokersim.playerIds.length; i++) {
            card = $("#" + player_prefix + pokersim.playerIds[i] + " .card2");
            pokersim.animateCard(card);
        }

        pokersim.cardCounter += 5;
        setTimeout(callback, pokersim.cardAnimateDelay * pokersim.cardCounter);
    },
    cardCounter: 0,
    cardViewportWidth: "5vw", cardViewportHeight: "calc(5vw * 1.38)",
    animateCard: function (card) {
        card.offset(pokersim.cardDefaultOffset);
        card.width(pokersim.cardWidth / 2).height(pokersim.cardHeight / 2);

        setTimeout(function () {
            card.animate(
                { top: 0, left: 0, opacity: 1, width: pokersim.cardWidth, height: pokersim.cardHeight }, 400, function () {
                    $(this).css("width", pokersim.cardViewportWidth);
                    $(this).css("height", pokersim.cardViewportHeight);
            });
        }, pokersim.cardAnimateDelay * pokersim.cardCounter);
        pokersim.cardCounter++;
    },

    // ADDING CARDS TO TABLE
    addcard: function (card) {
        var id = Math.random().toString(5).substr(2, 7);
        $(pokersim.flopline).append("<img src='" + cards_prefix + card + cards_ext + "' id='img" + id + "' class='" + card + "' />");
        pokersim.animateCard($("#img" + id));
    },
    highlightPlayer: function (playerId) {
        $(".player div").removeClass("active")
        $("#" + player_prefix + playerId + " div").addClass("active");
    },
    setAction: function (playerId, status, callback) {
        $("#" + player_prefix + playerId + " div span").css("visibility", "hidden");
        var statusRu = actionsRU[status];
        // pokersim.roundData[pokersim.roundStep].action.replace(/^\w/, c => c.toUpperCase())
        $("#status_" + playerId).css("display", "block").text(statusRu);

        // here we return player name and proceed with the round
        setTimeout(function () {
            $("#" + player_prefix + playerId + " div span").css("visibility", "visible");
            callback();
        }, pokersim.statusFadeTimeout);

        $("#status_" + playerId).fadeOut(pokersim.statusFadeTimeout);
    },

    // MANUAL PLAYABLE LOGIC
    playableBet: 0,
    showPlayerControls: function (playerId) {

        // TODO: copy slider info here
        $("#" + slider_object).slider({
            range: "min",
            value: $("#playerBet").val(),
            min: 1,
            step: Math.ceil(pokersim.blindSize / 2),
            max: 700,
            slide: function (event, ui) {
                $("#playerBet").val(ui.value);
                pokersim.playerUpdateRaiseBtnText();
            }
        });

        // raise/bet call option button set
        if (pokersim.playerRequiredToRaise(playerId)) {
            var sumToRaise = pokersim.playerRequiredToRaise_Sum(playerId);

            $("#betButtons .actionCall span").html(sumToRaise + " р.");
            $("#playerCallBet").val(sumToRaise);

            pokersim.playableBet = pokersim.currentRoundBet * 2; // you can raise only 2x the bet

            $("#betButtons button").show();
            $("#betButtons .actionCheck").hide();
        } // check and raise button set
        else {
            pokersim.playableBet = pokersim.blindSize;

            $("#betButtons button").show();
            $("#betButtons .actionCall").hide();
        }

        // Setting value, min and max for player
        var sliderObject = $("#" + slider_object);
        sliderObject.slider("option", "min", pokersim.betToCall(playerId)); 
        sliderObject.slider("option", "max", pokersim.playerMaxBet(playerId));
        sliderObject.slider("option", "value", pokersim.playableBet);
        sliderObject.slider('option', 'slide').call(sliderObject, null, { handle: $('.ui-slider-handle', sliderObject), value: pokersim.playableBet });

        $("#betButtons .actionRaise span").html(pokersim.playableBet + " р.");
        //$("#playerControls #playerCallBet").val(pokersim.playableBet);
        $("#playerControls #playerBet").val(pokersim.playableBet);

        $("#playerControls").show();
    },
    betStepChange: function (delta) {
        var currentValue = parseInt($("#playerControls #playerBet").val());
        currentValue += delta;
        var sliderObject = $("#" + slider_object);
        var min = sliderObject.slider("option", "min");
        var max = sliderObject.slider("option", "max");
        if (currentValue < min) currentValue = min;
        if (currentValue > max) currentValue = max;
        sliderObject.slider("option", "value", currentValue);
        sliderObject.slider('option', 'slide').call(sliderObject, null, { handle: $('.ui-slider-handle', sliderObject), value: currentValue });

        $("#playerControls #playerBet").val(currentValue);
        $("#playerControls #playerBet").change();
        pokersim.playerUpdateRaiseBtnText();
    },
    showFakePlayerControls: function (playerId) {
        pokersim.showPlayerControls(playerId);
        // thought that in that case nobody will click anything so why not
    },
    hidePlayerControls: function () {
        $("#playerCallBet").val(0);
        $("#playerControls").hide();
    },
    playerUpdateRaiseBtnText: function () {
        var bet = parseInt($("#playerBet").val());
        //$("#betButtons button span").html(bet + " р.");
        $(".actionRaise span").html(bet + " р.");
        
    },
    playerSelectBetMultuplier: function (multiplier) {
        var bet = (pokersim.isClosingRoundStage == true) ? pokersim.closingRoundMaxBet : pokersim.currentRoundBet;
        var playerId = (pokersim.isClosingRoundStage == true) ? pokersim.roundData[pokersim.closingRoundStep].id : pokersim.roundData[pokersim.roundStep].id;

        if (bet == 0) bet = pokersim.blindSize;
        switch (multiplier.toLowerCase()) {
            case "x2": bet *= 2; break;
            case "x3": bet *= 3; break;
            case "x5": bet *= 5; break;
            case "1d2": bet = parseInt(pokersim.totalBankNow() / 2); break;
            case "2d3": bet = parseInt(pokersim.totalBankNow() * 2 / 3); break;
            case "bank": bet = pokersim.totalBankNow(); break;
            case "max": bet = pokersim.playerBalance(playerId); break;
        }

        if (bet > pokersim.playerBalance(playerId)) bet = pokersim.playerBalance(playerId);

        $("#playerBet").val(bet);
        $("#betButtons .actionRaise span").html(bet + " р.");
    },
    playerAction: function (action) {
        // get the actions and bet
        var bet = parseInt( $("#playerBet").val() );

        if (action == "call") {
            action = "raise";
            bet = parseInt( $("#playerCallBet").val() );
        }

        pokersim.hidePlayerControls();

        if (pokersim.isClosingRoundStage == true)
            pokersim.closingRoundStepExecute(bet, action);
        else {  // set these actions according to input
            pokersim.roundData[pokersim.roundStep].action = action;
            pokersim.roundData[pokersim.roundStep].bet = bet;
            pokersim.runRoundStep();
        }
    },

    // ROUNDS LOGIC
    resetRoundBets: function () {
        pokersim.currentRoundBet = 0;
        for (var i = 0; i < pokersim.playerIds.length; i++) {
            pokersim.roundBets[pokersim.playerIds[i]] = 0;
            pokersim.players[pokersim.playerIds[i]].roundBet = 0;
        }
    },
    setRoundStepAfterBlinds: function () {
        for (var i = 0; i < pokersim.roundData.length; i++) {
            if (pokersim.roundData[i].id == pokersim.bigBlindPlayerId) {
                pokersim.roundStep = (i + 1 >= pokersim.roundData.length) ? 0 : i + 1;
            }
        }
    },
    startRound: function (doNotResetBets) {
        pokersim.roundData = scenario.rounds[pokersim.rounds[pokersim.round]];

        pokersim.roundStep = 0;
        if (pokersim.round != "preflop") {
            pokersim.resetRoundBets();
        }
        else {
            pokersim.setRoundStepAfterBlinds();
        }
                
        console.log("startRound: " + pokersim.round + ", round size: " + pokersim.roundData.length);
        pokersim.delayedStep(pokersim.runRoundStepStart, pokersim.activePlayerIds[pokersim.roundStep]);
    },
    delayedStep: function(nextStep, playerId) {
        var timeout = Math.ceil(Math.random() * 1000) * pokersim.playerSecondsToReact; // makes a up-to <playerSecondsToReact> second delay
        if (pokersim.players[playerId].playable == "yes") timeout = 0; // if it's player's turn then no delays or timeouts there
        if (pokersim.activePlayerIds.includes(playerId) == false)
            nextStep();
        else {
            pokersim.highlightPlayer(playerId);
            setTimeout(nextStep, timeout);
        }
    },
    runRoundStepStart: function () {
        var playerId = pokersim.roundData[pokersim.roundStep].id;

        if (pokersim.activePlayerIds.includes(playerId) == false) {
            pokersim.runRoundStepProceed(); return;
        }

        if (pokersim.players[playerId].playable == "yes") { // a player makes choice
            pokersim.showPlayerControls(playerId);
        }
        else // bot runs scenario
        {
            pokersim.runRoundStep();
        }
    },
    runRoundStep: function () {
        var playerId = pokersim.roundData[pokersim.roundStep].id;
        var action = pokersim.roundData[pokersim.roundStep].action.toLowerCase();
        var bet = pokersim.roundData[pokersim.roundStep].bet;

        // do the player action here
        console.log("Player " + playerId + " action: " + action);

        if (action == "fold") pokersim.foldPlayer(pokersim.roundData[pokersim.roundStep].id, pokersim.roundStep);
        else if (action == "smallblind")
            pokersim.makeBet(playerId, pokersim.blindSize / 2);
        else if (action == "blind")
            pokersim.makeBet(playerId, pokersim.blindSize);
        else if (action == "check") {
            if (pokersim.playerRequiredToRaise(playerId)) {
                if (pokersim.alwaysFold(playerId))
                    pokersim.foldPlayer(playerId, pokersim.roundStep);
                else if (pokersim.round == "preflop" && pokersim.playerCanCall(playerId) && pokersim.alwaysBlinds(playerId))
                    pokersim.makeBet(playerId, pokersim.betToCall(playerId));

                else if (pokersim.alwaysCall(playerId) && pokersim.players[playerId].balance >= pokersim.currentRoundBet - pokersim.roundBets[playerId]) {
                    pokersim.roundData[pokersim.roundStep].action = "call";
                    pokersim.makeBet(playerId, pokersim.currentRoundBet - pokersim.roundBets[playerId]);
                }
                    
                else
                    pokersim.foldPlayer(playerId, pokersim.roundStep);
            } else {
                console.log("Player CHECK should not raise");
            }
        }
        else if (action == "call") {
            if (pokersim.playerCanCall(playerId)) {
                pokersim.makeBet(playerId, pokersim.betToCall(playerId));
            }
            else
                pokersim.foldPlayer(playerId, pokersim.roundStep);
        }
        else if (action == "raise" || action == "reraise") {
            if (pokersim.playerCanBet(playerId, bet) && pokersim.playerTotalBetToSet(playerId, bet) >= pokersim.currentRoundBet) {
                pokersim.makeBet(playerId, bet);
            }
            else if (pokersim.alwaysCall(playerId) && pokersim.playerCanCall(playerId))
                pokersim.makeBet(playerId, pokersim.betToCall(playerId));
            else
                pokersim.foldPlayer(playerId, pokersim.roundStep);
        }

        if (pokersim.players[playerId].playable == "fake")
            pokersim.showFakePlayerControls(playerId);

        action = pokersim.roundData[pokersim.roundStep].action; // updating action in case of changes to fold
        pokersim.setAction(playerId, action, pokersim.runRoundStepProceed);
        pokersim.systemPanelUpdate();
    },
    runRoundStepProceed: function () {
        pokersim.hidePlayerControls(); // in case they were shown in fake mode
        pokersim.roundStep++;
        if (pokersim.roundStep < pokersim.playerIds.length)
            pokersim.delayedStep(pokersim.runRoundStepStart, pokersim.roundData[pokersim.roundStep].id);
        else {
            $(".player div").removeClass("active"); // we're done
            pokersim.startClosingRound();
        }
    },

    playerTotalBetToSet: function(playerId, extraBet) { return pokersim.roundBets[playerId] + extraBet; },
    alwaysCall: function(playerId) { return pokersim.players[playerId].strategy == "alwayscall"; },
    alwaysFold: function(playerId) { return pokersim.players[playerId].strategy == "alwaysfold"; },
    alwaysBlinds: function(playerId) { return pokersim.players[playerId].strategy == "callblinds"; },
    playerRequiredToRaise: function (playerId) { return pokersim.currentRoundBet > pokersim.roundBets[playerId]; },
    playerRequiredToRaise_Sum: function (playerId) { return pokersim.currentRoundBet - pokersim.roundBets[playerId]; },
    playerMaxBet: function (playerId) { return pokersim.players[playerId].balance - pokersim.currentRoundBet; },
    playerCanCall: function(playerId) { return pokersim.players[playerId].balance >= pokersim.betToCall(playerId); },
    betToCall: function(playerId) { return pokersim.currentRoundBet - pokersim.roundBets[playerId]; },
    playerCanBet: function (playerId, bet) { return pokersim.players[playerId].balance >= bet; },
    playerBalance: function (playerId, bet) { return pokersim.players[playerId].balance; },
    totalBankNow: function () {
        var roundBank = 0;
        for (var i = 0; i < pokersim.playerIds.length; i++)
            roundBank += pokersim.roundBets[pokersim.playerIds[i]];

        return pokersim.totalBank + roundBank;
    },

    makeBet: function (playerId, bet) {
        console.log("BET by Player " + playerId + " bet: " + bet + " // " + pokersim.players[playerId].name);

        if (bet == 0) return;
        pokersim.roundBets[playerId] += bet;
        pokersim.players[playerId].roundBet += bet;
        pokersim.players[playerId].balance -= bet;
        if (pokersim.currentRoundBet < pokersim.roundBets[playerId])
            pokersim.currentRoundBet = pokersim.roundBets[playerId]
        $("#" + player_prefix + playerId + " strong").text(pokersim.players[playerId].balance + balanceSuffix);

        pokersim.updatePlayerChip(playerId, pokersim.roundBets[playerId]);
    },
    foldPlayer: function(playerId, step) {
        $("#" + player_prefix + playerId + " img.card").remove();
        // $("#" + player_prefix + playerId + " div").html(""); // we don't hide folded player now, but we did
        pokersim.roundData[step].action = "fold";
        pokersim.players[playerId].isActive = false;
        pokersim.activePlayerIds.remove(playerId);
    },
    updatePlayerChip: function(playerId, totalBet) {
        $("#chip_player_" + playerId).show();
        $("#chip_player_" + playerId + " span").text(totalBet + balanceSuffix);
    },

    // CLOSING ROUND ROUTINES
    playersAreEven: function () {
        var bet = pokersim.roundBets[0];
        var playerId = null, playersEven = true;
        for (var i = 0; i < pokersim.activePlayerIds.length; i++) {
            playerId = pokersim.activePlayerIds[i];
            if (bet != pokersim.roundBets[playerId]) {
                console.log("found another bet for player " + playerId + " with bet: " + pokersim.roundBets[playerId]);
                playersEven = false;
                if (pokersim.roundBets[playerId] > bet)
                    bet = pokersim.roundBets[playerId];
            }
        }
         return (playersEven) ? 0 : bet; // 0 means everybody is even, <bet> means max bet

        // shorter version
        //var bet = 0; var playerId = null, playersEven = true;
        //for (var i = 0; i < pokersim.activePlayerIds.length; i++) {
        //    playerId = pokersim.activePlayerIds[i];
        //    if (pokersim.roundBets[playerId] > bet)
        //        return false;
        //}
        //return true;
    },
    closingRoundStep: 0,
    closingRoundMaxBet: 0,
    isClosingRoundStage: false,
    startClosingRound: function () {
        // check here if all bets are set and do "closing round"
        // check if the bets are all even, act with "strategy" for players that are not even, all others do "check"
        pokersim.closingRoundMaxBet = pokersim.playersAreEven();
        pokersim.isClosingRoundStage = true;

        pokersim.closingRoundStep = -1; // so we start with ++ and go for 0
        pokersim.closingRoundProceed();
    },
    closingRoundProceed: function () {
        pokersim.closingRoundStep++;
        
        if (pokersim.closingRoundStep < pokersim.roundData.length && pokersim.playersAreEven() > 0) {
            pokersim.delayedStep(pokersim.closingRoundStepStart, pokersim.roundData[pokersim.closingRoundStep].id);
        }
        else {  // we're done
            $(".player div").removeClass("active");
            pokersim.isClosingRoundStage = false;
            pokersim.updateBank();
        }
    },
    closingRoundStepStart: function () {
        var playerId = pokersim.roundData[pokersim.closingRoundStep].id;

        if (pokersim.activePlayerIds.includes(playerId) == false || pokersim.roundBets[playerId] == pokersim.closingRoundMaxBet) {
            pokersim.closingRoundProceed(); return;
        }

        if (pokersim.players[playerId].playable == "yes") { // a player makes choice
            pokersim.showPlayerControls(playerId);
        }
        else // bot runs scenario
        {
            var bet = pokersim.roundBets[playerId].bet;
            var action = "check";
            pokersim.closingRoundStepExecute(bet, action);
        }
    },
    closingRoundStepExecute: function (bet, action) {
        var playerId = pokersim.roundData[pokersim.closingRoundStep].id;
        console.log("Closing round for player " + playerId);

        // if this if player turn, he does whatever he wants to do
        if (pokersim.players[playerId].playable == "yes") // TODO: provide error check if player has not enough money
        {
            if (action == "call") pokersim.makeBet(playerId, pokersim.betToCall(playerId));
            if (action == "raise") pokersim.makeBet(playerId, bet);
            if (action == "fold") pokersim.foldPlayer(playerId, pokersim.closingRoundStep);
        }
        // we do something if this bot did not made the money right
        else if (pokersim.roundBets[playerId] < pokersim.closingRoundMaxBet) {
            if (pokersim.round == "preflop" && pokersim.playerCanCall(playerId) && pokersim.alwaysBlinds(playerId))
            {
                action = "call"; pokersim.makeBet(playerId, pokersim.betToCall(playerId));
            }
            else if (pokersim.playerCanCall(playerId) && pokersim.alwaysCall(playerId))
            {
                action = "call"; pokersim.makeBet(playerId, pokersim.betToCall(playerId));
            }
            else
            {
                action = "fold"; pokersim.foldPlayer(playerId, pokersim.closingRoundStep);
            }
        }

        if (pokersim.round == "preflop" && pokersim.closingRoundMaxBet == pokersim.blindSize && pokersim.playersAreEven() == 0)
            pokersim.closingRoundStep = 100; // we close round if blinds are even and clear highlight
        
        pokersim.setAction(playerId, action, pokersim.closingRoundProceed);
    },

    updateBank: function () {
        for (var i = 0; i < pokersim.playerIds.length; i++) {
            var playerId = pokersim.playerIds[i];
            pokersim.totalBank += pokersim.players[playerId].roundBet;
            $("#chip_player_" + playerId).hide(); 
        }

        $("#bank").text("Банк " + pokersim.totalBank + balanceSuffix).show();
        pokersim.continue();
    },
    continue: function () {
        // TODO: check case http://pokersim.shlygin.com/?scenario=game3-korovin-money.json&debug
        // here has a BUG that gosha does the stake AFTER the card is on table
        // REPAIR


        // if all players dropped we stop the game
        if (pokersim.activePlayerIds.length == 1) {
            console.log("we have only one player left and he is a winner: " + pokersim.players[pokersim.activePlayerIds[0]].name);

            pokersim.showWinner();
        }

        // the game
        else if (pokersim.round == "start")
        {
            pokersim.round = "cardsOnHands";
            pokersim.animatePlayerCards(pokersim.continue);
            return;
        }
        else if (pokersim.round == "cardsOnHands")
        {
            pokersim.round = "preflop";
            pokersim.continueCommit();
        }
        else if (pokersim.round == "preflop") {
            pokersim.round = "flop";
            pokersim.cardCounter = 0;
            for (var i = 0; i < scenario.flop.length; i++)
                pokersim.addcard(scenario.flop[i]);

            setTimeout(pokersim.continueCommit, pokersim.cardAnimateDelay * 4);
        }
        else if (pokersim.round == "flop") {
            pokersim.cardCounter = 0;
            pokersim.round = "turn";
            pokersim.addcard(scenario.turn);
            setTimeout(pokersim.continueCommit, pokersim.cardAnimateDelay);
        }

        else if (pokersim.round == "turn") {
            pokersim.cardCounter = 0;
            pokersim.round = "river";
            pokersim.addcard(scenario.river);
            setTimeout(pokersim.continueCommit, pokersim.cardAnimateDelay);
        }
        else if (pokersim.round == "river") {
            pokersim.round = "end";
            console.log("Last round, no extra play");
            pokersim.showWinner();
        }
    },
    continueCommit: function ()
    {
        pokersim.startRound(); // TODO: start round after card is animated on callback
    },

    // WINNER DETECTION AND SHOW UP
    prepareHandForWinCheck: function (flop, playerCards) {
        var card = null, data = [];
        for (var i = 0; i < flop.length; i++) {
            card = flop[i];
            if (card.substring(1, card.length) == "10") card = card[0] + "T";
            data.push(card[1].toString().toUpperCase() + card[0]); 
            //data.push(flop[i].substring(1, flop[i].length) + flop[i][0]); // changing places for the symbols for HAND lib
        }
        
        for (var i = 0; i < playerCards.length; i++) {
            card = playerCards[i];
            if (card.substring(1, card.length) == "10") card = card[0] + "T";
            data.push(card[1].toString().toUpperCase() + card[0]);
            //data.push(flop[i].substring(1, flop[i].length) + flop[i][0]); // changing places for the symbols for HAND lib
        }

        return data;
    },
    highestRank: 0,
    winnerHand: null,
    calculateWinner: function () {
        // TODO: write pokersim.winnerId that he gets the bank
        var hand = null, handData = [], playerHands = [];
        var floplineCards = scenario.flop;
        floplineCards.push(scenario.turn);
        floplineCards.push(scenario.river);
        var highestRank = 0;
        for (var i = 0; i < pokersim.activePlayerIds.length; i++) {
            if (pokersim.players[pokersim.activePlayerIds[i]].isActive == false) continue;
            handData = pokersim.prepareHandForWinCheck(floplineCards, pokersim.players[pokersim.activePlayerIds[i]].cards);
            hand = Hand.solve(handData);
            console.log("Player id: " + pokersim.activePlayerIds[i] + " hand is: ");
            console.log(handData); console.log("and resolved is");
            console.log(hand);
            pokersim.players[pokersim.activePlayerIds[i]].handRank = hand.rank;

            if (hand.rank > pokersim.highestRank) pokersim.highestRank = hand.rank;
            playerHands.push(hand);
        }

        pokersim.winnerHand = Hand.winners(playerHands);
        console.log("Winner: " + pokersim.winnerHand); // TODO: save winning hand to match winner in the game
    },
    showPlayerCards: function () {
        for (var i = 0; i < pokersim.activePlayerIds.length; i++) {
            var playerId = pokersim.activePlayerIds[i];
            $("#" + player_prefix + playerId + " img.card1").attr("src", cards_prefix + pokersim.players[playerId].cards[0] + cards_ext);
            $("#" + player_prefix + playerId + " img.card2").attr("src", cards_prefix + pokersim.players[playerId].cards[1] + cards_ext);

        }
    },
    winners: [],
    showWinner: function () {
        if (pokersim.activePlayerIds.length == 0) return;

        // only one player left and he is a winner
        if (pokersim.activePlayerIds.length == 1) {
            pokersim.winners.push(pokersim.activePlayerIds[0])
        } // few players left and we calculate the winner
        else {
            pokersim.calculateWinner();
            pokersim.showPlayerCards();
            var playerId = 0;
            for (var i = 0; i < pokersim.activePlayerIds.length; i++) {
                playerId = pokersim.activePlayerIds[i];
                if (pokersim.players[playerId].handRank == pokersim.highestRank) {
                    pokersim.winners.push(playerId);
                }
            }
        }

        // we add bank to winners
        var winBank = pokersim.totalBank / pokersim.winners.length;
        for (var i = 0; i < pokersim.winners.length; i++) {
            playerId = pokersim.winners[i];
            pokersim.players[playerId].balance += winBank;
            $("#" + player_prefix + playerId + " strong").text(pokersim.players[playerId].balance + balanceSuffix);
            $("#" + player_prefix + playerId).append("<mark class='winner'>Выиграл</mark>");
        }

        //$("#bank").addClass("bankimg");

        if (pokersim.activePlayerIds.length > 1) // we animate exact winner if few players left in the game
            pokersim.showWinningCards();

        pokersim.animateBankToWinner(pokersim.winners);
        setTimeout(() => pokersim.showWinnerOverlay(pokersim.winners), pokersim.winDelaySec * 1000);
        //pokersim.showWinnerOverlay(pokersim.winners);
    },
    showWinningCards: function () {
        var mTop, cardCode;
        var winnerHandCards = pokersim.winnerHand[0].toString().split(",");
        
        $("#flopline img").css("opacity", "0.7");
        for (var i = 0; i < winnerHandCards.length; i++) {
            var rawCode = winnerHandCards[i].toString().trim().toLowerCase();

            cardCode = rawCode[rawCode.length-1] + rawCode.substring(0, rawCode.length - 1);
            //console.log("rawcode is: " + cardCode);
            //$("." + cardCode).css("border", "3px solid #d38400");
            $("." + cardCode).css("border-radius", "0");
            $("." + cardCode).css("opacity", "1");
            $("." + cardCode).animate({ top: "-30px" });
        }
    },
    animateBankToWinner: function (winnerIds) {
        // TODO: animate
    },

    showWinnerOverlay: function (winnerIds) {
        if (winnerIds == null) return; // we cannot show that
        var firstWinner = winnerIds[0];

        // adding realdata
        var winnerName = $("#player_" + firstWinner + " div span").text();
        var winnerImg = $("#player_" + firstWinner + " div img").attr("src");;
        var winnerSum = $("#player_" + firstWinner + " div strong").text();
        $("#woAvatar").attr("src", winnerImg);
        $("#woAvatar").attr("alt", winnerName);
        $("#woName").text(winnerName);
        $("#woSum").text(winnerSum);

        // TODO: assign winner text here
        //$("#winnerTextBlock").htlm(pokersim.winnerText);

        $("body").addClass("overflowhidden");
        $("#winOverlay").fadeIn();
    },

    systemPanelUpdate: function() {
        var data = "<p>Round bet: " + pokersim.currentRoundBet + "</p><p>Round: " + pokersim.round + "</p>";
        $("#systems_panel").html(data);
    }
};

function loadJSON(callback) {
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', scenariofile, true);
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
            callback(xobj.responseText);
        }
    }
    xobj.send(null);
}

Array.prototype.remove = function () {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

window.onload = pokersim.initialize;
window.onresize = pokersim.setupCenter;