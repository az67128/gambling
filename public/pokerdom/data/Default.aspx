<%@ Page language="c#" CodeFile="Default.aspx.cs" AutoEventWireup="false" Inherits="CSharpUpload.WebForm1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
     <meta charset="utf-8" />
    <title>Texas Holdem files</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </HEAD>
  <body MS_POSITIONING="GridLayout">
  <h1>Add new scenario</h1>
<form id="Form1" method="post" enctype="multipart/form-data" runat="server">
<INPUT type=file id=File1 name=File1 runat="server" style="padding: 5px" >
<br>
<input type="submit" id="Submit1" value="Upload" runat="server" NAME="Submit1" style="margin: 5px; padding: 5px">
</form>

<h2>Scenarios</h2>
<ul>
    <%
        string buildsFolder = @"D:\Web\Pokersim\data\";
        string webPrefix = "http://pokersim.shlygin.com/data/";
		string launchPrefix = "http://pokersim.shlygin.com/?scenario=";

        System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(buildsFolder);

        var files = di.GetFiles().Where(x => x.Extension.ToLower() == ".json").OrderByDescending(x => x.CreationTime);

        foreach (var fi in files)
        {
            Response.Write("<li><i>"+fi.CreationTime.ToString("dd.MM.yyyy")+"</i> <a href='"+launchPrefix+fi.Name+"' target='_blank' href=''>Open</a>&nbsp;&nbsp;<a href='" + webPrefix + fi.Name + "'>" + fi.Name + "</a></li>");
        }
    %>
</ul>


<h2>Files</h2>
<ul>
    <%

        di = new System.IO.DirectoryInfo(buildsFolder);

        files = di.GetFiles().Where(x => x.Extension.ToLower() != ".json" && x.Extension.ToLower() != ".cs" && x.Extension.ToLower() != ".aspx").OrderByDescending(x => x.CreationTime);

        foreach (var fi in files)
        {
            Response.Write("<li><i>"+fi.CreationTime.ToString("dd.MM.yyyy")+"</i> &nbsp;<a href='" + webPrefix + fi.Name + "'>" + fi.Name + "</a></li>");
        }
    %>
</ul>

  </body>
</HTML>