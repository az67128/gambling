
(function(l, r) { if (!l || l.getElementById('livereloadscript')) return; r = l.createElement('script'); r.async = 1; r.src = '//' + (self.location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1'; r.id = 'livereloadscript'; l.getElementsByTagName('head')[0].appendChild(r) })(self.document);
var app = (function () {
  'use strict';

  function _typeof(obj) {
    "@babel/helpers - typeof";

    return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) {
      return typeof obj;
    } : function (obj) {
      return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    }, _typeof(obj);
  }

  function _toPrimitive(input, hint) {
    if (_typeof(input) !== "object" || input === null) return input;
    var prim = input[Symbol.toPrimitive];
    if (prim !== undefined) {
      var res = prim.call(input, hint || "default");
      if (_typeof(res) !== "object") return res;
      throw new TypeError("@@toPrimitive must return a primitive value.");
    }
    return (hint === "string" ? String : Number)(input);
  }

  function _toPropertyKey(arg) {
    var key = _toPrimitive(arg, "string");
    return _typeof(key) === "symbol" ? key : String(key);
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor);
    }
  }
  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    Object.defineProperty(Constructor, "prototype", {
      writable: false
    });
    return Constructor;
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }
    return self;
  }

  function _setPrototypeOf(o, p) {
    _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };
    return _setPrototypeOf(o, p);
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }
    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    Object.defineProperty(subClass, "prototype", {
      writable: false
    });
    if (superClass) _setPrototypeOf(subClass, superClass);
  }

  function _possibleConstructorReturn(self, call) {
    if (call && (_typeof(call) === "object" || typeof call === "function")) {
      return call;
    } else if (call !== void 0) {
      throw new TypeError("Derived constructors may only return object or undefined");
    }
    return _assertThisInitialized(self);
  }

  function _getPrototypeOf(o) {
    _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return _getPrototypeOf(o);
  }

  function _arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
  }

  function _iterableToArrayLimit(arr, i) {
    var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"];
    if (null != _i) {
      var _s,
        _e,
        _x,
        _r,
        _arr = [],
        _n = !0,
        _d = !1;
      try {
        if (_x = (_i = _i.call(arr)).next, 0 === i) {
          if (Object(_i) !== _i) return;
          _n = !1;
        } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0);
      } catch (err) {
        _d = !0, _e = err;
      } finally {
        try {
          if (!_n && null != _i["return"] && (_r = _i["return"](), Object(_r) !== _r)) return;
        } finally {
          if (_d) throw _e;
        }
      }
      return _arr;
    }
  }

  function _arrayLikeToArray$1(arr, len) {
    if (len == null || len > arr.length) len = arr.length;
    for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];
    return arr2;
  }

  function _unsupportedIterableToArray$1(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return _arrayLikeToArray$1(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray$1(o, minLen);
  }

  function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  function _slicedToArray(arr, i) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray$1(arr, i) || _nonIterableRest();
  }

  var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

  function unwrapExports (x) {
  	return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
  }

  function createCommonjsModule(fn, module) {
  	return module = { exports: {} }, fn(module, module.exports), module.exports;
  }

  var check = function (it) {
    return it && it.Math == Math && it;
  };

  // https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
  var global_1 =
    // eslint-disable-next-line es/no-global-this -- safe
    check(typeof globalThis == 'object' && globalThis) ||
    check(typeof window == 'object' && window) ||
    // eslint-disable-next-line no-restricted-globals -- safe
    check(typeof self == 'object' && self) ||
    check(typeof commonjsGlobal == 'object' && commonjsGlobal) ||
    // eslint-disable-next-line no-new-func -- fallback
    (function () { return this; })() || commonjsGlobal || Function('return this')();

  var fails = function (exec) {
    try {
      return !!exec();
    } catch (error) {
      return true;
    }
  };

  // Detect IE8's incomplete defineProperty implementation
  var descriptors = !fails(function () {
    // eslint-disable-next-line es/no-object-defineproperty -- required for testing
    return Object.defineProperty({}, 1, { get: function () { return 7; } })[1] != 7;
  });

  var functionBindNative = !fails(function () {
    // eslint-disable-next-line es/no-function-prototype-bind -- safe
    var test = (function () { /* empty */ }).bind();
    // eslint-disable-next-line no-prototype-builtins -- safe
    return typeof test != 'function' || test.hasOwnProperty('prototype');
  });

  var call$2 = Function.prototype.call;

  var functionCall = functionBindNative ? call$2.bind(call$2) : function () {
    return call$2.apply(call$2, arguments);
  };

  var $propertyIsEnumerable$1 = {}.propertyIsEnumerable;
  // eslint-disable-next-line es/no-object-getownpropertydescriptor -- safe
  var getOwnPropertyDescriptor$6 = Object.getOwnPropertyDescriptor;

  // Nashorn ~ JDK8 bug
  var NASHORN_BUG = getOwnPropertyDescriptor$6 && !$propertyIsEnumerable$1.call({ 1: 2 }, 1);

  // `Object.prototype.propertyIsEnumerable` method implementation
  // https://tc39.es/ecma262/#sec-object.prototype.propertyisenumerable
  var f$8 = NASHORN_BUG ? function propertyIsEnumerable(V) {
    var descriptor = getOwnPropertyDescriptor$6(this, V);
    return !!descriptor && descriptor.enumerable;
  } : $propertyIsEnumerable$1;

  var objectPropertyIsEnumerable = {
  	f: f$8
  };

  var createPropertyDescriptor = function (bitmap, value) {
    return {
      enumerable: !(bitmap & 1),
      configurable: !(bitmap & 2),
      writable: !(bitmap & 4),
      value: value
    };
  };

  var FunctionPrototype$3 = Function.prototype;
  var call$1 = FunctionPrototype$3.call;
  var uncurryThisWithBind = functionBindNative && FunctionPrototype$3.bind.bind(call$1, call$1);

  var functionUncurryThis = functionBindNative ? uncurryThisWithBind : function (fn) {
    return function () {
      return call$1.apply(fn, arguments);
    };
  };

  var toString$1 = functionUncurryThis({}.toString);
  var stringSlice$a = functionUncurryThis(''.slice);

  var classofRaw = function (it) {
    return stringSlice$a(toString$1(it), 8, -1);
  };

  var $Object$5 = Object;
  var split$3 = functionUncurryThis(''.split);

  // fallback for non-array-like ES3 and non-enumerable old V8 strings
  var indexedObject = fails(function () {
    // throws an error in rhino, see https://github.com/mozilla/rhino/issues/346
    // eslint-disable-next-line no-prototype-builtins -- safe
    return !$Object$5('z').propertyIsEnumerable(0);
  }) ? function (it) {
    return classofRaw(it) == 'String' ? split$3(it, '') : $Object$5(it);
  } : $Object$5;

  // we can't use just `it == null` since of `document.all` special case
  // https://tc39.es/ecma262/#sec-IsHTMLDDA-internal-slot-aec
  var isNullOrUndefined = function (it) {
    return it === null || it === undefined;
  };

  var $TypeError$j = TypeError;

  // `RequireObjectCoercible` abstract operation
  // https://tc39.es/ecma262/#sec-requireobjectcoercible
  var requireObjectCoercible = function (it) {
    if (isNullOrUndefined(it)) throw $TypeError$j("Can't call method on " + it);
    return it;
  };

  // toObject with fallback for non-array-like ES3 strings



  var toIndexedObject = function (it) {
    return indexedObject(requireObjectCoercible(it));
  };

  var documentAll$2 = typeof document == 'object' && document.all;

  // https://tc39.es/ecma262/#sec-IsHTMLDDA-internal-slot
  // eslint-disable-next-line unicorn/no-typeof-undefined -- required for testing
  var IS_HTMLDDA = typeof documentAll$2 == 'undefined' && documentAll$2 !== undefined;

  var documentAll_1 = {
    all: documentAll$2,
    IS_HTMLDDA: IS_HTMLDDA
  };

  var documentAll$1 = documentAll_1.all;

  // `IsCallable` abstract operation
  // https://tc39.es/ecma262/#sec-iscallable
  var isCallable = documentAll_1.IS_HTMLDDA ? function (argument) {
    return typeof argument == 'function' || argument === documentAll$1;
  } : function (argument) {
    return typeof argument == 'function';
  };

  var documentAll = documentAll_1.all;

  var isObject = documentAll_1.IS_HTMLDDA ? function (it) {
    return typeof it == 'object' ? it !== null : isCallable(it) || it === documentAll;
  } : function (it) {
    return typeof it == 'object' ? it !== null : isCallable(it);
  };

  var aFunction = function (argument) {
    return isCallable(argument) ? argument : undefined;
  };

  var getBuiltIn = function (namespace, method) {
    return arguments.length < 2 ? aFunction(global_1[namespace]) : global_1[namespace] && global_1[namespace][method];
  };

  var objectIsPrototypeOf = functionUncurryThis({}.isPrototypeOf);

  var engineUserAgent = typeof navigator != 'undefined' && String(navigator.userAgent) || '';

  var process$4 = global_1.process;
  var Deno$1 = global_1.Deno;
  var versions = process$4 && process$4.versions || Deno$1 && Deno$1.version;
  var v8 = versions && versions.v8;
  var match, version;

  if (v8) {
    match = v8.split('.');
    // in old Chrome, versions of V8 isn't V8 = Chrome / 10
    // but their correct versions are not interesting for us
    version = match[0] > 0 && match[0] < 4 ? 1 : +(match[0] + match[1]);
  }

  // BrowserFS NodeJS `process` polyfill incorrectly set `.v8` to `0.0`
  // so check `userAgent` even if `.v8` exists, but 0
  if (!version && engineUserAgent) {
    match = engineUserAgent.match(/Edge\/(\d+)/);
    if (!match || match[1] >= 74) {
      match = engineUserAgent.match(/Chrome\/(\d+)/);
      if (match) version = +match[1];
    }
  }

  var engineV8Version = version;

  /* eslint-disable es/no-symbol -- required for testing */




  var $String$6 = global_1.String;

  // eslint-disable-next-line es/no-object-getownpropertysymbols -- required for testing
  var symbolConstructorDetection = !!Object.getOwnPropertySymbols && !fails(function () {
    var symbol = Symbol();
    // Chrome 38 Symbol has incorrect toString conversion
    // `get-own-property-symbols` polyfill symbols converted to object are not Symbol instances
    // nb: Do not call `String` directly to avoid this being optimized out to `symbol+''` which will,
    // of course, fail.
    return !$String$6(symbol) || !(Object(symbol) instanceof Symbol) ||
      // Chrome 38-40 symbols are not inherited from DOM collections prototypes to instances
      !Symbol.sham && engineV8Version && engineV8Version < 41;
  });

  /* eslint-disable es/no-symbol -- required for testing */


  var useSymbolAsUid = symbolConstructorDetection
    && !Symbol.sham
    && typeof Symbol.iterator == 'symbol';

  var $Object$4 = Object;

  var isSymbol = useSymbolAsUid ? function (it) {
    return typeof it == 'symbol';
  } : function (it) {
    var $Symbol = getBuiltIn('Symbol');
    return isCallable($Symbol) && objectIsPrototypeOf($Symbol.prototype, $Object$4(it));
  };

  var $String$5 = String;

  var tryToString = function (argument) {
    try {
      return $String$5(argument);
    } catch (error) {
      return 'Object';
    }
  };

  var $TypeError$i = TypeError;

  // `Assert: IsCallable(argument) is true`
  var aCallable = function (argument) {
    if (isCallable(argument)) return argument;
    throw $TypeError$i(tryToString(argument) + ' is not a function');
  };

  // `GetMethod` abstract operation
  // https://tc39.es/ecma262/#sec-getmethod
  var getMethod = function (V, P) {
    var func = V[P];
    return isNullOrUndefined(func) ? undefined : aCallable(func);
  };

  var $TypeError$h = TypeError;

  // `OrdinaryToPrimitive` abstract operation
  // https://tc39.es/ecma262/#sec-ordinarytoprimitive
  var ordinaryToPrimitive = function (input, pref) {
    var fn, val;
    if (pref === 'string' && isCallable(fn = input.toString) && !isObject(val = functionCall(fn, input))) return val;
    if (isCallable(fn = input.valueOf) && !isObject(val = functionCall(fn, input))) return val;
    if (pref !== 'string' && isCallable(fn = input.toString) && !isObject(val = functionCall(fn, input))) return val;
    throw $TypeError$h("Can't convert object to primitive value");
  };

  var isPure = false;

  // eslint-disable-next-line es/no-object-defineproperty -- safe
  var defineProperty$6 = Object.defineProperty;

  var defineGlobalProperty = function (key, value) {
    try {
      defineProperty$6(global_1, key, { value: value, configurable: true, writable: true });
    } catch (error) {
      global_1[key] = value;
    } return value;
  };

  var SHARED = '__core-js_shared__';
  var store$1 = global_1[SHARED] || defineGlobalProperty(SHARED, {});

  var sharedStore = store$1;

  var shared = createCommonjsModule(function (module) {



  (module.exports = function (key, value) {
    return sharedStore[key] || (sharedStore[key] = value !== undefined ? value : {});
  })('versions', []).push({
    version: '3.32.0',
    mode: 'global',
    copyright: '© 2014-2023 Denis Pushkarev (zloirock.ru)',
    license: 'https://github.com/zloirock/core-js/blob/v3.32.0/LICENSE',
    source: 'https://github.com/zloirock/core-js'
  });
  });

  var $Object$3 = Object;

  // `ToObject` abstract operation
  // https://tc39.es/ecma262/#sec-toobject
  var toObject = function (argument) {
    return $Object$3(requireObjectCoercible(argument));
  };

  var hasOwnProperty = functionUncurryThis({}.hasOwnProperty);

  // `HasOwnProperty` abstract operation
  // https://tc39.es/ecma262/#sec-hasownproperty
  // eslint-disable-next-line es/no-object-hasown -- safe
  var hasOwnProperty_1 = Object.hasOwn || function hasOwn(it, key) {
    return hasOwnProperty(toObject(it), key);
  };

  var id$1 = 0;
  var postfix = Math.random();
  var toString = functionUncurryThis(1.0.toString);

  var uid = function (key) {
    return 'Symbol(' + (key === undefined ? '' : key) + ')_' + toString(++id$1 + postfix, 36);
  };

  var Symbol$1 = global_1.Symbol;
  var WellKnownSymbolsStore$1 = shared('wks');
  var createWellKnownSymbol = useSymbolAsUid ? Symbol$1['for'] || Symbol$1 : Symbol$1 && Symbol$1.withoutSetter || uid;

  var wellKnownSymbol = function (name) {
    if (!hasOwnProperty_1(WellKnownSymbolsStore$1, name)) {
      WellKnownSymbolsStore$1[name] = symbolConstructorDetection && hasOwnProperty_1(Symbol$1, name)
        ? Symbol$1[name]
        : createWellKnownSymbol('Symbol.' + name);
    } return WellKnownSymbolsStore$1[name];
  };

  var $TypeError$g = TypeError;
  var TO_PRIMITIVE = wellKnownSymbol('toPrimitive');

  // `ToPrimitive` abstract operation
  // https://tc39.es/ecma262/#sec-toprimitive
  var toPrimitive = function (input, pref) {
    if (!isObject(input) || isSymbol(input)) return input;
    var exoticToPrim = getMethod(input, TO_PRIMITIVE);
    var result;
    if (exoticToPrim) {
      if (pref === undefined) pref = 'default';
      result = functionCall(exoticToPrim, input, pref);
      if (!isObject(result) || isSymbol(result)) return result;
      throw $TypeError$g("Can't convert object to primitive value");
    }
    if (pref === undefined) pref = 'number';
    return ordinaryToPrimitive(input, pref);
  };

  // `ToPropertyKey` abstract operation
  // https://tc39.es/ecma262/#sec-topropertykey
  var toPropertyKey = function (argument) {
    var key = toPrimitive(argument, 'string');
    return isSymbol(key) ? key : key + '';
  };

  var document$3 = global_1.document;
  // typeof document.createElement is 'object' in old IE
  var EXISTS$1 = isObject(document$3) && isObject(document$3.createElement);

  var documentCreateElement = function (it) {
    return EXISTS$1 ? document$3.createElement(it) : {};
  };

  // Thanks to IE8 for its funny defineProperty
  var ie8DomDefine = !descriptors && !fails(function () {
    // eslint-disable-next-line es/no-object-defineproperty -- required for testing
    return Object.defineProperty(documentCreateElement('div'), 'a', {
      get: function () { return 7; }
    }).a != 7;
  });

  // eslint-disable-next-line es/no-object-getownpropertydescriptor -- safe
  var $getOwnPropertyDescriptor$2 = Object.getOwnPropertyDescriptor;

  // `Object.getOwnPropertyDescriptor` method
  // https://tc39.es/ecma262/#sec-object.getownpropertydescriptor
  var f$7 = descriptors ? $getOwnPropertyDescriptor$2 : function getOwnPropertyDescriptor(O, P) {
    O = toIndexedObject(O);
    P = toPropertyKey(P);
    if (ie8DomDefine) try {
      return $getOwnPropertyDescriptor$2(O, P);
    } catch (error) { /* empty */ }
    if (hasOwnProperty_1(O, P)) return createPropertyDescriptor(!functionCall(objectPropertyIsEnumerable.f, O, P), O[P]);
  };

  var objectGetOwnPropertyDescriptor = {
  	f: f$7
  };

  // V8 ~ Chrome 36-
  // https://bugs.chromium.org/p/v8/issues/detail?id=3334
  var v8PrototypeDefineBug = descriptors && fails(function () {
    // eslint-disable-next-line es/no-object-defineproperty -- required for testing
    return Object.defineProperty(function () { /* empty */ }, 'prototype', {
      value: 42,
      writable: false
    }).prototype != 42;
  });

  var $String$4 = String;
  var $TypeError$f = TypeError;

  // `Assert: Type(argument) is Object`
  var anObject = function (argument) {
    if (isObject(argument)) return argument;
    throw $TypeError$f($String$4(argument) + ' is not an object');
  };

  var $TypeError$e = TypeError;
  // eslint-disable-next-line es/no-object-defineproperty -- safe
  var $defineProperty$1 = Object.defineProperty;
  // eslint-disable-next-line es/no-object-getownpropertydescriptor -- safe
  var $getOwnPropertyDescriptor$1 = Object.getOwnPropertyDescriptor;
  var ENUMERABLE = 'enumerable';
  var CONFIGURABLE$1 = 'configurable';
  var WRITABLE = 'writable';

  // `Object.defineProperty` method
  // https://tc39.es/ecma262/#sec-object.defineproperty
  var f$6 = descriptors ? v8PrototypeDefineBug ? function defineProperty(O, P, Attributes) {
    anObject(O);
    P = toPropertyKey(P);
    anObject(Attributes);
    if (typeof O === 'function' && P === 'prototype' && 'value' in Attributes && WRITABLE in Attributes && !Attributes[WRITABLE]) {
      var current = $getOwnPropertyDescriptor$1(O, P);
      if (current && current[WRITABLE]) {
        O[P] = Attributes.value;
        Attributes = {
          configurable: CONFIGURABLE$1 in Attributes ? Attributes[CONFIGURABLE$1] : current[CONFIGURABLE$1],
          enumerable: ENUMERABLE in Attributes ? Attributes[ENUMERABLE] : current[ENUMERABLE],
          writable: false
        };
      }
    } return $defineProperty$1(O, P, Attributes);
  } : $defineProperty$1 : function defineProperty(O, P, Attributes) {
    anObject(O);
    P = toPropertyKey(P);
    anObject(Attributes);
    if (ie8DomDefine) try {
      return $defineProperty$1(O, P, Attributes);
    } catch (error) { /* empty */ }
    if ('get' in Attributes || 'set' in Attributes) throw $TypeError$e('Accessors not supported');
    if ('value' in Attributes) O[P] = Attributes.value;
    return O;
  };

  var objectDefineProperty = {
  	f: f$6
  };

  var createNonEnumerableProperty = descriptors ? function (object, key, value) {
    return objectDefineProperty.f(object, key, createPropertyDescriptor(1, value));
  } : function (object, key, value) {
    object[key] = value;
    return object;
  };

  var FunctionPrototype$2 = Function.prototype;
  // eslint-disable-next-line es/no-object-getownpropertydescriptor -- safe
  var getDescriptor = descriptors && Object.getOwnPropertyDescriptor;

  var EXISTS = hasOwnProperty_1(FunctionPrototype$2, 'name');
  // additional protection from minified / mangled / dropped function names
  var PROPER = EXISTS && (function something() { /* empty */ }).name === 'something';
  var CONFIGURABLE = EXISTS && (!descriptors || (descriptors && getDescriptor(FunctionPrototype$2, 'name').configurable));

  var functionName = {
    EXISTS: EXISTS,
    PROPER: PROPER,
    CONFIGURABLE: CONFIGURABLE
  };

  var functionToString$1 = functionUncurryThis(Function.toString);

  // this helper broken in `core-js@3.4.1-3.4.4`, so we can't use `shared` helper
  if (!isCallable(sharedStore.inspectSource)) {
    sharedStore.inspectSource = function (it) {
      return functionToString$1(it);
    };
  }

  var inspectSource = sharedStore.inspectSource;

  var WeakMap$1 = global_1.WeakMap;

  var weakMapBasicDetection = isCallable(WeakMap$1) && /native code/.test(String(WeakMap$1));

  var keys$1 = shared('keys');

  var sharedKey = function (key) {
    return keys$1[key] || (keys$1[key] = uid(key));
  };

  var hiddenKeys$1 = {};

  var OBJECT_ALREADY_INITIALIZED = 'Object already initialized';
  var TypeError$7 = global_1.TypeError;
  var WeakMap = global_1.WeakMap;
  var set$2, get$1, has;

  var enforce = function (it) {
    return has(it) ? get$1(it) : set$2(it, {});
  };

  var getterFor = function (TYPE) {
    return function (it) {
      var state;
      if (!isObject(it) || (state = get$1(it)).type !== TYPE) {
        throw TypeError$7('Incompatible receiver, ' + TYPE + ' required');
      } return state;
    };
  };

  if (weakMapBasicDetection || sharedStore.state) {
    var store = sharedStore.state || (sharedStore.state = new WeakMap());
    /* eslint-disable no-self-assign -- prototype methods protection */
    store.get = store.get;
    store.has = store.has;
    store.set = store.set;
    /* eslint-enable no-self-assign -- prototype methods protection */
    set$2 = function (it, metadata) {
      if (store.has(it)) throw TypeError$7(OBJECT_ALREADY_INITIALIZED);
      metadata.facade = it;
      store.set(it, metadata);
      return metadata;
    };
    get$1 = function (it) {
      return store.get(it) || {};
    };
    has = function (it) {
      return store.has(it);
    };
  } else {
    var STATE = sharedKey('state');
    hiddenKeys$1[STATE] = true;
    set$2 = function (it, metadata) {
      if (hasOwnProperty_1(it, STATE)) throw TypeError$7(OBJECT_ALREADY_INITIALIZED);
      metadata.facade = it;
      createNonEnumerableProperty(it, STATE, metadata);
      return metadata;
    };
    get$1 = function (it) {
      return hasOwnProperty_1(it, STATE) ? it[STATE] : {};
    };
    has = function (it) {
      return hasOwnProperty_1(it, STATE);
    };
  }

  var internalState = {
    set: set$2,
    get: get$1,
    has: has,
    enforce: enforce,
    getterFor: getterFor
  };

  var makeBuiltIn_1 = createCommonjsModule(function (module) {





  var CONFIGURABLE_FUNCTION_NAME = functionName.CONFIGURABLE;



  var enforceInternalState = internalState.enforce;
  var getInternalState = internalState.get;
  var $String = String;
  // eslint-disable-next-line es/no-object-defineproperty -- safe
  var defineProperty = Object.defineProperty;
  var stringSlice = functionUncurryThis(''.slice);
  var replace = functionUncurryThis(''.replace);
  var join = functionUncurryThis([].join);

  var CONFIGURABLE_LENGTH = descriptors && !fails(function () {
    return defineProperty(function () { /* empty */ }, 'length', { value: 8 }).length !== 8;
  });

  var TEMPLATE = String(String).split('String');

  var makeBuiltIn = module.exports = function (value, name, options) {
    if (stringSlice($String(name), 0, 7) === 'Symbol(') {
      name = '[' + replace($String(name), /^Symbol\(([^)]*)\)/, '$1') + ']';
    }
    if (options && options.getter) name = 'get ' + name;
    if (options && options.setter) name = 'set ' + name;
    if (!hasOwnProperty_1(value, 'name') || (CONFIGURABLE_FUNCTION_NAME && value.name !== name)) {
      if (descriptors) defineProperty(value, 'name', { value: name, configurable: true });
      else value.name = name;
    }
    if (CONFIGURABLE_LENGTH && options && hasOwnProperty_1(options, 'arity') && value.length !== options.arity) {
      defineProperty(value, 'length', { value: options.arity });
    }
    try {
      if (options && hasOwnProperty_1(options, 'constructor') && options.constructor) {
        if (descriptors) defineProperty(value, 'prototype', { writable: false });
      // in V8 ~ Chrome 53, prototypes of some methods, like `Array.prototype.values`, are non-writable
      } else if (value.prototype) value.prototype = undefined;
    } catch (error) { /* empty */ }
    var state = enforceInternalState(value);
    if (!hasOwnProperty_1(state, 'source')) {
      state.source = join(TEMPLATE, typeof name == 'string' ? name : '');
    } return value;
  };

  // add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative
  // eslint-disable-next-line no-extend-native -- required
  Function.prototype.toString = makeBuiltIn(function toString() {
    return isCallable(this) && getInternalState(this).source || inspectSource(this);
  }, 'toString');
  });

  var defineBuiltIn = function (O, key, value, options) {
    if (!options) options = {};
    var simple = options.enumerable;
    var name = options.name !== undefined ? options.name : key;
    if (isCallable(value)) makeBuiltIn_1(value, name, options);
    if (options.global) {
      if (simple) O[key] = value;
      else defineGlobalProperty(key, value);
    } else {
      try {
        if (!options.unsafe) delete O[key];
        else if (O[key]) simple = true;
      } catch (error) { /* empty */ }
      if (simple) O[key] = value;
      else objectDefineProperty.f(O, key, {
        value: value,
        enumerable: false,
        configurable: !options.nonConfigurable,
        writable: !options.nonWritable
      });
    } return O;
  };

  var ceil = Math.ceil;
  var floor$8 = Math.floor;

  // `Math.trunc` method
  // https://tc39.es/ecma262/#sec-math.trunc
  // eslint-disable-next-line es/no-math-trunc -- safe
  var mathTrunc = Math.trunc || function trunc(x) {
    var n = +x;
    return (n > 0 ? floor$8 : ceil)(n);
  };

  // `ToIntegerOrInfinity` abstract operation
  // https://tc39.es/ecma262/#sec-tointegerorinfinity
  var toIntegerOrInfinity = function (argument) {
    var number = +argument;
    // eslint-disable-next-line no-self-compare -- NaN check
    return number !== number || number === 0 ? 0 : mathTrunc(number);
  };

  var max$4 = Math.max;
  var min$6 = Math.min;

  // Helper for a popular repeating case of the spec:
  // Let integer be ? ToInteger(index).
  // If integer < 0, let result be max((length + integer), 0); else let result be min(integer, length).
  var toAbsoluteIndex = function (index, length) {
    var integer = toIntegerOrInfinity(index);
    return integer < 0 ? max$4(integer + length, 0) : min$6(integer, length);
  };

  var min$5 = Math.min;

  // `ToLength` abstract operation
  // https://tc39.es/ecma262/#sec-tolength
  var toLength = function (argument) {
    return argument > 0 ? min$5(toIntegerOrInfinity(argument), 0x1FFFFFFFFFFFFF) : 0; // 2 ** 53 - 1 == 9007199254740991
  };

  // `LengthOfArrayLike` abstract operation
  // https://tc39.es/ecma262/#sec-lengthofarraylike
  var lengthOfArrayLike = function (obj) {
    return toLength(obj.length);
  };

  // `Array.prototype.{ indexOf, includes }` methods implementation
  var createMethod$4 = function (IS_INCLUDES) {
    return function ($this, el, fromIndex) {
      var O = toIndexedObject($this);
      var length = lengthOfArrayLike(O);
      var index = toAbsoluteIndex(fromIndex, length);
      var value;
      // Array#includes uses SameValueZero equality algorithm
      // eslint-disable-next-line no-self-compare -- NaN check
      if (IS_INCLUDES && el != el) while (length > index) {
        value = O[index++];
        // eslint-disable-next-line no-self-compare -- NaN check
        if (value != value) return true;
      // Array#indexOf ignores holes, Array#includes - not
      } else for (;length > index; index++) {
        if ((IS_INCLUDES || index in O) && O[index] === el) return IS_INCLUDES || index || 0;
      } return !IS_INCLUDES && -1;
    };
  };

  var arrayIncludes = {
    // `Array.prototype.includes` method
    // https://tc39.es/ecma262/#sec-array.prototype.includes
    includes: createMethod$4(true),
    // `Array.prototype.indexOf` method
    // https://tc39.es/ecma262/#sec-array.prototype.indexof
    indexOf: createMethod$4(false)
  };

  var indexOf$1 = arrayIncludes.indexOf;


  var push$9 = functionUncurryThis([].push);

  var objectKeysInternal = function (object, names) {
    var O = toIndexedObject(object);
    var i = 0;
    var result = [];
    var key;
    for (key in O) !hasOwnProperty_1(hiddenKeys$1, key) && hasOwnProperty_1(O, key) && push$9(result, key);
    // Don't enum bug & hidden keys
    while (names.length > i) if (hasOwnProperty_1(O, key = names[i++])) {
      ~indexOf$1(result, key) || push$9(result, key);
    }
    return result;
  };

  // IE8- don't enum bug keys
  var enumBugKeys = [
    'constructor',
    'hasOwnProperty',
    'isPrototypeOf',
    'propertyIsEnumerable',
    'toLocaleString',
    'toString',
    'valueOf'
  ];

  var hiddenKeys = enumBugKeys.concat('length', 'prototype');

  // `Object.getOwnPropertyNames` method
  // https://tc39.es/ecma262/#sec-object.getownpropertynames
  // eslint-disable-next-line es/no-object-getownpropertynames -- safe
  var f$5 = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
    return objectKeysInternal(O, hiddenKeys);
  };

  var objectGetOwnPropertyNames = {
  	f: f$5
  };

  // eslint-disable-next-line es/no-object-getownpropertysymbols -- safe
  var f$4 = Object.getOwnPropertySymbols;

  var objectGetOwnPropertySymbols = {
  	f: f$4
  };

  var concat$3 = functionUncurryThis([].concat);

  // all object keys, includes non-enumerable and symbols
  var ownKeys = getBuiltIn('Reflect', 'ownKeys') || function ownKeys(it) {
    var keys = objectGetOwnPropertyNames.f(anObject(it));
    var getOwnPropertySymbols = objectGetOwnPropertySymbols.f;
    return getOwnPropertySymbols ? concat$3(keys, getOwnPropertySymbols(it)) : keys;
  };

  var copyConstructorProperties$1 = function (target, source, exceptions) {
    var keys = ownKeys(source);
    var defineProperty = objectDefineProperty.f;
    var getOwnPropertyDescriptor = objectGetOwnPropertyDescriptor.f;
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      if (!hasOwnProperty_1(target, key) && !(exceptions && hasOwnProperty_1(exceptions, key))) {
        defineProperty(target, key, getOwnPropertyDescriptor(source, key));
      }
    }
  };

  var replacement = /#|\.prototype\./;

  var isForced = function (feature, detection) {
    var value = data[normalize(feature)];
    return value == POLYFILL ? true
      : value == NATIVE ? false
      : isCallable(detection) ? fails(detection)
      : !!detection;
  };

  var normalize = isForced.normalize = function (string) {
    return String(string).replace(replacement, '.').toLowerCase();
  };

  var data = isForced.data = {};
  var NATIVE = isForced.NATIVE = 'N';
  var POLYFILL = isForced.POLYFILL = 'P';

  var isForced_1 = isForced;

  var getOwnPropertyDescriptor$5 = objectGetOwnPropertyDescriptor.f;






  /*
    options.target         - name of the target object
    options.global         - target is the global object
    options.stat           - export as static methods of target
    options.proto          - export as prototype methods of target
    options.real           - real prototype method for the `pure` version
    options.forced         - export even if the native feature is available
    options.bind           - bind methods to the target, required for the `pure` version
    options.wrap           - wrap constructors to preventing global pollution, required for the `pure` version
    options.unsafe         - use the simple assignment of property instead of delete + defineProperty
    options.sham           - add a flag to not completely full polyfills
    options.enumerable     - export as enumerable property
    options.dontCallGetSet - prevent calling a getter on target
    options.name           - the .name of the function if it does not match the key
  */
  var _export = function (options, source) {
    var TARGET = options.target;
    var GLOBAL = options.global;
    var STATIC = options.stat;
    var FORCED, target, key, targetProperty, sourceProperty, descriptor;
    if (GLOBAL) {
      target = global_1;
    } else if (STATIC) {
      target = global_1[TARGET] || defineGlobalProperty(TARGET, {});
    } else {
      target = (global_1[TARGET] || {}).prototype;
    }
    if (target) for (key in source) {
      sourceProperty = source[key];
      if (options.dontCallGetSet) {
        descriptor = getOwnPropertyDescriptor$5(target, key);
        targetProperty = descriptor && descriptor.value;
      } else targetProperty = target[key];
      FORCED = isForced_1(GLOBAL ? key : TARGET + (STATIC ? '.' : '#') + key, options.forced);
      // contained in target
      if (!FORCED && targetProperty !== undefined) {
        if (typeof sourceProperty == typeof targetProperty) continue;
        copyConstructorProperties$1(sourceProperty, targetProperty);
      }
      // add a flag to not completely full polyfills
      if (options.sham || (targetProperty && targetProperty.sham)) {
        createNonEnumerableProperty(sourceProperty, 'sham', true);
      }
      defineBuiltIn(target, key, sourceProperty, options);
    }
  };

  // `IsArray` abstract operation
  // https://tc39.es/ecma262/#sec-isarray
  // eslint-disable-next-line es/no-array-isarray -- safe
  var isArray$1 = Array.isArray || function isArray(argument) {
    return classofRaw(argument) == 'Array';
  };

  var TO_STRING_TAG$4 = wellKnownSymbol('toStringTag');
  var test$1 = {};

  test$1[TO_STRING_TAG$4] = 'z';

  var toStringTagSupport = String(test$1) === '[object z]';

  var TO_STRING_TAG$3 = wellKnownSymbol('toStringTag');
  var $Object$2 = Object;

  // ES3 wrong here
  var CORRECT_ARGUMENTS = classofRaw(function () { return arguments; }()) == 'Arguments';

  // fallback for IE11 Script Access Denied error
  var tryGet = function (it, key) {
    try {
      return it[key];
    } catch (error) { /* empty */ }
  };

  // getting tag from ES6+ `Object.prototype.toString`
  var classof = toStringTagSupport ? classofRaw : function (it) {
    var O, tag, result;
    return it === undefined ? 'Undefined' : it === null ? 'Null'
      // @@toStringTag case
      : typeof (tag = tryGet(O = $Object$2(it), TO_STRING_TAG$3)) == 'string' ? tag
      // builtinTag case
      : CORRECT_ARGUMENTS ? classofRaw(O)
      // ES3 arguments fallback
      : (result = classofRaw(O)) == 'Object' && isCallable(O.callee) ? 'Arguments' : result;
  };

  var noop$1 = function () { /* empty */ };
  var empty$1 = [];
  var construct$1 = getBuiltIn('Reflect', 'construct');
  var constructorRegExp = /^\s*(?:class|function)\b/;
  var exec$3 = functionUncurryThis(constructorRegExp.exec);
  var INCORRECT_TO_STRING = !constructorRegExp.exec(noop$1);

  var isConstructorModern = function isConstructor(argument) {
    if (!isCallable(argument)) return false;
    try {
      construct$1(noop$1, empty$1, argument);
      return true;
    } catch (error) {
      return false;
    }
  };

  var isConstructorLegacy = function isConstructor(argument) {
    if (!isCallable(argument)) return false;
    switch (classof(argument)) {
      case 'AsyncFunction':
      case 'GeneratorFunction':
      case 'AsyncGeneratorFunction': return false;
    }
    try {
      // we can't check .prototype since constructors produced by .bind haven't it
      // `Function#toString` throws on some built-it function in some legacy engines
      // (for example, `DOMQuad` and similar in FF41-)
      return INCORRECT_TO_STRING || !!exec$3(constructorRegExp, inspectSource(argument));
    } catch (error) {
      return true;
    }
  };

  isConstructorLegacy.sham = true;

  // `IsConstructor` abstract operation
  // https://tc39.es/ecma262/#sec-isconstructor
  var isConstructor = !construct$1 || fails(function () {
    var called;
    return isConstructorModern(isConstructorModern.call)
      || !isConstructorModern(Object)
      || !isConstructorModern(function () { called = true; })
      || called;
  }) ? isConstructorLegacy : isConstructorModern;

  var createProperty = function (object, key, value) {
    var propertyKey = toPropertyKey(key);
    if (propertyKey in object) objectDefineProperty.f(object, propertyKey, createPropertyDescriptor(0, value));
    else object[propertyKey] = value;
  };

  var SPECIES$6 = wellKnownSymbol('species');

  var arrayMethodHasSpeciesSupport = function (METHOD_NAME) {
    // We can't use this feature detection in V8 since it causes
    // deoptimization and serious performance degradation
    // https://github.com/zloirock/core-js/issues/677
    return engineV8Version >= 51 || !fails(function () {
      var array = [];
      var constructor = array.constructor = {};
      constructor[SPECIES$6] = function () {
        return { foo: 1 };
      };
      return array[METHOD_NAME](Boolean).foo !== 1;
    });
  };

  var arraySlice = functionUncurryThis([].slice);

  var HAS_SPECIES_SUPPORT$3 = arrayMethodHasSpeciesSupport('slice');

  var SPECIES$5 = wellKnownSymbol('species');
  var $Array$4 = Array;
  var max$3 = Math.max;

  // `Array.prototype.slice` method
  // https://tc39.es/ecma262/#sec-array.prototype.slice
  // fallback for not array-like ES3 strings and DOM objects
  _export({ target: 'Array', proto: true, forced: !HAS_SPECIES_SUPPORT$3 }, {
    slice: function slice(start, end) {
      var O = toIndexedObject(this);
      var length = lengthOfArrayLike(O);
      var k = toAbsoluteIndex(start, length);
      var fin = toAbsoluteIndex(end === undefined ? length : end, length);
      // inline `ArraySpeciesCreate` for usage native `Array#slice` where it's possible
      var Constructor, result, n;
      if (isArray$1(O)) {
        Constructor = O.constructor;
        // cross-realm fallback
        if (isConstructor(Constructor) && (Constructor === $Array$4 || isArray$1(Constructor.prototype))) {
          Constructor = undefined;
        } else if (isObject(Constructor)) {
          Constructor = Constructor[SPECIES$5];
          if (Constructor === null) Constructor = undefined;
        }
        if (Constructor === $Array$4 || Constructor === undefined) {
          return arraySlice(O, k, fin);
        }
      }
      result = new (Constructor === undefined ? $Array$4 : Constructor)(max$3(fin - k, 0));
      for (n = 0; k < fin; k++, n++) if (k in O) createProperty(result, n, O[k]);
      result.length = n;
      return result;
    }
  });

  var defineBuiltInAccessor = function (target, name, descriptor) {
    if (descriptor.get) makeBuiltIn_1(descriptor.get, name, { getter: true });
    if (descriptor.set) makeBuiltIn_1(descriptor.set, name, { setter: true });
    return objectDefineProperty.f(target, name, descriptor);
  };

  var FUNCTION_NAME_EXISTS = functionName.EXISTS;



  var FunctionPrototype$1 = Function.prototype;
  var functionToString = functionUncurryThis(FunctionPrototype$1.toString);
  var nameRE = /function\b(?:\s|\/\*[\S\s]*?\*\/|\/\/[^\n\r]*[\n\r]+)*([^\s(/]*)/;
  var regExpExec = functionUncurryThis(nameRE.exec);
  var NAME$1 = 'name';

  // Function instances `.name` property
  // https://tc39.es/ecma262/#sec-function-instances-name
  if (descriptors && !FUNCTION_NAME_EXISTS) {
    defineBuiltInAccessor(FunctionPrototype$1, NAME$1, {
      configurable: true,
      get: function () {
        try {
          return regExpExec(nameRE, functionToString(this))[1];
        } catch (error) {
          return '';
        }
      }
    });
  }

  // `Object.keys` method
  // https://tc39.es/ecma262/#sec-object.keys
  // eslint-disable-next-line es/no-object-keys -- safe
  var objectKeys = Object.keys || function keys(O) {
    return objectKeysInternal(O, enumBugKeys);
  };

  // `Object.defineProperties` method
  // https://tc39.es/ecma262/#sec-object.defineproperties
  // eslint-disable-next-line es/no-object-defineproperties -- safe
  var f$3 = descriptors && !v8PrototypeDefineBug ? Object.defineProperties : function defineProperties(O, Properties) {
    anObject(O);
    var props = toIndexedObject(Properties);
    var keys = objectKeys(Properties);
    var length = keys.length;
    var index = 0;
    var key;
    while (length > index) objectDefineProperty.f(O, key = keys[index++], props[key]);
    return O;
  };

  var objectDefineProperties = {
  	f: f$3
  };

  var html = getBuiltIn('document', 'documentElement');

  /* global ActiveXObject -- old IE, WSH */








  var GT = '>';
  var LT = '<';
  var PROTOTYPE$2 = 'prototype';
  var SCRIPT = 'script';
  var IE_PROTO$1 = sharedKey('IE_PROTO');

  var EmptyConstructor = function () { /* empty */ };

  var scriptTag = function (content) {
    return LT + SCRIPT + GT + content + LT + '/' + SCRIPT + GT;
  };

  // Create object with fake `null` prototype: use ActiveX Object with cleared prototype
  var NullProtoObjectViaActiveX = function (activeXDocument) {
    activeXDocument.write(scriptTag(''));
    activeXDocument.close();
    var temp = activeXDocument.parentWindow.Object;
    activeXDocument = null; // avoid memory leak
    return temp;
  };

  // Create object with fake `null` prototype: use iframe Object with cleared prototype
  var NullProtoObjectViaIFrame = function () {
    // Thrash, waste and sodomy: IE GC bug
    var iframe = documentCreateElement('iframe');
    var JS = 'java' + SCRIPT + ':';
    var iframeDocument;
    iframe.style.display = 'none';
    html.appendChild(iframe);
    // https://github.com/zloirock/core-js/issues/475
    iframe.src = String(JS);
    iframeDocument = iframe.contentWindow.document;
    iframeDocument.open();
    iframeDocument.write(scriptTag('document.F=Object'));
    iframeDocument.close();
    return iframeDocument.F;
  };

  // Check for document.domain and active x support
  // No need to use active x approach when document.domain is not set
  // see https://github.com/es-shims/es5-shim/issues/150
  // variation of https://github.com/kitcambridge/es5-shim/commit/4f738ac066346
  // avoid IE GC bug
  var activeXDocument;
  var NullProtoObject = function () {
    try {
      activeXDocument = new ActiveXObject('htmlfile');
    } catch (error) { /* ignore */ }
    NullProtoObject = typeof document != 'undefined'
      ? document.domain && activeXDocument
        ? NullProtoObjectViaActiveX(activeXDocument) // old IE
        : NullProtoObjectViaIFrame()
      : NullProtoObjectViaActiveX(activeXDocument); // WSH
    var length = enumBugKeys.length;
    while (length--) delete NullProtoObject[PROTOTYPE$2][enumBugKeys[length]];
    return NullProtoObject();
  };

  hiddenKeys$1[IE_PROTO$1] = true;

  // `Object.create` method
  // https://tc39.es/ecma262/#sec-object.create
  // eslint-disable-next-line es/no-object-create -- safe
  var objectCreate = Object.create || function create(O, Properties) {
    var result;
    if (O !== null) {
      EmptyConstructor[PROTOTYPE$2] = anObject(O);
      result = new EmptyConstructor();
      EmptyConstructor[PROTOTYPE$2] = null;
      // add "__proto__" for Object.getPrototypeOf polyfill
      result[IE_PROTO$1] = O;
    } else result = NullProtoObject();
    return Properties === undefined ? result : objectDefineProperties.f(result, Properties);
  };

  var defineProperty$5 = objectDefineProperty.f;

  var UNSCOPABLES = wellKnownSymbol('unscopables');
  var ArrayPrototype$1 = Array.prototype;

  // Array.prototype[@@unscopables]
  // https://tc39.es/ecma262/#sec-array.prototype-@@unscopables
  if (ArrayPrototype$1[UNSCOPABLES] == undefined) {
    defineProperty$5(ArrayPrototype$1, UNSCOPABLES, {
      configurable: true,
      value: objectCreate(null)
    });
  }

  // add a key to Array.prototype[@@unscopables]
  var addToUnscopables = function (key) {
    ArrayPrototype$1[UNSCOPABLES][key] = true;
  };

  var iterators = {};

  var correctPrototypeGetter = !fails(function () {
    function F() { /* empty */ }
    F.prototype.constructor = null;
    // eslint-disable-next-line es/no-object-getprototypeof -- required for testing
    return Object.getPrototypeOf(new F()) !== F.prototype;
  });

  var IE_PROTO = sharedKey('IE_PROTO');
  var $Object$1 = Object;
  var ObjectPrototype$4 = $Object$1.prototype;

  // `Object.getPrototypeOf` method
  // https://tc39.es/ecma262/#sec-object.getprototypeof
  // eslint-disable-next-line es/no-object-getprototypeof -- safe
  var objectGetPrototypeOf = correctPrototypeGetter ? $Object$1.getPrototypeOf : function (O) {
    var object = toObject(O);
    if (hasOwnProperty_1(object, IE_PROTO)) return object[IE_PROTO];
    var constructor = object.constructor;
    if (isCallable(constructor) && object instanceof constructor) {
      return constructor.prototype;
    } return object instanceof $Object$1 ? ObjectPrototype$4 : null;
  };

  var ITERATOR$8 = wellKnownSymbol('iterator');
  var BUGGY_SAFARI_ITERATORS$1 = false;

  // `%IteratorPrototype%` object
  // https://tc39.es/ecma262/#sec-%iteratorprototype%-object
  var IteratorPrototype$2, PrototypeOfArrayIteratorPrototype, arrayIterator;

  /* eslint-disable es/no-array-prototype-keys -- safe */
  if ([].keys) {
    arrayIterator = [].keys();
    // Safari 8 has buggy iterators w/o `next`
    if (!('next' in arrayIterator)) BUGGY_SAFARI_ITERATORS$1 = true;
    else {
      PrototypeOfArrayIteratorPrototype = objectGetPrototypeOf(objectGetPrototypeOf(arrayIterator));
      if (PrototypeOfArrayIteratorPrototype !== Object.prototype) IteratorPrototype$2 = PrototypeOfArrayIteratorPrototype;
    }
  }

  var NEW_ITERATOR_PROTOTYPE = !isObject(IteratorPrototype$2) || fails(function () {
    var test = {};
    // FF44- legacy iterators case
    return IteratorPrototype$2[ITERATOR$8].call(test) !== test;
  });

  if (NEW_ITERATOR_PROTOTYPE) IteratorPrototype$2 = {};

  // `%IteratorPrototype%[@@iterator]()` method
  // https://tc39.es/ecma262/#sec-%iteratorprototype%-@@iterator
  if (!isCallable(IteratorPrototype$2[ITERATOR$8])) {
    defineBuiltIn(IteratorPrototype$2, ITERATOR$8, function () {
      return this;
    });
  }

  var iteratorsCore = {
    IteratorPrototype: IteratorPrototype$2,
    BUGGY_SAFARI_ITERATORS: BUGGY_SAFARI_ITERATORS$1
  };

  var defineProperty$4 = objectDefineProperty.f;



  var TO_STRING_TAG$2 = wellKnownSymbol('toStringTag');

  var setToStringTag = function (target, TAG, STATIC) {
    if (target && !STATIC) target = target.prototype;
    if (target && !hasOwnProperty_1(target, TO_STRING_TAG$2)) {
      defineProperty$4(target, TO_STRING_TAG$2, { configurable: true, value: TAG });
    }
  };

  var IteratorPrototype$1 = iteratorsCore.IteratorPrototype;





  var returnThis$1 = function () { return this; };

  var iteratorCreateConstructor = function (IteratorConstructor, NAME, next, ENUMERABLE_NEXT) {
    var TO_STRING_TAG = NAME + ' Iterator';
    IteratorConstructor.prototype = objectCreate(IteratorPrototype$1, { next: createPropertyDescriptor(+!ENUMERABLE_NEXT, next) });
    setToStringTag(IteratorConstructor, TO_STRING_TAG, false);
    iterators[TO_STRING_TAG] = returnThis$1;
    return IteratorConstructor;
  };

  var functionUncurryThisAccessor = function (object, key, method) {
    try {
      // eslint-disable-next-line es/no-object-getownpropertydescriptor -- safe
      return functionUncurryThis(aCallable(Object.getOwnPropertyDescriptor(object, key)[method]));
    } catch (error) { /* empty */ }
  };

  var $String$3 = String;
  var $TypeError$d = TypeError;

  var aPossiblePrototype = function (argument) {
    if (typeof argument == 'object' || isCallable(argument)) return argument;
    throw $TypeError$d("Can't set " + $String$3(argument) + ' as a prototype');
  };

  /* eslint-disable no-proto -- safe */




  // `Object.setPrototypeOf` method
  // https://tc39.es/ecma262/#sec-object.setprototypeof
  // Works with __proto__ only. Old v8 can't work with null proto objects.
  // eslint-disable-next-line es/no-object-setprototypeof -- safe
  var objectSetPrototypeOf = Object.setPrototypeOf || ('__proto__' in {} ? function () {
    var CORRECT_SETTER = false;
    var test = {};
    var setter;
    try {
      setter = functionUncurryThisAccessor(Object.prototype, '__proto__', 'set');
      setter(test, []);
      CORRECT_SETTER = test instanceof Array;
    } catch (error) { /* empty */ }
    return function setPrototypeOf(O, proto) {
      anObject(O);
      aPossiblePrototype(proto);
      if (CORRECT_SETTER) setter(O, proto);
      else O.__proto__ = proto;
      return O;
    };
  }() : undefined);

  var PROPER_FUNCTION_NAME$3 = functionName.PROPER;
  var CONFIGURABLE_FUNCTION_NAME$1 = functionName.CONFIGURABLE;
  var IteratorPrototype = iteratorsCore.IteratorPrototype;
  var BUGGY_SAFARI_ITERATORS = iteratorsCore.BUGGY_SAFARI_ITERATORS;
  var ITERATOR$7 = wellKnownSymbol('iterator');
  var KEYS = 'keys';
  var VALUES = 'values';
  var ENTRIES = 'entries';

  var returnThis = function () { return this; };

  var iteratorDefine = function (Iterable, NAME, IteratorConstructor, next, DEFAULT, IS_SET, FORCED) {
    iteratorCreateConstructor(IteratorConstructor, NAME, next);

    var getIterationMethod = function (KIND) {
      if (KIND === DEFAULT && defaultIterator) return defaultIterator;
      if (!BUGGY_SAFARI_ITERATORS && KIND in IterablePrototype) return IterablePrototype[KIND];
      switch (KIND) {
        case KEYS: return function keys() { return new IteratorConstructor(this, KIND); };
        case VALUES: return function values() { return new IteratorConstructor(this, KIND); };
        case ENTRIES: return function entries() { return new IteratorConstructor(this, KIND); };
      } return function () { return new IteratorConstructor(this); };
    };

    var TO_STRING_TAG = NAME + ' Iterator';
    var INCORRECT_VALUES_NAME = false;
    var IterablePrototype = Iterable.prototype;
    var nativeIterator = IterablePrototype[ITERATOR$7]
      || IterablePrototype['@@iterator']
      || DEFAULT && IterablePrototype[DEFAULT];
    var defaultIterator = !BUGGY_SAFARI_ITERATORS && nativeIterator || getIterationMethod(DEFAULT);
    var anyNativeIterator = NAME == 'Array' ? IterablePrototype.entries || nativeIterator : nativeIterator;
    var CurrentIteratorPrototype, methods, KEY;

    // fix native
    if (anyNativeIterator) {
      CurrentIteratorPrototype = objectGetPrototypeOf(anyNativeIterator.call(new Iterable()));
      if (CurrentIteratorPrototype !== Object.prototype && CurrentIteratorPrototype.next) {
        if (objectGetPrototypeOf(CurrentIteratorPrototype) !== IteratorPrototype) {
          if (objectSetPrototypeOf) {
            objectSetPrototypeOf(CurrentIteratorPrototype, IteratorPrototype);
          } else if (!isCallable(CurrentIteratorPrototype[ITERATOR$7])) {
            defineBuiltIn(CurrentIteratorPrototype, ITERATOR$7, returnThis);
          }
        }
        // Set @@toStringTag to native iterators
        setToStringTag(CurrentIteratorPrototype, TO_STRING_TAG, true);
      }
    }

    // fix Array.prototype.{ values, @@iterator }.name in V8 / FF
    if (PROPER_FUNCTION_NAME$3 && DEFAULT == VALUES && nativeIterator && nativeIterator.name !== VALUES) {
      if (CONFIGURABLE_FUNCTION_NAME$1) {
        createNonEnumerableProperty(IterablePrototype, 'name', VALUES);
      } else {
        INCORRECT_VALUES_NAME = true;
        defaultIterator = function values() { return functionCall(nativeIterator, this); };
      }
    }

    // export additional methods
    if (DEFAULT) {
      methods = {
        values: getIterationMethod(VALUES),
        keys: IS_SET ? defaultIterator : getIterationMethod(KEYS),
        entries: getIterationMethod(ENTRIES)
      };
      if (FORCED) for (KEY in methods) {
        if (BUGGY_SAFARI_ITERATORS || INCORRECT_VALUES_NAME || !(KEY in IterablePrototype)) {
          defineBuiltIn(IterablePrototype, KEY, methods[KEY]);
        }
      } else _export({ target: NAME, proto: true, forced: BUGGY_SAFARI_ITERATORS || INCORRECT_VALUES_NAME }, methods);
    }

    // define iterator
    if (IterablePrototype[ITERATOR$7] !== defaultIterator) {
      defineBuiltIn(IterablePrototype, ITERATOR$7, defaultIterator, { name: DEFAULT });
    }
    iterators[NAME] = defaultIterator;

    return methods;
  };

  // `CreateIterResultObject` abstract operation
  // https://tc39.es/ecma262/#sec-createiterresultobject
  var createIterResultObject = function (value, done) {
    return { value: value, done: done };
  };

  var defineProperty$3 = objectDefineProperty.f;





  var ARRAY_ITERATOR = 'Array Iterator';
  var setInternalState$8 = internalState.set;
  var getInternalState$4 = internalState.getterFor(ARRAY_ITERATOR);

  // `Array.prototype.entries` method
  // https://tc39.es/ecma262/#sec-array.prototype.entries
  // `Array.prototype.keys` method
  // https://tc39.es/ecma262/#sec-array.prototype.keys
  // `Array.prototype.values` method
  // https://tc39.es/ecma262/#sec-array.prototype.values
  // `Array.prototype[@@iterator]` method
  // https://tc39.es/ecma262/#sec-array.prototype-@@iterator
  // `CreateArrayIterator` internal method
  // https://tc39.es/ecma262/#sec-createarrayiterator
  var es_array_iterator = iteratorDefine(Array, 'Array', function (iterated, kind) {
    setInternalState$8(this, {
      type: ARRAY_ITERATOR,
      target: toIndexedObject(iterated), // target
      index: 0,                          // next index
      kind: kind                         // kind
    });
  // `%ArrayIteratorPrototype%.next` method
  // https://tc39.es/ecma262/#sec-%arrayiteratorprototype%.next
  }, function () {
    var state = getInternalState$4(this);
    var target = state.target;
    var kind = state.kind;
    var index = state.index++;
    if (!target || index >= target.length) {
      state.target = undefined;
      return createIterResultObject(undefined, true);
    }
    if (kind == 'keys') return createIterResultObject(index, false);
    if (kind == 'values') return createIterResultObject(target[index], false);
    return createIterResultObject([index, target[index]], false);
  }, 'values');

  // argumentsList[@@iterator] is %ArrayProto_values%
  // https://tc39.es/ecma262/#sec-createunmappedargumentsobject
  // https://tc39.es/ecma262/#sec-createmappedargumentsobject
  var values = iterators.Arguments = iterators.Array;

  // https://tc39.es/ecma262/#sec-array.prototype-@@unscopables
  addToUnscopables('keys');
  addToUnscopables('values');
  addToUnscopables('entries');

  // V8 ~ Chrome 45- bug
  if (descriptors && values.name !== 'values') try {
    defineProperty$3(values, 'name', { value: 'values' });
  } catch (error) { /* empty */ }

  var $Array$3 = Array;
  var max$2 = Math.max;

  var arraySliceSimple = function (O, start, end) {
    var length = lengthOfArrayLike(O);
    var k = toAbsoluteIndex(start, length);
    var fin = toAbsoluteIndex(end === undefined ? length : end, length);
    var result = $Array$3(max$2(fin - k, 0));
    for (var n = 0; k < fin; k++, n++) createProperty(result, n, O[k]);
    result.length = n;
    return result;
  };

  /* eslint-disable es/no-object-getownpropertynames -- safe */


  var $getOwnPropertyNames$1 = objectGetOwnPropertyNames.f;


  var windowNames = typeof window == 'object' && window && Object.getOwnPropertyNames
    ? Object.getOwnPropertyNames(window) : [];

  var getWindowNames = function (it) {
    try {
      return $getOwnPropertyNames$1(it);
    } catch (error) {
      return arraySliceSimple(windowNames);
    }
  };

  // fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
  var f$2 = function getOwnPropertyNames(it) {
    return windowNames && classofRaw(it) == 'Window'
      ? getWindowNames(it)
      : $getOwnPropertyNames$1(toIndexedObject(it));
  };

  var objectGetOwnPropertyNamesExternal = {
  	f: f$2
  };

  // FF26- bug: ArrayBuffers are non-extensible, but Object.isExtensible does not report it


  var arrayBufferNonExtensible = fails(function () {
    if (typeof ArrayBuffer == 'function') {
      var buffer = new ArrayBuffer(8);
      // eslint-disable-next-line es/no-object-isextensible, es/no-object-defineproperty -- safe
      if (Object.isExtensible(buffer)) Object.defineProperty(buffer, 'a', { value: 8 });
    }
  });

  // eslint-disable-next-line es/no-object-isextensible -- safe
  var $isExtensible = Object.isExtensible;
  var FAILS_ON_PRIMITIVES$2 = fails(function () { $isExtensible(1); });

  // `Object.isExtensible` method
  // https://tc39.es/ecma262/#sec-object.isextensible
  var objectIsExtensible = (FAILS_ON_PRIMITIVES$2 || arrayBufferNonExtensible) ? function isExtensible(it) {
    if (!isObject(it)) return false;
    if (arrayBufferNonExtensible && classofRaw(it) == 'ArrayBuffer') return false;
    return $isExtensible ? $isExtensible(it) : true;
  } : $isExtensible;

  var freezing = !fails(function () {
    // eslint-disable-next-line es/no-object-isextensible, es/no-object-preventextensions -- required for testing
    return Object.isExtensible(Object.preventExtensions({}));
  });

  var internalMetadata = createCommonjsModule(function (module) {





  var defineProperty = objectDefineProperty.f;






  var REQUIRED = false;
  var METADATA = uid('meta');
  var id = 0;

  var setMetadata = function (it) {
    defineProperty(it, METADATA, { value: {
      objectID: 'O' + id++, // object ID
      weakData: {}          // weak collections IDs
    } });
  };

  var fastKey = function (it, create) {
    // return a primitive with prefix
    if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
    if (!hasOwnProperty_1(it, METADATA)) {
      // can't set metadata to uncaught frozen object
      if (!objectIsExtensible(it)) return 'F';
      // not necessary to add metadata
      if (!create) return 'E';
      // add missing metadata
      setMetadata(it);
    // return object ID
    } return it[METADATA].objectID;
  };

  var getWeakData = function (it, create) {
    if (!hasOwnProperty_1(it, METADATA)) {
      // can't set metadata to uncaught frozen object
      if (!objectIsExtensible(it)) return true;
      // not necessary to add metadata
      if (!create) return false;
      // add missing metadata
      setMetadata(it);
    // return the store of weak collections IDs
    } return it[METADATA].weakData;
  };

  // add metadata on freeze-family methods calling
  var onFreeze = function (it) {
    if (freezing && REQUIRED && objectIsExtensible(it) && !hasOwnProperty_1(it, METADATA)) setMetadata(it);
    return it;
  };

  var enable = function () {
    meta.enable = function () { /* empty */ };
    REQUIRED = true;
    var getOwnPropertyNames = objectGetOwnPropertyNames.f;
    var splice = functionUncurryThis([].splice);
    var test = {};
    test[METADATA] = 1;

    // prevent exposing of metadata key
    if (getOwnPropertyNames(test).length) {
      objectGetOwnPropertyNames.f = function (it) {
        var result = getOwnPropertyNames(it);
        for (var i = 0, length = result.length; i < length; i++) {
          if (result[i] === METADATA) {
            splice(result, i, 1);
            break;
          }
        } return result;
      };

      _export({ target: 'Object', stat: true, forced: true }, {
        getOwnPropertyNames: objectGetOwnPropertyNamesExternal.f
      });
    }
  };

  var meta = module.exports = {
    enable: enable,
    fastKey: fastKey,
    getWeakData: getWeakData,
    onFreeze: onFreeze
  };

  hiddenKeys$1[METADATA] = true;
  });
  internalMetadata.enable;
  internalMetadata.fastKey;
  internalMetadata.getWeakData;
  internalMetadata.onFreeze;

  var functionUncurryThisClause = function (fn) {
    // Nashorn bug:
    //   https://github.com/zloirock/core-js/issues/1128
    //   https://github.com/zloirock/core-js/issues/1130
    if (classofRaw(fn) === 'Function') return functionUncurryThis(fn);
  };

  var bind$2 = functionUncurryThisClause(functionUncurryThisClause.bind);

  // optional / simple context binding
  var functionBindContext = function (fn, that) {
    aCallable(fn);
    return that === undefined ? fn : functionBindNative ? bind$2(fn, that) : function (/* ...args */) {
      return fn.apply(that, arguments);
    };
  };

  var ITERATOR$6 = wellKnownSymbol('iterator');
  var ArrayPrototype = Array.prototype;

  // check on default Array iterator
  var isArrayIteratorMethod = function (it) {
    return it !== undefined && (iterators.Array === it || ArrayPrototype[ITERATOR$6] === it);
  };

  var ITERATOR$5 = wellKnownSymbol('iterator');

  var getIteratorMethod = function (it) {
    if (!isNullOrUndefined(it)) return getMethod(it, ITERATOR$5)
      || getMethod(it, '@@iterator')
      || iterators[classof(it)];
  };

  var $TypeError$c = TypeError;

  var getIterator = function (argument, usingIterator) {
    var iteratorMethod = arguments.length < 2 ? getIteratorMethod(argument) : usingIterator;
    if (aCallable(iteratorMethod)) return anObject(functionCall(iteratorMethod, argument));
    throw $TypeError$c(tryToString(argument) + ' is not iterable');
  };

  var iteratorClose = function (iterator, kind, value) {
    var innerResult, innerError;
    anObject(iterator);
    try {
      innerResult = getMethod(iterator, 'return');
      if (!innerResult) {
        if (kind === 'throw') throw value;
        return value;
      }
      innerResult = functionCall(innerResult, iterator);
    } catch (error) {
      innerError = true;
      innerResult = error;
    }
    if (kind === 'throw') throw value;
    if (innerError) throw innerResult;
    anObject(innerResult);
    return value;
  };

  var $TypeError$b = TypeError;

  var Result = function (stopped, result) {
    this.stopped = stopped;
    this.result = result;
  };

  var ResultPrototype = Result.prototype;

  var iterate = function (iterable, unboundFunction, options) {
    var that = options && options.that;
    var AS_ENTRIES = !!(options && options.AS_ENTRIES);
    var IS_RECORD = !!(options && options.IS_RECORD);
    var IS_ITERATOR = !!(options && options.IS_ITERATOR);
    var INTERRUPTED = !!(options && options.INTERRUPTED);
    var fn = functionBindContext(unboundFunction, that);
    var iterator, iterFn, index, length, result, next, step;

    var stop = function (condition) {
      if (iterator) iteratorClose(iterator, 'normal', condition);
      return new Result(true, condition);
    };

    var callFn = function (value) {
      if (AS_ENTRIES) {
        anObject(value);
        return INTERRUPTED ? fn(value[0], value[1], stop) : fn(value[0], value[1]);
      } return INTERRUPTED ? fn(value, stop) : fn(value);
    };

    if (IS_RECORD) {
      iterator = iterable.iterator;
    } else if (IS_ITERATOR) {
      iterator = iterable;
    } else {
      iterFn = getIteratorMethod(iterable);
      if (!iterFn) throw $TypeError$b(tryToString(iterable) + ' is not iterable');
      // optimisation for array iterators
      if (isArrayIteratorMethod(iterFn)) {
        for (index = 0, length = lengthOfArrayLike(iterable); length > index; index++) {
          result = callFn(iterable[index]);
          if (result && objectIsPrototypeOf(ResultPrototype, result)) return result;
        } return new Result(false);
      }
      iterator = getIterator(iterable, iterFn);
    }

    next = IS_RECORD ? iterable.next : iterator.next;
    while (!(step = functionCall(next, iterator)).done) {
      try {
        result = callFn(step.value);
      } catch (error) {
        iteratorClose(iterator, 'throw', error);
      }
      if (typeof result == 'object' && result && objectIsPrototypeOf(ResultPrototype, result)) return result;
    } return new Result(false);
  };

  var $TypeError$a = TypeError;

  var anInstance = function (it, Prototype) {
    if (objectIsPrototypeOf(Prototype, it)) return it;
    throw $TypeError$a('Incorrect invocation');
  };

  var ITERATOR$4 = wellKnownSymbol('iterator');
  var SAFE_CLOSING = false;

  try {
    var called = 0;
    var iteratorWithReturn = {
      next: function () {
        return { done: !!called++ };
      },
      'return': function () {
        SAFE_CLOSING = true;
      }
    };
    iteratorWithReturn[ITERATOR$4] = function () {
      return this;
    };
    // eslint-disable-next-line es/no-array-from, no-throw-literal -- required for testing
    Array.from(iteratorWithReturn, function () { throw 2; });
  } catch (error) { /* empty */ }

  var checkCorrectnessOfIteration = function (exec, SKIP_CLOSING) {
    if (!SKIP_CLOSING && !SAFE_CLOSING) return false;
    var ITERATION_SUPPORT = false;
    try {
      var object = {};
      object[ITERATOR$4] = function () {
        return {
          next: function () {
            return { done: ITERATION_SUPPORT = true };
          }
        };
      };
      exec(object);
    } catch (error) { /* empty */ }
    return ITERATION_SUPPORT;
  };

  // makes subclassing work correct for wrapped built-ins
  var inheritIfRequired = function ($this, dummy, Wrapper) {
    var NewTarget, NewTargetPrototype;
    if (
      // it can work only with native `setPrototypeOf`
      objectSetPrototypeOf &&
      // we haven't completely correct pre-ES6 way for getting `new.target`, so use this
      isCallable(NewTarget = dummy.constructor) &&
      NewTarget !== Wrapper &&
      isObject(NewTargetPrototype = NewTarget.prototype) &&
      NewTargetPrototype !== Wrapper.prototype
    ) objectSetPrototypeOf($this, NewTargetPrototype);
    return $this;
  };

  var collection = function (CONSTRUCTOR_NAME, wrapper, common) {
    var IS_MAP = CONSTRUCTOR_NAME.indexOf('Map') !== -1;
    var IS_WEAK = CONSTRUCTOR_NAME.indexOf('Weak') !== -1;
    var ADDER = IS_MAP ? 'set' : 'add';
    var NativeConstructor = global_1[CONSTRUCTOR_NAME];
    var NativePrototype = NativeConstructor && NativeConstructor.prototype;
    var Constructor = NativeConstructor;
    var exported = {};

    var fixMethod = function (KEY) {
      var uncurriedNativeMethod = functionUncurryThis(NativePrototype[KEY]);
      defineBuiltIn(NativePrototype, KEY,
        KEY == 'add' ? function add(value) {
          uncurriedNativeMethod(this, value === 0 ? 0 : value);
          return this;
        } : KEY == 'delete' ? function (key) {
          return IS_WEAK && !isObject(key) ? false : uncurriedNativeMethod(this, key === 0 ? 0 : key);
        } : KEY == 'get' ? function get(key) {
          return IS_WEAK && !isObject(key) ? undefined : uncurriedNativeMethod(this, key === 0 ? 0 : key);
        } : KEY == 'has' ? function has(key) {
          return IS_WEAK && !isObject(key) ? false : uncurriedNativeMethod(this, key === 0 ? 0 : key);
        } : function set(key, value) {
          uncurriedNativeMethod(this, key === 0 ? 0 : key, value);
          return this;
        }
      );
    };

    var REPLACE = isForced_1(
      CONSTRUCTOR_NAME,
      !isCallable(NativeConstructor) || !(IS_WEAK || NativePrototype.forEach && !fails(function () {
        new NativeConstructor().entries().next();
      }))
    );

    if (REPLACE) {
      // create collection constructor
      Constructor = common.getConstructor(wrapper, CONSTRUCTOR_NAME, IS_MAP, ADDER);
      internalMetadata.enable();
    } else if (isForced_1(CONSTRUCTOR_NAME, true)) {
      var instance = new Constructor();
      // early implementations not supports chaining
      var HASNT_CHAINING = instance[ADDER](IS_WEAK ? {} : -0, 1) != instance;
      // V8 ~ Chromium 40- weak-collections throws on primitives, but should return false
      var THROWS_ON_PRIMITIVES = fails(function () { instance.has(1); });
      // most early implementations doesn't supports iterables, most modern - not close it correctly
      // eslint-disable-next-line no-new -- required for testing
      var ACCEPT_ITERABLES = checkCorrectnessOfIteration(function (iterable) { new NativeConstructor(iterable); });
      // for early implementations -0 and +0 not the same
      var BUGGY_ZERO = !IS_WEAK && fails(function () {
        // V8 ~ Chromium 42- fails only with 5+ elements
        var $instance = new NativeConstructor();
        var index = 5;
        while (index--) $instance[ADDER](index, index);
        return !$instance.has(-0);
      });

      if (!ACCEPT_ITERABLES) {
        Constructor = wrapper(function (dummy, iterable) {
          anInstance(dummy, NativePrototype);
          var that = inheritIfRequired(new NativeConstructor(), dummy, Constructor);
          if (!isNullOrUndefined(iterable)) iterate(iterable, that[ADDER], { that: that, AS_ENTRIES: IS_MAP });
          return that;
        });
        Constructor.prototype = NativePrototype;
        NativePrototype.constructor = Constructor;
      }

      if (THROWS_ON_PRIMITIVES || BUGGY_ZERO) {
        fixMethod('delete');
        fixMethod('has');
        IS_MAP && fixMethod('get');
      }

      if (BUGGY_ZERO || HASNT_CHAINING) fixMethod(ADDER);

      // weak collections should not contains .clear method
      if (IS_WEAK && NativePrototype.clear) delete NativePrototype.clear;
    }

    exported[CONSTRUCTOR_NAME] = Constructor;
    _export({ global: true, constructor: true, forced: Constructor != NativeConstructor }, exported);

    setToStringTag(Constructor, CONSTRUCTOR_NAME);

    if (!IS_WEAK) common.setStrong(Constructor, CONSTRUCTOR_NAME, IS_MAP);

    return Constructor;
  };

  var defineBuiltIns = function (target, src, options) {
    for (var key in src) defineBuiltIn(target, key, src[key], options);
    return target;
  };

  var SPECIES$4 = wellKnownSymbol('species');

  var setSpecies = function (CONSTRUCTOR_NAME) {
    var Constructor = getBuiltIn(CONSTRUCTOR_NAME);

    if (descriptors && Constructor && !Constructor[SPECIES$4]) {
      defineBuiltInAccessor(Constructor, SPECIES$4, {
        configurable: true,
        get: function () { return this; }
      });
    }
  };

  var fastKey = internalMetadata.fastKey;


  var setInternalState$7 = internalState.set;
  var internalStateGetterFor$1 = internalState.getterFor;

  var collectionStrong = {
    getConstructor: function (wrapper, CONSTRUCTOR_NAME, IS_MAP, ADDER) {
      var Constructor = wrapper(function (that, iterable) {
        anInstance(that, Prototype);
        setInternalState$7(that, {
          type: CONSTRUCTOR_NAME,
          index: objectCreate(null),
          first: undefined,
          last: undefined,
          size: 0
        });
        if (!descriptors) that.size = 0;
        if (!isNullOrUndefined(iterable)) iterate(iterable, that[ADDER], { that: that, AS_ENTRIES: IS_MAP });
      });

      var Prototype = Constructor.prototype;

      var getInternalState = internalStateGetterFor$1(CONSTRUCTOR_NAME);

      var define = function (that, key, value) {
        var state = getInternalState(that);
        var entry = getEntry(that, key);
        var previous, index;
        // change existing entry
        if (entry) {
          entry.value = value;
        // create new entry
        } else {
          state.last = entry = {
            index: index = fastKey(key, true),
            key: key,
            value: value,
            previous: previous = state.last,
            next: undefined,
            removed: false
          };
          if (!state.first) state.first = entry;
          if (previous) previous.next = entry;
          if (descriptors) state.size++;
          else that.size++;
          // add to index
          if (index !== 'F') state.index[index] = entry;
        } return that;
      };

      var getEntry = function (that, key) {
        var state = getInternalState(that);
        // fast case
        var index = fastKey(key);
        var entry;
        if (index !== 'F') return state.index[index];
        // frozen object case
        for (entry = state.first; entry; entry = entry.next) {
          if (entry.key == key) return entry;
        }
      };

      defineBuiltIns(Prototype, {
        // `{ Map, Set }.prototype.clear()` methods
        // https://tc39.es/ecma262/#sec-map.prototype.clear
        // https://tc39.es/ecma262/#sec-set.prototype.clear
        clear: function clear() {
          var that = this;
          var state = getInternalState(that);
          var data = state.index;
          var entry = state.first;
          while (entry) {
            entry.removed = true;
            if (entry.previous) entry.previous = entry.previous.next = undefined;
            delete data[entry.index];
            entry = entry.next;
          }
          state.first = state.last = undefined;
          if (descriptors) state.size = 0;
          else that.size = 0;
        },
        // `{ Map, Set }.prototype.delete(key)` methods
        // https://tc39.es/ecma262/#sec-map.prototype.delete
        // https://tc39.es/ecma262/#sec-set.prototype.delete
        'delete': function (key) {
          var that = this;
          var state = getInternalState(that);
          var entry = getEntry(that, key);
          if (entry) {
            var next = entry.next;
            var prev = entry.previous;
            delete state.index[entry.index];
            entry.removed = true;
            if (prev) prev.next = next;
            if (next) next.previous = prev;
            if (state.first == entry) state.first = next;
            if (state.last == entry) state.last = prev;
            if (descriptors) state.size--;
            else that.size--;
          } return !!entry;
        },
        // `{ Map, Set }.prototype.forEach(callbackfn, thisArg = undefined)` methods
        // https://tc39.es/ecma262/#sec-map.prototype.foreach
        // https://tc39.es/ecma262/#sec-set.prototype.foreach
        forEach: function forEach(callbackfn /* , that = undefined */) {
          var state = getInternalState(this);
          var boundFunction = functionBindContext(callbackfn, arguments.length > 1 ? arguments[1] : undefined);
          var entry;
          while (entry = entry ? entry.next : state.first) {
            boundFunction(entry.value, entry.key, this);
            // revert to the last existing entry
            while (entry && entry.removed) entry = entry.previous;
          }
        },
        // `{ Map, Set}.prototype.has(key)` methods
        // https://tc39.es/ecma262/#sec-map.prototype.has
        // https://tc39.es/ecma262/#sec-set.prototype.has
        has: function has(key) {
          return !!getEntry(this, key);
        }
      });

      defineBuiltIns(Prototype, IS_MAP ? {
        // `Map.prototype.get(key)` method
        // https://tc39.es/ecma262/#sec-map.prototype.get
        get: function get(key) {
          var entry = getEntry(this, key);
          return entry && entry.value;
        },
        // `Map.prototype.set(key, value)` method
        // https://tc39.es/ecma262/#sec-map.prototype.set
        set: function set(key, value) {
          return define(this, key === 0 ? 0 : key, value);
        }
      } : {
        // `Set.prototype.add(value)` method
        // https://tc39.es/ecma262/#sec-set.prototype.add
        add: function add(value) {
          return define(this, value = value === 0 ? 0 : value, value);
        }
      });
      if (descriptors) defineBuiltInAccessor(Prototype, 'size', {
        configurable: true,
        get: function () {
          return getInternalState(this).size;
        }
      });
      return Constructor;
    },
    setStrong: function (Constructor, CONSTRUCTOR_NAME, IS_MAP) {
      var ITERATOR_NAME = CONSTRUCTOR_NAME + ' Iterator';
      var getInternalCollectionState = internalStateGetterFor$1(CONSTRUCTOR_NAME);
      var getInternalIteratorState = internalStateGetterFor$1(ITERATOR_NAME);
      // `{ Map, Set }.prototype.{ keys, values, entries, @@iterator }()` methods
      // https://tc39.es/ecma262/#sec-map.prototype.entries
      // https://tc39.es/ecma262/#sec-map.prototype.keys
      // https://tc39.es/ecma262/#sec-map.prototype.values
      // https://tc39.es/ecma262/#sec-map.prototype-@@iterator
      // https://tc39.es/ecma262/#sec-set.prototype.entries
      // https://tc39.es/ecma262/#sec-set.prototype.keys
      // https://tc39.es/ecma262/#sec-set.prototype.values
      // https://tc39.es/ecma262/#sec-set.prototype-@@iterator
      iteratorDefine(Constructor, CONSTRUCTOR_NAME, function (iterated, kind) {
        setInternalState$7(this, {
          type: ITERATOR_NAME,
          target: iterated,
          state: getInternalCollectionState(iterated),
          kind: kind,
          last: undefined
        });
      }, function () {
        var state = getInternalIteratorState(this);
        var kind = state.kind;
        var entry = state.last;
        // revert to the last existing entry
        while (entry && entry.removed) entry = entry.previous;
        // get next entry
        if (!state.target || !(state.last = entry = entry ? entry.next : state.state.first)) {
          // or finish the iteration
          state.target = undefined;
          return createIterResultObject(undefined, true);
        }
        // return step by kind
        if (kind == 'keys') return createIterResultObject(entry.key, false);
        if (kind == 'values') return createIterResultObject(entry.value, false);
        return createIterResultObject([entry.key, entry.value], false);
      }, IS_MAP ? 'entries' : 'values', !IS_MAP, true);

      // `{ Map, Set }.prototype[@@species]` accessors
      // https://tc39.es/ecma262/#sec-get-map-@@species
      // https://tc39.es/ecma262/#sec-get-set-@@species
      setSpecies(CONSTRUCTOR_NAME);
    }
  };
  collectionStrong.getConstructor;
  collectionStrong.setStrong;

  // `Map` constructor
  // https://tc39.es/ecma262/#sec-map-objects
  collection('Map', function (init) {
    return function Map() { return init(this, arguments.length ? arguments[0] : undefined); };
  }, collectionStrong);

  // `Object.prototype.toString` method implementation
  // https://tc39.es/ecma262/#sec-object.prototype.tostring
  var objectToString = toStringTagSupport ? {}.toString : function toString() {
    return '[object ' + classof(this) + ']';
  };

  // `Object.prototype.toString` method
  // https://tc39.es/ecma262/#sec-object.prototype.tostring
  if (!toStringTagSupport) {
    defineBuiltIn(Object.prototype, 'toString', objectToString, { unsafe: true });
  }

  var $String$2 = String;

  var toString_1 = function (argument) {
    if (classof(argument) === 'Symbol') throw TypeError('Cannot convert a Symbol value to a string');
    return $String$2(argument);
  };

  var charAt$7 = functionUncurryThis(''.charAt);
  var charCodeAt$3 = functionUncurryThis(''.charCodeAt);
  var stringSlice$9 = functionUncurryThis(''.slice);

  var createMethod$3 = function (CONVERT_TO_STRING) {
    return function ($this, pos) {
      var S = toString_1(requireObjectCoercible($this));
      var position = toIntegerOrInfinity(pos);
      var size = S.length;
      var first, second;
      if (position < 0 || position >= size) return CONVERT_TO_STRING ? '' : undefined;
      first = charCodeAt$3(S, position);
      return first < 0xD800 || first > 0xDBFF || position + 1 === size
        || (second = charCodeAt$3(S, position + 1)) < 0xDC00 || second > 0xDFFF
          ? CONVERT_TO_STRING
            ? charAt$7(S, position)
            : first
          : CONVERT_TO_STRING
            ? stringSlice$9(S, position, position + 2)
            : (first - 0xD800 << 10) + (second - 0xDC00) + 0x10000;
    };
  };

  var stringMultibyte = {
    // `String.prototype.codePointAt` method
    // https://tc39.es/ecma262/#sec-string.prototype.codepointat
    codeAt: createMethod$3(false),
    // `String.prototype.at` method
    // https://github.com/mathiasbynens/String.prototype.at
    charAt: createMethod$3(true)
  };

  var charAt$6 = stringMultibyte.charAt;





  var STRING_ITERATOR = 'String Iterator';
  var setInternalState$6 = internalState.set;
  var getInternalState$3 = internalState.getterFor(STRING_ITERATOR);

  // `String.prototype[@@iterator]` method
  // https://tc39.es/ecma262/#sec-string.prototype-@@iterator
  iteratorDefine(String, 'String', function (iterated) {
    setInternalState$6(this, {
      type: STRING_ITERATOR,
      string: toString_1(iterated),
      index: 0
    });
  // `%StringIteratorPrototype%.next` method
  // https://tc39.es/ecma262/#sec-%stringiteratorprototype%.next
  }, function next() {
    var state = getInternalState$3(this);
    var string = state.string;
    var index = state.index;
    var point;
    if (index >= string.length) return createIterResultObject(undefined, true);
    point = charAt$6(string, index);
    state.index += point.length;
    return createIterResultObject(point, false);
  });

  // iterable DOM collections
  // flag - `iterable` interface - 'entries', 'keys', 'values', 'forEach' methods
  var domIterables = {
    CSSRuleList: 0,
    CSSStyleDeclaration: 0,
    CSSValueList: 0,
    ClientRectList: 0,
    DOMRectList: 0,
    DOMStringList: 0,
    DOMTokenList: 1,
    DataTransferItemList: 0,
    FileList: 0,
    HTMLAllCollection: 0,
    HTMLCollection: 0,
    HTMLFormElement: 0,
    HTMLSelectElement: 0,
    MediaList: 0,
    MimeTypeArray: 0,
    NamedNodeMap: 0,
    NodeList: 1,
    PaintRequestList: 0,
    Plugin: 0,
    PluginArray: 0,
    SVGLengthList: 0,
    SVGNumberList: 0,
    SVGPathSegList: 0,
    SVGPointList: 0,
    SVGStringList: 0,
    SVGTransformList: 0,
    SourceBufferList: 0,
    StyleSheetList: 0,
    TextTrackCueList: 0,
    TextTrackList: 0,
    TouchList: 0
  };

  // in old WebKit versions, `element.classList` is not an instance of global `DOMTokenList`


  var classList = documentCreateElement('span').classList;
  var DOMTokenListPrototype = classList && classList.constructor && classList.constructor.prototype;

  var domTokenListPrototype = DOMTokenListPrototype === Object.prototype ? undefined : DOMTokenListPrototype;

  var ITERATOR$3 = wellKnownSymbol('iterator');
  var TO_STRING_TAG$1 = wellKnownSymbol('toStringTag');
  var ArrayValues = es_array_iterator.values;

  var handlePrototype$1 = function (CollectionPrototype, COLLECTION_NAME) {
    if (CollectionPrototype) {
      // some Chrome versions have non-configurable methods on DOMTokenList
      if (CollectionPrototype[ITERATOR$3] !== ArrayValues) try {
        createNonEnumerableProperty(CollectionPrototype, ITERATOR$3, ArrayValues);
      } catch (error) {
        CollectionPrototype[ITERATOR$3] = ArrayValues;
      }
      if (!CollectionPrototype[TO_STRING_TAG$1]) {
        createNonEnumerableProperty(CollectionPrototype, TO_STRING_TAG$1, COLLECTION_NAME);
      }
      if (domIterables[COLLECTION_NAME]) for (var METHOD_NAME in es_array_iterator) {
        // some Chrome versions have non-configurable methods on DOMTokenList
        if (CollectionPrototype[METHOD_NAME] !== es_array_iterator[METHOD_NAME]) try {
          createNonEnumerableProperty(CollectionPrototype, METHOD_NAME, es_array_iterator[METHOD_NAME]);
        } catch (error) {
          CollectionPrototype[METHOD_NAME] = es_array_iterator[METHOD_NAME];
        }
      }
    }
  };

  for (var COLLECTION_NAME$1 in domIterables) {
    handlePrototype$1(global_1[COLLECTION_NAME$1] && global_1[COLLECTION_NAME$1].prototype, COLLECTION_NAME$1);
  }

  handlePrototype$1(domTokenListPrototype, 'DOMTokenList');

  var SPECIES$3 = wellKnownSymbol('species');
  var $Array$2 = Array;

  // a part of `ArraySpeciesCreate` abstract operation
  // https://tc39.es/ecma262/#sec-arrayspeciescreate
  var arraySpeciesConstructor = function (originalArray) {
    var C;
    if (isArray$1(originalArray)) {
      C = originalArray.constructor;
      // cross-realm fallback
      if (isConstructor(C) && (C === $Array$2 || isArray$1(C.prototype))) C = undefined;
      else if (isObject(C)) {
        C = C[SPECIES$3];
        if (C === null) C = undefined;
      }
    } return C === undefined ? $Array$2 : C;
  };

  // `ArraySpeciesCreate` abstract operation
  // https://tc39.es/ecma262/#sec-arrayspeciescreate
  var arraySpeciesCreate = function (originalArray, length) {
    return new (arraySpeciesConstructor(originalArray))(length === 0 ? 0 : length);
  };

  var push$8 = functionUncurryThis([].push);

  // `Array.prototype.{ forEach, map, filter, some, every, find, findIndex, filterReject }` methods implementation
  var createMethod$2 = function (TYPE) {
    var IS_MAP = TYPE == 1;
    var IS_FILTER = TYPE == 2;
    var IS_SOME = TYPE == 3;
    var IS_EVERY = TYPE == 4;
    var IS_FIND_INDEX = TYPE == 6;
    var IS_FILTER_REJECT = TYPE == 7;
    var NO_HOLES = TYPE == 5 || IS_FIND_INDEX;
    return function ($this, callbackfn, that, specificCreate) {
      var O = toObject($this);
      var self = indexedObject(O);
      var boundFunction = functionBindContext(callbackfn, that);
      var length = lengthOfArrayLike(self);
      var index = 0;
      var create = specificCreate || arraySpeciesCreate;
      var target = IS_MAP ? create($this, length) : IS_FILTER || IS_FILTER_REJECT ? create($this, 0) : undefined;
      var value, result;
      for (;length > index; index++) if (NO_HOLES || index in self) {
        value = self[index];
        result = boundFunction(value, index, O);
        if (TYPE) {
          if (IS_MAP) target[index] = result; // map
          else if (result) switch (TYPE) {
            case 3: return true;              // some
            case 5: return value;             // find
            case 6: return index;             // findIndex
            case 2: push$8(target, value);      // filter
          } else switch (TYPE) {
            case 4: return false;             // every
            case 7: push$8(target, value);      // filterReject
          }
        }
      }
      return IS_FIND_INDEX ? -1 : IS_SOME || IS_EVERY ? IS_EVERY : target;
    };
  };

  var arrayIteration = {
    // `Array.prototype.forEach` method
    // https://tc39.es/ecma262/#sec-array.prototype.foreach
    forEach: createMethod$2(0),
    // `Array.prototype.map` method
    // https://tc39.es/ecma262/#sec-array.prototype.map
    map: createMethod$2(1),
    // `Array.prototype.filter` method
    // https://tc39.es/ecma262/#sec-array.prototype.filter
    filter: createMethod$2(2),
    // `Array.prototype.some` method
    // https://tc39.es/ecma262/#sec-array.prototype.some
    some: createMethod$2(3),
    // `Array.prototype.every` method
    // https://tc39.es/ecma262/#sec-array.prototype.every
    every: createMethod$2(4),
    // `Array.prototype.find` method
    // https://tc39.es/ecma262/#sec-array.prototype.find
    find: createMethod$2(5),
    // `Array.prototype.findIndex` method
    // https://tc39.es/ecma262/#sec-array.prototype.findIndex
    findIndex: createMethod$2(6),
    // `Array.prototype.filterReject` method
    // https://github.com/tc39/proposal-array-filtering
    filterReject: createMethod$2(7)
  };

  var arrayMethodIsStrict = function (METHOD_NAME, argument) {
    var method = [][METHOD_NAME];
    return !!method && fails(function () {
      // eslint-disable-next-line no-useless-call -- required for testing
      method.call(null, argument || function () { return 1; }, 1);
    });
  };

  var $forEach$2 = arrayIteration.forEach;


  var STRICT_METHOD$2 = arrayMethodIsStrict('forEach');

  // `Array.prototype.forEach` method implementation
  // https://tc39.es/ecma262/#sec-array.prototype.foreach
  var arrayForEach = !STRICT_METHOD$2 ? function forEach(callbackfn /* , thisArg */) {
    return $forEach$2(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
  // eslint-disable-next-line es/no-array-prototype-foreach -- safe
  } : [].forEach;

  var handlePrototype = function (CollectionPrototype) {
    // some Chrome versions have non-configurable methods on DOMTokenList
    if (CollectionPrototype && CollectionPrototype.forEach !== arrayForEach) try {
      createNonEnumerableProperty(CollectionPrototype, 'forEach', arrayForEach);
    } catch (error) {
      CollectionPrototype.forEach = arrayForEach;
    }
  };

  for (var COLLECTION_NAME in domIterables) {
    if (domIterables[COLLECTION_NAME]) {
      handlePrototype(global_1[COLLECTION_NAME] && global_1[COLLECTION_NAME].prototype);
    }
  }

  handlePrototype(domTokenListPrototype);

  var FAILS_ON_PRIMITIVES$1 = fails(function () { objectKeys(1); });

  // `Object.keys` method
  // https://tc39.es/ecma262/#sec-object.keys
  _export({ target: 'Object', stat: true, forced: FAILS_ON_PRIMITIVES$1 }, {
    keys: function keys(it) {
      return objectKeys(toObject(it));
    }
  });

  var FunctionPrototype = Function.prototype;
  var apply = FunctionPrototype.apply;
  var call = FunctionPrototype.call;

  // eslint-disable-next-line es/no-reflect -- safe
  var functionApply = typeof Reflect == 'object' && Reflect.apply || (functionBindNative ? call.bind(apply) : function () {
    return call.apply(apply, arguments);
  });

  var $Function = Function;
  var concat$2 = functionUncurryThis([].concat);
  var join$4 = functionUncurryThis([].join);
  var factories = {};

  var construct = function (C, argsLength, args) {
    if (!hasOwnProperty_1(factories, argsLength)) {
      for (var list = [], i = 0; i < argsLength; i++) list[i] = 'a[' + i + ']';
      factories[argsLength] = $Function('C,a', 'return new C(' + join$4(list, ',') + ')');
    } return factories[argsLength](C, args);
  };

  // `Function.prototype.bind` method implementation
  // https://tc39.es/ecma262/#sec-function.prototype.bind
  // eslint-disable-next-line es/no-function-prototype-bind -- detection
  var functionBind = functionBindNative ? $Function.bind : function bind(that /* , ...args */) {
    var F = aCallable(this);
    var Prototype = F.prototype;
    var partArgs = arraySlice(arguments, 1);
    var boundFunction = function bound(/* args... */) {
      var args = concat$2(partArgs, arraySlice(arguments));
      return this instanceof boundFunction ? construct(F, args.length, args) : F.apply(that, args);
    };
    if (isObject(Prototype)) boundFunction.prototype = Prototype;
    return boundFunction;
  };

  var $TypeError$9 = TypeError;

  // `Assert: IsConstructor(argument) is true`
  var aConstructor = function (argument) {
    if (isConstructor(argument)) return argument;
    throw $TypeError$9(tryToString(argument) + ' is not a constructor');
  };

  var nativeConstruct = getBuiltIn('Reflect', 'construct');
  var ObjectPrototype$3 = Object.prototype;
  var push$7 = [].push;

  // `Reflect.construct` method
  // https://tc39.es/ecma262/#sec-reflect.construct
  // MS Edge supports only 2 arguments and argumentsList argument is optional
  // FF Nightly sets third argument as `new.target`, but does not create `this` from it
  var NEW_TARGET_BUG = fails(function () {
    function F() { /* empty */ }
    return !(nativeConstruct(function () { /* empty */ }, [], F) instanceof F);
  });

  var ARGS_BUG = !fails(function () {
    nativeConstruct(function () { /* empty */ });
  });

  var FORCED$a = NEW_TARGET_BUG || ARGS_BUG;

  _export({ target: 'Reflect', stat: true, forced: FORCED$a, sham: FORCED$a }, {
    construct: function construct(Target, args /* , newTarget */) {
      aConstructor(Target);
      anObject(args);
      var newTarget = arguments.length < 3 ? Target : aConstructor(arguments[2]);
      if (ARGS_BUG && !NEW_TARGET_BUG) return nativeConstruct(Target, args, newTarget);
      if (Target == newTarget) {
        // w/o altered newTarget, optimization for 0-4 arguments
        switch (args.length) {
          case 0: return new Target();
          case 1: return new Target(args[0]);
          case 2: return new Target(args[0], args[1]);
          case 3: return new Target(args[0], args[1], args[2]);
          case 4: return new Target(args[0], args[1], args[2], args[3]);
        }
        // w/o altered newTarget, lot of arguments case
        var $args = [null];
        functionApply(push$7, $args, args);
        return new (functionApply(functionBind, Target, $args))();
      }
      // with altered newTarget, not support built-in constructors
      var proto = newTarget.prototype;
      var instance = objectCreate(isObject(proto) ? proto : ObjectPrototype$3);
      var result = functionApply(Target, instance, args);
      return isObject(result) ? result : instance;
    }
  });

  // `RegExp.prototype.flags` getter implementation
  // https://tc39.es/ecma262/#sec-get-regexp.prototype.flags
  var regexpFlags = function () {
    var that = anObject(this);
    var result = '';
    if (that.hasIndices) result += 'd';
    if (that.global) result += 'g';
    if (that.ignoreCase) result += 'i';
    if (that.multiline) result += 'm';
    if (that.dotAll) result += 's';
    if (that.unicode) result += 'u';
    if (that.unicodeSets) result += 'v';
    if (that.sticky) result += 'y';
    return result;
  };

  var RegExpPrototype$2 = RegExp.prototype;

  var regexpGetFlags = function (R) {
    var flags = R.flags;
    return flags === undefined && !('flags' in RegExpPrototype$2) && !hasOwnProperty_1(R, 'flags') && objectIsPrototypeOf(RegExpPrototype$2, R)
      ? functionCall(regexpFlags, R) : flags;
  };

  var PROPER_FUNCTION_NAME$2 = functionName.PROPER;






  var TO_STRING = 'toString';
  var RegExpPrototype$1 = RegExp.prototype;
  var nativeToString = RegExpPrototype$1[TO_STRING];

  var NOT_GENERIC = fails(function () { return nativeToString.call({ source: 'a', flags: 'b' }) != '/a/b'; });
  // FF44- RegExp#toString has a wrong name
  var INCORRECT_NAME = PROPER_FUNCTION_NAME$2 && nativeToString.name != TO_STRING;

  // `RegExp.prototype.toString` method
  // https://tc39.es/ecma262/#sec-regexp.prototype.tostring
  if (NOT_GENERIC || INCORRECT_NAME) {
    defineBuiltIn(RegExp.prototype, TO_STRING, function toString() {
      var R = anObject(this);
      var pattern = toString_1(R.source);
      var flags = toString_1(regexpGetFlags(R));
      return '/' + pattern + '/' + flags;
    }, { unsafe: true });
  }

  function _arrayWithoutHoles(arr) {
    if (Array.isArray(arr)) return _arrayLikeToArray$1(arr);
  }

  function _iterableToArray(iter) {
    if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter);
  }

  function _nonIterableSpread() {
    throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  function _toConsumableArray(arr) {
    return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray$1(arr) || _nonIterableSpread();
  }

  function _superPropBase(object, property) {
    while (!Object.prototype.hasOwnProperty.call(object, property)) {
      object = _getPrototypeOf(object);
      if (object === null) break;
    }
    return object;
  }

  function _get() {
    if (typeof Reflect !== "undefined" && Reflect.get) {
      _get = Reflect.get.bind();
    } else {
      _get = function _get(target, property, receiver) {
        var base = _superPropBase(target, property);
        if (!base) return;
        var desc = Object.getOwnPropertyDescriptor(base, property);
        if (desc.get) {
          return desc.get.call(arguments.length < 3 ? target : receiver);
        }
        return desc.value;
      };
    }
    return _get.apply(this, arguments);
  }

  // `Set` constructor
  // https://tc39.es/ecma262/#sec-set-objects
  collection('Set', function (init) {
    return function Set() { return init(this, arguments.length ? arguments[0] : undefined); };
  }, collectionStrong);

  var $TypeError$8 = TypeError;
  var MAX_SAFE_INTEGER = 0x1FFFFFFFFFFFFF; // 2 ** 53 - 1 == 9007199254740991

  var doesNotExceedSafeInteger = function (it) {
    if (it > MAX_SAFE_INTEGER) throw $TypeError$8('Maximum allowed index exceeded');
    return it;
  };

  var IS_CONCAT_SPREADABLE = wellKnownSymbol('isConcatSpreadable');

  // We can't use this feature detection in V8 since it causes
  // deoptimization and serious performance degradation
  // https://github.com/zloirock/core-js/issues/679
  var IS_CONCAT_SPREADABLE_SUPPORT = engineV8Version >= 51 || !fails(function () {
    var array = [];
    array[IS_CONCAT_SPREADABLE] = false;
    return array.concat()[0] !== array;
  });

  var isConcatSpreadable = function (O) {
    if (!isObject(O)) return false;
    var spreadable = O[IS_CONCAT_SPREADABLE];
    return spreadable !== undefined ? !!spreadable : isArray$1(O);
  };

  var FORCED$9 = !IS_CONCAT_SPREADABLE_SUPPORT || !arrayMethodHasSpeciesSupport('concat');

  // `Array.prototype.concat` method
  // https://tc39.es/ecma262/#sec-array.prototype.concat
  // with adding support of @@isConcatSpreadable and @@species
  _export({ target: 'Array', proto: true, arity: 1, forced: FORCED$9 }, {
    // eslint-disable-next-line no-unused-vars -- required for `.length`
    concat: function concat(arg) {
      var O = toObject(this);
      var A = arraySpeciesCreate(O, 0);
      var n = 0;
      var i, k, length, len, E;
      for (i = -1, length = arguments.length; i < length; i++) {
        E = i === -1 ? O : arguments[i];
        if (isConcatSpreadable(E)) {
          len = lengthOfArrayLike(E);
          doesNotExceedSafeInteger(n + len);
          for (k = 0; k < len; k++, n++) if (k in E) createProperty(A, n, E[k]);
        } else {
          doesNotExceedSafeInteger(n + 1);
          createProperty(A, n++, E);
        }
      }
      A.length = n;
      return A;
    }
  });

  // babel-minify and Closure Compiler transpiles RegExp('a', 'y') -> /a/y and it causes SyntaxError
  var $RegExp$2 = global_1.RegExp;

  var UNSUPPORTED_Y$1 = fails(function () {
    var re = $RegExp$2('a', 'y');
    re.lastIndex = 2;
    return re.exec('abcd') != null;
  });

  // UC Browser bug
  // https://github.com/zloirock/core-js/issues/1008
  var MISSED_STICKY = UNSUPPORTED_Y$1 || fails(function () {
    return !$RegExp$2('a', 'y').sticky;
  });

  var BROKEN_CARET = UNSUPPORTED_Y$1 || fails(function () {
    // https://bugzilla.mozilla.org/show_bug.cgi?id=773687
    var re = $RegExp$2('^r', 'gy');
    re.lastIndex = 2;
    return re.exec('str') != null;
  });

  var regexpStickyHelpers = {
    BROKEN_CARET: BROKEN_CARET,
    MISSED_STICKY: MISSED_STICKY,
    UNSUPPORTED_Y: UNSUPPORTED_Y$1
  };

  // babel-minify and Closure Compiler transpiles RegExp('.', 's') -> /./s and it causes SyntaxError
  var $RegExp$1 = global_1.RegExp;

  var regexpUnsupportedDotAll = fails(function () {
    var re = $RegExp$1('.', 's');
    return !(re.dotAll && re.exec('\n') && re.flags === 's');
  });

  // babel-minify and Closure Compiler transpiles RegExp('(?<a>b)', 'g') -> /(?<a>b)/g and it causes SyntaxError
  var $RegExp = global_1.RegExp;

  var regexpUnsupportedNcg = fails(function () {
    var re = $RegExp('(?<a>b)', 'g');
    return re.exec('b').groups.a !== 'b' ||
      'b'.replace(re, '$<a>c') !== 'bc';
  });

  /* eslint-disable regexp/no-empty-capturing-group, regexp/no-empty-group, regexp/no-lazy-ends -- testing */
  /* eslint-disable regexp/no-useless-quantifier -- testing */







  var getInternalState$2 = internalState.get;



  var nativeReplace = shared('native-string-replace', String.prototype.replace);
  var nativeExec = RegExp.prototype.exec;
  var patchedExec = nativeExec;
  var charAt$5 = functionUncurryThis(''.charAt);
  var indexOf = functionUncurryThis(''.indexOf);
  var replace$8 = functionUncurryThis(''.replace);
  var stringSlice$8 = functionUncurryThis(''.slice);

  var UPDATES_LAST_INDEX_WRONG = (function () {
    var re1 = /a/;
    var re2 = /b*/g;
    functionCall(nativeExec, re1, 'a');
    functionCall(nativeExec, re2, 'a');
    return re1.lastIndex !== 0 || re2.lastIndex !== 0;
  })();

  var UNSUPPORTED_Y = regexpStickyHelpers.BROKEN_CARET;

  // nonparticipating capturing group, copied from es5-shim's String#split patch.
  var NPCG_INCLUDED = /()??/.exec('')[1] !== undefined;

  var PATCH = UPDATES_LAST_INDEX_WRONG || NPCG_INCLUDED || UNSUPPORTED_Y || regexpUnsupportedDotAll || regexpUnsupportedNcg;

  if (PATCH) {
    patchedExec = function exec(string) {
      var re = this;
      var state = getInternalState$2(re);
      var str = toString_1(string);
      var raw = state.raw;
      var result, reCopy, lastIndex, match, i, object, group;

      if (raw) {
        raw.lastIndex = re.lastIndex;
        result = functionCall(patchedExec, raw, str);
        re.lastIndex = raw.lastIndex;
        return result;
      }

      var groups = state.groups;
      var sticky = UNSUPPORTED_Y && re.sticky;
      var flags = functionCall(regexpFlags, re);
      var source = re.source;
      var charsAdded = 0;
      var strCopy = str;

      if (sticky) {
        flags = replace$8(flags, 'y', '');
        if (indexOf(flags, 'g') === -1) {
          flags += 'g';
        }

        strCopy = stringSlice$8(str, re.lastIndex);
        // Support anchored sticky behavior.
        if (re.lastIndex > 0 && (!re.multiline || re.multiline && charAt$5(str, re.lastIndex - 1) !== '\n')) {
          source = '(?: ' + source + ')';
          strCopy = ' ' + strCopy;
          charsAdded++;
        }
        // ^(? + rx + ) is needed, in combination with some str slicing, to
        // simulate the 'y' flag.
        reCopy = new RegExp('^(?:' + source + ')', flags);
      }

      if (NPCG_INCLUDED) {
        reCopy = new RegExp('^' + source + '$(?!\\s)', flags);
      }
      if (UPDATES_LAST_INDEX_WRONG) lastIndex = re.lastIndex;

      match = functionCall(nativeExec, sticky ? reCopy : re, strCopy);

      if (sticky) {
        if (match) {
          match.input = stringSlice$8(match.input, charsAdded);
          match[0] = stringSlice$8(match[0], charsAdded);
          match.index = re.lastIndex;
          re.lastIndex += match[0].length;
        } else re.lastIndex = 0;
      } else if (UPDATES_LAST_INDEX_WRONG && match) {
        re.lastIndex = re.global ? match.index + match[0].length : lastIndex;
      }
      if (NPCG_INCLUDED && match && match.length > 1) {
        // Fix browsers whose `exec` methods don't consistently return `undefined`
        // for NPCG, like IE8. NOTE: This doesn't work for /(.?)?/
        functionCall(nativeReplace, match[0], reCopy, function () {
          for (i = 1; i < arguments.length - 2; i++) {
            if (arguments[i] === undefined) match[i] = undefined;
          }
        });
      }

      if (match && groups) {
        match.groups = object = objectCreate(null);
        for (i = 0; i < groups.length; i++) {
          group = groups[i];
          object[group[0]] = match[group[1]];
        }
      }

      return match;
    };
  }

  var regexpExec = patchedExec;

  // `RegExp.prototype.exec` method
  // https://tc39.es/ecma262/#sec-regexp.prototype.exec
  _export({ target: 'RegExp', proto: true, forced: /./.exec !== regexpExec }, {
    exec: regexpExec
  });

  // TODO: Remove from `core-js@4` since it's moved to entry points








  var SPECIES$2 = wellKnownSymbol('species');
  var RegExpPrototype = RegExp.prototype;

  var fixRegexpWellKnownSymbolLogic = function (KEY, exec, FORCED, SHAM) {
    var SYMBOL = wellKnownSymbol(KEY);

    var DELEGATES_TO_SYMBOL = !fails(function () {
      // String methods call symbol-named RegEp methods
      var O = {};
      O[SYMBOL] = function () { return 7; };
      return ''[KEY](O) != 7;
    });

    var DELEGATES_TO_EXEC = DELEGATES_TO_SYMBOL && !fails(function () {
      // Symbol-named RegExp methods call .exec
      var execCalled = false;
      var re = /a/;

      if (KEY === 'split') {
        // We can't use real regex here since it causes deoptimization
        // and serious performance degradation in V8
        // https://github.com/zloirock/core-js/issues/306
        re = {};
        // RegExp[@@split] doesn't call the regex's exec method, but first creates
        // a new one. We need to return the patched regex when creating the new one.
        re.constructor = {};
        re.constructor[SPECIES$2] = function () { return re; };
        re.flags = '';
        re[SYMBOL] = /./[SYMBOL];
      }

      re.exec = function () { execCalled = true; return null; };

      re[SYMBOL]('');
      return !execCalled;
    });

    if (
      !DELEGATES_TO_SYMBOL ||
      !DELEGATES_TO_EXEC ||
      FORCED
    ) {
      var uncurriedNativeRegExpMethod = functionUncurryThisClause(/./[SYMBOL]);
      var methods = exec(SYMBOL, ''[KEY], function (nativeMethod, regexp, str, arg2, forceStringMethod) {
        var uncurriedNativeMethod = functionUncurryThisClause(nativeMethod);
        var $exec = regexp.exec;
        if ($exec === regexpExec || $exec === RegExpPrototype.exec) {
          if (DELEGATES_TO_SYMBOL && !forceStringMethod) {
            // The native String method already delegates to @@method (this
            // polyfilled function), leasing to infinite recursion.
            // We avoid it by directly calling the native @@method method.
            return { done: true, value: uncurriedNativeRegExpMethod(regexp, str, arg2) };
          }
          return { done: true, value: uncurriedNativeMethod(str, regexp, arg2) };
        }
        return { done: false };
      });

      defineBuiltIn(String.prototype, KEY, methods[0]);
      defineBuiltIn(RegExpPrototype, SYMBOL, methods[1]);
    }

    if (SHAM) createNonEnumerableProperty(RegExpPrototype[SYMBOL], 'sham', true);
  };

  var charAt$4 = stringMultibyte.charAt;

  // `AdvanceStringIndex` abstract operation
  // https://tc39.es/ecma262/#sec-advancestringindex
  var advanceStringIndex = function (S, index, unicode) {
    return index + (unicode ? charAt$4(S, index).length : 1);
  };

  var $TypeError$7 = TypeError;

  // `RegExpExec` abstract operation
  // https://tc39.es/ecma262/#sec-regexpexec
  var regexpExecAbstract = function (R, S) {
    var exec = R.exec;
    if (isCallable(exec)) {
      var result = functionCall(exec, R, S);
      if (result !== null) anObject(result);
      return result;
    }
    if (classofRaw(R) === 'RegExp') return functionCall(regexpExec, R, S);
    throw $TypeError$7('RegExp#exec called on incompatible receiver');
  };

  // @@match logic
  fixRegexpWellKnownSymbolLogic('match', function (MATCH, nativeMatch, maybeCallNative) {
    return [
      // `String.prototype.match` method
      // https://tc39.es/ecma262/#sec-string.prototype.match
      function match(regexp) {
        var O = requireObjectCoercible(this);
        var matcher = isNullOrUndefined(regexp) ? undefined : getMethod(regexp, MATCH);
        return matcher ? functionCall(matcher, regexp, O) : new RegExp(regexp)[MATCH](toString_1(O));
      },
      // `RegExp.prototype[@@match]` method
      // https://tc39.es/ecma262/#sec-regexp.prototype-@@match
      function (string) {
        var rx = anObject(this);
        var S = toString_1(string);
        var res = maybeCallNative(nativeMatch, rx, S);

        if (res.done) return res.value;

        if (!rx.global) return regexpExecAbstract(rx, S);

        var fullUnicode = rx.unicode;
        rx.lastIndex = 0;
        var A = [];
        var n = 0;
        var result;
        while ((result = regexpExecAbstract(rx, S)) !== null) {
          var matchStr = toString_1(result[0]);
          A[n] = matchStr;
          if (matchStr === '') rx.lastIndex = advanceStringIndex(S, toLength(rx.lastIndex), fullUnicode);
          n++;
        }
        return n === 0 ? null : A;
      }
    ];
  });

  var engineIsNode = typeof process != 'undefined' && classofRaw(process) == 'process';

  var SPECIES$1 = wellKnownSymbol('species');

  // `SpeciesConstructor` abstract operation
  // https://tc39.es/ecma262/#sec-speciesconstructor
  var speciesConstructor = function (O, defaultConstructor) {
    var C = anObject(O).constructor;
    var S;
    return C === undefined || isNullOrUndefined(S = anObject(C)[SPECIES$1]) ? defaultConstructor : aConstructor(S);
  };

  var $TypeError$6 = TypeError;

  var validateArgumentsLength = function (passed, required) {
    if (passed < required) throw $TypeError$6('Not enough arguments');
    return passed;
  };

  // eslint-disable-next-line redos/no-vulnerable -- safe
  var engineIsIos = /(?:ipad|iphone|ipod).*applewebkit/i.test(engineUserAgent);

  var set$1 = global_1.setImmediate;
  var clear = global_1.clearImmediate;
  var process$3 = global_1.process;
  var Dispatch = global_1.Dispatch;
  var Function$1 = global_1.Function;
  var MessageChannel = global_1.MessageChannel;
  var String$1 = global_1.String;
  var counter = 0;
  var queue$2 = {};
  var ONREADYSTATECHANGE = 'onreadystatechange';
  var $location, defer, channel, port;

  fails(function () {
    // Deno throws a ReferenceError on `location` access without `--location` flag
    $location = global_1.location;
  });

  var run$1 = function (id) {
    if (hasOwnProperty_1(queue$2, id)) {
      var fn = queue$2[id];
      delete queue$2[id];
      fn();
    }
  };

  var runner = function (id) {
    return function () {
      run$1(id);
    };
  };

  var eventListener = function (event) {
    run$1(event.data);
  };

  var globalPostMessageDefer = function (id) {
    // old engines have not location.origin
    global_1.postMessage(String$1(id), $location.protocol + '//' + $location.host);
  };

  // Node.js 0.9+ & IE10+ has setImmediate, otherwise:
  if (!set$1 || !clear) {
    set$1 = function setImmediate(handler) {
      validateArgumentsLength(arguments.length, 1);
      var fn = isCallable(handler) ? handler : Function$1(handler);
      var args = arraySlice(arguments, 1);
      queue$2[++counter] = function () {
        functionApply(fn, undefined, args);
      };
      defer(counter);
      return counter;
    };
    clear = function clearImmediate(id) {
      delete queue$2[id];
    };
    // Node.js 0.8-
    if (engineIsNode) {
      defer = function (id) {
        process$3.nextTick(runner(id));
      };
    // Sphere (JS game engine) Dispatch API
    } else if (Dispatch && Dispatch.now) {
      defer = function (id) {
        Dispatch.now(runner(id));
      };
    // Browsers with MessageChannel, includes WebWorkers
    // except iOS - https://github.com/zloirock/core-js/issues/624
    } else if (MessageChannel && !engineIsIos) {
      channel = new MessageChannel();
      port = channel.port2;
      channel.port1.onmessage = eventListener;
      defer = functionBindContext(port.postMessage, port);
    // Browsers with postMessage, skip WebWorkers
    // IE8 has postMessage, but it's sync & typeof its postMessage is 'object'
    } else if (
      global_1.addEventListener &&
      isCallable(global_1.postMessage) &&
      !global_1.importScripts &&
      $location && $location.protocol !== 'file:' &&
      !fails(globalPostMessageDefer)
    ) {
      defer = globalPostMessageDefer;
      global_1.addEventListener('message', eventListener, false);
    // IE8-
    } else if (ONREADYSTATECHANGE in documentCreateElement('script')) {
      defer = function (id) {
        html.appendChild(documentCreateElement('script'))[ONREADYSTATECHANGE] = function () {
          html.removeChild(this);
          run$1(id);
        };
      };
    // Rest old browsers
    } else {
      defer = function (id) {
        setTimeout(runner(id), 0);
      };
    }
  }

  var task$1 = {
    set: set$1,
    clear: clear
  };

  var Queue = function () {
    this.head = null;
    this.tail = null;
  };

  Queue.prototype = {
    add: function (item) {
      var entry = { item: item, next: null };
      var tail = this.tail;
      if (tail) tail.next = entry;
      else this.head = entry;
      this.tail = entry;
    },
    get: function () {
      var entry = this.head;
      if (entry) {
        var next = this.head = entry.next;
        if (next === null) this.tail = null;
        return entry.item;
      }
    }
  };

  var queue$1 = Queue;

  var engineIsIosPebble = /ipad|iphone|ipod/i.test(engineUserAgent) && typeof Pebble != 'undefined';

  var engineIsWebosWebkit = /web0s(?!.*chrome)/i.test(engineUserAgent);

  var getOwnPropertyDescriptor$4 = objectGetOwnPropertyDescriptor.f;
  var macrotask = task$1.set;






  var MutationObserver = global_1.MutationObserver || global_1.WebKitMutationObserver;
  var document$2 = global_1.document;
  var process$2 = global_1.process;
  var Promise$1 = global_1.Promise;
  // Node.js 11 shows ExperimentalWarning on getting `queueMicrotask`
  var queueMicrotaskDescriptor = getOwnPropertyDescriptor$4(global_1, 'queueMicrotask');
  var microtask = queueMicrotaskDescriptor && queueMicrotaskDescriptor.value;
  var notify$1, toggle, node, promise$1, then;

  // modern engines have queueMicrotask method
  if (!microtask) {
    var queue = new queue$1();

    var flush$1 = function () {
      var parent, fn;
      if (engineIsNode && (parent = process$2.domain)) parent.exit();
      while (fn = queue.get()) try {
        fn();
      } catch (error) {
        if (queue.head) notify$1();
        throw error;
      }
      if (parent) parent.enter();
    };

    // browsers with MutationObserver, except iOS - https://github.com/zloirock/core-js/issues/339
    // also except WebOS Webkit https://github.com/zloirock/core-js/issues/898
    if (!engineIsIos && !engineIsNode && !engineIsWebosWebkit && MutationObserver && document$2) {
      toggle = true;
      node = document$2.createTextNode('');
      new MutationObserver(flush$1).observe(node, { characterData: true });
      notify$1 = function () {
        node.data = toggle = !toggle;
      };
    // environments with maybe non-completely correct, but existent Promise
    } else if (!engineIsIosPebble && Promise$1 && Promise$1.resolve) {
      // Promise.resolve without an argument throws an error in LG WebOS 2
      promise$1 = Promise$1.resolve(undefined);
      // workaround of WebKit ~ iOS Safari 10.1 bug
      promise$1.constructor = Promise$1;
      then = functionBindContext(promise$1.then, promise$1);
      notify$1 = function () {
        then(flush$1);
      };
    // Node.js without promises
    } else if (engineIsNode) {
      notify$1 = function () {
        process$2.nextTick(flush$1);
      };
    // for other environments - macrotask based on:
    // - setImmediate
    // - MessageChannel
    // - window.postMessage
    // - onreadystatechange
    // - setTimeout
    } else {
      // `webpack` dev server bug on IE global methods - use bind(fn, global)
      macrotask = functionBindContext(macrotask, global_1);
      notify$1 = function () {
        macrotask(flush$1);
      };
    }

    microtask = function (fn) {
      if (!queue.head) notify$1();
      queue.add(fn);
    };
  }

  var microtask_1 = microtask;

  var hostReportErrors = function (a, b) {
    try {
      // eslint-disable-next-line no-console -- safe
      arguments.length == 1 ? console.error(a) : console.error(a, b);
    } catch (error) { /* empty */ }
  };

  var perform = function (exec) {
    try {
      return { error: false, value: exec() };
    } catch (error) {
      return { error: true, value: error };
    }
  };

  var promiseNativeConstructor = global_1.Promise;

  /* global Deno -- Deno case */
  var engineIsDeno = typeof Deno == 'object' && Deno && typeof Deno.version == 'object';

  var engineIsBrowser = !engineIsDeno && !engineIsNode
    && typeof window == 'object'
    && typeof document == 'object';

  promiseNativeConstructor && promiseNativeConstructor.prototype;
  var SPECIES = wellKnownSymbol('species');
  var SUBCLASSING = false;
  var NATIVE_PROMISE_REJECTION_EVENT$1 = isCallable(global_1.PromiseRejectionEvent);

  var FORCED_PROMISE_CONSTRUCTOR$5 = isForced_1('Promise', function () {
    var PROMISE_CONSTRUCTOR_SOURCE = inspectSource(promiseNativeConstructor);
    var GLOBAL_CORE_JS_PROMISE = PROMISE_CONSTRUCTOR_SOURCE !== String(promiseNativeConstructor);
    // V8 6.6 (Node 10 and Chrome 66) have a bug with resolving custom thenables
    // https://bugs.chromium.org/p/chromium/issues/detail?id=830565
    // We can't detect it synchronously, so just check versions
    if (!GLOBAL_CORE_JS_PROMISE && engineV8Version === 66) return true;
    // We can't use @@species feature detection in V8 since it causes
    // deoptimization and performance degradation
    // https://github.com/zloirock/core-js/issues/679
    if (!engineV8Version || engineV8Version < 51 || !/native code/.test(PROMISE_CONSTRUCTOR_SOURCE)) {
      // Detect correctness of subclassing with @@species support
      var promise = new promiseNativeConstructor(function (resolve) { resolve(1); });
      var FakePromise = function (exec) {
        exec(function () { /* empty */ }, function () { /* empty */ });
      };
      var constructor = promise.constructor = {};
      constructor[SPECIES] = FakePromise;
      SUBCLASSING = promise.then(function () { /* empty */ }) instanceof FakePromise;
      if (!SUBCLASSING) return true;
    // Unhandled rejections tracking support, NodeJS Promise without it fails @@species test
    } return !GLOBAL_CORE_JS_PROMISE && (engineIsBrowser || engineIsDeno) && !NATIVE_PROMISE_REJECTION_EVENT$1;
  });

  var promiseConstructorDetection = {
    CONSTRUCTOR: FORCED_PROMISE_CONSTRUCTOR$5,
    REJECTION_EVENT: NATIVE_PROMISE_REJECTION_EVENT$1,
    SUBCLASSING: SUBCLASSING
  };

  var $TypeError$5 = TypeError;

  var PromiseCapability = function (C) {
    var resolve, reject;
    this.promise = new C(function ($$resolve, $$reject) {
      if (resolve !== undefined || reject !== undefined) throw $TypeError$5('Bad Promise constructor');
      resolve = $$resolve;
      reject = $$reject;
    });
    this.resolve = aCallable(resolve);
    this.reject = aCallable(reject);
  };

  // `NewPromiseCapability` abstract operation
  // https://tc39.es/ecma262/#sec-newpromisecapability
  var f$1 = function (C) {
    return new PromiseCapability(C);
  };

  var newPromiseCapability$1 = {
  	f: f$1
  };

  var task = task$1.set;









  var PROMISE = 'Promise';
  var FORCED_PROMISE_CONSTRUCTOR$4 = promiseConstructorDetection.CONSTRUCTOR;
  var NATIVE_PROMISE_REJECTION_EVENT = promiseConstructorDetection.REJECTION_EVENT;
  var NATIVE_PROMISE_SUBCLASSING = promiseConstructorDetection.SUBCLASSING;
  var getInternalPromiseState = internalState.getterFor(PROMISE);
  var setInternalState$5 = internalState.set;
  var NativePromisePrototype$1 = promiseNativeConstructor && promiseNativeConstructor.prototype;
  var PromiseConstructor = promiseNativeConstructor;
  var PromisePrototype = NativePromisePrototype$1;
  var TypeError$6 = global_1.TypeError;
  var document$1 = global_1.document;
  var process$1 = global_1.process;
  var newPromiseCapability = newPromiseCapability$1.f;
  var newGenericPromiseCapability = newPromiseCapability;

  var DISPATCH_EVENT = !!(document$1 && document$1.createEvent && global_1.dispatchEvent);
  var UNHANDLED_REJECTION = 'unhandledrejection';
  var REJECTION_HANDLED = 'rejectionhandled';
  var PENDING = 0;
  var FULFILLED = 1;
  var REJECTED = 2;
  var HANDLED = 1;
  var UNHANDLED = 2;

  var Internal, OwnPromiseCapability, PromiseWrapper, nativeThen;

  // helpers
  var isThenable = function (it) {
    var then;
    return isObject(it) && isCallable(then = it.then) ? then : false;
  };

  var callReaction = function (reaction, state) {
    var value = state.value;
    var ok = state.state == FULFILLED;
    var handler = ok ? reaction.ok : reaction.fail;
    var resolve = reaction.resolve;
    var reject = reaction.reject;
    var domain = reaction.domain;
    var result, then, exited;
    try {
      if (handler) {
        if (!ok) {
          if (state.rejection === UNHANDLED) onHandleUnhandled(state);
          state.rejection = HANDLED;
        }
        if (handler === true) result = value;
        else {
          if (domain) domain.enter();
          result = handler(value); // can throw
          if (domain) {
            domain.exit();
            exited = true;
          }
        }
        if (result === reaction.promise) {
          reject(TypeError$6('Promise-chain cycle'));
        } else if (then = isThenable(result)) {
          functionCall(then, result, resolve, reject);
        } else resolve(result);
      } else reject(value);
    } catch (error) {
      if (domain && !exited) domain.exit();
      reject(error);
    }
  };

  var notify = function (state, isReject) {
    if (state.notified) return;
    state.notified = true;
    microtask_1(function () {
      var reactions = state.reactions;
      var reaction;
      while (reaction = reactions.get()) {
        callReaction(reaction, state);
      }
      state.notified = false;
      if (isReject && !state.rejection) onUnhandled(state);
    });
  };

  var dispatchEvent = function (name, promise, reason) {
    var event, handler;
    if (DISPATCH_EVENT) {
      event = document$1.createEvent('Event');
      event.promise = promise;
      event.reason = reason;
      event.initEvent(name, false, true);
      global_1.dispatchEvent(event);
    } else event = { promise: promise, reason: reason };
    if (!NATIVE_PROMISE_REJECTION_EVENT && (handler = global_1['on' + name])) handler(event);
    else if (name === UNHANDLED_REJECTION) hostReportErrors('Unhandled promise rejection', reason);
  };

  var onUnhandled = function (state) {
    functionCall(task, global_1, function () {
      var promise = state.facade;
      var value = state.value;
      var IS_UNHANDLED = isUnhandled(state);
      var result;
      if (IS_UNHANDLED) {
        result = perform(function () {
          if (engineIsNode) {
            process$1.emit('unhandledRejection', value, promise);
          } else dispatchEvent(UNHANDLED_REJECTION, promise, value);
        });
        // Browsers should not trigger `rejectionHandled` event if it was handled here, NodeJS - should
        state.rejection = engineIsNode || isUnhandled(state) ? UNHANDLED : HANDLED;
        if (result.error) throw result.value;
      }
    });
  };

  var isUnhandled = function (state) {
    return state.rejection !== HANDLED && !state.parent;
  };

  var onHandleUnhandled = function (state) {
    functionCall(task, global_1, function () {
      var promise = state.facade;
      if (engineIsNode) {
        process$1.emit('rejectionHandled', promise);
      } else dispatchEvent(REJECTION_HANDLED, promise, state.value);
    });
  };

  var bind$1 = function (fn, state, unwrap) {
    return function (value) {
      fn(state, value, unwrap);
    };
  };

  var internalReject = function (state, value, unwrap) {
    if (state.done) return;
    state.done = true;
    if (unwrap) state = unwrap;
    state.value = value;
    state.state = REJECTED;
    notify(state, true);
  };

  var internalResolve = function (state, value, unwrap) {
    if (state.done) return;
    state.done = true;
    if (unwrap) state = unwrap;
    try {
      if (state.facade === value) throw TypeError$6("Promise can't be resolved itself");
      var then = isThenable(value);
      if (then) {
        microtask_1(function () {
          var wrapper = { done: false };
          try {
            functionCall(then, value,
              bind$1(internalResolve, wrapper, state),
              bind$1(internalReject, wrapper, state)
            );
          } catch (error) {
            internalReject(wrapper, error, state);
          }
        });
      } else {
        state.value = value;
        state.state = FULFILLED;
        notify(state, false);
      }
    } catch (error) {
      internalReject({ done: false }, error, state);
    }
  };

  // constructor polyfill
  if (FORCED_PROMISE_CONSTRUCTOR$4) {
    // 25.4.3.1 Promise(executor)
    PromiseConstructor = function Promise(executor) {
      anInstance(this, PromisePrototype);
      aCallable(executor);
      functionCall(Internal, this);
      var state = getInternalPromiseState(this);
      try {
        executor(bind$1(internalResolve, state), bind$1(internalReject, state));
      } catch (error) {
        internalReject(state, error);
      }
    };

    PromisePrototype = PromiseConstructor.prototype;

    // eslint-disable-next-line no-unused-vars -- required for `.length`
    Internal = function Promise(executor) {
      setInternalState$5(this, {
        type: PROMISE,
        done: false,
        notified: false,
        parent: false,
        reactions: new queue$1(),
        rejection: false,
        state: PENDING,
        value: undefined
      });
    };

    // `Promise.prototype.then` method
    // https://tc39.es/ecma262/#sec-promise.prototype.then
    Internal.prototype = defineBuiltIn(PromisePrototype, 'then', function then(onFulfilled, onRejected) {
      var state = getInternalPromiseState(this);
      var reaction = newPromiseCapability(speciesConstructor(this, PromiseConstructor));
      state.parent = true;
      reaction.ok = isCallable(onFulfilled) ? onFulfilled : true;
      reaction.fail = isCallable(onRejected) && onRejected;
      reaction.domain = engineIsNode ? process$1.domain : undefined;
      if (state.state == PENDING) state.reactions.add(reaction);
      else microtask_1(function () {
        callReaction(reaction, state);
      });
      return reaction.promise;
    });

    OwnPromiseCapability = function () {
      var promise = new Internal();
      var state = getInternalPromiseState(promise);
      this.promise = promise;
      this.resolve = bind$1(internalResolve, state);
      this.reject = bind$1(internalReject, state);
    };

    newPromiseCapability$1.f = newPromiseCapability = function (C) {
      return C === PromiseConstructor || C === PromiseWrapper
        ? new OwnPromiseCapability(C)
        : newGenericPromiseCapability(C);
    };

    if (isCallable(promiseNativeConstructor) && NativePromisePrototype$1 !== Object.prototype) {
      nativeThen = NativePromisePrototype$1.then;

      if (!NATIVE_PROMISE_SUBCLASSING) {
        // make `Promise#then` return a polyfilled `Promise` for native promise-based APIs
        defineBuiltIn(NativePromisePrototype$1, 'then', function then(onFulfilled, onRejected) {
          var that = this;
          return new PromiseConstructor(function (resolve, reject) {
            functionCall(nativeThen, that, resolve, reject);
          }).then(onFulfilled, onRejected);
        // https://github.com/zloirock/core-js/issues/640
        }, { unsafe: true });
      }

      // make `.constructor === Promise` work for native promise-based APIs
      try {
        delete NativePromisePrototype$1.constructor;
      } catch (error) { /* empty */ }

      // make `instanceof Promise` work for native promise-based APIs
      if (objectSetPrototypeOf) {
        objectSetPrototypeOf(NativePromisePrototype$1, PromisePrototype);
      }
    }
  }

  _export({ global: true, constructor: true, wrap: true, forced: FORCED_PROMISE_CONSTRUCTOR$4 }, {
    Promise: PromiseConstructor
  });

  setToStringTag(PromiseConstructor, PROMISE, false);
  setSpecies(PROMISE);

  var FORCED_PROMISE_CONSTRUCTOR$3 = promiseConstructorDetection.CONSTRUCTOR;

  var promiseStaticsIncorrectIteration = FORCED_PROMISE_CONSTRUCTOR$3 || !checkCorrectnessOfIteration(function (iterable) {
    promiseNativeConstructor.all(iterable).then(undefined, function () { /* empty */ });
  });

  // `Promise.all` method
  // https://tc39.es/ecma262/#sec-promise.all
  _export({ target: 'Promise', stat: true, forced: promiseStaticsIncorrectIteration }, {
    all: function all(iterable) {
      var C = this;
      var capability = newPromiseCapability$1.f(C);
      var resolve = capability.resolve;
      var reject = capability.reject;
      var result = perform(function () {
        var $promiseResolve = aCallable(C.resolve);
        var values = [];
        var counter = 0;
        var remaining = 1;
        iterate(iterable, function (promise) {
          var index = counter++;
          var alreadyCalled = false;
          remaining++;
          functionCall($promiseResolve, C, promise).then(function (value) {
            if (alreadyCalled) return;
            alreadyCalled = true;
            values[index] = value;
            --remaining || resolve(values);
          }, reject);
        });
        --remaining || resolve(values);
      });
      if (result.error) reject(result.value);
      return capability.promise;
    }
  });

  var FORCED_PROMISE_CONSTRUCTOR$2 = promiseConstructorDetection.CONSTRUCTOR;





  var NativePromisePrototype = promiseNativeConstructor && promiseNativeConstructor.prototype;

  // `Promise.prototype.catch` method
  // https://tc39.es/ecma262/#sec-promise.prototype.catch
  _export({ target: 'Promise', proto: true, forced: FORCED_PROMISE_CONSTRUCTOR$2, real: true }, {
    'catch': function (onRejected) {
      return this.then(undefined, onRejected);
    }
  });

  // makes sure that native promise-based APIs `Promise#catch` properly works with patched `Promise#then`
  if (isCallable(promiseNativeConstructor)) {
    var method = getBuiltIn('Promise').prototype['catch'];
    if (NativePromisePrototype['catch'] !== method) {
      defineBuiltIn(NativePromisePrototype, 'catch', method, { unsafe: true });
    }
  }

  // `Promise.race` method
  // https://tc39.es/ecma262/#sec-promise.race
  _export({ target: 'Promise', stat: true, forced: promiseStaticsIncorrectIteration }, {
    race: function race(iterable) {
      var C = this;
      var capability = newPromiseCapability$1.f(C);
      var reject = capability.reject;
      var result = perform(function () {
        var $promiseResolve = aCallable(C.resolve);
        iterate(iterable, function (promise) {
          functionCall($promiseResolve, C, promise).then(capability.resolve, reject);
        });
      });
      if (result.error) reject(result.value);
      return capability.promise;
    }
  });

  var FORCED_PROMISE_CONSTRUCTOR$1 = promiseConstructorDetection.CONSTRUCTOR;

  // `Promise.reject` method
  // https://tc39.es/ecma262/#sec-promise.reject
  _export({ target: 'Promise', stat: true, forced: FORCED_PROMISE_CONSTRUCTOR$1 }, {
    reject: function reject(r) {
      var capability = newPromiseCapability$1.f(this);
      functionCall(capability.reject, undefined, r);
      return capability.promise;
    }
  });

  var promiseResolve = function (C, x) {
    anObject(C);
    if (isObject(x) && x.constructor === C) return x;
    var promiseCapability = newPromiseCapability$1.f(C);
    var resolve = promiseCapability.resolve;
    resolve(x);
    return promiseCapability.promise;
  };

  var FORCED_PROMISE_CONSTRUCTOR = promiseConstructorDetection.CONSTRUCTOR;


  getBuiltIn('Promise');

  // `Promise.resolve` method
  // https://tc39.es/ecma262/#sec-promise.resolve
  _export({ target: 'Promise', stat: true, forced: FORCED_PROMISE_CONSTRUCTOR }, {
    resolve: function resolve(x) {
      return promiseResolve(this, x);
    }
  });

  // `globalThis` object
  // https://tc39.es/ecma262/#sec-globalthis
  _export({ global: true, forced: global_1.globalThis !== global_1 }, {
    globalThis: global_1
  });

  var getWeakData = internalMetadata.getWeakData;









  var setInternalState$4 = internalState.set;
  var internalStateGetterFor = internalState.getterFor;
  var find$1 = arrayIteration.find;
  var findIndex = arrayIteration.findIndex;
  var splice$1 = functionUncurryThis([].splice);
  var id = 0;

  // fallback for uncaught frozen keys
  var uncaughtFrozenStore = function (state) {
    return state.frozen || (state.frozen = new UncaughtFrozenStore());
  };

  var UncaughtFrozenStore = function () {
    this.entries = [];
  };

  var findUncaughtFrozen = function (store, key) {
    return find$1(store.entries, function (it) {
      return it[0] === key;
    });
  };

  UncaughtFrozenStore.prototype = {
    get: function (key) {
      var entry = findUncaughtFrozen(this, key);
      if (entry) return entry[1];
    },
    has: function (key) {
      return !!findUncaughtFrozen(this, key);
    },
    set: function (key, value) {
      var entry = findUncaughtFrozen(this, key);
      if (entry) entry[1] = value;
      else this.entries.push([key, value]);
    },
    'delete': function (key) {
      var index = findIndex(this.entries, function (it) {
        return it[0] === key;
      });
      if (~index) splice$1(this.entries, index, 1);
      return !!~index;
    }
  };

  var collectionWeak = {
    getConstructor: function (wrapper, CONSTRUCTOR_NAME, IS_MAP, ADDER) {
      var Constructor = wrapper(function (that, iterable) {
        anInstance(that, Prototype);
        setInternalState$4(that, {
          type: CONSTRUCTOR_NAME,
          id: id++,
          frozen: undefined
        });
        if (!isNullOrUndefined(iterable)) iterate(iterable, that[ADDER], { that: that, AS_ENTRIES: IS_MAP });
      });

      var Prototype = Constructor.prototype;

      var getInternalState = internalStateGetterFor(CONSTRUCTOR_NAME);

      var define = function (that, key, value) {
        var state = getInternalState(that);
        var data = getWeakData(anObject(key), true);
        if (data === true) uncaughtFrozenStore(state).set(key, value);
        else data[state.id] = value;
        return that;
      };

      defineBuiltIns(Prototype, {
        // `{ WeakMap, WeakSet }.prototype.delete(key)` methods
        // https://tc39.es/ecma262/#sec-weakmap.prototype.delete
        // https://tc39.es/ecma262/#sec-weakset.prototype.delete
        'delete': function (key) {
          var state = getInternalState(this);
          if (!isObject(key)) return false;
          var data = getWeakData(key);
          if (data === true) return uncaughtFrozenStore(state)['delete'](key);
          return data && hasOwnProperty_1(data, state.id) && delete data[state.id];
        },
        // `{ WeakMap, WeakSet }.prototype.has(key)` methods
        // https://tc39.es/ecma262/#sec-weakmap.prototype.has
        // https://tc39.es/ecma262/#sec-weakset.prototype.has
        has: function has(key) {
          var state = getInternalState(this);
          if (!isObject(key)) return false;
          var data = getWeakData(key);
          if (data === true) return uncaughtFrozenStore(state).has(key);
          return data && hasOwnProperty_1(data, state.id);
        }
      });

      defineBuiltIns(Prototype, IS_MAP ? {
        // `WeakMap.prototype.get(key)` method
        // https://tc39.es/ecma262/#sec-weakmap.prototype.get
        get: function get(key) {
          var state = getInternalState(this);
          if (isObject(key)) {
            var data = getWeakData(key);
            if (data === true) return uncaughtFrozenStore(state).get(key);
            return data ? data[state.id] : undefined;
          }
        },
        // `WeakMap.prototype.set(key, value)` method
        // https://tc39.es/ecma262/#sec-weakmap.prototype.set
        set: function set(key, value) {
          return define(this, key, value);
        }
      } : {
        // `WeakSet.prototype.add(value)` method
        // https://tc39.es/ecma262/#sec-weakset.prototype.add
        add: function add(value) {
          return define(this, value, true);
        }
      });

      return Constructor;
    }
  };
  collectionWeak.getConstructor;

  var enforceInternalState$1 = internalState.enforce;



  var $Object = Object;
  // eslint-disable-next-line es/no-array-isarray -- safe
  var isArray = Array.isArray;
  // eslint-disable-next-line es/no-object-isextensible -- safe
  var isExtensible = $Object.isExtensible;
  // eslint-disable-next-line es/no-object-isfrozen -- safe
  var isFrozen = $Object.isFrozen;
  // eslint-disable-next-line es/no-object-issealed -- safe
  var isSealed = $Object.isSealed;
  // eslint-disable-next-line es/no-object-freeze -- safe
  var freeze = $Object.freeze;
  // eslint-disable-next-line es/no-object-seal -- safe
  var seal = $Object.seal;

  var FROZEN = {};
  var SEALED = {};
  var IS_IE11 = !global_1.ActiveXObject && 'ActiveXObject' in global_1;
  var InternalWeakMap;

  var wrapper = function (init) {
    return function WeakMap() {
      return init(this, arguments.length ? arguments[0] : undefined);
    };
  };

  // `WeakMap` constructor
  // https://tc39.es/ecma262/#sec-weakmap-constructor
  var $WeakMap = collection('WeakMap', wrapper, collectionWeak);
  var WeakMapPrototype = $WeakMap.prototype;
  var nativeSet = functionUncurryThis(WeakMapPrototype.set);

  // Chakra Edge bug: adding frozen arrays to WeakMap unfreeze them
  var hasMSEdgeFreezingBug = function () {
    return freezing && fails(function () {
      var frozenArray = freeze([]);
      nativeSet(new $WeakMap(), frozenArray, 1);
      return !isFrozen(frozenArray);
    });
  };

  // IE11 WeakMap frozen keys fix
  // We can't use feature detection because it crash some old IE builds
  // https://github.com/zloirock/core-js/issues/485
  if (weakMapBasicDetection) if (IS_IE11) {
    InternalWeakMap = collectionWeak.getConstructor(wrapper, 'WeakMap', true);
    internalMetadata.enable();
    var nativeDelete = functionUncurryThis(WeakMapPrototype['delete']);
    var nativeHas = functionUncurryThis(WeakMapPrototype.has);
    var nativeGet = functionUncurryThis(WeakMapPrototype.get);
    defineBuiltIns(WeakMapPrototype, {
      'delete': function (key) {
        if (isObject(key) && !isExtensible(key)) {
          var state = enforceInternalState$1(this);
          if (!state.frozen) state.frozen = new InternalWeakMap();
          return nativeDelete(this, key) || state.frozen['delete'](key);
        } return nativeDelete(this, key);
      },
      has: function has(key) {
        if (isObject(key) && !isExtensible(key)) {
          var state = enforceInternalState$1(this);
          if (!state.frozen) state.frozen = new InternalWeakMap();
          return nativeHas(this, key) || state.frozen.has(key);
        } return nativeHas(this, key);
      },
      get: function get(key) {
        if (isObject(key) && !isExtensible(key)) {
          var state = enforceInternalState$1(this);
          if (!state.frozen) state.frozen = new InternalWeakMap();
          return nativeHas(this, key) ? nativeGet(this, key) : state.frozen.get(key);
        } return nativeGet(this, key);
      },
      set: function set(key, value) {
        if (isObject(key) && !isExtensible(key)) {
          var state = enforceInternalState$1(this);
          if (!state.frozen) state.frozen = new InternalWeakMap();
          nativeHas(this, key) ? nativeSet(this, key, value) : state.frozen.set(key, value);
        } else nativeSet(this, key, value);
        return this;
      }
    });
  // Chakra Edge frozen keys fix
  } else if (hasMSEdgeFreezingBug()) {
    defineBuiltIns(WeakMapPrototype, {
      set: function set(key, value) {
        var arrayIntegrityLevel;
        if (isArray(key)) {
          if (isFrozen(key)) arrayIntegrityLevel = FROZEN;
          else if (isSealed(key)) arrayIntegrityLevel = SEALED;
        }
        nativeSet(this, key, value);
        if (arrayIntegrityLevel == FROZEN) freeze(key);
        if (arrayIntegrityLevel == SEALED) seal(key);
        return this;
      }
    });
  }

  // eslint-disable-next-line es/no-typed-arrays -- safe
  var arrayBufferBasicDetection = typeof ArrayBuffer != 'undefined' && typeof DataView != 'undefined';

  var enforceInternalState = internalState.enforce;
  var getInternalState$1 = internalState.get;
  var Int8Array$4 = global_1.Int8Array;
  var Int8ArrayPrototype$1 = Int8Array$4 && Int8Array$4.prototype;
  var Uint8ClampedArray$1 = global_1.Uint8ClampedArray;
  var Uint8ClampedArrayPrototype = Uint8ClampedArray$1 && Uint8ClampedArray$1.prototype;
  var TypedArray = Int8Array$4 && objectGetPrototypeOf(Int8Array$4);
  var TypedArrayPrototype$1 = Int8ArrayPrototype$1 && objectGetPrototypeOf(Int8ArrayPrototype$1);
  var ObjectPrototype$2 = Object.prototype;
  var TypeError$5 = global_1.TypeError;

  var TO_STRING_TAG = wellKnownSymbol('toStringTag');
  var TYPED_ARRAY_TAG = uid('TYPED_ARRAY_TAG');
  var TYPED_ARRAY_CONSTRUCTOR = 'TypedArrayConstructor';
  // Fixing native typed arrays in Opera Presto crashes the browser, see #595
  var NATIVE_ARRAY_BUFFER_VIEWS$1 = arrayBufferBasicDetection && !!objectSetPrototypeOf && classof(global_1.opera) !== 'Opera';
  var TYPED_ARRAY_TAG_REQUIRED = false;
  var NAME, Constructor, Prototype;

  var TypedArrayConstructorsList = {
    Int8Array: 1,
    Uint8Array: 1,
    Uint8ClampedArray: 1,
    Int16Array: 2,
    Uint16Array: 2,
    Int32Array: 4,
    Uint32Array: 4,
    Float32Array: 4,
    Float64Array: 8
  };

  var BigIntArrayConstructorsList = {
    BigInt64Array: 8,
    BigUint64Array: 8
  };

  var isView = function isView(it) {
    if (!isObject(it)) return false;
    var klass = classof(it);
    return klass === 'DataView'
      || hasOwnProperty_1(TypedArrayConstructorsList, klass)
      || hasOwnProperty_1(BigIntArrayConstructorsList, klass);
  };

  var getTypedArrayConstructor$1 = function (it) {
    var proto = objectGetPrototypeOf(it);
    if (!isObject(proto)) return;
    var state = getInternalState$1(proto);
    return (state && hasOwnProperty_1(state, TYPED_ARRAY_CONSTRUCTOR)) ? state[TYPED_ARRAY_CONSTRUCTOR] : getTypedArrayConstructor$1(proto);
  };

  var isTypedArray = function (it) {
    if (!isObject(it)) return false;
    var klass = classof(it);
    return hasOwnProperty_1(TypedArrayConstructorsList, klass)
      || hasOwnProperty_1(BigIntArrayConstructorsList, klass);
  };

  var aTypedArray$m = function (it) {
    if (isTypedArray(it)) return it;
    throw TypeError$5('Target is not a typed array');
  };

  var aTypedArrayConstructor$2 = function (C) {
    if (isCallable(C) && (!objectSetPrototypeOf || objectIsPrototypeOf(TypedArray, C))) return C;
    throw TypeError$5(tryToString(C) + ' is not a typed array constructor');
  };

  var exportTypedArrayMethod$n = function (KEY, property, forced, options) {
    if (!descriptors) return;
    if (forced) for (var ARRAY in TypedArrayConstructorsList) {
      var TypedArrayConstructor = global_1[ARRAY];
      if (TypedArrayConstructor && hasOwnProperty_1(TypedArrayConstructor.prototype, KEY)) try {
        delete TypedArrayConstructor.prototype[KEY];
      } catch (error) {
        // old WebKit bug - some methods are non-configurable
        try {
          TypedArrayConstructor.prototype[KEY] = property;
        } catch (error2) { /* empty */ }
      }
    }
    if (!TypedArrayPrototype$1[KEY] || forced) {
      defineBuiltIn(TypedArrayPrototype$1, KEY, forced ? property
        : NATIVE_ARRAY_BUFFER_VIEWS$1 && Int8ArrayPrototype$1[KEY] || property, options);
    }
  };

  var exportTypedArrayStaticMethod = function (KEY, property, forced) {
    var ARRAY, TypedArrayConstructor;
    if (!descriptors) return;
    if (objectSetPrototypeOf) {
      if (forced) for (ARRAY in TypedArrayConstructorsList) {
        TypedArrayConstructor = global_1[ARRAY];
        if (TypedArrayConstructor && hasOwnProperty_1(TypedArrayConstructor, KEY)) try {
          delete TypedArrayConstructor[KEY];
        } catch (error) { /* empty */ }
      }
      if (!TypedArray[KEY] || forced) {
        // V8 ~ Chrome 49-50 `%TypedArray%` methods are non-writable non-configurable
        try {
          return defineBuiltIn(TypedArray, KEY, forced ? property : NATIVE_ARRAY_BUFFER_VIEWS$1 && TypedArray[KEY] || property);
        } catch (error) { /* empty */ }
      } else return;
    }
    for (ARRAY in TypedArrayConstructorsList) {
      TypedArrayConstructor = global_1[ARRAY];
      if (TypedArrayConstructor && (!TypedArrayConstructor[KEY] || forced)) {
        defineBuiltIn(TypedArrayConstructor, KEY, property);
      }
    }
  };

  for (NAME in TypedArrayConstructorsList) {
    Constructor = global_1[NAME];
    Prototype = Constructor && Constructor.prototype;
    if (Prototype) enforceInternalState(Prototype)[TYPED_ARRAY_CONSTRUCTOR] = Constructor;
    else NATIVE_ARRAY_BUFFER_VIEWS$1 = false;
  }

  for (NAME in BigIntArrayConstructorsList) {
    Constructor = global_1[NAME];
    Prototype = Constructor && Constructor.prototype;
    if (Prototype) enforceInternalState(Prototype)[TYPED_ARRAY_CONSTRUCTOR] = Constructor;
  }

  // WebKit bug - typed arrays constructors prototype is Object.prototype
  if (!NATIVE_ARRAY_BUFFER_VIEWS$1 || !isCallable(TypedArray) || TypedArray === Function.prototype) {
    // eslint-disable-next-line no-shadow -- safe
    TypedArray = function TypedArray() {
      throw TypeError$5('Incorrect invocation');
    };
    if (NATIVE_ARRAY_BUFFER_VIEWS$1) for (NAME in TypedArrayConstructorsList) {
      if (global_1[NAME]) objectSetPrototypeOf(global_1[NAME], TypedArray);
    }
  }

  if (!NATIVE_ARRAY_BUFFER_VIEWS$1 || !TypedArrayPrototype$1 || TypedArrayPrototype$1 === ObjectPrototype$2) {
    TypedArrayPrototype$1 = TypedArray.prototype;
    if (NATIVE_ARRAY_BUFFER_VIEWS$1) for (NAME in TypedArrayConstructorsList) {
      if (global_1[NAME]) objectSetPrototypeOf(global_1[NAME].prototype, TypedArrayPrototype$1);
    }
  }

  // WebKit bug - one more object in Uint8ClampedArray prototype chain
  if (NATIVE_ARRAY_BUFFER_VIEWS$1 && objectGetPrototypeOf(Uint8ClampedArrayPrototype) !== TypedArrayPrototype$1) {
    objectSetPrototypeOf(Uint8ClampedArrayPrototype, TypedArrayPrototype$1);
  }

  if (descriptors && !hasOwnProperty_1(TypedArrayPrototype$1, TO_STRING_TAG)) {
    TYPED_ARRAY_TAG_REQUIRED = true;
    defineBuiltInAccessor(TypedArrayPrototype$1, TO_STRING_TAG, {
      configurable: true,
      get: function () {
        return isObject(this) ? this[TYPED_ARRAY_TAG] : undefined;
      }
    });
    for (NAME in TypedArrayConstructorsList) if (global_1[NAME]) {
      createNonEnumerableProperty(global_1[NAME], TYPED_ARRAY_TAG, NAME);
    }
  }

  var arrayBufferViewCore = {
    NATIVE_ARRAY_BUFFER_VIEWS: NATIVE_ARRAY_BUFFER_VIEWS$1,
    TYPED_ARRAY_TAG: TYPED_ARRAY_TAG_REQUIRED && TYPED_ARRAY_TAG,
    aTypedArray: aTypedArray$m,
    aTypedArrayConstructor: aTypedArrayConstructor$2,
    exportTypedArrayMethod: exportTypedArrayMethod$n,
    exportTypedArrayStaticMethod: exportTypedArrayStaticMethod,
    getTypedArrayConstructor: getTypedArrayConstructor$1,
    isView: isView,
    isTypedArray: isTypedArray,
    TypedArray: TypedArray,
    TypedArrayPrototype: TypedArrayPrototype$1
  };

  /* eslint-disable no-new -- required for testing */



  var NATIVE_ARRAY_BUFFER_VIEWS = arrayBufferViewCore.NATIVE_ARRAY_BUFFER_VIEWS;

  var ArrayBuffer$2 = global_1.ArrayBuffer;
  var Int8Array$3 = global_1.Int8Array;

  var typedArrayConstructorsRequireWrappers = !NATIVE_ARRAY_BUFFER_VIEWS || !fails(function () {
    Int8Array$3(1);
  }) || !fails(function () {
    new Int8Array$3(-1);
  }) || !checkCorrectnessOfIteration(function (iterable) {
    new Int8Array$3();
    new Int8Array$3(null);
    new Int8Array$3(1.5);
    new Int8Array$3(iterable);
  }, true) || fails(function () {
    // Safari (11+) bug - a reason why even Safari 13 should load a typed array polyfill
    return new Int8Array$3(new ArrayBuffer$2(2), 1, undefined).length !== 1;
  });

  var $RangeError$5 = RangeError;

  // `ToIndex` abstract operation
  // https://tc39.es/ecma262/#sec-toindex
  var toIndex = function (it) {
    if (it === undefined) return 0;
    var number = toIntegerOrInfinity(it);
    var length = toLength(number);
    if (number !== length) throw $RangeError$5('Wrong length or index');
    return length;
  };

  // IEEE754 conversions based on https://github.com/feross/ieee754
  var $Array$1 = Array;
  var abs = Math.abs;
  var pow$2 = Math.pow;
  var floor$7 = Math.floor;
  var log$1 = Math.log;
  var LN2 = Math.LN2;

  var pack = function (number, mantissaLength, bytes) {
    var buffer = $Array$1(bytes);
    var exponentLength = bytes * 8 - mantissaLength - 1;
    var eMax = (1 << exponentLength) - 1;
    var eBias = eMax >> 1;
    var rt = mantissaLength === 23 ? pow$2(2, -24) - pow$2(2, -77) : 0;
    var sign = number < 0 || number === 0 && 1 / number < 0 ? 1 : 0;
    var index = 0;
    var exponent, mantissa, c;
    number = abs(number);
    // eslint-disable-next-line no-self-compare -- NaN check
    if (number != number || number === Infinity) {
      // eslint-disable-next-line no-self-compare -- NaN check
      mantissa = number != number ? 1 : 0;
      exponent = eMax;
    } else {
      exponent = floor$7(log$1(number) / LN2);
      c = pow$2(2, -exponent);
      if (number * c < 1) {
        exponent--;
        c *= 2;
      }
      if (exponent + eBias >= 1) {
        number += rt / c;
      } else {
        number += rt * pow$2(2, 1 - eBias);
      }
      if (number * c >= 2) {
        exponent++;
        c /= 2;
      }
      if (exponent + eBias >= eMax) {
        mantissa = 0;
        exponent = eMax;
      } else if (exponent + eBias >= 1) {
        mantissa = (number * c - 1) * pow$2(2, mantissaLength);
        exponent = exponent + eBias;
      } else {
        mantissa = number * pow$2(2, eBias - 1) * pow$2(2, mantissaLength);
        exponent = 0;
      }
    }
    while (mantissaLength >= 8) {
      buffer[index++] = mantissa & 255;
      mantissa /= 256;
      mantissaLength -= 8;
    }
    exponent = exponent << mantissaLength | mantissa;
    exponentLength += mantissaLength;
    while (exponentLength > 0) {
      buffer[index++] = exponent & 255;
      exponent /= 256;
      exponentLength -= 8;
    }
    buffer[--index] |= sign * 128;
    return buffer;
  };

  var unpack = function (buffer, mantissaLength) {
    var bytes = buffer.length;
    var exponentLength = bytes * 8 - mantissaLength - 1;
    var eMax = (1 << exponentLength) - 1;
    var eBias = eMax >> 1;
    var nBits = exponentLength - 7;
    var index = bytes - 1;
    var sign = buffer[index--];
    var exponent = sign & 127;
    var mantissa;
    sign >>= 7;
    while (nBits > 0) {
      exponent = exponent * 256 + buffer[index--];
      nBits -= 8;
    }
    mantissa = exponent & (1 << -nBits) - 1;
    exponent >>= -nBits;
    nBits += mantissaLength;
    while (nBits > 0) {
      mantissa = mantissa * 256 + buffer[index--];
      nBits -= 8;
    }
    if (exponent === 0) {
      exponent = 1 - eBias;
    } else if (exponent === eMax) {
      return mantissa ? NaN : sign ? -Infinity : Infinity;
    } else {
      mantissa = mantissa + pow$2(2, mantissaLength);
      exponent = exponent - eBias;
    } return (sign ? -1 : 1) * mantissa * pow$2(2, exponent - mantissaLength);
  };

  var ieee754 = {
    pack: pack,
    unpack: unpack
  };

  // `Array.prototype.fill` method implementation
  // https://tc39.es/ecma262/#sec-array.prototype.fill
  var arrayFill = function fill(value /* , start = 0, end = @length */) {
    var O = toObject(this);
    var length = lengthOfArrayLike(O);
    var argumentsLength = arguments.length;
    var index = toAbsoluteIndex(argumentsLength > 1 ? arguments[1] : undefined, length);
    var end = argumentsLength > 2 ? arguments[2] : undefined;
    var endPos = end === undefined ? length : toAbsoluteIndex(end, length);
    while (endPos > index) O[index++] = value;
    return O;
  };

  var getOwnPropertyNames$2 = objectGetOwnPropertyNames.f;





  var PROPER_FUNCTION_NAME$1 = functionName.PROPER;
  var CONFIGURABLE_FUNCTION_NAME = functionName.CONFIGURABLE;
  var ARRAY_BUFFER$1 = 'ArrayBuffer';
  var DATA_VIEW = 'DataView';
  var PROTOTYPE$1 = 'prototype';
  var WRONG_LENGTH = 'Wrong length';
  var WRONG_INDEX = 'Wrong index';
  var getInternalArrayBufferState = internalState.getterFor(ARRAY_BUFFER$1);
  var getInternalDataViewState = internalState.getterFor(DATA_VIEW);
  var setInternalState$3 = internalState.set;
  var NativeArrayBuffer$1 = global_1[ARRAY_BUFFER$1];
  var $ArrayBuffer = NativeArrayBuffer$1;
  var ArrayBufferPrototype = $ArrayBuffer && $ArrayBuffer[PROTOTYPE$1];
  var $DataView = global_1[DATA_VIEW];
  var DataViewPrototype = $DataView && $DataView[PROTOTYPE$1];
  var ObjectPrototype$1 = Object.prototype;
  var Array$1 = global_1.Array;
  var RangeError$2 = global_1.RangeError;
  var fill = functionUncurryThis(arrayFill);
  var reverse = functionUncurryThis([].reverse);

  var packIEEE754 = ieee754.pack;
  var unpackIEEE754 = ieee754.unpack;

  var packInt8 = function (number) {
    return [number & 0xFF];
  };

  var packInt16 = function (number) {
    return [number & 0xFF, number >> 8 & 0xFF];
  };

  var packInt32 = function (number) {
    return [number & 0xFF, number >> 8 & 0xFF, number >> 16 & 0xFF, number >> 24 & 0xFF];
  };

  var unpackInt32 = function (buffer) {
    return buffer[3] << 24 | buffer[2] << 16 | buffer[1] << 8 | buffer[0];
  };

  var packFloat32 = function (number) {
    return packIEEE754(number, 23, 4);
  };

  var packFloat64 = function (number) {
    return packIEEE754(number, 52, 8);
  };

  var addGetter = function (Constructor, key, getInternalState) {
    defineBuiltInAccessor(Constructor[PROTOTYPE$1], key, {
      configurable: true,
      get: function () {
        return getInternalState(this)[key];
      }
    });
  };

  var get = function (view, count, index, isLittleEndian) {
    var store = getInternalDataViewState(view);
    var intIndex = toIndex(index);
    var boolIsLittleEndian = !!isLittleEndian;
    if (intIndex + count > store.byteLength) throw RangeError$2(WRONG_INDEX);
    var bytes = store.bytes;
    var start = intIndex + store.byteOffset;
    var pack = arraySliceSimple(bytes, start, start + count);
    return boolIsLittleEndian ? pack : reverse(pack);
  };

  var set = function (view, count, index, conversion, value, isLittleEndian) {
    var store = getInternalDataViewState(view);
    var intIndex = toIndex(index);
    var pack = conversion(+value);
    var boolIsLittleEndian = !!isLittleEndian;
    if (intIndex + count > store.byteLength) throw RangeError$2(WRONG_INDEX);
    var bytes = store.bytes;
    var start = intIndex + store.byteOffset;
    for (var i = 0; i < count; i++) bytes[start + i] = pack[boolIsLittleEndian ? i : count - i - 1];
  };

  if (!arrayBufferBasicDetection) {
    $ArrayBuffer = function ArrayBuffer(length) {
      anInstance(this, ArrayBufferPrototype);
      var byteLength = toIndex(length);
      setInternalState$3(this, {
        type: ARRAY_BUFFER$1,
        bytes: fill(Array$1(byteLength), 0),
        byteLength: byteLength
      });
      if (!descriptors) {
        this.byteLength = byteLength;
        this.detached = false;
      }
    };

    ArrayBufferPrototype = $ArrayBuffer[PROTOTYPE$1];

    $DataView = function DataView(buffer, byteOffset, byteLength) {
      anInstance(this, DataViewPrototype);
      anInstance(buffer, ArrayBufferPrototype);
      var bufferState = getInternalArrayBufferState(buffer);
      var bufferLength = bufferState.byteLength;
      var offset = toIntegerOrInfinity(byteOffset);
      if (offset < 0 || offset > bufferLength) throw RangeError$2('Wrong offset');
      byteLength = byteLength === undefined ? bufferLength - offset : toLength(byteLength);
      if (offset + byteLength > bufferLength) throw RangeError$2(WRONG_LENGTH);
      setInternalState$3(this, {
        type: DATA_VIEW,
        buffer: buffer,
        byteLength: byteLength,
        byteOffset: offset,
        bytes: bufferState.bytes
      });
      if (!descriptors) {
        this.buffer = buffer;
        this.byteLength = byteLength;
        this.byteOffset = offset;
      }
    };

    DataViewPrototype = $DataView[PROTOTYPE$1];

    if (descriptors) {
      addGetter($ArrayBuffer, 'byteLength', getInternalArrayBufferState);
      addGetter($DataView, 'buffer', getInternalDataViewState);
      addGetter($DataView, 'byteLength', getInternalDataViewState);
      addGetter($DataView, 'byteOffset', getInternalDataViewState);
    }

    defineBuiltIns(DataViewPrototype, {
      getInt8: function getInt8(byteOffset) {
        return get(this, 1, byteOffset)[0] << 24 >> 24;
      },
      getUint8: function getUint8(byteOffset) {
        return get(this, 1, byteOffset)[0];
      },
      getInt16: function getInt16(byteOffset /* , littleEndian */) {
        var bytes = get(this, 2, byteOffset, arguments.length > 1 ? arguments[1] : false);
        return (bytes[1] << 8 | bytes[0]) << 16 >> 16;
      },
      getUint16: function getUint16(byteOffset /* , littleEndian */) {
        var bytes = get(this, 2, byteOffset, arguments.length > 1 ? arguments[1] : false);
        return bytes[1] << 8 | bytes[0];
      },
      getInt32: function getInt32(byteOffset /* , littleEndian */) {
        return unpackInt32(get(this, 4, byteOffset, arguments.length > 1 ? arguments[1] : false));
      },
      getUint32: function getUint32(byteOffset /* , littleEndian */) {
        return unpackInt32(get(this, 4, byteOffset, arguments.length > 1 ? arguments[1] : false)) >>> 0;
      },
      getFloat32: function getFloat32(byteOffset /* , littleEndian */) {
        return unpackIEEE754(get(this, 4, byteOffset, arguments.length > 1 ? arguments[1] : false), 23);
      },
      getFloat64: function getFloat64(byteOffset /* , littleEndian */) {
        return unpackIEEE754(get(this, 8, byteOffset, arguments.length > 1 ? arguments[1] : false), 52);
      },
      setInt8: function setInt8(byteOffset, value) {
        set(this, 1, byteOffset, packInt8, value);
      },
      setUint8: function setUint8(byteOffset, value) {
        set(this, 1, byteOffset, packInt8, value);
      },
      setInt16: function setInt16(byteOffset, value /* , littleEndian */) {
        set(this, 2, byteOffset, packInt16, value, arguments.length > 2 ? arguments[2] : false);
      },
      setUint16: function setUint16(byteOffset, value /* , littleEndian */) {
        set(this, 2, byteOffset, packInt16, value, arguments.length > 2 ? arguments[2] : false);
      },
      setInt32: function setInt32(byteOffset, value /* , littleEndian */) {
        set(this, 4, byteOffset, packInt32, value, arguments.length > 2 ? arguments[2] : false);
      },
      setUint32: function setUint32(byteOffset, value /* , littleEndian */) {
        set(this, 4, byteOffset, packInt32, value, arguments.length > 2 ? arguments[2] : false);
      },
      setFloat32: function setFloat32(byteOffset, value /* , littleEndian */) {
        set(this, 4, byteOffset, packFloat32, value, arguments.length > 2 ? arguments[2] : false);
      },
      setFloat64: function setFloat64(byteOffset, value /* , littleEndian */) {
        set(this, 8, byteOffset, packFloat64, value, arguments.length > 2 ? arguments[2] : false);
      }
    });
  } else {
    var INCORRECT_ARRAY_BUFFER_NAME = PROPER_FUNCTION_NAME$1 && NativeArrayBuffer$1.name !== ARRAY_BUFFER$1;
    /* eslint-disable no-new -- required for testing */
    if (!fails(function () {
      NativeArrayBuffer$1(1);
    }) || !fails(function () {
      new NativeArrayBuffer$1(-1);
    }) || fails(function () {
      new NativeArrayBuffer$1();
      new NativeArrayBuffer$1(1.5);
      new NativeArrayBuffer$1(NaN);
      return NativeArrayBuffer$1.length != 1 || INCORRECT_ARRAY_BUFFER_NAME && !CONFIGURABLE_FUNCTION_NAME;
    })) {
      /* eslint-enable no-new -- required for testing */
      $ArrayBuffer = function ArrayBuffer(length) {
        anInstance(this, ArrayBufferPrototype);
        return new NativeArrayBuffer$1(toIndex(length));
      };

      $ArrayBuffer[PROTOTYPE$1] = ArrayBufferPrototype;

      for (var keys = getOwnPropertyNames$2(NativeArrayBuffer$1), j = 0, key; keys.length > j;) {
        if (!((key = keys[j++]) in $ArrayBuffer)) {
          createNonEnumerableProperty($ArrayBuffer, key, NativeArrayBuffer$1[key]);
        }
      }

      ArrayBufferPrototype.constructor = $ArrayBuffer;
    } else if (INCORRECT_ARRAY_BUFFER_NAME && CONFIGURABLE_FUNCTION_NAME) {
      createNonEnumerableProperty(NativeArrayBuffer$1, 'name', ARRAY_BUFFER$1);
    }

    // WebKit bug - the same parent prototype for typed arrays and data view
    if (objectSetPrototypeOf && objectGetPrototypeOf(DataViewPrototype) !== ObjectPrototype$1) {
      objectSetPrototypeOf(DataViewPrototype, ObjectPrototype$1);
    }

    // iOS Safari 7.x bug
    var testView = new $DataView(new $ArrayBuffer(2));
    var $setInt8 = functionUncurryThis(DataViewPrototype.setInt8);
    testView.setInt8(0, 2147483648);
    testView.setInt8(1, 2147483649);
    if (testView.getInt8(0) || !testView.getInt8(1)) defineBuiltIns(DataViewPrototype, {
      setInt8: function setInt8(byteOffset, value) {
        $setInt8(this, byteOffset, value << 24 >> 24);
      },
      setUint8: function setUint8(byteOffset, value) {
        $setInt8(this, byteOffset, value << 24 >> 24);
      }
    }, { unsafe: true });
  }

  setToStringTag($ArrayBuffer, ARRAY_BUFFER$1);
  setToStringTag($DataView, DATA_VIEW);

  var arrayBuffer = {
    ArrayBuffer: $ArrayBuffer,
    DataView: $DataView
  };

  var floor$6 = Math.floor;

  // `IsIntegralNumber` abstract operation
  // https://tc39.es/ecma262/#sec-isintegralnumber
  // eslint-disable-next-line es/no-number-isinteger -- safe
  var isIntegralNumber = Number.isInteger || function isInteger(it) {
    return !isObject(it) && isFinite(it) && floor$6(it) === it;
  };

  var $RangeError$4 = RangeError;

  var toPositiveInteger = function (it) {
    var result = toIntegerOrInfinity(it);
    if (result < 0) throw $RangeError$4("The argument can't be less than 0");
    return result;
  };

  var $RangeError$3 = RangeError;

  var toOffset = function (it, BYTES) {
    var offset = toPositiveInteger(it);
    if (offset % BYTES) throw $RangeError$3('Wrong offset');
    return offset;
  };

  var round = Math.round;

  var toUint8Clamped = function (it) {
    var value = round(it);
    return value < 0 ? 0 : value > 0xFF ? 0xFF : value & 0xFF;
  };

  var isBigIntArray = function (it) {
    var klass = classof(it);
    return klass == 'BigInt64Array' || klass == 'BigUint64Array';
  };

  var $TypeError$4 = TypeError;

  // `ToBigInt` abstract operation
  // https://tc39.es/ecma262/#sec-tobigint
  var toBigInt = function (argument) {
    var prim = toPrimitive(argument, 'number');
    if (typeof prim == 'number') throw $TypeError$4("Can't convert number to bigint");
    // eslint-disable-next-line es/no-bigint -- safe
    return BigInt(prim);
  };

  var aTypedArrayConstructor$1 = arrayBufferViewCore.aTypedArrayConstructor;


  var typedArrayFrom = function from(source /* , mapfn, thisArg */) {
    var C = aConstructor(this);
    var O = toObject(source);
    var argumentsLength = arguments.length;
    var mapfn = argumentsLength > 1 ? arguments[1] : undefined;
    var mapping = mapfn !== undefined;
    var iteratorMethod = getIteratorMethod(O);
    var i, length, result, thisIsBigIntArray, value, step, iterator, next;
    if (iteratorMethod && !isArrayIteratorMethod(iteratorMethod)) {
      iterator = getIterator(O, iteratorMethod);
      next = iterator.next;
      O = [];
      while (!(step = functionCall(next, iterator)).done) {
        O.push(step.value);
      }
    }
    if (mapping && argumentsLength > 2) {
      mapfn = functionBindContext(mapfn, arguments[2]);
    }
    length = lengthOfArrayLike(O);
    result = new (aTypedArrayConstructor$1(C))(length);
    thisIsBigIntArray = isBigIntArray(result);
    for (i = 0; length > i; i++) {
      value = mapping ? mapfn(O[i], i) : O[i];
      // FF30- typed arrays doesn't properly convert objects to typed array values
      result[i] = thisIsBigIntArray ? toBigInt(value) : +value;
    }
    return result;
  };

  var typedArrayConstructor = createCommonjsModule(function (module) {























  var getOwnPropertyNames = objectGetOwnPropertyNames.f;

  var forEach = arrayIteration.forEach;







  var getInternalState = internalState.get;
  var setInternalState = internalState.set;
  var enforceInternalState = internalState.enforce;
  var nativeDefineProperty = objectDefineProperty.f;
  var nativeGetOwnPropertyDescriptor = objectGetOwnPropertyDescriptor.f;
  var RangeError = global_1.RangeError;
  var ArrayBuffer = arrayBuffer.ArrayBuffer;
  var ArrayBufferPrototype = ArrayBuffer.prototype;
  var DataView = arrayBuffer.DataView;
  var NATIVE_ARRAY_BUFFER_VIEWS = arrayBufferViewCore.NATIVE_ARRAY_BUFFER_VIEWS;
  var TYPED_ARRAY_TAG = arrayBufferViewCore.TYPED_ARRAY_TAG;
  var TypedArray = arrayBufferViewCore.TypedArray;
  var TypedArrayPrototype = arrayBufferViewCore.TypedArrayPrototype;
  var aTypedArrayConstructor = arrayBufferViewCore.aTypedArrayConstructor;
  var isTypedArray = arrayBufferViewCore.isTypedArray;
  var BYTES_PER_ELEMENT = 'BYTES_PER_ELEMENT';
  var WRONG_LENGTH = 'Wrong length';

  var fromList = function (C, list) {
    aTypedArrayConstructor(C);
    var index = 0;
    var length = list.length;
    var result = new C(length);
    while (length > index) result[index] = list[index++];
    return result;
  };

  var addGetter = function (it, key) {
    defineBuiltInAccessor(it, key, {
      configurable: true,
      get: function () {
        return getInternalState(this)[key];
      }
    });
  };

  var isArrayBuffer = function (it) {
    var klass;
    return objectIsPrototypeOf(ArrayBufferPrototype, it) || (klass = classof(it)) == 'ArrayBuffer' || klass == 'SharedArrayBuffer';
  };

  var isTypedArrayIndex = function (target, key) {
    return isTypedArray(target)
      && !isSymbol(key)
      && key in target
      && isIntegralNumber(+key)
      && key >= 0;
  };

  var wrappedGetOwnPropertyDescriptor = function getOwnPropertyDescriptor(target, key) {
    key = toPropertyKey(key);
    return isTypedArrayIndex(target, key)
      ? createPropertyDescriptor(2, target[key])
      : nativeGetOwnPropertyDescriptor(target, key);
  };

  var wrappedDefineProperty = function defineProperty(target, key, descriptor) {
    key = toPropertyKey(key);
    if (isTypedArrayIndex(target, key)
      && isObject(descriptor)
      && hasOwnProperty_1(descriptor, 'value')
      && !hasOwnProperty_1(descriptor, 'get')
      && !hasOwnProperty_1(descriptor, 'set')
      // TODO: add validation descriptor w/o calling accessors
      && !descriptor.configurable
      && (!hasOwnProperty_1(descriptor, 'writable') || descriptor.writable)
      && (!hasOwnProperty_1(descriptor, 'enumerable') || descriptor.enumerable)
    ) {
      target[key] = descriptor.value;
      return target;
    } return nativeDefineProperty(target, key, descriptor);
  };

  if (descriptors) {
    if (!NATIVE_ARRAY_BUFFER_VIEWS) {
      objectGetOwnPropertyDescriptor.f = wrappedGetOwnPropertyDescriptor;
      objectDefineProperty.f = wrappedDefineProperty;
      addGetter(TypedArrayPrototype, 'buffer');
      addGetter(TypedArrayPrototype, 'byteOffset');
      addGetter(TypedArrayPrototype, 'byteLength');
      addGetter(TypedArrayPrototype, 'length');
    }

    _export({ target: 'Object', stat: true, forced: !NATIVE_ARRAY_BUFFER_VIEWS }, {
      getOwnPropertyDescriptor: wrappedGetOwnPropertyDescriptor,
      defineProperty: wrappedDefineProperty
    });

    module.exports = function (TYPE, wrapper, CLAMPED) {
      var BYTES = TYPE.match(/\d+/)[0] / 8;
      var CONSTRUCTOR_NAME = TYPE + (CLAMPED ? 'Clamped' : '') + 'Array';
      var GETTER = 'get' + TYPE;
      var SETTER = 'set' + TYPE;
      var NativeTypedArrayConstructor = global_1[CONSTRUCTOR_NAME];
      var TypedArrayConstructor = NativeTypedArrayConstructor;
      var TypedArrayConstructorPrototype = TypedArrayConstructor && TypedArrayConstructor.prototype;
      var exported = {};

      var getter = function (that, index) {
        var data = getInternalState(that);
        return data.view[GETTER](index * BYTES + data.byteOffset, true);
      };

      var setter = function (that, index, value) {
        var data = getInternalState(that);
        data.view[SETTER](index * BYTES + data.byteOffset, CLAMPED ? toUint8Clamped(value) : value, true);
      };

      var addElement = function (that, index) {
        nativeDefineProperty(that, index, {
          get: function () {
            return getter(this, index);
          },
          set: function (value) {
            return setter(this, index, value);
          },
          enumerable: true
        });
      };

      if (!NATIVE_ARRAY_BUFFER_VIEWS) {
        TypedArrayConstructor = wrapper(function (that, data, offset, $length) {
          anInstance(that, TypedArrayConstructorPrototype);
          var index = 0;
          var byteOffset = 0;
          var buffer, byteLength, length;
          if (!isObject(data)) {
            length = toIndex(data);
            byteLength = length * BYTES;
            buffer = new ArrayBuffer(byteLength);
          } else if (isArrayBuffer(data)) {
            buffer = data;
            byteOffset = toOffset(offset, BYTES);
            var $len = data.byteLength;
            if ($length === undefined) {
              if ($len % BYTES) throw RangeError(WRONG_LENGTH);
              byteLength = $len - byteOffset;
              if (byteLength < 0) throw RangeError(WRONG_LENGTH);
            } else {
              byteLength = toLength($length) * BYTES;
              if (byteLength + byteOffset > $len) throw RangeError(WRONG_LENGTH);
            }
            length = byteLength / BYTES;
          } else if (isTypedArray(data)) {
            return fromList(TypedArrayConstructor, data);
          } else {
            return functionCall(typedArrayFrom, TypedArrayConstructor, data);
          }
          setInternalState(that, {
            buffer: buffer,
            byteOffset: byteOffset,
            byteLength: byteLength,
            length: length,
            view: new DataView(buffer)
          });
          while (index < length) addElement(that, index++);
        });

        if (objectSetPrototypeOf) objectSetPrototypeOf(TypedArrayConstructor, TypedArray);
        TypedArrayConstructorPrototype = TypedArrayConstructor.prototype = objectCreate(TypedArrayPrototype);
      } else if (typedArrayConstructorsRequireWrappers) {
        TypedArrayConstructor = wrapper(function (dummy, data, typedArrayOffset, $length) {
          anInstance(dummy, TypedArrayConstructorPrototype);
          return inheritIfRequired(function () {
            if (!isObject(data)) return new NativeTypedArrayConstructor(toIndex(data));
            if (isArrayBuffer(data)) return $length !== undefined
              ? new NativeTypedArrayConstructor(data, toOffset(typedArrayOffset, BYTES), $length)
              : typedArrayOffset !== undefined
                ? new NativeTypedArrayConstructor(data, toOffset(typedArrayOffset, BYTES))
                : new NativeTypedArrayConstructor(data);
            if (isTypedArray(data)) return fromList(TypedArrayConstructor, data);
            return functionCall(typedArrayFrom, TypedArrayConstructor, data);
          }(), dummy, TypedArrayConstructor);
        });

        if (objectSetPrototypeOf) objectSetPrototypeOf(TypedArrayConstructor, TypedArray);
        forEach(getOwnPropertyNames(NativeTypedArrayConstructor), function (key) {
          if (!(key in TypedArrayConstructor)) {
            createNonEnumerableProperty(TypedArrayConstructor, key, NativeTypedArrayConstructor[key]);
          }
        });
        TypedArrayConstructor.prototype = TypedArrayConstructorPrototype;
      }

      if (TypedArrayConstructorPrototype.constructor !== TypedArrayConstructor) {
        createNonEnumerableProperty(TypedArrayConstructorPrototype, 'constructor', TypedArrayConstructor);
      }

      enforceInternalState(TypedArrayConstructorPrototype).TypedArrayConstructor = TypedArrayConstructor;

      if (TYPED_ARRAY_TAG) {
        createNonEnumerableProperty(TypedArrayConstructorPrototype, TYPED_ARRAY_TAG, CONSTRUCTOR_NAME);
      }

      var FORCED = TypedArrayConstructor != NativeTypedArrayConstructor;

      exported[CONSTRUCTOR_NAME] = TypedArrayConstructor;

      _export({ global: true, constructor: true, forced: FORCED, sham: !NATIVE_ARRAY_BUFFER_VIEWS }, exported);

      if (!(BYTES_PER_ELEMENT in TypedArrayConstructor)) {
        createNonEnumerableProperty(TypedArrayConstructor, BYTES_PER_ELEMENT, BYTES);
      }

      if (!(BYTES_PER_ELEMENT in TypedArrayConstructorPrototype)) {
        createNonEnumerableProperty(TypedArrayConstructorPrototype, BYTES_PER_ELEMENT, BYTES);
      }

      setSpecies(CONSTRUCTOR_NAME);
    };
  } else module.exports = function () { /* empty */ };
  });

  // `Int32Array` constructor
  // https://tc39.es/ecma262/#sec-typedarray-objects
  typedArrayConstructor('Int32', function (init) {
    return function Int32Array(data, byteOffset, length) {
      return init(this, data, byteOffset, length);
    };
  });

  var $TypeError$3 = TypeError;

  var deletePropertyOrThrow = function (O, P) {
    if (!delete O[P]) throw $TypeError$3('Cannot delete property ' + tryToString(P) + ' of ' + tryToString(O));
  };

  var min$4 = Math.min;

  // `Array.prototype.copyWithin` method implementation
  // https://tc39.es/ecma262/#sec-array.prototype.copywithin
  // eslint-disable-next-line es/no-array-prototype-copywithin -- safe
  var arrayCopyWithin = [].copyWithin || function copyWithin(target /* = 0 */, start /* = 0, end = @length */) {
    var O = toObject(this);
    var len = lengthOfArrayLike(O);
    var to = toAbsoluteIndex(target, len);
    var from = toAbsoluteIndex(start, len);
    var end = arguments.length > 2 ? arguments[2] : undefined;
    var count = min$4((end === undefined ? len : toAbsoluteIndex(end, len)) - from, len - to);
    var inc = 1;
    if (from < to && to < from + count) {
      inc = -1;
      from += count - 1;
      to += count - 1;
    }
    while (count-- > 0) {
      if (from in O) O[to] = O[from];
      else deletePropertyOrThrow(O, to);
      to += inc;
      from += inc;
    } return O;
  };

  var u$ArrayCopyWithin = functionUncurryThis(arrayCopyWithin);
  var aTypedArray$l = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$m = arrayBufferViewCore.exportTypedArrayMethod;

  // `%TypedArray%.prototype.copyWithin` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.copywithin
  exportTypedArrayMethod$m('copyWithin', function copyWithin(target, start /* , end */) {
    return u$ArrayCopyWithin(aTypedArray$l(this), target, start, arguments.length > 2 ? arguments[2] : undefined);
  });

  var $every = arrayIteration.every;

  var aTypedArray$k = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$l = arrayBufferViewCore.exportTypedArrayMethod;

  // `%TypedArray%.prototype.every` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.every
  exportTypedArrayMethod$l('every', function every(callbackfn /* , thisArg */) {
    return $every(aTypedArray$k(this), callbackfn, arguments.length > 1 ? arguments[1] : undefined);
  });

  var aTypedArray$j = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$k = arrayBufferViewCore.exportTypedArrayMethod;
  var slice = functionUncurryThis(''.slice);

  // V8 ~ Chrome < 59, Safari < 14.1, FF < 55, Edge <=18
  var CONVERSION_BUG = fails(function () {
    var count = 0;
    // eslint-disable-next-line es/no-typed-arrays -- safe
    new Int8Array(2).fill({ valueOf: function () { return count++; } });
    return count !== 1;
  });

  // `%TypedArray%.prototype.fill` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.fill
  exportTypedArrayMethod$k('fill', function fill(value /* , start, end */) {
    var length = arguments.length;
    aTypedArray$j(this);
    var actualValue = slice(classof(this), 0, 3) === 'Big' ? toBigInt(value) : +value;
    return functionCall(arrayFill, this, actualValue, length > 1 ? arguments[1] : undefined, length > 2 ? arguments[2] : undefined);
  }, CONVERSION_BUG);

  var arrayFromConstructorAndList = function (Constructor, list) {
    var index = 0;
    var length = lengthOfArrayLike(list);
    var result = new Constructor(length);
    while (length > index) result[index] = list[index++];
    return result;
  };

  var aTypedArrayConstructor = arrayBufferViewCore.aTypedArrayConstructor;
  var getTypedArrayConstructor = arrayBufferViewCore.getTypedArrayConstructor;

  // a part of `TypedArraySpeciesCreate` abstract operation
  // https://tc39.es/ecma262/#typedarray-species-create
  var typedArraySpeciesConstructor = function (originalArray) {
    return aTypedArrayConstructor(speciesConstructor(originalArray, getTypedArrayConstructor(originalArray)));
  };

  var typedArrayFromSpeciesAndList = function (instance, list) {
    return arrayFromConstructorAndList(typedArraySpeciesConstructor(instance), list);
  };

  var $filter$1 = arrayIteration.filter;


  var aTypedArray$i = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$j = arrayBufferViewCore.exportTypedArrayMethod;

  // `%TypedArray%.prototype.filter` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.filter
  exportTypedArrayMethod$j('filter', function filter(callbackfn /* , thisArg */) {
    var list = $filter$1(aTypedArray$i(this), callbackfn, arguments.length > 1 ? arguments[1] : undefined);
    return typedArrayFromSpeciesAndList(this, list);
  });

  var $find = arrayIteration.find;

  var aTypedArray$h = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$i = arrayBufferViewCore.exportTypedArrayMethod;

  // `%TypedArray%.prototype.find` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.find
  exportTypedArrayMethod$i('find', function find(predicate /* , thisArg */) {
    return $find(aTypedArray$h(this), predicate, arguments.length > 1 ? arguments[1] : undefined);
  });

  var $findIndex = arrayIteration.findIndex;

  var aTypedArray$g = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$h = arrayBufferViewCore.exportTypedArrayMethod;

  // `%TypedArray%.prototype.findIndex` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.findindex
  exportTypedArrayMethod$h('findIndex', function findIndex(predicate /* , thisArg */) {
    return $findIndex(aTypedArray$g(this), predicate, arguments.length > 1 ? arguments[1] : undefined);
  });

  var $forEach$1 = arrayIteration.forEach;

  var aTypedArray$f = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$g = arrayBufferViewCore.exportTypedArrayMethod;

  // `%TypedArray%.prototype.forEach` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.foreach
  exportTypedArrayMethod$g('forEach', function forEach(callbackfn /* , thisArg */) {
    $forEach$1(aTypedArray$f(this), callbackfn, arguments.length > 1 ? arguments[1] : undefined);
  });

  var $includes$1 = arrayIncludes.includes;

  var aTypedArray$e = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$f = arrayBufferViewCore.exportTypedArrayMethod;

  // `%TypedArray%.prototype.includes` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.includes
  exportTypedArrayMethod$f('includes', function includes(searchElement /* , fromIndex */) {
    return $includes$1(aTypedArray$e(this), searchElement, arguments.length > 1 ? arguments[1] : undefined);
  });

  var $indexOf = arrayIncludes.indexOf;

  var aTypedArray$d = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$e = arrayBufferViewCore.exportTypedArrayMethod;

  // `%TypedArray%.prototype.indexOf` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.indexof
  exportTypedArrayMethod$e('indexOf', function indexOf(searchElement /* , fromIndex */) {
    return $indexOf(aTypedArray$d(this), searchElement, arguments.length > 1 ? arguments[1] : undefined);
  });

  var ITERATOR$2 = wellKnownSymbol('iterator');
  var Uint8Array$2 = global_1.Uint8Array;
  var arrayValues = functionUncurryThis(es_array_iterator.values);
  var arrayKeys = functionUncurryThis(es_array_iterator.keys);
  var arrayEntries = functionUncurryThis(es_array_iterator.entries);
  var aTypedArray$c = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$d = arrayBufferViewCore.exportTypedArrayMethod;
  var TypedArrayPrototype = Uint8Array$2 && Uint8Array$2.prototype;

  var GENERIC = !fails(function () {
    TypedArrayPrototype[ITERATOR$2].call([1]);
  });

  var ITERATOR_IS_VALUES = !!TypedArrayPrototype
    && TypedArrayPrototype.values
    && TypedArrayPrototype[ITERATOR$2] === TypedArrayPrototype.values
    && TypedArrayPrototype.values.name === 'values';

  var typedArrayValues = function values() {
    return arrayValues(aTypedArray$c(this));
  };

  // `%TypedArray%.prototype.entries` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.entries
  exportTypedArrayMethod$d('entries', function entries() {
    return arrayEntries(aTypedArray$c(this));
  }, GENERIC);
  // `%TypedArray%.prototype.keys` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.keys
  exportTypedArrayMethod$d('keys', function keys() {
    return arrayKeys(aTypedArray$c(this));
  }, GENERIC);
  // `%TypedArray%.prototype.values` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.values
  exportTypedArrayMethod$d('values', typedArrayValues, GENERIC || !ITERATOR_IS_VALUES, { name: 'values' });
  // `%TypedArray%.prototype[@@iterator]` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype-@@iterator
  exportTypedArrayMethod$d(ITERATOR$2, typedArrayValues, GENERIC || !ITERATOR_IS_VALUES, { name: 'values' });

  var aTypedArray$b = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$c = arrayBufferViewCore.exportTypedArrayMethod;
  var $join = functionUncurryThis([].join);

  // `%TypedArray%.prototype.join` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.join
  exportTypedArrayMethod$c('join', function join(separator) {
    return $join(aTypedArray$b(this), separator);
  });

  /* eslint-disable es/no-array-prototype-lastindexof -- safe */






  var min$3 = Math.min;
  var $lastIndexOf = [].lastIndexOf;
  var NEGATIVE_ZERO = !!$lastIndexOf && 1 / [1].lastIndexOf(1, -0) < 0;
  var STRICT_METHOD$1 = arrayMethodIsStrict('lastIndexOf');
  var FORCED$8 = NEGATIVE_ZERO || !STRICT_METHOD$1;

  // `Array.prototype.lastIndexOf` method implementation
  // https://tc39.es/ecma262/#sec-array.prototype.lastindexof
  var arrayLastIndexOf = FORCED$8 ? function lastIndexOf(searchElement /* , fromIndex = @[*-1] */) {
    // convert -0 to +0
    if (NEGATIVE_ZERO) return functionApply($lastIndexOf, this, arguments) || 0;
    var O = toIndexedObject(this);
    var length = lengthOfArrayLike(O);
    var index = length - 1;
    if (arguments.length > 1) index = min$3(index, toIntegerOrInfinity(arguments[1]));
    if (index < 0) index = length + index;
    for (;index >= 0; index--) if (index in O && O[index] === searchElement) return index || 0;
    return -1;
  } : $lastIndexOf;

  var aTypedArray$a = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$b = arrayBufferViewCore.exportTypedArrayMethod;

  // `%TypedArray%.prototype.lastIndexOf` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.lastindexof
  exportTypedArrayMethod$b('lastIndexOf', function lastIndexOf(searchElement /* , fromIndex */) {
    var length = arguments.length;
    return functionApply(arrayLastIndexOf, aTypedArray$a(this), length > 1 ? [searchElement, arguments[1]] : [searchElement]);
  });

  var $map$1 = arrayIteration.map;


  var aTypedArray$9 = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$a = arrayBufferViewCore.exportTypedArrayMethod;

  // `%TypedArray%.prototype.map` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.map
  exportTypedArrayMethod$a('map', function map(mapfn /* , thisArg */) {
    return $map$1(aTypedArray$9(this), mapfn, arguments.length > 1 ? arguments[1] : undefined, function (O, length) {
      return new (typedArraySpeciesConstructor(O))(length);
    });
  });

  var $TypeError$2 = TypeError;

  // `Array.prototype.{ reduce, reduceRight }` methods implementation
  var createMethod$1 = function (IS_RIGHT) {
    return function (that, callbackfn, argumentsLength, memo) {
      aCallable(callbackfn);
      var O = toObject(that);
      var self = indexedObject(O);
      var length = lengthOfArrayLike(O);
      var index = IS_RIGHT ? length - 1 : 0;
      var i = IS_RIGHT ? -1 : 1;
      if (argumentsLength < 2) while (true) {
        if (index in self) {
          memo = self[index];
          index += i;
          break;
        }
        index += i;
        if (IS_RIGHT ? index < 0 : length <= index) {
          throw $TypeError$2('Reduce of empty array with no initial value');
        }
      }
      for (;IS_RIGHT ? index >= 0 : length > index; index += i) if (index in self) {
        memo = callbackfn(memo, self[index], index, O);
      }
      return memo;
    };
  };

  var arrayReduce = {
    // `Array.prototype.reduce` method
    // https://tc39.es/ecma262/#sec-array.prototype.reduce
    left: createMethod$1(false),
    // `Array.prototype.reduceRight` method
    // https://tc39.es/ecma262/#sec-array.prototype.reduceright
    right: createMethod$1(true)
  };

  var $reduce$1 = arrayReduce.left;

  var aTypedArray$8 = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$9 = arrayBufferViewCore.exportTypedArrayMethod;

  // `%TypedArray%.prototype.reduce` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.reduce
  exportTypedArrayMethod$9('reduce', function reduce(callbackfn /* , initialValue */) {
    var length = arguments.length;
    return $reduce$1(aTypedArray$8(this), callbackfn, length, length > 1 ? arguments[1] : undefined);
  });

  var $reduceRight = arrayReduce.right;

  var aTypedArray$7 = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$8 = arrayBufferViewCore.exportTypedArrayMethod;

  // `%TypedArray%.prototype.reduceRight` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.reduceright
  exportTypedArrayMethod$8('reduceRight', function reduceRight(callbackfn /* , initialValue */) {
    var length = arguments.length;
    return $reduceRight(aTypedArray$7(this), callbackfn, length, length > 1 ? arguments[1] : undefined);
  });

  var aTypedArray$6 = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$7 = arrayBufferViewCore.exportTypedArrayMethod;
  var floor$5 = Math.floor;

  // `%TypedArray%.prototype.reverse` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.reverse
  exportTypedArrayMethod$7('reverse', function reverse() {
    var that = this;
    var length = aTypedArray$6(that).length;
    var middle = floor$5(length / 2);
    var index = 0;
    var value;
    while (index < middle) {
      value = that[index];
      that[index++] = that[--length];
      that[length] = value;
    } return that;
  });

  var RangeError$1 = global_1.RangeError;
  var Int8Array$2 = global_1.Int8Array;
  var Int8ArrayPrototype = Int8Array$2 && Int8Array$2.prototype;
  var $set = Int8ArrayPrototype && Int8ArrayPrototype.set;
  var aTypedArray$5 = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$6 = arrayBufferViewCore.exportTypedArrayMethod;

  var WORKS_WITH_OBJECTS_AND_GENERIC_ON_TYPED_ARRAYS = !fails(function () {
    // eslint-disable-next-line es/no-typed-arrays -- required for testing
    var array = new Uint8ClampedArray(2);
    functionCall($set, array, { length: 1, 0: 3 }, 1);
    return array[1] !== 3;
  });

  // https://bugs.chromium.org/p/v8/issues/detail?id=11294 and other
  var TO_OBJECT_BUG = WORKS_WITH_OBJECTS_AND_GENERIC_ON_TYPED_ARRAYS && arrayBufferViewCore.NATIVE_ARRAY_BUFFER_VIEWS && fails(function () {
    var array = new Int8Array$2(2);
    array.set(1);
    array.set('2', 1);
    return array[0] !== 0 || array[1] !== 2;
  });

  // `%TypedArray%.prototype.set` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.set
  exportTypedArrayMethod$6('set', function set(arrayLike /* , offset */) {
    aTypedArray$5(this);
    var offset = toOffset(arguments.length > 1 ? arguments[1] : undefined, 1);
    var src = toObject(arrayLike);
    if (WORKS_WITH_OBJECTS_AND_GENERIC_ON_TYPED_ARRAYS) return functionCall($set, this, src, offset);
    var length = this.length;
    var len = lengthOfArrayLike(src);
    var index = 0;
    if (len + offset > length) throw RangeError$1('Wrong length');
    while (index < len) this[offset + index] = src[index++];
  }, !WORKS_WITH_OBJECTS_AND_GENERIC_ON_TYPED_ARRAYS || TO_OBJECT_BUG);

  var aTypedArray$4 = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$5 = arrayBufferViewCore.exportTypedArrayMethod;

  var FORCED$7 = fails(function () {
    // eslint-disable-next-line es/no-typed-arrays -- required for testing
    new Int8Array(1).slice();
  });

  // `%TypedArray%.prototype.slice` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.slice
  exportTypedArrayMethod$5('slice', function slice(start, end) {
    var list = arraySlice(aTypedArray$4(this), start, end);
    var C = typedArraySpeciesConstructor(this);
    var index = 0;
    var length = list.length;
    var result = new C(length);
    while (length > index) result[index] = list[index++];
    return result;
  }, FORCED$7);

  var $some = arrayIteration.some;

  var aTypedArray$3 = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$4 = arrayBufferViewCore.exportTypedArrayMethod;

  // `%TypedArray%.prototype.some` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.some
  exportTypedArrayMethod$4('some', function some(callbackfn /* , thisArg */) {
    return $some(aTypedArray$3(this), callbackfn, arguments.length > 1 ? arguments[1] : undefined);
  });

  var floor$4 = Math.floor;

  var mergeSort = function (array, comparefn) {
    var length = array.length;
    var middle = floor$4(length / 2);
    return length < 8 ? insertionSort(array, comparefn) : merge(
      array,
      mergeSort(arraySliceSimple(array, 0, middle), comparefn),
      mergeSort(arraySliceSimple(array, middle), comparefn),
      comparefn
    );
  };

  var insertionSort = function (array, comparefn) {
    var length = array.length;
    var i = 1;
    var element, j;

    while (i < length) {
      j = i;
      element = array[i];
      while (j && comparefn(array[j - 1], element) > 0) {
        array[j] = array[--j];
      }
      if (j !== i++) array[j] = element;
    } return array;
  };

  var merge = function (array, left, right, comparefn) {
    var llength = left.length;
    var rlength = right.length;
    var lindex = 0;
    var rindex = 0;

    while (lindex < llength || rindex < rlength) {
      array[lindex + rindex] = (lindex < llength && rindex < rlength)
        ? comparefn(left[lindex], right[rindex]) <= 0 ? left[lindex++] : right[rindex++]
        : lindex < llength ? left[lindex++] : right[rindex++];
    } return array;
  };

  var arraySort = mergeSort;

  var firefox = engineUserAgent.match(/firefox\/(\d+)/i);

  var engineFfVersion = !!firefox && +firefox[1];

  var engineIsIeOrEdge = /MSIE|Trident/.test(engineUserAgent);

  var webkit = engineUserAgent.match(/AppleWebKit\/(\d+)\./);

  var engineWebkitVersion = !!webkit && +webkit[1];

  var aTypedArray$2 = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$3 = arrayBufferViewCore.exportTypedArrayMethod;
  var Uint16Array = global_1.Uint16Array;
  var nativeSort$1 = Uint16Array && functionUncurryThisClause(Uint16Array.prototype.sort);

  // WebKit
  var ACCEPT_INCORRECT_ARGUMENTS = !!nativeSort$1 && !(fails(function () {
    nativeSort$1(new Uint16Array(2), null);
  }) && fails(function () {
    nativeSort$1(new Uint16Array(2), {});
  }));

  var STABLE_SORT$1 = !!nativeSort$1 && !fails(function () {
    // feature detection can be too slow, so check engines versions
    if (engineV8Version) return engineV8Version < 74;
    if (engineFfVersion) return engineFfVersion < 67;
    if (engineIsIeOrEdge) return true;
    if (engineWebkitVersion) return engineWebkitVersion < 602;

    var array = new Uint16Array(516);
    var expected = Array(516);
    var index, mod;

    for (index = 0; index < 516; index++) {
      mod = index % 4;
      array[index] = 515 - index;
      expected[index] = index - 2 * mod + 3;
    }

    nativeSort$1(array, function (a, b) {
      return (a / 4 | 0) - (b / 4 | 0);
    });

    for (index = 0; index < 516; index++) {
      if (array[index] !== expected[index]) return true;
    }
  });

  var getSortCompare$1 = function (comparefn) {
    return function (x, y) {
      if (comparefn !== undefined) return +comparefn(x, y) || 0;
      // eslint-disable-next-line no-self-compare -- NaN check
      if (y !== y) return -1;
      // eslint-disable-next-line no-self-compare -- NaN check
      if (x !== x) return 1;
      if (x === 0 && y === 0) return 1 / x > 0 && 1 / y < 0 ? 1 : -1;
      return x > y;
    };
  };

  // `%TypedArray%.prototype.sort` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.sort
  exportTypedArrayMethod$3('sort', function sort(comparefn) {
    if (comparefn !== undefined) aCallable(comparefn);
    if (STABLE_SORT$1) return nativeSort$1(this, comparefn);

    return arraySort(aTypedArray$2(this), getSortCompare$1(comparefn));
  }, !STABLE_SORT$1 || ACCEPT_INCORRECT_ARGUMENTS);

  var aTypedArray$1 = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$2 = arrayBufferViewCore.exportTypedArrayMethod;

  // `%TypedArray%.prototype.subarray` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.subarray
  exportTypedArrayMethod$2('subarray', function subarray(begin, end) {
    var O = aTypedArray$1(this);
    var length = O.length;
    var beginIndex = toAbsoluteIndex(begin, length);
    var C = typedArraySpeciesConstructor(O);
    return new C(
      O.buffer,
      O.byteOffset + beginIndex * O.BYTES_PER_ELEMENT,
      toLength((end === undefined ? length : toAbsoluteIndex(end, length)) - beginIndex)
    );
  });

  var Int8Array$1 = global_1.Int8Array;
  var aTypedArray = arrayBufferViewCore.aTypedArray;
  var exportTypedArrayMethod$1 = arrayBufferViewCore.exportTypedArrayMethod;
  var $toLocaleString = [].toLocaleString;

  // iOS Safari 6.x fails here
  var TO_LOCALE_STRING_BUG = !!Int8Array$1 && fails(function () {
    $toLocaleString.call(new Int8Array$1(1));
  });

  var FORCED$6 = fails(function () {
    return [1, 2].toLocaleString() != new Int8Array$1([1, 2]).toLocaleString();
  }) || !fails(function () {
    Int8Array$1.prototype.toLocaleString.call([1, 2]);
  });

  // `%TypedArray%.prototype.toLocaleString` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.tolocalestring
  exportTypedArrayMethod$1('toLocaleString', function toLocaleString() {
    return functionApply(
      $toLocaleString,
      TO_LOCALE_STRING_BUG ? arraySlice(aTypedArray(this)) : aTypedArray(this),
      arraySlice(arguments)
    );
  }, FORCED$6);

  var exportTypedArrayMethod = arrayBufferViewCore.exportTypedArrayMethod;




  var Uint8Array$1 = global_1.Uint8Array;
  var Uint8ArrayPrototype = Uint8Array$1 && Uint8Array$1.prototype || {};
  var arrayToString = [].toString;
  var join$3 = functionUncurryThis([].join);

  if (fails(function () { arrayToString.call({}); })) {
    arrayToString = function toString() {
      return join$3(this);
    };
  }

  var IS_NOT_ARRAY_METHOD = Uint8ArrayPrototype.toString != arrayToString;

  // `%TypedArray%.prototype.toString` method
  // https://tc39.es/ecma262/#sec-%typedarray%.prototype.tostring
  exportTypedArrayMethod('toString', arrayToString, IS_NOT_ARRAY_METHOD);

  var test = [];
  var nativeSort = functionUncurryThis(test.sort);
  var push$6 = functionUncurryThis(test.push);

  // IE8-
  var FAILS_ON_UNDEFINED = fails(function () {
    test.sort(undefined);
  });
  // V8 bug
  var FAILS_ON_NULL = fails(function () {
    test.sort(null);
  });
  // Old WebKit
  var STRICT_METHOD = arrayMethodIsStrict('sort');

  var STABLE_SORT = !fails(function () {
    // feature detection can be too slow, so check engines versions
    if (engineV8Version) return engineV8Version < 70;
    if (engineFfVersion && engineFfVersion > 3) return;
    if (engineIsIeOrEdge) return true;
    if (engineWebkitVersion) return engineWebkitVersion < 603;

    var result = '';
    var code, chr, value, index;

    // generate an array with more 512 elements (Chakra and old V8 fails only in this case)
    for (code = 65; code < 76; code++) {
      chr = String.fromCharCode(code);

      switch (code) {
        case 66: case 69: case 70: case 72: value = 3; break;
        case 68: case 71: value = 4; break;
        default: value = 2;
      }

      for (index = 0; index < 47; index++) {
        test.push({ k: chr + index, v: value });
      }
    }

    test.sort(function (a, b) { return b.v - a.v; });

    for (index = 0; index < test.length; index++) {
      chr = test[index].k.charAt(0);
      if (result.charAt(result.length - 1) !== chr) result += chr;
    }

    return result !== 'DGBEFHACIJK';
  });

  var FORCED$5 = FAILS_ON_UNDEFINED || !FAILS_ON_NULL || !STRICT_METHOD || !STABLE_SORT;

  var getSortCompare = function (comparefn) {
    return function (x, y) {
      if (y === undefined) return -1;
      if (x === undefined) return 1;
      if (comparefn !== undefined) return +comparefn(x, y) || 0;
      return toString_1(x) > toString_1(y) ? 1 : -1;
    };
  };

  // `Array.prototype.sort` method
  // https://tc39.es/ecma262/#sec-array.prototype.sort
  _export({ target: 'Array', proto: true, forced: FORCED$5 }, {
    sort: function sort(comparefn) {
      if (comparefn !== undefined) aCallable(comparefn);

      var array = toObject(this);

      if (STABLE_SORT) return comparefn === undefined ? nativeSort(array) : nativeSort(array, comparefn);

      var items = [];
      var arrayLength = lengthOfArrayLike(array);
      var itemsLength, index;

      for (index = 0; index < arrayLength; index++) {
        if (index in array) push$6(items, array[index]);
      }

      arraySort(items, getSortCompare(comparefn));

      itemsLength = lengthOfArrayLike(items);
      index = 0;

      while (index < itemsLength) array[index] = items[index++];
      while (index < arrayLength) deletePropertyOrThrow(array, index++);

      return array;
    }
  });

  // `Object.getOwnPropertyDescriptors` method
  // https://tc39.es/ecma262/#sec-object.getownpropertydescriptors
  _export({ target: 'Object', stat: true, sham: !descriptors }, {
    getOwnPropertyDescriptors: function getOwnPropertyDescriptors(object) {
      var O = toIndexedObject(object);
      var getOwnPropertyDescriptor = objectGetOwnPropertyDescriptor.f;
      var keys = ownKeys(O);
      var result = {};
      var index = 0;
      var key, descriptor;
      while (keys.length > index) {
        descriptor = getOwnPropertyDescriptor(O, key = keys[index++]);
        if (descriptor !== undefined) createProperty(result, key, descriptor);
      }
      return result;
    }
  });

  // call something on iterator step with safe closing on error
  var callWithSafeIterationClosing = function (iterator, fn, value, ENTRIES) {
    try {
      return ENTRIES ? fn(anObject(value)[0], value[1]) : fn(value);
    } catch (error) {
      iteratorClose(iterator, 'throw', error);
    }
  };

  var $Array = Array;

  // `Array.from` method implementation
  // https://tc39.es/ecma262/#sec-array.from
  var arrayFrom = function from(arrayLike /* , mapfn = undefined, thisArg = undefined */) {
    var O = toObject(arrayLike);
    var IS_CONSTRUCTOR = isConstructor(this);
    var argumentsLength = arguments.length;
    var mapfn = argumentsLength > 1 ? arguments[1] : undefined;
    var mapping = mapfn !== undefined;
    if (mapping) mapfn = functionBindContext(mapfn, argumentsLength > 2 ? arguments[2] : undefined);
    var iteratorMethod = getIteratorMethod(O);
    var index = 0;
    var length, result, step, iterator, next, value;
    // if the target is not iterable or it's an array with the default iterator - use a simple case
    if (iteratorMethod && !(this === $Array && isArrayIteratorMethod(iteratorMethod))) {
      iterator = getIterator(O, iteratorMethod);
      next = iterator.next;
      result = IS_CONSTRUCTOR ? new this() : [];
      for (;!(step = functionCall(next, iterator)).done; index++) {
        value = mapping ? callWithSafeIterationClosing(iterator, mapfn, [step.value, index], true) : step.value;
        createProperty(result, index, value);
      }
    } else {
      length = lengthOfArrayLike(O);
      result = IS_CONSTRUCTOR ? new this(length) : $Array(length);
      for (;length > index; index++) {
        value = mapping ? mapfn(O[index], index) : O[index];
        createProperty(result, index, value);
      }
    }
    result.length = index;
    return result;
  };

  var INCORRECT_ITERATION = !checkCorrectnessOfIteration(function (iterable) {
    // eslint-disable-next-line es/no-array-from -- required for testing
    Array.from(iterable);
  });

  // `Array.from` method
  // https://tc39.es/ecma262/#sec-array.from
  _export({ target: 'Array', stat: true, forced: INCORRECT_ITERATION }, {
    from: arrayFrom
  });

  var $TypeError$1 = TypeError;
  // eslint-disable-next-line es/no-object-getownpropertydescriptor -- safe
  var getOwnPropertyDescriptor$3 = Object.getOwnPropertyDescriptor;

  // Safari < 13 does not throw an error in this case
  var SILENT_ON_NON_WRITABLE_LENGTH_SET = descriptors && !function () {
    // makes no sense without proper strict mode support
    if (this !== undefined) return true;
    try {
      // eslint-disable-next-line es/no-object-defineproperty -- safe
      Object.defineProperty([], 'length', { writable: false }).length = 1;
    } catch (error) {
      return error instanceof TypeError;
    }
  }();

  var arraySetLength = SILENT_ON_NON_WRITABLE_LENGTH_SET ? function (O, length) {
    if (isArray$1(O) && !getOwnPropertyDescriptor$3(O, 'length').writable) {
      throw $TypeError$1('Cannot set read only .length');
    } return O.length = length;
  } : function (O, length) {
    return O.length = length;
  };

  var HAS_SPECIES_SUPPORT$2 = arrayMethodHasSpeciesSupport('splice');

  var max$1 = Math.max;
  var min$2 = Math.min;

  // `Array.prototype.splice` method
  // https://tc39.es/ecma262/#sec-array.prototype.splice
  // with adding support of @@species
  _export({ target: 'Array', proto: true, forced: !HAS_SPECIES_SUPPORT$2 }, {
    splice: function splice(start, deleteCount /* , ...items */) {
      var O = toObject(this);
      var len = lengthOfArrayLike(O);
      var actualStart = toAbsoluteIndex(start, len);
      var argumentsLength = arguments.length;
      var insertCount, actualDeleteCount, A, k, from, to;
      if (argumentsLength === 0) {
        insertCount = actualDeleteCount = 0;
      } else if (argumentsLength === 1) {
        insertCount = 0;
        actualDeleteCount = len - actualStart;
      } else {
        insertCount = argumentsLength - 2;
        actualDeleteCount = min$2(max$1(toIntegerOrInfinity(deleteCount), 0), len - actualStart);
      }
      doesNotExceedSafeInteger(len + insertCount - actualDeleteCount);
      A = arraySpeciesCreate(O, actualDeleteCount);
      for (k = 0; k < actualDeleteCount; k++) {
        from = actualStart + k;
        if (from in O) createProperty(A, k, O[from]);
      }
      A.length = actualDeleteCount;
      if (insertCount < actualDeleteCount) {
        for (k = actualStart; k < len - actualDeleteCount; k++) {
          from = k + actualDeleteCount;
          to = k + insertCount;
          if (from in O) O[to] = O[from];
          else deletePropertyOrThrow(O, to);
        }
        for (k = len; k > len - actualDeleteCount + insertCount; k--) deletePropertyOrThrow(O, k - 1);
      } else if (insertCount > actualDeleteCount) {
        for (k = len - actualDeleteCount; k > actualStart; k--) {
          from = k + actualDeleteCount - 1;
          to = k + insertCount - 1;
          if (from in O) O[to] = O[from];
          else deletePropertyOrThrow(O, to);
        }
      }
      for (k = 0; k < insertCount; k++) {
        O[k + actualStart] = arguments[k + 2];
      }
      arraySetLength(O, len - actualDeleteCount + insertCount);
      return A;
    }
  });

  var MATCH$1 = wellKnownSymbol('match');

  // `IsRegExp` abstract operation
  // https://tc39.es/ecma262/#sec-isregexp
  var isRegexp = function (it) {
    var isRegExp;
    return isObject(it) && ((isRegExp = it[MATCH$1]) !== undefined ? !!isRegExp : classofRaw(it) == 'RegExp');
  };

  var $TypeError = TypeError;

  var notARegexp = function (it) {
    if (isRegexp(it)) {
      throw $TypeError("The method doesn't accept regular expressions");
    } return it;
  };

  var MATCH = wellKnownSymbol('match');

  var correctIsRegexpLogic = function (METHOD_NAME) {
    var regexp = /./;
    try {
      '/./'[METHOD_NAME](regexp);
    } catch (error1) {
      try {
        regexp[MATCH] = false;
        return '/./'[METHOD_NAME](regexp);
      } catch (error2) { /* empty */ }
    } return false;
  };

  var getOwnPropertyDescriptor$2 = objectGetOwnPropertyDescriptor.f;







  // eslint-disable-next-line es/no-string-prototype-startswith -- safe
  var nativeStartsWith = functionUncurryThisClause(''.startsWith);
  var stringSlice$7 = functionUncurryThisClause(''.slice);
  var min$1 = Math.min;

  var CORRECT_IS_REGEXP_LOGIC = correctIsRegexpLogic('startsWith');
  // https://github.com/zloirock/core-js/pull/702
  var MDN_POLYFILL_BUG = !CORRECT_IS_REGEXP_LOGIC && !!function () {
    var descriptor = getOwnPropertyDescriptor$2(String.prototype, 'startsWith');
    return descriptor && !descriptor.writable;
  }();

  // `String.prototype.startsWith` method
  // https://tc39.es/ecma262/#sec-string.prototype.startswith
  _export({ target: 'String', proto: true, forced: !MDN_POLYFILL_BUG && !CORRECT_IS_REGEXP_LOGIC }, {
    startsWith: function startsWith(searchString /* , position = 0 */) {
      var that = toString_1(requireObjectCoercible(this));
      notARegexp(searchString);
      var index = toLength(min$1(arguments.length > 1 ? arguments[1] : undefined, that.length));
      var search = toString_1(searchString);
      return nativeStartsWith
        ? nativeStartsWith(that, search, index)
        : stringSlice$7(that, index, index + search.length) === search;
    }
  });

  // a string of all valid unicode whitespaces
  var whitespaces = '\u0009\u000A\u000B\u000C\u000D\u0020\u00A0\u1680\u2000\u2001\u2002' +
    '\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\u2028\u2029\uFEFF';

  var replace$7 = functionUncurryThis(''.replace);
  var ltrim = RegExp('^[' + whitespaces + ']+');
  var rtrim = RegExp('(^|[^' + whitespaces + '])[' + whitespaces + ']+$');

  // `String.prototype.{ trim, trimStart, trimEnd, trimLeft, trimRight }` methods implementation
  var createMethod = function (TYPE) {
    return function ($this) {
      var string = toString_1(requireObjectCoercible($this));
      if (TYPE & 1) string = replace$7(string, ltrim, '');
      if (TYPE & 2) string = replace$7(string, rtrim, '$1');
      return string;
    };
  };

  var stringTrim = {
    // `String.prototype.{ trimLeft, trimStart }` methods
    // https://tc39.es/ecma262/#sec-string.prototype.trimstart
    start: createMethod(1),
    // `String.prototype.{ trimRight, trimEnd }` methods
    // https://tc39.es/ecma262/#sec-string.prototype.trimend
    end: createMethod(2),
    // `String.prototype.trim` method
    // https://tc39.es/ecma262/#sec-string.prototype.trim
    trim: createMethod(3)
  };

  var PROPER_FUNCTION_NAME = functionName.PROPER;



  var non = '\u200B\u0085\u180E';

  // check that a method works with the correct list
  // of whitespaces and has a correct name
  var stringTrimForced = function (METHOD_NAME) {
    return fails(function () {
      return !!whitespaces[METHOD_NAME]()
        || non[METHOD_NAME]() !== non
        || (PROPER_FUNCTION_NAME && whitespaces[METHOD_NAME].name !== METHOD_NAME);
    });
  };

  var $trim = stringTrim.trim;


  // `String.prototype.trim` method
  // https://tc39.es/ecma262/#sec-string.prototype.trim
  _export({ target: 'String', proto: true, forced: stringTrimForced('trim') }, {
    trim: function trim() {
      return $trim(this);
    }
  });

  var $map = arrayIteration.map;


  var HAS_SPECIES_SUPPORT$1 = arrayMethodHasSpeciesSupport('map');

  // `Array.prototype.map` method
  // https://tc39.es/ecma262/#sec-array.prototype.map
  // with adding support of @@species
  _export({ target: 'Array', proto: true, forced: !HAS_SPECIES_SUPPORT$1 }, {
    map: function map(callbackfn /* , thisArg */) {
      return $map(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
    }
  });

  var $filter = arrayIteration.filter;


  var HAS_SPECIES_SUPPORT = arrayMethodHasSpeciesSupport('filter');

  // `Array.prototype.filter` method
  // https://tc39.es/ecma262/#sec-array.prototype.filter
  // with adding support of @@species
  _export({ target: 'Array', proto: true, forced: !HAS_SPECIES_SUPPORT }, {
    filter: function filter(callbackfn /* , thisArg */) {
      return $filter(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
    }
  });

  var nativeJoin = functionUncurryThis([].join);

  var ES3_STRINGS = indexedObject != Object;
  var FORCED$4 = ES3_STRINGS || !arrayMethodIsStrict('join', ',');

  // `Array.prototype.join` method
  // https://tc39.es/ecma262/#sec-array.prototype.join
  _export({ target: 'Array', proto: true, forced: FORCED$4 }, {
    join: function join(separator) {
      return nativeJoin(toIndexedObject(this), separator === undefined ? ',' : separator);
    }
  });

  var quot = /"/g;
  var replace$6 = functionUncurryThis(''.replace);

  // `CreateHTML` abstract operation
  // https://tc39.es/ecma262/#sec-createhtml
  var createHtml = function (string, tag, attribute, value) {
    var S = toString_1(requireObjectCoercible(string));
    var p1 = '<' + tag;
    if (attribute !== '') p1 += ' ' + attribute + '="' + replace$6(toString_1(value), quot, '&quot;') + '"';
    return p1 + '>' + S + '</' + tag + '>';
  };

  // check the existence of a method, lowercase
  // of a tag and escaping quotes in arguments
  var stringHtmlForced = function (METHOD_NAME) {
    return fails(function () {
      var test = ''[METHOD_NAME]('"');
      return test !== test.toLowerCase() || test.split('"').length > 3;
    });
  };

  // `String.prototype.anchor` method
  // https://tc39.es/ecma262/#sec-string.prototype.anchor
  _export({ target: 'String', proto: true, forced: stringHtmlForced('anchor') }, {
    anchor: function anchor(name) {
      return createHtml(this, 'a', 'name', name);
    }
  });

  // eslint-disable-next-line es/no-object-assign -- safe
  var $assign = Object.assign;
  // eslint-disable-next-line es/no-object-defineproperty -- required for testing
  var defineProperty$2 = Object.defineProperty;
  var concat$1 = functionUncurryThis([].concat);

  // `Object.assign` method
  // https://tc39.es/ecma262/#sec-object.assign
  var objectAssign = !$assign || fails(function () {
    // should have correct order of operations (Edge bug)
    if (descriptors && $assign({ b: 1 }, $assign(defineProperty$2({}, 'a', {
      enumerable: true,
      get: function () {
        defineProperty$2(this, 'b', {
          value: 3,
          enumerable: false
        });
      }
    }), { b: 2 })).b !== 1) return true;
    // should work with symbols and should have deterministic property order (V8 bug)
    var A = {};
    var B = {};
    // eslint-disable-next-line es/no-symbol -- safe
    var symbol = Symbol();
    var alphabet = 'abcdefghijklmnopqrst';
    A[symbol] = 7;
    alphabet.split('').forEach(function (chr) { B[chr] = chr; });
    return $assign({}, A)[symbol] != 7 || objectKeys($assign({}, B)).join('') != alphabet;
  }) ? function assign(target, source) { // eslint-disable-line no-unused-vars -- required for `.length`
    var T = toObject(target);
    var argumentsLength = arguments.length;
    var index = 1;
    var getOwnPropertySymbols = objectGetOwnPropertySymbols.f;
    var propertyIsEnumerable = objectPropertyIsEnumerable.f;
    while (argumentsLength > index) {
      var S = indexedObject(arguments[index++]);
      var keys = getOwnPropertySymbols ? concat$1(objectKeys(S), getOwnPropertySymbols(S)) : objectKeys(S);
      var length = keys.length;
      var j = 0;
      var key;
      while (length > j) {
        key = keys[j++];
        if (!descriptors || functionCall(propertyIsEnumerable, S, key)) T[key] = S[key];
      }
    } return T;
  } : $assign;

  // `Object.assign` method
  // https://tc39.es/ecma262/#sec-object.assign
  // eslint-disable-next-line es/no-object-assign -- required for testing
  _export({ target: 'Object', stat: true, arity: 2, forced: Object.assign !== objectAssign }, {
    assign: objectAssign
  });

  // `Array.prototype.fill` method
  // https://tc39.es/ecma262/#sec-array.prototype.fill
  _export({ target: 'Array', proto: true }, {
    fill: arrayFill
  });

  // https://tc39.es/ecma262/#sec-array.prototype-@@unscopables
  addToUnscopables('fill');

  var f = wellKnownSymbol;

  var wellKnownSymbolWrapped = {
  	f: f
  };

  var path = global_1;

  var defineProperty$1 = objectDefineProperty.f;

  var wellKnownSymbolDefine = function (NAME) {
    var Symbol = path.Symbol || (path.Symbol = {});
    if (!hasOwnProperty_1(Symbol, NAME)) defineProperty$1(Symbol, NAME, {
      value: wellKnownSymbolWrapped.f(NAME)
    });
  };

  var symbolDefineToPrimitive = function () {
    var Symbol = getBuiltIn('Symbol');
    var SymbolPrototype = Symbol && Symbol.prototype;
    var valueOf = SymbolPrototype && SymbolPrototype.valueOf;
    var TO_PRIMITIVE = wellKnownSymbol('toPrimitive');

    if (SymbolPrototype && !SymbolPrototype[TO_PRIMITIVE]) {
      // `Symbol.prototype[@@toPrimitive]` method
      // https://tc39.es/ecma262/#sec-symbol.prototype-@@toprimitive
      // eslint-disable-next-line no-unused-vars -- required for .length
      defineBuiltIn(SymbolPrototype, TO_PRIMITIVE, function (hint) {
        return functionCall(valueOf, this);
      }, { arity: 1 });
    }
  };

  var $forEach = arrayIteration.forEach;

  var HIDDEN = sharedKey('hidden');
  var SYMBOL = 'Symbol';
  var PROTOTYPE = 'prototype';

  var setInternalState$2 = internalState.set;
  var getInternalState = internalState.getterFor(SYMBOL);

  var ObjectPrototype = Object[PROTOTYPE];
  var $Symbol = global_1.Symbol;
  var SymbolPrototype$1 = $Symbol && $Symbol[PROTOTYPE];
  var TypeError$4 = global_1.TypeError;
  var QObject = global_1.QObject;
  var nativeGetOwnPropertyDescriptor = objectGetOwnPropertyDescriptor.f;
  var nativeDefineProperty = objectDefineProperty.f;
  var nativeGetOwnPropertyNames = objectGetOwnPropertyNamesExternal.f;
  var nativePropertyIsEnumerable = objectPropertyIsEnumerable.f;
  var push$5 = functionUncurryThis([].push);

  var AllSymbols = shared('symbols');
  var ObjectPrototypeSymbols = shared('op-symbols');
  var WellKnownSymbolsStore = shared('wks');

  // Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
  var USE_SETTER = !QObject || !QObject[PROTOTYPE] || !QObject[PROTOTYPE].findChild;

  // fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
  var setSymbolDescriptor = descriptors && fails(function () {
    return objectCreate(nativeDefineProperty({}, 'a', {
      get: function () { return nativeDefineProperty(this, 'a', { value: 7 }).a; }
    })).a != 7;
  }) ? function (O, P, Attributes) {
    var ObjectPrototypeDescriptor = nativeGetOwnPropertyDescriptor(ObjectPrototype, P);
    if (ObjectPrototypeDescriptor) delete ObjectPrototype[P];
    nativeDefineProperty(O, P, Attributes);
    if (ObjectPrototypeDescriptor && O !== ObjectPrototype) {
      nativeDefineProperty(ObjectPrototype, P, ObjectPrototypeDescriptor);
    }
  } : nativeDefineProperty;

  var wrap = function (tag, description) {
    var symbol = AllSymbols[tag] = objectCreate(SymbolPrototype$1);
    setInternalState$2(symbol, {
      type: SYMBOL,
      tag: tag,
      description: description
    });
    if (!descriptors) symbol.description = description;
    return symbol;
  };

  var $defineProperty = function defineProperty(O, P, Attributes) {
    if (O === ObjectPrototype) $defineProperty(ObjectPrototypeSymbols, P, Attributes);
    anObject(O);
    var key = toPropertyKey(P);
    anObject(Attributes);
    if (hasOwnProperty_1(AllSymbols, key)) {
      if (!Attributes.enumerable) {
        if (!hasOwnProperty_1(O, HIDDEN)) nativeDefineProperty(O, HIDDEN, createPropertyDescriptor(1, {}));
        O[HIDDEN][key] = true;
      } else {
        if (hasOwnProperty_1(O, HIDDEN) && O[HIDDEN][key]) O[HIDDEN][key] = false;
        Attributes = objectCreate(Attributes, { enumerable: createPropertyDescriptor(0, false) });
      } return setSymbolDescriptor(O, key, Attributes);
    } return nativeDefineProperty(O, key, Attributes);
  };

  var $defineProperties = function defineProperties(O, Properties) {
    anObject(O);
    var properties = toIndexedObject(Properties);
    var keys = objectKeys(properties).concat($getOwnPropertySymbols(properties));
    $forEach(keys, function (key) {
      if (!descriptors || functionCall($propertyIsEnumerable, properties, key)) $defineProperty(O, key, properties[key]);
    });
    return O;
  };

  var $create = function create(O, Properties) {
    return Properties === undefined ? objectCreate(O) : $defineProperties(objectCreate(O), Properties);
  };

  var $propertyIsEnumerable = function propertyIsEnumerable(V) {
    var P = toPropertyKey(V);
    var enumerable = functionCall(nativePropertyIsEnumerable, this, P);
    if (this === ObjectPrototype && hasOwnProperty_1(AllSymbols, P) && !hasOwnProperty_1(ObjectPrototypeSymbols, P)) return false;
    return enumerable || !hasOwnProperty_1(this, P) || !hasOwnProperty_1(AllSymbols, P) || hasOwnProperty_1(this, HIDDEN) && this[HIDDEN][P]
      ? enumerable : true;
  };

  var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(O, P) {
    var it = toIndexedObject(O);
    var key = toPropertyKey(P);
    if (it === ObjectPrototype && hasOwnProperty_1(AllSymbols, key) && !hasOwnProperty_1(ObjectPrototypeSymbols, key)) return;
    var descriptor = nativeGetOwnPropertyDescriptor(it, key);
    if (descriptor && hasOwnProperty_1(AllSymbols, key) && !(hasOwnProperty_1(it, HIDDEN) && it[HIDDEN][key])) {
      descriptor.enumerable = true;
    }
    return descriptor;
  };

  var $getOwnPropertyNames = function getOwnPropertyNames(O) {
    var names = nativeGetOwnPropertyNames(toIndexedObject(O));
    var result = [];
    $forEach(names, function (key) {
      if (!hasOwnProperty_1(AllSymbols, key) && !hasOwnProperty_1(hiddenKeys$1, key)) push$5(result, key);
    });
    return result;
  };

  var $getOwnPropertySymbols = function (O) {
    var IS_OBJECT_PROTOTYPE = O === ObjectPrototype;
    var names = nativeGetOwnPropertyNames(IS_OBJECT_PROTOTYPE ? ObjectPrototypeSymbols : toIndexedObject(O));
    var result = [];
    $forEach(names, function (key) {
      if (hasOwnProperty_1(AllSymbols, key) && (!IS_OBJECT_PROTOTYPE || hasOwnProperty_1(ObjectPrototype, key))) {
        push$5(result, AllSymbols[key]);
      }
    });
    return result;
  };

  // `Symbol` constructor
  // https://tc39.es/ecma262/#sec-symbol-constructor
  if (!symbolConstructorDetection) {
    $Symbol = function Symbol() {
      if (objectIsPrototypeOf(SymbolPrototype$1, this)) throw TypeError$4('Symbol is not a constructor');
      var description = !arguments.length || arguments[0] === undefined ? undefined : toString_1(arguments[0]);
      var tag = uid(description);
      var setter = function (value) {
        if (this === ObjectPrototype) functionCall(setter, ObjectPrototypeSymbols, value);
        if (hasOwnProperty_1(this, HIDDEN) && hasOwnProperty_1(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
        setSymbolDescriptor(this, tag, createPropertyDescriptor(1, value));
      };
      if (descriptors && USE_SETTER) setSymbolDescriptor(ObjectPrototype, tag, { configurable: true, set: setter });
      return wrap(tag, description);
    };

    SymbolPrototype$1 = $Symbol[PROTOTYPE];

    defineBuiltIn(SymbolPrototype$1, 'toString', function toString() {
      return getInternalState(this).tag;
    });

    defineBuiltIn($Symbol, 'withoutSetter', function (description) {
      return wrap(uid(description), description);
    });

    objectPropertyIsEnumerable.f = $propertyIsEnumerable;
    objectDefineProperty.f = $defineProperty;
    objectDefineProperties.f = $defineProperties;
    objectGetOwnPropertyDescriptor.f = $getOwnPropertyDescriptor;
    objectGetOwnPropertyNames.f = objectGetOwnPropertyNamesExternal.f = $getOwnPropertyNames;
    objectGetOwnPropertySymbols.f = $getOwnPropertySymbols;

    wellKnownSymbolWrapped.f = function (name) {
      return wrap(wellKnownSymbol(name), name);
    };

    if (descriptors) {
      // https://github.com/tc39/proposal-Symbol-description
      defineBuiltInAccessor(SymbolPrototype$1, 'description', {
        configurable: true,
        get: function description() {
          return getInternalState(this).description;
        }
      });
      {
        defineBuiltIn(ObjectPrototype, 'propertyIsEnumerable', $propertyIsEnumerable, { unsafe: true });
      }
    }
  }

  _export({ global: true, constructor: true, wrap: true, forced: !symbolConstructorDetection, sham: !symbolConstructorDetection }, {
    Symbol: $Symbol
  });

  $forEach(objectKeys(WellKnownSymbolsStore), function (name) {
    wellKnownSymbolDefine(name);
  });

  _export({ target: SYMBOL, stat: true, forced: !symbolConstructorDetection }, {
    useSetter: function () { USE_SETTER = true; },
    useSimple: function () { USE_SETTER = false; }
  });

  _export({ target: 'Object', stat: true, forced: !symbolConstructorDetection, sham: !descriptors }, {
    // `Object.create` method
    // https://tc39.es/ecma262/#sec-object.create
    create: $create,
    // `Object.defineProperty` method
    // https://tc39.es/ecma262/#sec-object.defineproperty
    defineProperty: $defineProperty,
    // `Object.defineProperties` method
    // https://tc39.es/ecma262/#sec-object.defineproperties
    defineProperties: $defineProperties,
    // `Object.getOwnPropertyDescriptor` method
    // https://tc39.es/ecma262/#sec-object.getownpropertydescriptors
    getOwnPropertyDescriptor: $getOwnPropertyDescriptor
  });

  _export({ target: 'Object', stat: true, forced: !symbolConstructorDetection }, {
    // `Object.getOwnPropertyNames` method
    // https://tc39.es/ecma262/#sec-object.getownpropertynames
    getOwnPropertyNames: $getOwnPropertyNames
  });

  // `Symbol.prototype[@@toPrimitive]` method
  // https://tc39.es/ecma262/#sec-symbol.prototype-@@toprimitive
  symbolDefineToPrimitive();

  // `Symbol.prototype[@@toStringTag]` property
  // https://tc39.es/ecma262/#sec-symbol.prototype-@@tostringtag
  setToStringTag($Symbol, SYMBOL);

  hiddenKeys$1[HIDDEN] = true;

  /* eslint-disable es/no-symbol -- safe */
  var symbolRegistryDetection = symbolConstructorDetection && !!Symbol['for'] && !!Symbol.keyFor;

  var StringToSymbolRegistry = shared('string-to-symbol-registry');
  var SymbolToStringRegistry$1 = shared('symbol-to-string-registry');

  // `Symbol.for` method
  // https://tc39.es/ecma262/#sec-symbol.for
  _export({ target: 'Symbol', stat: true, forced: !symbolRegistryDetection }, {
    'for': function (key) {
      var string = toString_1(key);
      if (hasOwnProperty_1(StringToSymbolRegistry, string)) return StringToSymbolRegistry[string];
      var symbol = getBuiltIn('Symbol')(string);
      StringToSymbolRegistry[string] = symbol;
      SymbolToStringRegistry$1[symbol] = string;
      return symbol;
    }
  });

  var SymbolToStringRegistry = shared('symbol-to-string-registry');

  // `Symbol.keyFor` method
  // https://tc39.es/ecma262/#sec-symbol.keyfor
  _export({ target: 'Symbol', stat: true, forced: !symbolRegistryDetection }, {
    keyFor: function keyFor(sym) {
      if (!isSymbol(sym)) throw TypeError(tryToString(sym) + ' is not a symbol');
      if (hasOwnProperty_1(SymbolToStringRegistry, sym)) return SymbolToStringRegistry[sym];
    }
  });

  var push$4 = functionUncurryThis([].push);

  var getJsonReplacerFunction = function (replacer) {
    if (isCallable(replacer)) return replacer;
    if (!isArray$1(replacer)) return;
    var rawLength = replacer.length;
    var keys = [];
    for (var i = 0; i < rawLength; i++) {
      var element = replacer[i];
      if (typeof element == 'string') push$4(keys, element);
      else if (typeof element == 'number' || classofRaw(element) == 'Number' || classofRaw(element) == 'String') push$4(keys, toString_1(element));
    }
    var keysLength = keys.length;
    var root = true;
    return function (key, value) {
      if (root) {
        root = false;
        return value;
      }
      if (isArray$1(this)) return value;
      for (var j = 0; j < keysLength; j++) if (keys[j] === key) return value;
    };
  };

  var $String$1 = String;
  var $stringify = getBuiltIn('JSON', 'stringify');
  var exec$2 = functionUncurryThis(/./.exec);
  var charAt$3 = functionUncurryThis(''.charAt);
  var charCodeAt$2 = functionUncurryThis(''.charCodeAt);
  var replace$5 = functionUncurryThis(''.replace);
  var numberToString$1 = functionUncurryThis(1.0.toString);

  var tester = /[\uD800-\uDFFF]/g;
  var low = /^[\uD800-\uDBFF]$/;
  var hi = /^[\uDC00-\uDFFF]$/;

  var WRONG_SYMBOLS_CONVERSION = !symbolConstructorDetection || fails(function () {
    var symbol = getBuiltIn('Symbol')();
    // MS Edge converts symbol values to JSON as {}
    return $stringify([symbol]) != '[null]'
      // WebKit converts symbol values to JSON as null
      || $stringify({ a: symbol }) != '{}'
      // V8 throws on boxed symbols
      || $stringify(Object(symbol)) != '{}';
  });

  // https://github.com/tc39/proposal-well-formed-stringify
  var ILL_FORMED_UNICODE = fails(function () {
    return $stringify('\uDF06\uD834') !== '"\\udf06\\ud834"'
      || $stringify('\uDEAD') !== '"\\udead"';
  });

  var stringifyWithSymbolsFix = function (it, replacer) {
    var args = arraySlice(arguments);
    var $replacer = getJsonReplacerFunction(replacer);
    if (!isCallable($replacer) && (it === undefined || isSymbol(it))) return; // IE8 returns string on undefined
    args[1] = function (key, value) {
      // some old implementations (like WebKit) could pass numbers as keys
      if (isCallable($replacer)) value = functionCall($replacer, this, $String$1(key), value);
      if (!isSymbol(value)) return value;
    };
    return functionApply($stringify, null, args);
  };

  var fixIllFormed = function (match, offset, string) {
    var prev = charAt$3(string, offset - 1);
    var next = charAt$3(string, offset + 1);
    if ((exec$2(low, match) && !exec$2(hi, next)) || (exec$2(hi, match) && !exec$2(low, prev))) {
      return '\\u' + numberToString$1(charCodeAt$2(match, 0), 16);
    } return match;
  };

  if ($stringify) {
    // `JSON.stringify` method
    // https://tc39.es/ecma262/#sec-json.stringify
    _export({ target: 'JSON', stat: true, arity: 3, forced: WRONG_SYMBOLS_CONVERSION || ILL_FORMED_UNICODE }, {
      // eslint-disable-next-line no-unused-vars -- required for `.length`
      stringify: function stringify(it, replacer, space) {
        var args = arraySlice(arguments);
        var result = functionApply(WRONG_SYMBOLS_CONVERSION ? stringifyWithSymbolsFix : $stringify, null, args);
        return ILL_FORMED_UNICODE && typeof result == 'string' ? replace$5(result, tester, fixIllFormed) : result;
      }
    });
  }

  // V8 ~ Chrome 38 and 39 `Object.getOwnPropertySymbols` fails on primitives
  // https://bugs.chromium.org/p/v8/issues/detail?id=3443
  var FORCED$3 = !symbolConstructorDetection || fails(function () { objectGetOwnPropertySymbols.f(1); });

  // `Object.getOwnPropertySymbols` method
  // https://tc39.es/ecma262/#sec-object.getownpropertysymbols
  _export({ target: 'Object', stat: true, forced: FORCED$3 }, {
    getOwnPropertySymbols: function getOwnPropertySymbols(it) {
      var $getOwnPropertySymbols = objectGetOwnPropertySymbols.f;
      return $getOwnPropertySymbols ? $getOwnPropertySymbols(toObject(it)) : [];
    }
  });

  var NativeSymbol = global_1.Symbol;
  var SymbolPrototype = NativeSymbol && NativeSymbol.prototype;

  if (descriptors && isCallable(NativeSymbol) && (!('description' in SymbolPrototype) ||
    // Safari 12 bug
    NativeSymbol().description !== undefined
  )) {
    var EmptyStringDescriptionStore = {};
    // wrap Symbol constructor for correct work with undefined description
    var SymbolWrapper = function Symbol() {
      var description = arguments.length < 1 || arguments[0] === undefined ? undefined : toString_1(arguments[0]);
      var result = objectIsPrototypeOf(SymbolPrototype, this)
        ? new NativeSymbol(description)
        // in Edge 13, String(Symbol(undefined)) === 'Symbol(undefined)'
        : description === undefined ? NativeSymbol() : NativeSymbol(description);
      if (description === '') EmptyStringDescriptionStore[result] = true;
      return result;
    };

    copyConstructorProperties$1(SymbolWrapper, NativeSymbol);
    SymbolWrapper.prototype = SymbolPrototype;
    SymbolPrototype.constructor = SymbolWrapper;

    var NATIVE_SYMBOL = String(NativeSymbol('test')) == 'Symbol(test)';
    var thisSymbolValue = functionUncurryThis(SymbolPrototype.valueOf);
    var symbolDescriptiveString = functionUncurryThis(SymbolPrototype.toString);
    var regexp = /^Symbol\((.*)\)[^)]+$/;
    var replace$4 = functionUncurryThis(''.replace);
    var stringSlice$6 = functionUncurryThis(''.slice);

    defineBuiltInAccessor(SymbolPrototype, 'description', {
      configurable: true,
      get: function description() {
        var symbol = thisSymbolValue(this);
        if (hasOwnProperty_1(EmptyStringDescriptionStore, symbol)) return '';
        var string = symbolDescriptiveString(symbol);
        var desc = NATIVE_SYMBOL ? stringSlice$6(string, 7, -1) : replace$4(string, regexp, '$1');
        return desc === '' ? undefined : desc;
      }
    });

    _export({ global: true, constructor: true, forced: true }, {
      Symbol: SymbolWrapper
    });
  }

  // `Symbol.iterator` well-known symbol
  // https://tc39.es/ecma262/#sec-symbol.iterator
  wellKnownSymbolDefine('iterator');

  function _createSuper$v(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$v(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$v() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  function noop() {}
  var identity = function identity(x) {
    return x;
  };
  function add_location(element, file, line, column, char) {
    element.__svelte_meta = {
      loc: {
        file: file,
        line: line,
        column: column,
        char: char
      }
    };
  }
  function run(fn) {
    return fn();
  }
  function blank_object() {
    return Object.create(null);
  }
  function run_all(fns) {
    fns.forEach(run);
  }
  function is_function(thing) {
    return typeof thing === 'function';
  }
  function safe_not_equal(a, b) {
    return a != a ? b == b : a !== b || a && _typeof(a) === 'object' || typeof a === 'function';
  }
  var src_url_equal_anchor;
  function src_url_equal(element_src, url) {
    if (!src_url_equal_anchor) {
      src_url_equal_anchor = document.createElement('a');
    }
    src_url_equal_anchor.href = url;
    return element_src === src_url_equal_anchor.href;
  }
  function is_empty(obj) {
    return Object.keys(obj).length === 0;
  }
  function validate_store(store, name) {
    if (store != null && typeof store.subscribe !== 'function') {
      throw new Error("'".concat(name, "' is not a store with a 'subscribe' method"));
    }
  }
  function subscribe(store) {
    if (store == null) {
      return noop;
    }
    for (var _len = arguments.length, callbacks = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      callbacks[_key - 1] = arguments[_key];
    }
    var unsub = store.subscribe.apply(store, callbacks);
    return unsub.unsubscribe ? function () {
      return unsub.unsubscribe();
    } : unsub;
  }
  function get_store_value(store) {
    var value;
    subscribe(store, function (_) {
      return value = _;
    })();
    return value;
  }
  function component_subscribe(component, store, callback) {
    component.$$.on_destroy.push(subscribe(store, callback));
  }
  function set_store_value(store, ret, value) {
    store.set(value);
    return ret;
  }
  function split_css_unit(value) {
    var split = typeof value === 'string' && value.match(/^\s*(-?[\d.]+)([^\s]*)\s*$/);
    return split ? [parseFloat(split[1]), split[2] || 'px'] : [value, 'px'];
  }
  var is_client = typeof window !== 'undefined';
  var now = is_client ? function () {
    return window.performance.now();
  } : function () {
    return Date.now();
  };
  var raf = is_client ? function (cb) {
    return requestAnimationFrame(cb);
  } : noop;
  var tasks = new Set();
  function run_tasks(now) {
    tasks.forEach(function (task) {
      if (!task.c(now)) {
        tasks.delete(task);
        task.f();
      }
    });
    if (tasks.size !== 0) raf(run_tasks);
  }
  /**
   * Creates a new task that runs on each raf frame
   * until it returns a falsy value or is aborted
   */
  function loop(callback) {
    var task;
    if (tasks.size === 0) raf(run_tasks);
    return {
      promise: new Promise(function (fulfill) {
        tasks.add(task = {
          c: callback,
          f: fulfill
        });
      }),
      abort: function abort() {
        tasks.delete(task);
      }
    };
  }
  var globals = typeof window !== 'undefined' ? window : typeof globalThis !== 'undefined' ? globalThis : global;
  function append(target, node) {
    target.appendChild(node);
  }
  function get_root_for_style(node) {
    if (!node) return document;
    var root = node.getRootNode ? node.getRootNode() : node.ownerDocument;
    if (root && root.host) {
      return root;
    }
    return node.ownerDocument;
  }
  function append_empty_stylesheet(node) {
    var style_element = element('style');
    append_stylesheet(get_root_for_style(node), style_element);
    return style_element.sheet;
  }
  function append_stylesheet(node, style) {
    append(node.head || node, style);
    return style.sheet;
  }
  function insert(target, node, anchor) {
    target.insertBefore(node, anchor || null);
  }
  function detach(node) {
    if (node.parentNode) {
      node.parentNode.removeChild(node);
    }
  }
  function destroy_each(iterations, detaching) {
    for (var i = 0; i < iterations.length; i += 1) {
      if (iterations[i]) iterations[i].d(detaching);
    }
  }
  function element(name) {
    return document.createElement(name);
  }
  function svg_element(name) {
    return document.createElementNS('http://www.w3.org/2000/svg', name);
  }
  function text(data) {
    return document.createTextNode(data);
  }
  function space() {
    return text(' ');
  }
  function empty() {
    return text('');
  }
  function listen(node, event, handler, options) {
    node.addEventListener(event, handler, options);
    return function () {
      return node.removeEventListener(event, handler, options);
    };
  }
  function prevent_default(fn) {
    return function (event) {
      event.preventDefault();
      // @ts-ignore
      return fn.call(this, event);
    };
  }
  function attr(node, attribute, value) {
    if (value == null) node.removeAttribute(attribute);else if (node.getAttribute(attribute) !== value) node.setAttribute(attribute, value);
  }
  function to_number(value) {
    return value === '' ? null : +value;
  }
  function children(element) {
    return Array.from(element.childNodes);
  }
  function set_input_value(input, value) {
    input.value = value == null ? '' : value;
  }
  // unfortunately this can't be a constant as that wouldn't be tree-shakeable
  // so we cache the result instead
  var crossorigin;
  function is_crossorigin() {
    if (crossorigin === undefined) {
      crossorigin = false;
      try {
        if (typeof window !== 'undefined' && window.parent) {
          void window.parent.document;
        }
      } catch (error) {
        crossorigin = true;
      }
    }
    return crossorigin;
  }
  function add_iframe_resize_listener(node, fn) {
    var computed_style = getComputedStyle(node);
    if (computed_style.position === 'static') {
      node.style.position = 'relative';
    }
    var iframe = element('iframe');
    iframe.setAttribute('style', 'display: block; position: absolute; top: 0; left: 0; width: 100%; height: 100%; ' + 'overflow: hidden; border: 0; opacity: 0; pointer-events: none; z-index: -1;');
    iframe.setAttribute('aria-hidden', 'true');
    iframe.tabIndex = -1;
    var crossorigin = is_crossorigin();
    var unsubscribe;
    if (crossorigin) {
      iframe.src = "data:text/html,<script>onresize=function(){parent.postMessage(0,'*')}</script>";
      unsubscribe = listen(window, 'message', function (event) {
        if (event.source === iframe.contentWindow) fn();
      });
    } else {
      iframe.src = 'about:blank';
      iframe.onload = function () {
        unsubscribe = listen(iframe.contentWindow, 'resize', fn);
        // make sure an initial resize event is fired _after_ the iframe is loaded (which is asynchronous)
        // see https://github.com/sveltejs/svelte/issues/4233
        fn();
      };
    }
    append(node, iframe);
    return function () {
      if (crossorigin) {
        unsubscribe();
      } else if (unsubscribe && iframe.contentWindow) {
        unsubscribe();
      }
      detach(iframe);
    };
  }
  function toggle_class(element, name, toggle) {
    element.classList[toggle ? 'add' : 'remove'](name);
  }
  function custom_event(type, detail) {
    var _ref = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
      _ref$bubbles = _ref.bubbles,
      bubbles = _ref$bubbles === void 0 ? false : _ref$bubbles,
      _ref$cancelable = _ref.cancelable,
      cancelable = _ref$cancelable === void 0 ? false : _ref$cancelable;
    var e = document.createEvent('CustomEvent');
    e.initCustomEvent(type, bubbles, cancelable, detail);
    return e;
  }

  // we need to store the information for multiple documents because a Svelte application could also contain iframes
  // https://github.com/sveltejs/svelte/issues/3624
  var managed_styles = new Map();
  var active = 0;
  // https://github.com/darkskyapp/string-hash/blob/master/index.js
  function hash(str) {
    var hash = 5381;
    var i = str.length;
    while (i--) hash = (hash << 5) - hash ^ str.charCodeAt(i);
    return hash >>> 0;
  }
  function create_style_information(doc, node) {
    var info = {
      stylesheet: append_empty_stylesheet(node),
      rules: {}
    };
    managed_styles.set(doc, info);
    return info;
  }
  function create_rule(node, a, b, duration, delay, ease, fn) {
    var uid = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : 0;
    var step = 16.666 / duration;
    var keyframes = '{\n';
    for (var p = 0; p <= 1; p += step) {
      var t = a + (b - a) * ease(p);
      keyframes += p * 100 + "%{".concat(fn(t, 1 - t), "}\n");
    }
    var rule = keyframes + "100% {".concat(fn(b, 1 - b), "}\n}");
    var name = "__svelte_".concat(hash(rule), "_").concat(uid);
    var doc = get_root_for_style(node);
    var _ref2 = managed_styles.get(doc) || create_style_information(doc, node),
      stylesheet = _ref2.stylesheet,
      rules = _ref2.rules;
    if (!rules[name]) {
      rules[name] = true;
      stylesheet.insertRule("@keyframes ".concat(name, " ").concat(rule), stylesheet.cssRules.length);
    }
    var animation = node.style.animation || '';
    node.style.animation = "".concat(animation ? "".concat(animation, ", ") : '').concat(name, " ").concat(duration, "ms linear ").concat(delay, "ms 1 both");
    active += 1;
    return name;
  }
  function delete_rule(node, name) {
    var previous = (node.style.animation || '').split(', ');
    var next = previous.filter(name ? function (anim) {
      return anim.indexOf(name) < 0;
    } // remove specific animation
    : function (anim) {
      return anim.indexOf('__svelte') === -1;
    } // remove all Svelte animations
    );

    var deleted = previous.length - next.length;
    if (deleted) {
      node.style.animation = next.join(', ');
      active -= deleted;
      if (!active) clear_rules();
    }
  }
  function clear_rules() {
    raf(function () {
      if (active) return;
      managed_styles.forEach(function (info) {
        var ownerNode = info.stylesheet.ownerNode;
        // there is no ownerNode if it runs on jsdom.
        if (ownerNode) detach(ownerNode);
      });
      managed_styles.clear();
    });
  }
  var current_component;
  function set_current_component(component) {
    current_component = component;
  }
  function get_current_component() {
    if (!current_component) throw new Error('Function called outside component initialization');
    return current_component;
  }
  /**
   * The `onMount` function schedules a callback to run as soon as the component has been mounted to the DOM.
   * It must be called during the component's initialisation (but doesn't need to live *inside* the component;
   * it can be called from an external module).
   *
   * `onMount` does not run inside a [server-side component](/docs#run-time-server-side-component-api).
   *
   * https://svelte.dev/docs#run-time-svelte-onmount
   */
  function onMount(fn) {
    get_current_component().$$.on_mount.push(fn);
  }
  /**
   * Creates an event dispatcher that can be used to dispatch [component events](/docs#template-syntax-component-directives-on-eventname).
   * Event dispatchers are functions that can take two arguments: `name` and `detail`.
   *
   * Component events created with `createEventDispatcher` create a
   * [CustomEvent](https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent).
   * These events do not [bubble](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Building_blocks/Events#Event_bubbling_and_capture).
   * The `detail` argument corresponds to the [CustomEvent.detail](https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/detail)
   * property and can contain any type of data.
   *
   * https://svelte.dev/docs#run-time-svelte-createeventdispatcher
   */
  function createEventDispatcher() {
    var component = get_current_component();
    return function (type, detail) {
      var _ref3 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
        _ref3$cancelable = _ref3.cancelable,
        cancelable = _ref3$cancelable === void 0 ? false : _ref3$cancelable;
      var callbacks = component.$$.callbacks[type];
      if (callbacks) {
        // TODO are there situations where events could be dispatched
        // in a server (non-DOM) environment?
        var event = custom_event(type, detail, {
          cancelable: cancelable
        });
        callbacks.slice().forEach(function (fn) {
          fn.call(component, event);
        });
        return !event.defaultPrevented;
      }
      return true;
    };
  }
  var dirty_components = [];
  var binding_callbacks = [];
  var render_callbacks = [];
  var flush_callbacks = [];
  var resolved_promise = /* @__PURE__ */Promise.resolve();
  var update_scheduled = false;
  function schedule_update() {
    if (!update_scheduled) {
      update_scheduled = true;
      resolved_promise.then(flush);
    }
  }
  function add_render_callback(fn) {
    render_callbacks.push(fn);
  }
  function add_flush_callback(fn) {
    flush_callbacks.push(fn);
  }
  // flush() calls callbacks in this order:
  // 1. All beforeUpdate callbacks, in order: parents before children
  // 2. All bind:this callbacks, in reverse order: children before parents.
  // 3. All afterUpdate callbacks, in order: parents before children. EXCEPT
  //    for afterUpdates called during the initial onMount, which are called in
  //    reverse order: children before parents.
  // Since callbacks might update component values, which could trigger another
  // call to flush(), the following steps guard against this:
  // 1. During beforeUpdate, any updated components will be added to the
  //    dirty_components array and will cause a reentrant call to flush(). Because
  //    the flush index is kept outside the function, the reentrant call will pick
  //    up where the earlier call left off and go through all dirty components. The
  //    current_component value is saved and restored so that the reentrant call will
  //    not interfere with the "parent" flush() call.
  // 2. bind:this callbacks cannot trigger new flush() calls.
  // 3. During afterUpdate, any updated components will NOT have their afterUpdate
  //    callback called a second time; the seen_callbacks set, outside the flush()
  //    function, guarantees this behavior.
  var seen_callbacks = new Set();
  var flushidx = 0; // Do *not* move this inside the flush() function
  function flush() {
    // Do not reenter flush while dirty components are updated, as this can
    // result in an infinite loop. Instead, let the inner flush handle it.
    // Reentrancy is ok afterwards for bindings etc.
    if (flushidx !== 0) {
      return;
    }
    var saved_component = current_component;
    do {
      // first, call beforeUpdate functions
      // and update components
      try {
        while (flushidx < dirty_components.length) {
          var component = dirty_components[flushidx];
          flushidx++;
          set_current_component(component);
          update(component.$$);
        }
      } catch (e) {
        // reset dirty state to not end up in a deadlocked state and then rethrow
        dirty_components.length = 0;
        flushidx = 0;
        throw e;
      }
      set_current_component(null);
      dirty_components.length = 0;
      flushidx = 0;
      while (binding_callbacks.length) binding_callbacks.pop()();
      // then, once components are updated, call
      // afterUpdate functions. This may cause
      // subsequent updates...
      for (var i = 0; i < render_callbacks.length; i += 1) {
        var callback = render_callbacks[i];
        if (!seen_callbacks.has(callback)) {
          // ...so guard against infinite loops
          seen_callbacks.add(callback);
          callback();
        }
      }
      render_callbacks.length = 0;
    } while (dirty_components.length);
    while (flush_callbacks.length) {
      flush_callbacks.pop()();
    }
    update_scheduled = false;
    seen_callbacks.clear();
    set_current_component(saved_component);
  }
  function update($$) {
    if ($$.fragment !== null) {
      $$.update();
      run_all($$.before_update);
      var dirty = $$.dirty;
      $$.dirty = [-1];
      $$.fragment && $$.fragment.p($$.ctx, dirty);
      $$.after_update.forEach(add_render_callback);
    }
  }
  /**
   * Useful for example to execute remaining `afterUpdate` callbacks before executing `destroy`.
   */
  function flush_render_callbacks(fns) {
    var filtered = [];
    var targets = [];
    render_callbacks.forEach(function (c) {
      return fns.indexOf(c) === -1 ? filtered.push(c) : targets.push(c);
    });
    targets.forEach(function (c) {
      return c();
    });
    render_callbacks = filtered;
  }
  var promise;
  function wait() {
    if (!promise) {
      promise = Promise.resolve();
      promise.then(function () {
        promise = null;
      });
    }
    return promise;
  }
  function dispatch(node, direction, kind) {
    node.dispatchEvent(custom_event("".concat(direction ? 'intro' : 'outro').concat(kind)));
  }
  var outroing = new Set();
  var outros;
  function group_outros() {
    outros = {
      r: 0,
      c: [],
      p: outros // parent group
    };
  }

  function check_outros() {
    if (!outros.r) {
      run_all(outros.c);
    }
    outros = outros.p;
  }
  function transition_in(block, local) {
    if (block && block.i) {
      outroing.delete(block);
      block.i(local);
    }
  }
  function transition_out(block, local, detach, callback) {
    if (block && block.o) {
      if (outroing.has(block)) return;
      outroing.add(block);
      outros.c.push(function () {
        outroing.delete(block);
        if (callback) {
          if (detach) block.d(1);
          callback();
        }
      });
      block.o(local);
    } else if (callback) {
      callback();
    }
  }
  var null_transition = {
    duration: 0
  };
  function create_in_transition(node, fn, params) {
    var options = {
      direction: 'in'
    };
    var config = fn(node, params, options);
    var running = false;
    var animation_name;
    var task;
    var uid = 0;
    function cleanup() {
      if (animation_name) delete_rule(node, animation_name);
    }
    function go() {
      var _ref4 = config || null_transition,
        _ref4$delay = _ref4.delay,
        delay = _ref4$delay === void 0 ? 0 : _ref4$delay,
        _ref4$duration = _ref4.duration,
        duration = _ref4$duration === void 0 ? 300 : _ref4$duration,
        _ref4$easing = _ref4.easing,
        easing = _ref4$easing === void 0 ? identity : _ref4$easing,
        _ref4$tick = _ref4.tick,
        tick = _ref4$tick === void 0 ? noop : _ref4$tick,
        css = _ref4.css;
      if (css) animation_name = create_rule(node, 0, 1, duration, delay, easing, css, uid++);
      tick(0, 1);
      var start_time = now() + delay;
      var end_time = start_time + duration;
      if (task) task.abort();
      running = true;
      add_render_callback(function () {
        return dispatch(node, true, 'start');
      });
      task = loop(function (now) {
        if (running) {
          if (now >= end_time) {
            tick(1, 0);
            dispatch(node, true, 'end');
            cleanup();
            return running = false;
          }
          if (now >= start_time) {
            var t = easing((now - start_time) / duration);
            tick(t, 1 - t);
          }
        }
        return running;
      });
    }
    var started = false;
    return {
      start: function start() {
        if (started) return;
        started = true;
        delete_rule(node);
        if (is_function(config)) {
          config = config(options);
          wait().then(go);
        } else {
          go();
        }
      },
      invalidate: function invalidate() {
        started = false;
      },
      end: function end() {
        if (running) {
          cleanup();
          running = false;
        }
      }
    };
  }
  function create_out_transition(node, fn, params) {
    var options = {
      direction: 'out'
    };
    var config = fn(node, params, options);
    var running = true;
    var animation_name;
    var group = outros;
    group.r += 1;
    function go() {
      var _ref5 = config || null_transition,
        _ref5$delay = _ref5.delay,
        delay = _ref5$delay === void 0 ? 0 : _ref5$delay,
        _ref5$duration = _ref5.duration,
        duration = _ref5$duration === void 0 ? 300 : _ref5$duration,
        _ref5$easing = _ref5.easing,
        easing = _ref5$easing === void 0 ? identity : _ref5$easing,
        _ref5$tick = _ref5.tick,
        tick = _ref5$tick === void 0 ? noop : _ref5$tick,
        css = _ref5.css;
      if (css) animation_name = create_rule(node, 1, 0, duration, delay, easing, css);
      var start_time = now() + delay;
      var end_time = start_time + duration;
      add_render_callback(function () {
        return dispatch(node, false, 'start');
      });
      loop(function (now) {
        if (running) {
          if (now >= end_time) {
            tick(0, 1);
            dispatch(node, false, 'end');
            if (! --group.r) {
              // this will result in `end()` being called,
              // so we don't need to clean up here
              run_all(group.c);
            }
            return false;
          }
          if (now >= start_time) {
            var t = easing((now - start_time) / duration);
            tick(1 - t, t);
          }
        }
        return running;
      });
    }
    if (is_function(config)) {
      wait().then(function () {
        // @ts-ignore
        config = config(options);
        go();
      });
    } else {
      go();
    }
    return {
      end: function end(reset) {
        if (reset && config.tick) {
          config.tick(1, 0);
        }
        if (running) {
          if (animation_name) delete_rule(node, animation_name);
          running = false;
        }
      }
    };
  }
  function create_bidirectional_transition(node, fn, params, intro) {
    var options = {
      direction: 'both'
    };
    var config = fn(node, params, options);
    var t = intro ? 0 : 1;
    var running_program = null;
    var pending_program = null;
    var animation_name = null;
    function clear_animation() {
      if (animation_name) delete_rule(node, animation_name);
    }
    function init(program, duration) {
      var d = program.b - t;
      duration *= Math.abs(d);
      return {
        a: t,
        b: program.b,
        d: d,
        duration: duration,
        start: program.start,
        end: program.start + duration,
        group: program.group
      };
    }
    function go(b) {
      var _ref6 = config || null_transition,
        _ref6$delay = _ref6.delay,
        delay = _ref6$delay === void 0 ? 0 : _ref6$delay,
        _ref6$duration = _ref6.duration,
        duration = _ref6$duration === void 0 ? 300 : _ref6$duration,
        _ref6$easing = _ref6.easing,
        easing = _ref6$easing === void 0 ? identity : _ref6$easing,
        _ref6$tick = _ref6.tick,
        tick = _ref6$tick === void 0 ? noop : _ref6$tick,
        css = _ref6.css;
      var program = {
        start: now() + delay,
        b: b
      };
      if (!b) {
        // @ts-ignore todo: improve typings
        program.group = outros;
        outros.r += 1;
      }
      if (running_program || pending_program) {
        pending_program = program;
      } else {
        // if this is an intro, and there's a delay, we need to do
        // an initial tick and/or apply CSS animation immediately
        if (css) {
          clear_animation();
          animation_name = create_rule(node, t, b, duration, delay, easing, css);
        }
        if (b) tick(0, 1);
        running_program = init(program, duration);
        add_render_callback(function () {
          return dispatch(node, b, 'start');
        });
        loop(function (now) {
          if (pending_program && now > pending_program.start) {
            running_program = init(pending_program, duration);
            pending_program = null;
            dispatch(node, running_program.b, 'start');
            if (css) {
              clear_animation();
              animation_name = create_rule(node, t, running_program.b, running_program.duration, 0, easing, config.css);
            }
          }
          if (running_program) {
            if (now >= running_program.end) {
              tick(t = running_program.b, 1 - t);
              dispatch(node, running_program.b, 'end');
              if (!pending_program) {
                // we're done
                if (running_program.b) {
                  // intro — we can tidy up immediately
                  clear_animation();
                } else {
                  // outro — needs to be coordinated
                  if (! --running_program.group.r) run_all(running_program.group.c);
                }
              }
              running_program = null;
            } else if (now >= running_program.start) {
              var p = now - running_program.start;
              t = running_program.a + running_program.d * easing(p / running_program.duration);
              tick(t, 1 - t);
            }
          }
          return !!(running_program || pending_program);
        });
      }
    }
    return {
      run: function run(b) {
        if (is_function(config)) {
          wait().then(function () {
            // @ts-ignore
            config = config(options);
            go(b);
          });
        } else {
          go(b);
        }
      },
      end: function end() {
        clear_animation();
        running_program = pending_program = null;
      }
    };
  }
  function destroy_block(block, lookup) {
    block.d(1);
    lookup.delete(block.key);
  }
  function outro_and_destroy_block(block, lookup) {
    transition_out(block, 1, 1, function () {
      lookup.delete(block.key);
    });
  }
  function update_keyed_each(old_blocks, dirty, get_key, dynamic, ctx, list, lookup, node, destroy, create_each_block, next, get_context) {
    var o = old_blocks.length;
    var n = list.length;
    var i = o;
    var old_indexes = {};
    while (i--) old_indexes[old_blocks[i].key] = i;
    var new_blocks = [];
    var new_lookup = new Map();
    var deltas = new Map();
    var updates = [];
    i = n;
    var _loop = function _loop() {
      var child_ctx = get_context(ctx, list, i);
      var key = get_key(child_ctx);
      var block = lookup.get(key);
      if (!block) {
        block = create_each_block(key, child_ctx);
        block.c();
      } else if (dynamic) {
        // defer updates until all the DOM shuffling is done
        updates.push(function () {
          return block.p(child_ctx, dirty);
        });
      }
      new_lookup.set(key, new_blocks[i] = block);
      if (key in old_indexes) deltas.set(key, Math.abs(i - old_indexes[key]));
    };
    while (i--) {
      _loop();
    }
    var will_move = new Set();
    var did_move = new Set();
    function insert(block) {
      transition_in(block, 1);
      block.m(node, next);
      lookup.set(block.key, block);
      next = block.first;
      n--;
    }
    while (o && n) {
      var new_block = new_blocks[n - 1];
      var old_block = old_blocks[o - 1];
      var new_key = new_block.key;
      var old_key = old_block.key;
      if (new_block === old_block) {
        // do nothing
        next = new_block.first;
        o--;
        n--;
      } else if (!new_lookup.has(old_key)) {
        // remove old block
        destroy(old_block, lookup);
        o--;
      } else if (!lookup.has(new_key) || will_move.has(new_key)) {
        insert(new_block);
      } else if (did_move.has(old_key)) {
        o--;
      } else if (deltas.get(new_key) > deltas.get(old_key)) {
        did_move.add(new_key);
        insert(new_block);
      } else {
        will_move.add(old_key);
        o--;
      }
    }
    while (o--) {
      var _old_block = old_blocks[o];
      if (!new_lookup.has(_old_block.key)) destroy(_old_block, lookup);
    }
    while (n) insert(new_blocks[n - 1]);
    run_all(updates);
    return new_blocks;
  }
  function validate_each_keys(ctx, list, get_context, get_key) {
    var keys = new Set();
    for (var i = 0; i < list.length; i++) {
      var key = get_key(get_context(ctx, list, i));
      if (keys.has(key)) {
        throw new Error('Cannot have duplicate keys in a keyed each');
      }
      keys.add(key);
    }
  }
  function bind(component, name, callback) {
    var index = component.$$.props[name];
    if (index !== undefined) {
      component.$$.bound[index] = callback;
      callback(component.$$.ctx[index]);
    }
  }
  function create_component(block) {
    block && block.c();
  }
  function mount_component(component, target, anchor, customElement) {
    var _component$$$ = component.$$,
      fragment = _component$$$.fragment,
      after_update = _component$$$.after_update;
    fragment && fragment.m(target, anchor);
    if (!customElement) {
      // onMount happens before the initial afterUpdate
      add_render_callback(function () {
        var new_on_destroy = component.$$.on_mount.map(run).filter(is_function);
        // if the component was destroyed immediately
        // it will update the `$$.on_destroy` reference to `null`.
        // the destructured on_destroy may still reference to the old array
        if (component.$$.on_destroy) {
          var _component$$$$on_dest;
          (_component$$$$on_dest = component.$$.on_destroy).push.apply(_component$$$$on_dest, _toConsumableArray(new_on_destroy));
        } else {
          // Edge case - component was destroyed immediately,
          // most likely as a result of a binding initialising
          run_all(new_on_destroy);
        }
        component.$$.on_mount = [];
      });
    }
    after_update.forEach(add_render_callback);
  }
  function destroy_component(component, detaching) {
    var $$ = component.$$;
    if ($$.fragment !== null) {
      flush_render_callbacks($$.after_update);
      run_all($$.on_destroy);
      $$.fragment && $$.fragment.d(detaching);
      // TODO null out other refs, including component.$$ (but need to
      // preserve final state?)
      $$.on_destroy = $$.fragment = null;
      $$.ctx = [];
    }
  }
  function make_dirty(component, i) {
    if (component.$$.dirty[0] === -1) {
      dirty_components.push(component);
      schedule_update();
      component.$$.dirty.fill(0);
    }
    component.$$.dirty[i / 31 | 0] |= 1 << i % 31;
  }
  function init(component, options, instance, create_fragment, not_equal, props, append_styles) {
    var dirty = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : [-1];
    var parent_component = current_component;
    set_current_component(component);
    var $$ = component.$$ = {
      fragment: null,
      ctx: [],
      // state
      props: props,
      update: noop,
      not_equal: not_equal,
      bound: blank_object(),
      // lifecycle
      on_mount: [],
      on_destroy: [],
      on_disconnect: [],
      before_update: [],
      after_update: [],
      context: new Map(options.context || (parent_component ? parent_component.$$.context : [])),
      // everything else
      callbacks: blank_object(),
      dirty: dirty,
      skip_bound: false,
      root: options.target || parent_component.$$.root
    };
    append_styles && append_styles($$.root);
    var ready = false;
    $$.ctx = instance ? instance(component, options.props || {}, function (i, ret) {
      var value = (arguments.length <= 2 ? 0 : arguments.length - 2) ? arguments.length <= 2 ? undefined : arguments[2] : ret;
      if ($$.ctx && not_equal($$.ctx[i], $$.ctx[i] = value)) {
        if (!$$.skip_bound && $$.bound[i]) $$.bound[i](value);
        if (ready) make_dirty(component, i);
      }
      return ret;
    }) : [];
    $$.update();
    ready = true;
    run_all($$.before_update);
    // `false` as a special case of no DOM component
    $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
    if (options.target) {
      if (options.hydrate) {
        var nodes = children(options.target);
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        $$.fragment && $$.fragment.l(nodes);
        nodes.forEach(detach);
      } else {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        $$.fragment && $$.fragment.c();
      }
      if (options.intro) transition_in(component.$$.fragment);
      mount_component(component, options.target, options.anchor, options.customElement);
      flush();
    }
    set_current_component(parent_component);
  }
  /**
   * Base class for Svelte components. Used when dev=false.
   */
  var SvelteComponent = /*#__PURE__*/function () {
    function SvelteComponent() {
      _classCallCheck(this, SvelteComponent);
    }
    _createClass(SvelteComponent, [{
      key: "$destroy",
      value: function $destroy() {
        destroy_component(this, 1);
        this.$destroy = noop;
      }
    }, {
      key: "$on",
      value: function $on(type, callback) {
        if (!is_function(callback)) {
          return noop;
        }
        var callbacks = this.$$.callbacks[type] || (this.$$.callbacks[type] = []);
        callbacks.push(callback);
        return function () {
          var index = callbacks.indexOf(callback);
          if (index !== -1) callbacks.splice(index, 1);
        };
      }
    }, {
      key: "$set",
      value: function $set($$props) {
        if (this.$$set && !is_empty($$props)) {
          this.$$.skip_bound = true;
          this.$$set($$props);
          this.$$.skip_bound = false;
        }
      }
    }]);
    return SvelteComponent;
  }();
  function dispatch_dev(type, detail) {
    document.dispatchEvent(custom_event(type, Object.assign({
      version: '3.59.2'
    }, detail), {
      bubbles: true
    }));
  }
  function append_dev(target, node) {
    dispatch_dev('SvelteDOMInsert', {
      target: target,
      node: node
    });
    append(target, node);
  }
  function insert_dev(target, node, anchor) {
    dispatch_dev('SvelteDOMInsert', {
      target: target,
      node: node,
      anchor: anchor
    });
    insert(target, node, anchor);
  }
  function detach_dev(node) {
    dispatch_dev('SvelteDOMRemove', {
      node: node
    });
    detach(node);
  }
  function listen_dev(node, event, handler, options, has_prevent_default, has_stop_propagation, has_stop_immediate_propagation) {
    var modifiers = options === true ? ['capture'] : options ? Array.from(Object.keys(options)) : [];
    if (has_prevent_default) modifiers.push('preventDefault');
    if (has_stop_propagation) modifiers.push('stopPropagation');
    if (has_stop_immediate_propagation) modifiers.push('stopImmediatePropagation');
    dispatch_dev('SvelteDOMAddEventListener', {
      node: node,
      event: event,
      handler: handler,
      modifiers: modifiers
    });
    var dispose = listen(node, event, handler, options);
    return function () {
      dispatch_dev('SvelteDOMRemoveEventListener', {
        node: node,
        event: event,
        handler: handler,
        modifiers: modifiers
      });
      dispose();
    };
  }
  function attr_dev(node, attribute, value) {
    attr(node, attribute, value);
    if (value == null) dispatch_dev('SvelteDOMRemoveAttribute', {
      node: node,
      attribute: attribute
    });else dispatch_dev('SvelteDOMSetAttribute', {
      node: node,
      attribute: attribute,
      value: value
    });
  }
  function set_data_dev(text, data) {
    data = '' + data;
    if (text.data === data) return;
    dispatch_dev('SvelteDOMSetData', {
      node: text,
      data: data
    });
    text.data = data;
  }
  function validate_each_argument(arg) {
    if (typeof arg !== 'string' && !(arg && _typeof(arg) === 'object' && 'length' in arg)) {
      var msg = '{#each} only iterates over array-like objects.';
      if (typeof Symbol === 'function' && arg && Symbol.iterator in arg) {
        msg += ' You can use a spread to convert this iterable into an array.';
      }
      throw new Error(msg);
    }
  }
  function validate_slots(name, slot, keys) {
    for (var _i4 = 0, _Object$keys = Object.keys(slot); _i4 < _Object$keys.length; _i4++) {
      var slot_key = _Object$keys[_i4];
      if (!~keys.indexOf(slot_key)) {
        console.warn("<".concat(name, "> received an unexpected slot \"").concat(slot_key, "\"."));
      }
    }
  }
  function construct_svelte_component_dev(component, props) {
    var error_message = 'this={...} of <svelte:component> should specify a Svelte component.';
    try {
      var instance = new component(props);
      if (!instance.$$ || !instance.$set || !instance.$on || !instance.$destroy) {
        throw new Error(error_message);
      }
      return instance;
    } catch (err) {
      var message = err.message;
      if (typeof message === 'string' && message.indexOf('is not a constructor') !== -1) {
        throw new Error(error_message);
      } else {
        throw err;
      }
    }
  }
  /**
   * Base class for Svelte components with some minor dev-enhancements. Used when dev=true.
   */
  var SvelteComponentDev = /*#__PURE__*/function (_SvelteComponent) {
    _inherits(SvelteComponentDev, _SvelteComponent);
    var _super3 = _createSuper$v(SvelteComponentDev);
    function SvelteComponentDev(options) {
      _classCallCheck(this, SvelteComponentDev);
      if (!options || !options.target && !options.$$inline) {
        throw new Error("'target' is a required option");
      }
      return _super3.call(this);
    }
    _createClass(SvelteComponentDev, [{
      key: "$destroy",
      value: function $destroy() {
        _get(_getPrototypeOf(SvelteComponentDev.prototype), "$destroy", this).call(this);
        this.$destroy = function () {
          console.warn('Component was already destroyed'); // eslint-disable-line no-console
        };
      }
    }, {
      key: "$capture_state",
      value: function $capture_state() {}
    }, {
      key: "$inject_state",
      value: function $inject_state() {}
    }]);
    return SvelteComponentDev;
  }(SvelteComponent);

  var $includes = arrayIncludes.includes;



  // FF99+ bug
  var BROKEN_ON_SPARSE = fails(function () {
    // eslint-disable-next-line es/no-array-prototype-includes -- detection
    return !Array(1).includes();
  });

  // `Array.prototype.includes` method
  // https://tc39.es/ecma262/#sec-array.prototype.includes
  _export({ target: 'Array', proto: true, forced: BROKEN_ON_SPARSE }, {
    includes: function includes(el /* , fromIndex = 0 */) {
      return $includes(this, el, arguments.length > 1 ? arguments[1] : undefined);
    }
  });

  // https://tc39.es/ecma262/#sec-array.prototype-@@unscopables
  addToUnscopables('includes');

  var stringIndexOf$1 = functionUncurryThis(''.indexOf);

  // `String.prototype.includes` method
  // https://tc39.es/ecma262/#sec-string.prototype.includes
  _export({ target: 'String', proto: true, forced: !correctIsRegexpLogic('includes') }, {
    includes: function includes(searchString /* , position = 0 */) {
      return !!~stringIndexOf$1(
        toString_1(requireObjectCoercible(this)),
        toString_1(notARegexp(searchString)),
        arguments.length > 1 ? arguments[1] : undefined
      );
    }
  });

  function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }
  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
  var subscriber_queue = [];
  /**
   * Creates a `Readable` store that allows reading by subscription.
   * @param value initial value
   * @param {StartStopNotifier} [start]
   */
  function readable(value, start) {
    return {
      subscribe: writable(value, start).subscribe
    };
  }
  /**
   * Create a `Writable` store that allows both updating and reading by subscription.
   * @param {*=}value initial value
   * @param {StartStopNotifier=} start
   */
  function writable(value) {
    var start = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : noop;
    var stop;
    var subscribers = new Set();
    function set(new_value) {
      if (safe_not_equal(value, new_value)) {
        value = new_value;
        if (stop) {
          // store is ready
          var run_queue = !subscriber_queue.length;
          var _iterator = _createForOfIteratorHelper(subscribers),
            _step;
          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var subscriber = _step.value;
              subscriber[1]();
              subscriber_queue.push(subscriber, value);
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }
          if (run_queue) {
            for (var i = 0; i < subscriber_queue.length; i += 2) {
              subscriber_queue[i][0](subscriber_queue[i + 1]);
            }
            subscriber_queue.length = 0;
          }
        }
      }
    }
    function update(fn) {
      set(fn(value));
    }
    function subscribe(run) {
      var invalidate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : noop;
      var subscriber = [run, invalidate];
      subscribers.add(subscriber);
      if (subscribers.size === 1) {
        stop = start(set) || noop;
      }
      run(value);
      return function () {
        subscribers.delete(subscriber);
        if (subscribers.size === 0 && stop) {
          stop();
          stop = null;
        }
      };
    }
    return {
      set: set,
      update: update,
      subscribe: subscribe
    };
  }
  function derived(stores, fn, initial_value) {
    var single = !Array.isArray(stores);
    var stores_array = single ? [stores] : stores;
    var auto = fn.length < 2;
    return readable(initial_value, function (set) {
      var started = false;
      var values = [];
      var pending = 0;
      var cleanup = noop;
      var sync = function sync() {
        if (pending) {
          return;
        }
        cleanup();
        var result = fn(single ? values[0] : values, set);
        if (auto) {
          set(result);
        } else {
          cleanup = is_function(result) ? result : noop;
        }
      };
      var unsubscribers = stores_array.map(function (store, i) {
        return subscribe(store, function (value) {
          values[i] = value;
          pending &= ~(1 << i);
          if (started) {
            sync();
          }
        }, function () {
          pending |= 1 << i;
        });
      });
      started = true;
      sync();
      return function stop() {
        run_all(unsubscribers);
        cleanup();
        // We need to set this to false because callbacks can still happen despite having unsubscribed:
        // Callbacks might already be placed in the queue which doesn't know it should no longer
        // invoke this derived store.
        started = false;
      };
    });
  }

  function isIos() {
    return ["iPad Simulator", "iPhone Simulator", "iPod Simulator", "iPad", "iPhone", "iPod"].includes(navigator.platform) ||
    // iPad on iOS 13 detection
    navigator.userAgent.includes("Mac") && "ontouchend" in document;
  }
  var isIpadOS = function isIpadOS() {
    return navigator.maxTouchPoints && navigator.maxTouchPoints > 2 && /MacIntel/.test(navigator.platform);
  };
  var BASE_URL = function () {
    if (location.href.includes("gitlab")) return "https://az67128.gitlab.io/gambling";
    if (location.href.includes("localhost") || location.href.includes("192.168")) return ".";
    return "https://az67128.gitlab.io/gambling";
  }();

  // export const IS_TEST = /(localhost|192\.168|az67128.gitlab.io)/.test(
  var IS_TEST$1 = /(localhost|192\.168)/.test(window.location.href);
  var sendMetrik$1 = function sendMetrik(name, payload) {
    var consoleMetrik = function consoleMetrik(id, type, name, payload) {
      return console.log(name, payload);
    };
    var ym = IS_TEST$1 ? consoleMetrik : window.ym || consoleMetrik;
    var dummy = function dummy() {};
    var gtag = window.gtag || dummy;
    // const ym = window.ym || consoleMetrik;

    ym(70641691, "reachGoal", "Gambler.".concat(name), payload);
    if (!IS_TEST$1) {
      gtag('event', "Gambler.".concat(name), payload);
    }
  };
  var externalLink = writable();
  var iframeLink = writable();
  var gameWon = writable(false);
  var gameClosed = writable(false);
  var gameSkipped = writable(true);
  var isCharityButtonPressed = writable(false);
  var activeIndex = writable(0);
  var players = writable([]);
  var activePlayerCurrentTime = writable(0);
  var adDisabled = writable(false);
  var gamblerLastSeries = writable(localStorage.getItem('gamblerLastSeries'));
  activeIndex.subscribe(function (state) {
    localStorage.setItem('gamblerLastSeries', state);
  });
  var gamblerLastTime = writable(localStorage.getItem('gamblerLastTime'));
  var location1 = writable('mom');

  var $reduce = arrayReduce.left;




  // Chrome 80-82 has a critical bug
  // https://bugs.chromium.org/p/chromium/issues/detail?id=1049982
  var CHROME_BUG = !engineIsNode && engineV8Version > 79 && engineV8Version < 83;
  var FORCED$2 = CHROME_BUG || !arrayMethodIsStrict('reduce');

  // `Array.prototype.reduce` method
  // https://tc39.es/ecma262/#sec-array.prototype.reduce
  _export({ target: 'Array', proto: true, forced: FORCED$2 }, {
    reduce: function reduce(callbackfn /* , initialValue */) {
      var length = arguments.length;
      return $reduce(this, callbackfn, length, length > 1 ? arguments[1] : undefined);
    }
  });

  // `thisNumberValue` abstract operation
  // https://tc39.es/ecma262/#sec-thisnumbervalue
  var thisNumberValue = functionUncurryThis(1.0.valueOf);

  var $RangeError$2 = RangeError;

  // `String.prototype.repeat` method implementation
  // https://tc39.es/ecma262/#sec-string.prototype.repeat
  var stringRepeat = function repeat(count) {
    var str = toString_1(requireObjectCoercible(this));
    var result = '';
    var n = toIntegerOrInfinity(count);
    if (n < 0 || n == Infinity) throw $RangeError$2('Wrong number of repetitions');
    for (;n > 0; (n >>>= 1) && (str += str)) if (n & 1) result += str;
    return result;
  };

  var $RangeError$1 = RangeError;
  var $String = String;
  var floor$3 = Math.floor;
  var repeat = functionUncurryThis(stringRepeat);
  var stringSlice$5 = functionUncurryThis(''.slice);
  var nativeToFixed = functionUncurryThis(1.0.toFixed);

  var pow$1 = function (x, n, acc) {
    return n === 0 ? acc : n % 2 === 1 ? pow$1(x, n - 1, acc * x) : pow$1(x * x, n / 2, acc);
  };

  var log = function (x) {
    var n = 0;
    var x2 = x;
    while (x2 >= 4096) {
      n += 12;
      x2 /= 4096;
    }
    while (x2 >= 2) {
      n += 1;
      x2 /= 2;
    } return n;
  };

  var multiply = function (data, n, c) {
    var index = -1;
    var c2 = c;
    while (++index < 6) {
      c2 += n * data[index];
      data[index] = c2 % 1e7;
      c2 = floor$3(c2 / 1e7);
    }
  };

  var divide = function (data, n) {
    var index = 6;
    var c = 0;
    while (--index >= 0) {
      c += data[index];
      data[index] = floor$3(c / n);
      c = (c % n) * 1e7;
    }
  };

  var dataToString = function (data) {
    var index = 6;
    var s = '';
    while (--index >= 0) {
      if (s !== '' || index === 0 || data[index] !== 0) {
        var t = $String(data[index]);
        s = s === '' ? t : s + repeat('0', 7 - t.length) + t;
      }
    } return s;
  };

  var FORCED$1 = fails(function () {
    return nativeToFixed(0.00008, 3) !== '0.000' ||
      nativeToFixed(0.9, 0) !== '1' ||
      nativeToFixed(1.255, 2) !== '1.25' ||
      nativeToFixed(1000000000000000128.0, 0) !== '1000000000000000128';
  }) || !fails(function () {
    // V8 ~ Android 4.3-
    nativeToFixed({});
  });

  // `Number.prototype.toFixed` method
  // https://tc39.es/ecma262/#sec-number.prototype.tofixed
  _export({ target: 'Number', proto: true, forced: FORCED$1 }, {
    toFixed: function toFixed(fractionDigits) {
      var number = thisNumberValue(this);
      var fractDigits = toIntegerOrInfinity(fractionDigits);
      var data = [0, 0, 0, 0, 0, 0];
      var sign = '';
      var result = '0';
      var e, z, j, k;

      // TODO: ES2018 increased the maximum number of fraction digits to 100, need to improve the implementation
      if (fractDigits < 0 || fractDigits > 20) throw $RangeError$1('Incorrect fraction digits');
      // eslint-disable-next-line no-self-compare -- NaN check
      if (number != number) return 'NaN';
      if (number <= -1e21 || number >= 1e21) return $String(number);
      if (number < 0) {
        sign = '-';
        number = -number;
      }
      if (number > 1e-21) {
        e = log(number * pow$1(2, 69, 1)) - 69;
        z = e < 0 ? number * pow$1(2, -e, 1) : number / pow$1(2, e, 1);
        z *= 0x10000000000000;
        e = 52 - e;
        if (e > 0) {
          multiply(data, 0, z);
          j = fractDigits;
          while (j >= 7) {
            multiply(data, 1e7, 0);
            j -= 7;
          }
          multiply(data, pow$1(10, j, 1), 0);
          j = e - 1;
          while (j >= 23) {
            divide(data, 1 << 23);
            j -= 23;
          }
          divide(data, 1 << j);
          multiply(data, 1, 1);
          divide(data, 2);
          result = dataToString(data);
        } else {
          multiply(data, 0, z);
          multiply(data, 1 << -e, 0);
          result = dataToString(data) + repeat('0', fractDigits);
        }
      }
      if (fractDigits > 0) {
        k = result.length;
        result = sign + (k <= fractDigits
          ? '0.' + repeat('0', fractDigits - k) + result
          : stringSlice$5(result, 0, k - fractDigits) + '.' + stringSlice$5(result, k - fractDigits));
      } else {
        result = sign + result;
      } return result;
    }
  });

  var sequence = [{
    episode: "11",
    duration: 362,
    src: "https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/0273fe4a-b532-4523-bf9b-5c8427bbea9d/GAMBLER INTR 210106 P1-1-1.ism/manifest",
    activeElemenets: [{
      type: "continuewatch",
      start: 0,
      end: 5
    }, {
      type: "rss",
      start: 54.517502,
      end: 59.517502,
      rssType: "tele"
    },
    //00.55
    // admitad
    {
      type: "image",
      start: 24.798292,
      // 0.24 ++
      end: 27.098288,
      image: "https://ad.admitad.com/b/gu4hjj01fm6a099e88707a660ebfae/",
      href: "https://alitems.com/g/gu4hjj01fm6a099e88707a660ebfae/?i=4",
      width: 8.3544,
      left: 11.4883,
      top: 65.594,
      showInIframe: true
    }, {
      type: "image",
      start: 186.668385,
      // 3.09 ++
      end: 189.568385,
      image: "https://ad.admitad.com/b/dwe38s72q66a099e88704b9351d46e/",
      href: "https://pafutos.com/g/dwe38s72q66a099e88704b9351d46e/?i=4",
      width: 10.3544,
      left: 6.4883,
      top: 65.594,
      showInIframe: true
    }, {
      type: "image",
      start: 196.505049,
      // 3.16 ++
      end: 197.605049,
      image: "https://ad.admitad.com/b/gu4hjj01fm6a099e88707a660ebfae/",
      href: "https://alitems.com/g/gu4hjj01fm6a099e88707a660ebfae/?i=4",
      width: 10.3544,
      left: 6.4883,
      top: 65.594,
      showInIframe: true
    }, {
      type: "image",
      start: 213.31347,
      // 3.33 ++
      end: 215.01347,
      image: "https://ad.admitad.com/b/dwe38s72q66a099e88704b9351d46e/",
      href: "https://pafutos.com/g/dwe38s72q66a099e88704b9351d46e/?i=4",
      width: 8.3544,
      left: 11.4883,
      top: 65.594,
      showInIframe: true
    }, {
      type: "image",
      start: 270.871968,
      // 4.30 ++
      end: 273.771966,
      image: "https://ad.admitad.com/b/uhjftxxkth6a099e8870b62ed2b196/",
      // ---
      href: "https://ad.admitad.com/g/uhjftxxkth6a099e8870b62ed2b196/?i=4",
      width: 13.2,
      left: 7.18883,
      top: 24.594,
      showInIframe: false
    }, {
      type: "image",
      start: 280.558317,
      // 4.40 ++
      end: 286.458314,
      image: "https://ad.admitad.com/b/uhjftxxkth6a099e8870b62ed2b196/",
      // ---
      href: "https://ad.admitad.com/g/uhjftxxkth6a099e8870b62ed2b196/?i=4",
      width: 13.2,
      left: 7.18883,
      top: 24.594,
      showInIframe: false
    }, {
      type: "image",
      start: 327.4313,
      //5.27 ++
      end: 331.031298,
      image: "https://ad.admitad.com/b/dwe38s72q66a099e88704b9351d46e/",
      href: "https://pafutos.com/g/dwe38s72q66a099e88704b9351d46e/?i=4",
      width: 10.3544,
      left: 11.18883,
      top: 65.594,
      showInIframe: true
    },
    // admitad
    {
      type: "zone",
      start: 37.837645,
      // 00.37 supejob ++
      end: 50.037645,
      width: 9.81,
      top: 71.3,
      paddingTop: 13,
      left: 11.34,
      href: "https://www.superjob.ru/?utm_source=movie_igrok&utm_medium=promo&utm_campaign=banner",
      withoutBorder: true,
      color: "white",
      showInIframe: true
    }, {
      type: "zone",
      start: 114.605595,
      // 1.56 supejob ++
      end: 117.592414,
      width: 12.51,
      top: 66.7,
      paddingTop: 16.4,
      left: 7.5,
      href: "https://www.superjob.ru/?utm_source=movie_igrok&utm_medium=promo&utm_campaign=banner",
      withoutBorder: true,
      color: "white",
      showInIframe: true
    }, {
      type: "zone",
      start: 27.196787,
      // 00.27 kurochkin ++
      end: 37.596785,
      width: 37.9,
      top: 43.3,
      paddingTop: 5.56,
      left: 31.15,
      href: "https://vk.com/id543045945",
      showInIframe: false
    }, {
      type: "zone",
      start: 115.748393,
      // 1.55 ++
      end: 118.748393,
      width: 50.9,
      top: 9.3,
      paddingTop: 14.56,
      left: 7.2,
      href: "https://vk.com/id542950209",
      // nataliya
      showInIframe: false
    }, {
      type: "zone",
      start: 286.633729,
      // 4.46 korovin ++
      end: 288.633726,
      width: 67.9,
      top: 4.76,
      paddingTop: 18.76,
      left: 10.9,
      href: "https://vk.com/id542008339",
      showInIframe: false
    }, {
      type: 'selectLocation',
      start: 361.5,
      end: 362
    }]
  }, {
    episode: "11otvet1",
    src: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/5855ef06-477f-4543-9145-41c14c60528b/1ural.ism/manifest',
    activeElemenets: [{
      type: 'selectLocation2',
      start: 20.5,
      end: 21
    }]
  }, {
    episode: "11otvet2",
    src: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/0bc5fdc5-3551-4107-8d63-07041c0889f1/1ural2spb.ism/manifest'
  }, {
    episode: "112",
    duration: 530,
    src: "https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/26f0c7c9-f79c-4933-af41-8236511c04ef/GAMBLER INTR 210118 P1-1-2.ism/manifest",
    activeElemenets: [{
      type: "disablead",
      start: 0.001,
      end: 1
    },
    // {
    //   type: "midroll",
    //   start: 431.524896,
    //   end: 433.524896,
    //   src:
    //     "https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/ae9bbeb3-863c-4169-a192-69ccdc41f481/SuperJob Gosha.ism/manifest",
    //   href:
    //     "https://www.superjob.ru/?utm_source=movie_igrok&utm_medium=promo&utm_campaign=banner",
    //   showInIframe: true,
    // },
    {
      type: "zone",
      start: 176.519468,
      // 9.32 coral 
      end: 184.519467,
      width: 52.2,
      top: 81.3,
      paddingTop: 6.76,
      left: 23.9,
      href: "https://zanzibar.igrok.film/",
      withoutBorder: true,
      color: "white",
      hideControls: true,
      showInIframe: true
    }, {
      type: "zone",
      start: 186.829208,
      // 9.42 coral 
      end: 192.429208,
      width: 52.2,
      top: 81.3,
      paddingTop: 6.76,
      left: 23.9,
      href: "https://zanzibar.igrok.film/",
      withoutBorder: true,
      color: "white",
      hideControls: true,
      showInIframe: true
    }]
  }, {
    episode: "12",
    duration: 22,
    src: "https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/c2c9cae1-f782-4fd8-995c-6f990d286805/GAMBLERINTR201227P12.ism/manifest",
    interactive: "PROMO1",
    // game: "https://screenreality.com/pokerdom/?scenario=1st.json",
    skipIfPlayed: false
  }, {
    episode: "21",
    duration: 1340,
    src: "https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/650e0bff-1cc2-4355-a330-97f6c0bd48e6/GAMBLER INTR 210115 P2-1.ism/manifest",
    activeElemenets: [{
      type: "push",
      start: 51.328351,
      // 00.51
      end: 52.950137,
      href: "https://ad.admitad.com/g/5b8ecfe8b96a099e887000da8bef3b/",
      src: "".concat(BASE_URL, "/images/docdoc.png"),
      title: "docdoc.ru",
      text: "Запиши сына на прием",
      right: 1.19,
      top: 49,
      width: 90.1,
      imageWidth: 0.1233,
      fontSize: 0.045,
      paddingVertical: 0.046,
      paddingHorizontal: 0.0325,
      needPause: true,
      showInIframe: true
    }, {
      type: "push",
      start: 154.454415,
      // 2.34 okey
      end: 156.829762,
      href: "https://ad.admitad.com/coupon/mqv6n1pj1y6a099e88703cb2d68e81/",
      src: "".concat(BASE_URL, "/images/okey.png"),
      title: "okmarket.ru",
      text: "В Окей скидки до 42%",
      right: 1.19,
      top: 49,
      width: 90.1,
      imageWidth: 0.1233,
      fontSize: 0.045,
      paddingVertical: 0.046,
      paddingHorizontal: 0.0325,
      needPause: true,
      showInIframe: true
    }, {
      type: "rss",
      start: 24.5,
      end: 30,
      rssType: "tele"
    }, {
      type: "rss",
      start: 424.321632,
      end: 430,
      rssType: "7days"
    }, {
      type: "image",
      start: 889.30171,
      // 14.42
      end: 896.032458,
      image: "https://ad.admitad.com/b/gu4hjj01fm6a099e88707a660ebfae/",
      //ali
      href: "https://alitems.com/g/gu4hjj01fm6a099e88707a660ebfae/?i=4",
      width: 10.3544,
      left: 9.18883,
      top: 8.594,
      showInIframe: true
    }, {
      type: "image",
      start: 918.869234,
      //15.18
      end: 920.458845,
      image: "https://ad.admitad.com/b/rl5meo7nnw6a099e8870b62ed2b196/",
      href: "https://ad.admitad.com/g/rl5meo7nnw6a099e8870b62ed2b196/?i=4",
      //--
      width: 10.3544,
      left: 7.58883,
      top: 64.594,
      showInIframe: false
    }, {
      type: "zone",
      start: 396.004065,
      // 6.36 levin
      end: 409.192835,
      width: 78.9,
      top: 40.9,
      paddingTop: 15.76,
      left: 9.9,
      href: "https://vk.com/id542013857",
      showInIframe: false
    }, {
      type: "zone",
      start: 899.684104,
      // 15.01 kurochkin
      end: 917.674893,
      width: 39.9,
      top: 8.9,
      paddingTop: 10.36,
      left: 7.9,
      href: "https://vk.com/id543055212",
      showInIframe: false
    }]
  }, {
    episode: "22",
    duration: 21,
    src: "https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/8df9c314-0af0-4c82-97e9-e43c76d47f68/GAMBLERINTR201227P22.ism/manifest",
    interactive: "TWO",
    game: "./pokerdom/?scenario=2nd.json",
    skipIfPlayed: true
  }, {
    episode: "31",
    duration: 1708,
    src: "https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/be070cdf-d61e-49ca-9f89-375bf97cf401/GAMBLER INTR 210115 P3-1.ism/manifest",
    activeElemenets: [{
      type: "image",
      start: 1074.794674,
      // 17.54
      end: 1077.017932,
      image: "https://ad.admitad.com/b/gu4hjj01fm6a099e88707a660ebfae/",
      //ali
      href: "https://alitems.com/g/gu4hjj01fm6a099e88707a660ebfae/?i=4",
      width: 10.3544,
      left: 5.58883,
      top: 63.594,
      showInIframe: true
    }]
  }, {
    episode: "32",
    duration: 144,
    src: "https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/b31fde27-c829-4b4b-9e2c-f5a0b0fa6d87/GAMBLER INTR 201227 P3-2.ism/manifest",
    interactive: "CREDITS"
  }];

  function _createSuper$u(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$u(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$u() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$u = "src/TimeBar.svelte";
  function create_fragment$u(ctx) {
    var div1;
    var input;
    var t0;
    var div0;
    var t1_value = /*convertToTime*/ctx[5]( /*currentTime*/ctx[0]) + "";
    var t1;
    var t2;
    var t3_value = /*convertToTime*/ctx[5]( /*totalDuration*/ctx[4]) + "";
    var t3;
    var mounted;
    var dispose;
    var block = {
      c: function create() {
        div1 = element("div");
        input = element("input");
        t0 = space();
        div0 = element("div");
        t1 = text(t1_value);
        t2 = text("\n    /\n    ");
        t3 = text(t3_value);
        attr_dev(input, "binf:this", /*sliderRef*/ctx[6]);
        attr_dev(input, "type", "range");
        attr_dev(input, "min", /*min*/ctx[1]);
        attr_dev(input, "max", /*max*/ctx[2]);
        attr_dev(input, "step", 1);
        attr_dev(input, "style", /*style*/ctx[3]);
        attr_dev(input, "class", "svelte-3ai91m");
        add_location(input, file$u, 74, 2, 2174);
        attr_dev(div0, "class", "time svelte-3ai91m");
        add_location(div0, file$u, 87, 2, 2408);
        attr_dev(div1, "class", "container svelte-3ai91m");
        toggle_class(div1, "isIpadOS", isIpadOS());
        add_location(div1, file$u, 73, 0, 2120);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, div1, anchor);
        append_dev(div1, input);
        set_input_value(input, /*currentTime*/ctx[0]);
        append_dev(div1, t0);
        append_dev(div1, div0);
        append_dev(div0, t1);
        append_dev(div0, t2);
        append_dev(div0, t3);
        if (!mounted) {
          dispose = [listen_dev(input, "mousedown", /*lock*/ctx[7], false, false, false, false), listen_dev(input, "mouseup", /*release*/ctx[8], false, false, false, false), listen_dev(input, "touchstart", /*lock*/ctx[7], {
            passive: true
          }, false, false, false), listen_dev(input, "touchend", /*release*/ctx[8], false, false, false, false), listen_dev(input, "change", /*input_change_input_handler*/ctx[12]), listen_dev(input, "input", /*input_change_input_handler*/ctx[12])];
          mounted = true;
        }
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if (dirty & /*min*/2) {
          attr_dev(input, "min", /*min*/ctx[1]);
        }
        if (dirty & /*max*/4) {
          attr_dev(input, "max", /*max*/ctx[2]);
        }
        if (dirty & /*style*/8) {
          attr_dev(input, "style", /*style*/ctx[3]);
        }
        if (dirty & /*currentTime*/1) {
          set_input_value(input, /*currentTime*/ctx[0]);
        }
        if (dirty & /*currentTime*/1 && t1_value !== (t1_value = /*convertToTime*/ctx[5]( /*currentTime*/ctx[0]) + "")) set_data_dev(t1, t1_value);
      },
      i: noop,
      o: noop,
      d: function destroy(detaching) {
        if (detaching) detach_dev(div1);
        mounted = false;
        run_all(dispose);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$u.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$u($$self, $$props, $$invalidate) {
    var min;
    var max;
    var $activeIndex;
    var $players;
    var $activePlayerCurrentTime;
    validate_store(activeIndex, 'activeIndex');
    component_subscribe($$self, activeIndex, function ($$value) {
      return $$invalidate(10, $activeIndex = $$value);
    });
    validate_store(players, 'players');
    component_subscribe($$self, players, function ($$value) {
      return $$invalidate(13, $players = $$value);
    });
    validate_store(activePlayerCurrentTime, 'activePlayerCurrentTime');
    component_subscribe($$self, activePlayerCurrentTime, function ($$value) {
      return $$invalidate(11, $activePlayerCurrentTime = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('TimeBar', slots, []);
    var totalDuration = sequence.reduce(function (memo, item) {
      return memo + (item.duration || 0);
    }, 0);
    var convertToTime = function convertToTime(seconds) {
      return new Date((seconds || 0) * 1000).toISOString().substr(11, 8);
    };
    var style = "";
    var sliderRef;
    var isSliderActive = false;
    var currentTime = 0;
    var timeoutId;

    // const setActivePlayerCurrentTime = (value) => {
    //   clearTimeout(timeoutId);
    //   if ($players[$activeIndex] && $players[$activeIndex].videoHeight()) {
    //     $players[$activeIndex].currentTime(value);
    //     $players[$activeIndex].play()
    //   } else {
    //     timeoutId = setTimeout(() => setActivePlayerCurrentTime(value), 10);
    //   }
    // };
    var onChange = function onChange(e) {
      var value = e.target.value;
      var durationSum = 0;
      var _loop = function _loop(i) {
        durationSum += sequence[i].duration || 0;
        if (value < durationSum) {
          setTimeout(function () {
            return $players[i].currentTime(value - (durationSum - (sequence[i].duration || 0)));
          }, 500);
          set_store_value(activeIndex, $activeIndex = i, $activeIndex);
          $players.forEach(function (item, i) {
            if (item && i !== $activeIndex) {
              try {
                item.pause();
              } catch (e) {}
            } else {
              item.play();
              item.controlBar.el_.style.display = "";
            }
          });
          return "break";
        }
      };
      for (var i = 0; i < sequence.length; i++) {
        var _ret = _loop(i);
        if (_ret === "break") break;
      }
    };
    var lock = function lock() {
      return $$invalidate(9, isSliderActive = true);
    };
    var release = function release(e) {
      onChange(e);
      $$invalidate(9, isSliderActive = false);
    };
    var writable_props = [];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<TimeBar> was created with unknown prop '".concat(key, "'"));
    });
    function input_change_input_handler() {
      currentTime = to_number(this.value);
      (($$invalidate(0, currentTime), $$invalidate(9, isSliderActive)), $$invalidate(10, $activeIndex)), $$invalidate(11, $activePlayerCurrentTime);
    }
    $$self.$capture_state = function () {
      return {
        activeIndex: activeIndex,
        players: players,
        activePlayerCurrentTime: activePlayerCurrentTime,
        isIpadOS: isIpadOS,
        sequence: sequence,
        totalDuration: totalDuration,
        convertToTime: convertToTime,
        style: style,
        sliderRef: sliderRef,
        isSliderActive: isSliderActive,
        currentTime: currentTime,
        timeoutId: timeoutId,
        onChange: onChange,
        lock: lock,
        release: release,
        min: min,
        max: max,
        $activeIndex: $activeIndex,
        $players: $players,
        $activePlayerCurrentTime: $activePlayerCurrentTime
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('style' in $$props) $$invalidate(3, style = $$props.style);
      if ('sliderRef' in $$props) $$invalidate(6, sliderRef = $$props.sliderRef);
      if ('isSliderActive' in $$props) $$invalidate(9, isSliderActive = $$props.isSliderActive);
      if ('currentTime' in $$props) $$invalidate(0, currentTime = $$props.currentTime);
      if ('timeoutId' in $$props) timeoutId = $$props.timeoutId;
      if ('min' in $$props) $$invalidate(1, min = $$props.min);
      if ('max' in $$props) $$invalidate(2, max = $$props.max);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    $$self.$$.update = function () {
      if ($$self.$$.dirty & /*isSliderActive, $activeIndex, $activePlayerCurrentTime*/3584) {
        {
          if (!isSliderActive) {
            $$invalidate(0, currentTime = sequence.reduce(function (memo, item, i) {
              return $activeIndex > i ? memo + (item.duration || 0) : memo;
            }, 0) + $activePlayerCurrentTime);
          }
        }
      }
      if ($$self.$$.dirty & /*currentTime, min, max*/7) {
        {
          var percent = ((currentTime - min) / (max - min)).toFixed(2);
          $$invalidate(3, style = "background-image: -webkit-gradient(linear, left top, right top, color-stop(".concat(percent, ", #FF1C1C), color-stop(").concat(percent, ", #272729))"));
        }
      }
    };
    $$invalidate(1, min = 0);
    $$invalidate(2, max = totalDuration);
    return [currentTime, min, max, style, totalDuration, convertToTime, sliderRef, lock, release, isSliderActive, $activeIndex, $activePlayerCurrentTime, input_change_input_handler];
  }
  var TimeBar = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(TimeBar, _SvelteComponentDev);
    var _super = _createSuper$u(TimeBar);
    function TimeBar(options) {
      var _this;
      _classCallCheck(this, TimeBar);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$u, create_fragment$u, safe_not_equal, {});
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "TimeBar",
        options: options,
        id: create_fragment$u.name
      });
      return _this;
    }
    return _createClass(TimeBar);
  }(SvelteComponentDev);

  function _createSuper$t(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$t(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$t() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$t = "src/Interactive/Timer.svelte";
  function create_fragment$t(ctx) {
    var svg;
    var path0;
    var path1;
    var path1_stroke_dasharray_value;
    var text_1;
    var t;
    var block = {
      c: function create() {
        svg = svg_element("svg");
        path0 = svg_element("path");
        path1 = svg_element("path");
        text_1 = svg_element("text");
        t = text( /*seconds*/ctx[0]);
        attr_dev(path0, "class", "timerBackground svelte-xx9qxy");
        attr_dev(path0, "d", "M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0\n    -31.831");
        attr_dev(path0, "fill", "none");
        attr_dev(path0, "stroke-width", "1");
        attr_dev(path0, "stroke-dasharray", "100, 100");
        add_location(path0, file$t, 31, 2, 660);
        attr_dev(path1, "class", "timer svelte-xx9qxy");
        attr_dev(path1, "d", "M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0\n    -31.831");
        attr_dev(path1, "fill", "none");
        attr_dev(path1, "stroke", "#444");
        attr_dev(path1, "stroke-width", "3");
        attr_dev(path1, "stroke-dasharray", path1_stroke_dasharray_value = "".concat( /*percent*/ctx[1], ", 100"));
        add_location(path1, file$t, 38, 2, 858);
        attr_dev(text_1, "x", "18");
        attr_dev(text_1, "y", "23");
        attr_dev(text_1, "class", "text svelte-xx9qxy");
        attr_dev(text_1, "text-anchor", "middle");
        add_location(text_1, file$t, 46, 2, 1073);
        attr_dev(svg, "viewBox", "0 0 36 36");
        attr_dev(svg, "class", "timerSvg svelte-xx9qxy");
        add_location(svg, file$t, 30, 0, 615);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, svg, anchor);
        append_dev(svg, path0);
        append_dev(svg, path1);
        append_dev(svg, text_1);
        append_dev(text_1, t);
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if (dirty & /*percent*/2 && path1_stroke_dasharray_value !== (path1_stroke_dasharray_value = "".concat( /*percent*/ctx[1], ", 100"))) {
          attr_dev(path1, "stroke-dasharray", path1_stroke_dasharray_value);
        }
        if (dirty & /*seconds*/1) set_data_dev(t, /*seconds*/ctx[0]);
      },
      i: noop,
      o: noop,
      d: function destroy(detaching) {
        if (detaching) detach_dev(svg);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$t.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$t($$self, $$props, $$invalidate) {
    var percent;
    var seconds;
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('Timer', slots, []);
    var duration = $$props.duration;
    var TURN_TIME = duration * 1000;
    var dispatch = createEventDispatcher();
    var time = TURN_TIME;
    var timerId = null;
    onMount(function () {
      var tick = function tick() {
        $$invalidate(3, time = time - 10);
        if (time === 0) {
          dispatch("timerend", null);
          return;
        }
        timerId = setTimeout(tick, 10);
      };
      tick();
      return function () {
        clearTimeout(timerId);
      };
    });
    $$self.$$.on_mount.push(function () {
      if (duration === undefined && !('duration' in $$props || $$self.$$.bound[$$self.$$.props['duration']])) {
        console.warn("<Timer> was created without expected prop 'duration'");
      }
    });
    var writable_props = ['duration'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<Timer> was created with unknown prop '".concat(key, "'"));
    });
    $$self.$$set = function ($$props) {
      if ('duration' in $$props) $$invalidate(2, duration = $$props.duration);
    };
    $$self.$capture_state = function () {
      return {
        onMount: onMount,
        createEventDispatcher: createEventDispatcher,
        duration: duration,
        TURN_TIME: TURN_TIME,
        dispatch: dispatch,
        time: time,
        timerId: timerId,
        seconds: seconds,
        percent: percent
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('duration' in $$props) $$invalidate(2, duration = $$props.duration);
      if ('time' in $$props) $$invalidate(3, time = $$props.time);
      if ('timerId' in $$props) timerId = $$props.timerId;
      if ('seconds' in $$props) $$invalidate(0, seconds = $$props.seconds);
      if ('percent' in $$props) $$invalidate(1, percent = $$props.percent);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    $$self.$$.update = function () {
      if ($$self.$$.dirty & /*time*/8) {
        $$invalidate(1, percent = time / TURN_TIME * 100);
      }
      if ($$self.$$.dirty & /*time*/8) {
        $$invalidate(0, seconds = Math.ceil(time / 1000));
      }
    };
    return [seconds, percent, duration, time];
  }
  var Timer = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(Timer, _SvelteComponentDev);
    var _super = _createSuper$t(Timer);
    function Timer(options) {
      var _this;
      _classCallCheck(this, Timer);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$t, create_fragment$t, safe_not_equal, {
        duration: 2
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "Timer",
        options: options,
        id: create_fragment$t.name
      });
      return _this;
    }
    _createClass(Timer, [{
      key: "duration",
      get: function get() {
        throw new Error("<Timer>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Timer>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return Timer;
  }(SvelteComponentDev);

  function _createSuper$s(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$s(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$s() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$s = "src/Interactive/One.svelte";

  // (34:2) {#if duration && isTimerVisible}
  function create_if_block$g(ctx) {
    var div;
    var timer;
    var t;
    var img;
    var img_src_value;
    var current;
    var mounted;
    var dispose;
    timer = new Timer({
      props: {
        duration: /*duration*/ctx[0]
      },
      $$inline: true
    });
    timer.$on("timerend", /*timerEnd*/ctx[2]);
    var block = {
      c: function create() {
        div = element("div");
        create_component(timer.$$.fragment);
        t = space();
        img = element("img");
        attr_dev(div, "class", "timer svelte-e8gohb");
        add_location(div, file$s, 34, 4, 1198);
        if (!src_url_equal(img.src, img_src_value = "".concat(BASE_URL, "/images/continue.png"))) attr_dev(img, "src", img_src_value);
        attr_dev(img, "alt", "cards");
        attr_dev(img, "class", "continue appear svelte-e8gohb");
        add_location(img, file$s, 38, 4, 1284);
      },
      m: function mount(target, anchor) {
        insert_dev(target, div, anchor);
        mount_component(timer, div, null);
        insert_dev(target, t, anchor);
        insert_dev(target, img, anchor);
        current = true;
        if (!mounted) {
          dispose = listen_dev(img, "click", /*onSkip*/ctx[4], false, false, false, false);
          mounted = true;
        }
      },
      p: function update(ctx, dirty) {
        var timer_changes = {};
        if (dirty & /*duration*/1) timer_changes.duration = /*duration*/ctx[0];
        timer.$set(timer_changes);
      },
      i: function intro(local) {
        if (current) return;
        transition_in(timer.$$.fragment, local);
        current = true;
      },
      o: function outro(local) {
        transition_out(timer.$$.fragment, local);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div);
        destroy_component(timer);
        if (detaching) detach_dev(t);
        if (detaching) detach_dev(img);
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block$g.name,
      type: "if",
      source: "(34:2) {#if duration && isTimerVisible}",
      ctx: ctx
    });
    return block;
  }
  function create_fragment$s(ctx) {
    var div;
    var img0;
    var img0_src_value;
    var t0;
    var img1;
    var img1_src_value;
    var t1;
    var img2;
    var img2_src_value;
    var t2;
    var img3;
    var img3_src_value;
    var t3;
    var img4;
    var img4_src_value;
    var t4;
    var img5;
    var img5_src_value;
    var t5;
    var img6;
    var img6_src_value;
    var t6;
    var current;
    var mounted;
    var dispose;
    var if_block = /*duration*/ctx[0] && /*isTimerVisible*/ctx[1] && create_if_block$g(ctx);
    var block = {
      c: function create() {
        div = element("div");
        img0 = element("img");
        t0 = space();
        img1 = element("img");
        t1 = space();
        img2 = element("img");
        t2 = space();
        img3 = element("img");
        t3 = space();
        img4 = element("img");
        t4 = space();
        img5 = element("img");
        t5 = space();
        img6 = element("img");
        t6 = space();
        if (if_block) if_block.c();
        if (!src_url_equal(img0.src, img0_src_value = "".concat(BASE_URL, "/images/chip3.png"))) attr_dev(img0, "src", img0_src_value);
        attr_dev(img0, "alt", "chip1");
        attr_dev(img0, "class", "chip3 svelte-e8gohb");
        add_location(img0, file$s, 22, 2, 612);
        if (!src_url_equal(img1.src, img1_src_value = "".concat(BASE_URL, "/images/chip1.png"))) attr_dev(img1, "src", img1_src_value);
        attr_dev(img1, "alt", "chip1");
        attr_dev(img1, "class", "chip1 svelte-e8gohb");
        add_location(img1, file$s, 23, 2, 685);
        if (!src_url_equal(img2.src, img2_src_value = "".concat(BASE_URL, "/images/chip4.png"))) attr_dev(img2, "src", img2_src_value);
        attr_dev(img2, "alt", "chip1");
        attr_dev(img2, "class", "chip4 svelte-e8gohb");
        add_location(img2, file$s, 24, 2, 758);
        if (!src_url_equal(img3.src, img3_src_value = "".concat(BASE_URL, "/images/chip2.png"))) attr_dev(img3, "src", img3_src_value);
        attr_dev(img3, "alt", "chip1");
        attr_dev(img3, "class", "chip2 svelte-e8gohb");
        add_location(img3, file$s, 25, 2, 831);
        if (!src_url_equal(img4.src, img4_src_value = "".concat(BASE_URL, "/images/logo.png"))) attr_dev(img4, "src", img4_src_value);
        attr_dev(img4, "alt", "logo");
        attr_dev(img4, "class", "logo svelte-e8gohb");
        add_location(img4, file$s, 26, 2, 904);
        if (!src_url_equal(img5.src, img5_src_value = "".concat(BASE_URL, "/images/help1.png"))) attr_dev(img5, "src", img5_src_value);
        attr_dev(img5, "alt", "cards");
        attr_dev(img5, "class", "help1 svelte-e8gohb");
        add_location(img5, file$s, 27, 2, 974);
        if (!src_url_equal(img6.src, img6_src_value = "".concat(BASE_URL, "/images/playpocker.png"))) attr_dev(img6, "src", img6_src_value);
        attr_dev(img6, "alt", "cards");
        attr_dev(img6, "class", "button svelte-e8gohb");
        add_location(img6, file$s, 28, 2, 1047);
        attr_dev(div, "class", "container appear svelte-e8gohb");
        add_location(div, file$s, 21, 0, 579);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, div, anchor);
        append_dev(div, img0);
        append_dev(div, t0);
        append_dev(div, img1);
        append_dev(div, t1);
        append_dev(div, img2);
        append_dev(div, t2);
        append_dev(div, img3);
        append_dev(div, t3);
        append_dev(div, img4);
        append_dev(div, t4);
        append_dev(div, img5);
        append_dev(div, t5);
        append_dev(div, img6);
        append_dev(div, t6);
        if (if_block) if_block.m(div, null);
        current = true;
        if (!mounted) {
          dispose = listen_dev(img6, "click", /*onClick*/ctx[3], false, false, false, false);
          mounted = true;
        }
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if ( /*duration*/ctx[0] && /*isTimerVisible*/ctx[1]) {
          if (if_block) {
            if_block.p(ctx, dirty);
            if (dirty & /*duration, isTimerVisible*/3) {
              transition_in(if_block, 1);
            }
          } else {
            if_block = create_if_block$g(ctx);
            if_block.c();
            transition_in(if_block, 1);
            if_block.m(div, null);
          }
        } else if (if_block) {
          group_outros();
          transition_out(if_block, 1, 1, function () {
            if_block = null;
          });
          check_outros();
        }
      },
      i: function intro(local) {
        if (current) return;
        transition_in(if_block);
        current = true;
      },
      o: function outro(local) {
        transition_out(if_block);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div);
        if (if_block) if_block.d();
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$s.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$s($$self, $$props, $$invalidate) {
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('One', slots, []);
    var nextVideo = $$props.nextVideo;
    var openGame = $$props.openGame;
    var duration = $$props.duration;
    var episode = $$props.episode;
    var isTimerVisible = true;
    var timerEnd = function timerEnd() {
      return $$invalidate(1, isTimerVisible = false);
    };
    sendMetrik$1("GameAdShow");
    var onClick = function onClick() {
      openGame();
      sendMetrik$1("GameAdClick", {
        "Gambler.GameAdClickEpisode": episode
      });
    };
    var onSkip = function onSkip() {
      sendMetrik$1("GameAdSkip", {
        "Gambler.GameAdSkipEpisode": episode
      });
      nextVideo();
    };
    $$self.$$.on_mount.push(function () {
      if (nextVideo === undefined && !('nextVideo' in $$props || $$self.$$.bound[$$self.$$.props['nextVideo']])) {
        console.warn("<One> was created without expected prop 'nextVideo'");
      }
      if (openGame === undefined && !('openGame' in $$props || $$self.$$.bound[$$self.$$.props['openGame']])) {
        console.warn("<One> was created without expected prop 'openGame'");
      }
      if (duration === undefined && !('duration' in $$props || $$self.$$.bound[$$self.$$.props['duration']])) {
        console.warn("<One> was created without expected prop 'duration'");
      }
      if (episode === undefined && !('episode' in $$props || $$self.$$.bound[$$self.$$.props['episode']])) {
        console.warn("<One> was created without expected prop 'episode'");
      }
    });
    var writable_props = ['nextVideo', 'openGame', 'duration', 'episode'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<One> was created with unknown prop '".concat(key, "'"));
    });
    $$self.$$set = function ($$props) {
      if ('nextVideo' in $$props) $$invalidate(5, nextVideo = $$props.nextVideo);
      if ('openGame' in $$props) $$invalidate(6, openGame = $$props.openGame);
      if ('duration' in $$props) $$invalidate(0, duration = $$props.duration);
      if ('episode' in $$props) $$invalidate(7, episode = $$props.episode);
    };
    $$self.$capture_state = function () {
      return {
        BASE_URL: BASE_URL,
        Timer: Timer,
        sendMetrik: sendMetrik$1,
        nextVideo: nextVideo,
        openGame: openGame,
        duration: duration,
        episode: episode,
        isTimerVisible: isTimerVisible,
        timerEnd: timerEnd,
        onClick: onClick,
        onSkip: onSkip
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('nextVideo' in $$props) $$invalidate(5, nextVideo = $$props.nextVideo);
      if ('openGame' in $$props) $$invalidate(6, openGame = $$props.openGame);
      if ('duration' in $$props) $$invalidate(0, duration = $$props.duration);
      if ('episode' in $$props) $$invalidate(7, episode = $$props.episode);
      if ('isTimerVisible' in $$props) $$invalidate(1, isTimerVisible = $$props.isTimerVisible);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    return [duration, isTimerVisible, timerEnd, onClick, onSkip, nextVideo, openGame, episode];
  }
  var One = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(One, _SvelteComponentDev);
    var _super = _createSuper$s(One);
    function One(options) {
      var _this;
      _classCallCheck(this, One);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$s, create_fragment$s, safe_not_equal, {
        nextVideo: 5,
        openGame: 6,
        duration: 0,
        episode: 7
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "One",
        options: options,
        id: create_fragment$s.name
      });
      return _this;
    }
    _createClass(One, [{
      key: "nextVideo",
      get: function get() {
        throw new Error("<One>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<One>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "openGame",
      get: function get() {
        throw new Error("<One>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<One>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "duration",
      get: function get() {
        throw new Error("<One>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<One>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "episode",
      get: function get() {
        throw new Error("<One>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<One>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return One;
  }(SvelteComponentDev);

  function _createSuper$r(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$r(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$r() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$r = "src/Interactive/Two.svelte";

  // (37:2) {#if duration && isTimerVisible}
  function create_if_block$f(ctx) {
    var div;
    var timer;
    var t;
    var img;
    var img_src_value;
    var current;
    var mounted;
    var dispose;
    timer = new Timer({
      props: {
        duration: /*duration*/ctx[0]
      },
      $$inline: true
    });
    timer.$on("timerend", /*timerEnd*/ctx[2]);
    var block = {
      c: function create() {
        div = element("div");
        create_component(timer.$$.fragment);
        t = space();
        img = element("img");
        attr_dev(div, "class", "timer svelte-e8gohb");
        add_location(div, file$r, 37, 4, 1290);
        if (!src_url_equal(img.src, img_src_value = "".concat(BASE_URL, "/images/continue.png"))) attr_dev(img, "src", img_src_value);
        attr_dev(img, "alt", "cards");
        attr_dev(img, "class", "continue appear svelte-e8gohb");
        add_location(img, file$r, 41, 4, 1376);
      },
      m: function mount(target, anchor) {
        insert_dev(target, div, anchor);
        mount_component(timer, div, null);
        insert_dev(target, t, anchor);
        insert_dev(target, img, anchor);
        current = true;
        if (!mounted) {
          dispose = listen_dev(img, "click", /*onSkip*/ctx[4], false, false, false, false);
          mounted = true;
        }
      },
      p: function update(ctx, dirty) {
        var timer_changes = {};
        if (dirty & /*duration*/1) timer_changes.duration = /*duration*/ctx[0];
        timer.$set(timer_changes);
      },
      i: function intro(local) {
        if (current) return;
        transition_in(timer.$$.fragment, local);
        current = true;
      },
      o: function outro(local) {
        transition_out(timer.$$.fragment, local);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div);
        destroy_component(timer);
        if (detaching) detach_dev(t);
        if (detaching) detach_dev(img);
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block$f.name,
      type: "if",
      source: "(37:2) {#if duration && isTimerVisible}",
      ctx: ctx
    });
    return block;
  }
  function create_fragment$r(ctx) {
    var div;
    var img0;
    var img0_src_value;
    var t0;
    var img1;
    var img1_src_value;
    var t1;
    var img2;
    var img2_src_value;
    var t2;
    var img3;
    var img3_src_value;
    var t3;
    var img4;
    var img4_src_value;
    var t4;
    var img5;
    var img5_src_value;
    var t5;
    var img6;
    var img6_src_value;
    var t6;
    var current;
    var mounted;
    var dispose;
    var if_block = /*duration*/ctx[0] && /*isTimerVisible*/ctx[1] && create_if_block$f(ctx);
    var block = {
      c: function create() {
        div = element("div");
        img0 = element("img");
        t0 = space();
        img1 = element("img");
        t1 = space();
        img2 = element("img");
        t2 = space();
        img3 = element("img");
        t3 = space();
        img4 = element("img");
        t4 = space();
        img5 = element("img");
        t5 = space();
        img6 = element("img");
        t6 = space();
        if (if_block) if_block.c();
        if (!src_url_equal(img0.src, img0_src_value = "".concat(BASE_URL, "/images/chip3.png"))) attr_dev(img0, "src", img0_src_value);
        attr_dev(img0, "alt", "chip1");
        attr_dev(img0, "class", "chip3 svelte-e8gohb");
        add_location(img0, file$r, 25, 2, 704);
        if (!src_url_equal(img1.src, img1_src_value = "".concat(BASE_URL, "/images/chip1.png"))) attr_dev(img1, "src", img1_src_value);
        attr_dev(img1, "alt", "chip1");
        attr_dev(img1, "class", "chip1 svelte-e8gohb");
        add_location(img1, file$r, 26, 2, 777);
        if (!src_url_equal(img2.src, img2_src_value = "".concat(BASE_URL, "/images/chip4.png"))) attr_dev(img2, "src", img2_src_value);
        attr_dev(img2, "alt", "chip1");
        attr_dev(img2, "class", "chip4 svelte-e8gohb");
        add_location(img2, file$r, 27, 2, 850);
        if (!src_url_equal(img3.src, img3_src_value = "".concat(BASE_URL, "/images/chip2.png"))) attr_dev(img3, "src", img3_src_value);
        attr_dev(img3, "alt", "chip1");
        attr_dev(img3, "class", "chip2 svelte-e8gohb");
        add_location(img3, file$r, 28, 2, 923);
        if (!src_url_equal(img4.src, img4_src_value = "".concat(BASE_URL, "/images/logo.png"))) attr_dev(img4, "src", img4_src_value);
        attr_dev(img4, "alt", "logo");
        attr_dev(img4, "class", "logo svelte-e8gohb");
        add_location(img4, file$r, 29, 2, 996);
        if (!src_url_equal(img5.src, img5_src_value = "".concat(BASE_URL, "/images/help1.png"))) attr_dev(img5, "src", img5_src_value);
        attr_dev(img5, "alt", "cards");
        attr_dev(img5, "class", "help1 svelte-e8gohb");
        add_location(img5, file$r, 30, 2, 1066);
        if (!src_url_equal(img6.src, img6_src_value = "".concat(BASE_URL, "/images/playpocker.png"))) attr_dev(img6, "src", img6_src_value);
        attr_dev(img6, "alt", "cards");
        attr_dev(img6, "class", "button svelte-e8gohb");
        add_location(img6, file$r, 31, 2, 1139);
        attr_dev(div, "class", "container appear svelte-e8gohb");
        add_location(div, file$r, 24, 0, 671);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, div, anchor);
        append_dev(div, img0);
        append_dev(div, t0);
        append_dev(div, img1);
        append_dev(div, t1);
        append_dev(div, img2);
        append_dev(div, t2);
        append_dev(div, img3);
        append_dev(div, t3);
        append_dev(div, img4);
        append_dev(div, t4);
        append_dev(div, img5);
        append_dev(div, t5);
        append_dev(div, img6);
        append_dev(div, t6);
        if (if_block) if_block.m(div, null);
        current = true;
        if (!mounted) {
          dispose = listen_dev(img6, "click", /*onClick*/ctx[3], false, false, false, false);
          mounted = true;
        }
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if ( /*duration*/ctx[0] && /*isTimerVisible*/ctx[1]) {
          if (if_block) {
            if_block.p(ctx, dirty);
            if (dirty & /*duration, isTimerVisible*/3) {
              transition_in(if_block, 1);
            }
          } else {
            if_block = create_if_block$f(ctx);
            if_block.c();
            transition_in(if_block, 1);
            if_block.m(div, null);
          }
        } else if (if_block) {
          group_outros();
          transition_out(if_block, 1, 1, function () {
            if_block = null;
          });
          check_outros();
        }
      },
      i: function intro(local) {
        if (current) return;
        transition_in(if_block);
        current = true;
      },
      o: function outro(local) {
        transition_out(if_block);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div);
        if (if_block) if_block.d();
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$r.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$r($$self, $$props, $$invalidate) {
    var $adDisabled;
    validate_store(adDisabled, 'adDisabled');
    component_subscribe($$self, adDisabled, function ($$value) {
      return $$invalidate(8, $adDisabled = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('Two', slots, []);
    var nextVideo = $$props.nextVideo;
    var openGame = $$props.openGame;
    var duration = $$props.duration;
    var episode = $$props.episode;
    var isTimerVisible = true;
    var timerEnd = function timerEnd() {
      return $$invalidate(1, isTimerVisible = false);
    };
    sendMetrik$1("GameAdShow", {
      "Gambler.GameAdEpisode": episode
    });
    var onClick = function onClick() {
      openGame();
      sendMetrik$1("GameAdClick", {
        "Gambler.GameAdClickEpisode": episode
      });
    };
    var onSkip = function onSkip() {
      sendMetrik$1("GameAdSkip", {
        "Gambler.GameAdSkipEpisode": episode
      });
      nextVideo();
    };
    if ($adDisabled) {
      nextVideo();
    }
    $$self.$$.on_mount.push(function () {
      if (nextVideo === undefined && !('nextVideo' in $$props || $$self.$$.bound[$$self.$$.props['nextVideo']])) {
        console.warn("<Two> was created without expected prop 'nextVideo'");
      }
      if (openGame === undefined && !('openGame' in $$props || $$self.$$.bound[$$self.$$.props['openGame']])) {
        console.warn("<Two> was created without expected prop 'openGame'");
      }
      if (duration === undefined && !('duration' in $$props || $$self.$$.bound[$$self.$$.props['duration']])) {
        console.warn("<Two> was created without expected prop 'duration'");
      }
      if (episode === undefined && !('episode' in $$props || $$self.$$.bound[$$self.$$.props['episode']])) {
        console.warn("<Two> was created without expected prop 'episode'");
      }
    });
    var writable_props = ['nextVideo', 'openGame', 'duration', 'episode'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<Two> was created with unknown prop '".concat(key, "'"));
    });
    $$self.$$set = function ($$props) {
      if ('nextVideo' in $$props) $$invalidate(5, nextVideo = $$props.nextVideo);
      if ('openGame' in $$props) $$invalidate(6, openGame = $$props.openGame);
      if ('duration' in $$props) $$invalidate(0, duration = $$props.duration);
      if ('episode' in $$props) $$invalidate(7, episode = $$props.episode);
    };
    $$self.$capture_state = function () {
      return {
        BASE_URL: BASE_URL,
        Timer: Timer,
        sendMetrik: sendMetrik$1,
        adDisabled: adDisabled,
        nextVideo: nextVideo,
        openGame: openGame,
        duration: duration,
        episode: episode,
        isTimerVisible: isTimerVisible,
        timerEnd: timerEnd,
        onClick: onClick,
        onSkip: onSkip,
        $adDisabled: $adDisabled
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('nextVideo' in $$props) $$invalidate(5, nextVideo = $$props.nextVideo);
      if ('openGame' in $$props) $$invalidate(6, openGame = $$props.openGame);
      if ('duration' in $$props) $$invalidate(0, duration = $$props.duration);
      if ('episode' in $$props) $$invalidate(7, episode = $$props.episode);
      if ('isTimerVisible' in $$props) $$invalidate(1, isTimerVisible = $$props.isTimerVisible);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    return [duration, isTimerVisible, timerEnd, onClick, onSkip, nextVideo, openGame, episode];
  }
  var Two = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(Two, _SvelteComponentDev);
    var _super = _createSuper$r(Two);
    function Two(options) {
      var _this;
      _classCallCheck(this, Two);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$r, create_fragment$r, safe_not_equal, {
        nextVideo: 5,
        openGame: 6,
        duration: 0,
        episode: 7
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "Two",
        options: options,
        id: create_fragment$r.name
      });
      return _this;
    }
    _createClass(Two, [{
      key: "nextVideo",
      get: function get() {
        throw new Error("<Two>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Two>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "openGame",
      get: function get() {
        throw new Error("<Two>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Two>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "duration",
      get: function get() {
        throw new Error("<Two>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Two>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "episode",
      get: function get() {
        throw new Error("<Two>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Two>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return Two;
  }(SvelteComponentDev);

  function _createSuper$q(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$q(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$q() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$q = "src/Interactive/Three.svelte";

  // (24:6) {#if duration && isTimerVisible}
  function create_if_block$e(ctx) {
    var div;
    var timer;
    var t;
    var img;
    var img_src_value;
    var current;
    var mounted;
    var dispose;
    timer = new Timer({
      props: {
        duration: /*duration*/ctx[2]
      },
      $$inline: true
    });
    timer.$on("timerend", /*timerEnd*/ctx[4]);
    var block = {
      c: function create() {
        div = element("div");
        create_component(timer.$$.fragment);
        t = space();
        img = element("img");
        attr_dev(div, "class", "timer svelte-57qhit");
        add_location(div, file$q, 24, 6, 901);
        if (!src_url_equal(img.src, img_src_value = "".concat(BASE_URL, "/images/continue.png"))) attr_dev(img, "src", img_src_value);
        attr_dev(img, "alt", "cards");
        attr_dev(img, "class", "continue appear svelte-57qhit");
        add_location(img, file$q, 28, 6, 995);
      },
      m: function mount(target, anchor) {
        insert_dev(target, div, anchor);
        mount_component(timer, div, null);
        insert_dev(target, t, anchor);
        insert_dev(target, img, anchor);
        current = true;
        if (!mounted) {
          dispose = listen_dev(img, "click", function () {
            if (is_function( /*nextVideo*/ctx[0])) /*nextVideo*/ctx[0].apply(this, arguments);
          }, false, false, false, false);
          mounted = true;
        }
      },
      p: function update(new_ctx, dirty) {
        ctx = new_ctx;
        var timer_changes = {};
        if (dirty & /*duration*/4) timer_changes.duration = /*duration*/ctx[2];
        timer.$set(timer_changes);
      },
      i: function intro(local) {
        if (current) return;
        transition_in(timer.$$.fragment, local);
        current = true;
      },
      o: function outro(local) {
        transition_out(timer.$$.fragment, local);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div);
        destroy_component(timer);
        if (detaching) detach_dev(t);
        if (detaching) detach_dev(img);
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block$e.name,
      type: "if",
      source: "(24:6) {#if duration && isTimerVisible}",
      ctx: ctx
    });
    return block;
  }
  function create_fragment$q(ctx) {
    var div;
    var img0;
    var img0_src_value;
    var t0;
    var img1;
    var img1_src_value;
    var t1;
    var img2;
    var img2_src_value;
    var t2;
    var img3;
    var img3_src_value;
    var t3;
    var img4;
    var img4_src_value;
    var t4;
    var img5;
    var img5_src_value;
    var t5;
    var img6;
    var img6_src_value;
    var t6;
    var current;
    var mounted;
    var dispose;
    var if_block = /*duration*/ctx[2] && /*isTimerVisible*/ctx[3] && create_if_block$e(ctx);
    var block = {
      c: function create() {
        div = element("div");
        img0 = element("img");
        t0 = space();
        img1 = element("img");
        t1 = space();
        img2 = element("img");
        t2 = space();
        img3 = element("img");
        t3 = space();
        img4 = element("img");
        t4 = space();
        img5 = element("img");
        t5 = space();
        img6 = element("img");
        t6 = space();
        if (if_block) if_block.c();
        if (!src_url_equal(img0.src, img0_src_value = "".concat(BASE_URL, "/images/chip3.png"))) attr_dev(img0, "src", img0_src_value);
        attr_dev(img0, "alt", "chip1");
        attr_dev(img0, "class", "chip3 svelte-57qhit");
        add_location(img0, file$q, 12, 4, 292);
        if (!src_url_equal(img1.src, img1_src_value = "".concat(BASE_URL, "/images/chip1.png"))) attr_dev(img1, "src", img1_src_value);
        attr_dev(img1, "alt", "chip1");
        attr_dev(img1, "class", "chip1 svelte-57qhit");
        add_location(img1, file$q, 13, 4, 367);
        if (!src_url_equal(img2.src, img2_src_value = "".concat(BASE_URL, "/images/chip4.png"))) attr_dev(img2, "src", img2_src_value);
        attr_dev(img2, "alt", "chip1");
        attr_dev(img2, "class", "chip4 svelte-57qhit");
        add_location(img2, file$q, 14, 4, 442);
        if (!src_url_equal(img3.src, img3_src_value = "".concat(BASE_URL, "/images/chip2.png"))) attr_dev(img3, "src", img3_src_value);
        attr_dev(img3, "alt", "chip1");
        attr_dev(img3, "class", "chip2 svelte-57qhit");
        add_location(img3, file$q, 15, 4, 517);
        if (!src_url_equal(img4.src, img4_src_value = "".concat(BASE_URL, "/images/logo.png"))) attr_dev(img4, "src", img4_src_value);
        attr_dev(img4, "alt", "logo");
        attr_dev(img4, "class", "logo svelte-57qhit");
        add_location(img4, file$q, 16, 4, 592);
        if (!src_url_equal(img5.src, img5_src_value = "".concat(BASE_URL, "/images/help2.png"))) attr_dev(img5, "src", img5_src_value);
        attr_dev(img5, "alt", "cards");
        attr_dev(img5, "class", "help2 svelte-57qhit");
        add_location(img5, file$q, 17, 4, 664);
        if (!src_url_equal(img6.src, img6_src_value = "".concat(BASE_URL, "/images/button.png"))) attr_dev(img6, "src", img6_src_value);
        attr_dev(img6, "alt", "cards");
        attr_dev(img6, "class", "button svelte-57qhit");
        add_location(img6, file$q, 18, 4, 739);
        attr_dev(div, "class", "container appear svelte-57qhit");
        add_location(div, file$q, 11, 2, 257);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, div, anchor);
        append_dev(div, img0);
        append_dev(div, t0);
        append_dev(div, img1);
        append_dev(div, t1);
        append_dev(div, img2);
        append_dev(div, t2);
        append_dev(div, img3);
        append_dev(div, t3);
        append_dev(div, img4);
        append_dev(div, t4);
        append_dev(div, img5);
        append_dev(div, t5);
        append_dev(div, img6);
        append_dev(div, t6);
        if (if_block) if_block.m(div, null);
        current = true;
        if (!mounted) {
          dispose = listen_dev(img6, "click", function () {
            if (is_function( /*openGame*/ctx[1])) /*openGame*/ctx[1].apply(this, arguments);
          }, false, false, false, false);
          mounted = true;
        }
      },
      p: function update(new_ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        ctx = new_ctx;
        if ( /*duration*/ctx[2] && /*isTimerVisible*/ctx[3]) {
          if (if_block) {
            if_block.p(ctx, dirty);
            if (dirty & /*duration, isTimerVisible*/12) {
              transition_in(if_block, 1);
            }
          } else {
            if_block = create_if_block$e(ctx);
            if_block.c();
            transition_in(if_block, 1);
            if_block.m(div, null);
          }
        } else if (if_block) {
          group_outros();
          transition_out(if_block, 1, 1, function () {
            if_block = null;
          });
          check_outros();
        }
      },
      i: function intro(local) {
        if (current) return;
        transition_in(if_block);
        current = true;
      },
      o: function outro(local) {
        transition_out(if_block);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div);
        if (if_block) if_block.d();
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$q.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$q($$self, $$props, $$invalidate) {
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('Three', slots, []);
    var nextVideo = $$props.nextVideo;
    var openGame = $$props.openGame;
    var duration = $$props.duration;
    var isTimerVisible = true;
    var timerEnd = function timerEnd() {
      return $$invalidate(3, isTimerVisible = false);
    };
    $$self.$$.on_mount.push(function () {
      if (nextVideo === undefined && !('nextVideo' in $$props || $$self.$$.bound[$$self.$$.props['nextVideo']])) {
        console.warn("<Three> was created without expected prop 'nextVideo'");
      }
      if (openGame === undefined && !('openGame' in $$props || $$self.$$.bound[$$self.$$.props['openGame']])) {
        console.warn("<Three> was created without expected prop 'openGame'");
      }
      if (duration === undefined && !('duration' in $$props || $$self.$$.bound[$$self.$$.props['duration']])) {
        console.warn("<Three> was created without expected prop 'duration'");
      }
    });
    var writable_props = ['nextVideo', 'openGame', 'duration'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<Three> was created with unknown prop '".concat(key, "'"));
    });
    $$self.$$set = function ($$props) {
      if ('nextVideo' in $$props) $$invalidate(0, nextVideo = $$props.nextVideo);
      if ('openGame' in $$props) $$invalidate(1, openGame = $$props.openGame);
      if ('duration' in $$props) $$invalidate(2, duration = $$props.duration);
    };
    $$self.$capture_state = function () {
      return {
        BASE_URL: BASE_URL,
        Timer: Timer,
        nextVideo: nextVideo,
        openGame: openGame,
        duration: duration,
        isTimerVisible: isTimerVisible,
        timerEnd: timerEnd
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('nextVideo' in $$props) $$invalidate(0, nextVideo = $$props.nextVideo);
      if ('openGame' in $$props) $$invalidate(1, openGame = $$props.openGame);
      if ('duration' in $$props) $$invalidate(2, duration = $$props.duration);
      if ('isTimerVisible' in $$props) $$invalidate(3, isTimerVisible = $$props.isTimerVisible);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    return [nextVideo, openGame, duration, isTimerVisible, timerEnd];
  }
  var Three = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(Three, _SvelteComponentDev);
    var _super = _createSuper$q(Three);
    function Three(options) {
      var _this;
      _classCallCheck(this, Three);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$q, create_fragment$q, safe_not_equal, {
        nextVideo: 0,
        openGame: 1,
        duration: 2
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "Three",
        options: options,
        id: create_fragment$q.name
      });
      return _this;
    }
    _createClass(Three, [{
      key: "nextVideo",
      get: function get() {
        throw new Error("<Three>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Three>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "openGame",
      get: function get() {
        throw new Error("<Three>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Three>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "duration",
      get: function get() {
        throw new Error("<Three>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Three>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return Three;
  }(SvelteComponentDev);

  function cubicOut(t) {
    var f = t - 1.0;
    return f * f * f + 1.0;
  }

  function fade(node) {
    var _ref2 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      _ref2$delay = _ref2.delay,
      delay = _ref2$delay === void 0 ? 0 : _ref2$delay,
      _ref2$duration = _ref2.duration,
      duration = _ref2$duration === void 0 ? 400 : _ref2$duration,
      _ref2$easing = _ref2.easing,
      easing = _ref2$easing === void 0 ? identity : _ref2$easing;
    var o = +getComputedStyle(node).opacity;
    return {
      delay: delay,
      duration: duration,
      easing: easing,
      css: function css(t) {
        return "opacity: ".concat(t * o);
      }
    };
  }
  function fly(node) {
    var _ref3 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      _ref3$delay = _ref3.delay,
      delay = _ref3$delay === void 0 ? 0 : _ref3$delay,
      _ref3$duration = _ref3.duration,
      duration = _ref3$duration === void 0 ? 400 : _ref3$duration,
      _ref3$easing = _ref3.easing,
      easing = _ref3$easing === void 0 ? cubicOut : _ref3$easing,
      _ref3$x = _ref3.x,
      x = _ref3$x === void 0 ? 0 : _ref3$x,
      _ref3$y = _ref3.y,
      y = _ref3$y === void 0 ? 0 : _ref3$y,
      _ref3$opacity = _ref3.opacity,
      opacity = _ref3$opacity === void 0 ? 0 : _ref3$opacity;
    var style = getComputedStyle(node);
    var target_opacity = +style.opacity;
    var transform = style.transform === 'none' ? '' : style.transform;
    var od = target_opacity * (1 - opacity);
    var _split_css_unit3 = split_css_unit(x),
      _split_css_unit4 = _slicedToArray(_split_css_unit3, 2),
      xValue = _split_css_unit4[0],
      xUnit = _split_css_unit4[1];
    var _split_css_unit5 = split_css_unit(y),
      _split_css_unit6 = _slicedToArray(_split_css_unit5, 2),
      yValue = _split_css_unit6[0],
      yUnit = _split_css_unit6[1];
    return {
      delay: delay,
      duration: duration,
      easing: easing,
      css: function css(t, u) {
        return "\n\t\t\ttransform: ".concat(transform, " translate(").concat((1 - t) * xValue).concat(xUnit, ", ").concat((1 - t) * yValue).concat(yUnit, ");\n\t\t\topacity: ").concat(target_opacity - od * u);
      }
    };
  }

  function _createSuper$p(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$p(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$p() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$p = "src/Charity/assets/PressButton.svelte";
  function create_fragment$p(ctx) {
    var svg;
    var path;
    var svg_width_value;
    var block = {
      c: function create() {
        svg = svg_element("svg");
        path = svg_element("path");
        attr_dev(path, "d", "M55.7686 10.5418C55.7686 10.0884 55.7751 9.70633 55.7881 9.39548C55.814\n    9.07167 55.8399 8.74139 55.8658 8.40463H55.5355C55.4837 8.50825 55.4189\n    8.62482 55.3412 8.75434C55.2765 8.87091 55.2052 8.98748 55.1275\n    9.10406C55.0627 9.20767 54.998 9.30481 54.9332 9.39548C54.8685 9.47319\n    54.8231 9.53148 54.7972 9.57034L51.339 14H47.531L53.1652 6.77265L47.9779\n    0.167013H51.7081L54.7972 4.11097C54.8231 4.14982 54.8685 4.21459 54.9332\n    4.30525C54.998 4.38297 55.0627 4.48011 55.1275 4.59668C55.2052 4.7003\n    55.2765 4.81687 55.3412 4.94639C55.4189 5.06296 55.4837 5.17305 55.5355\n    5.27667H55.8658C55.8399 4.93991 55.814 4.55134 55.7881 4.11097C55.7751\n    3.65764 55.7686 3.21726 55.7686 2.78984V0.167013H58.8189V2.78984C58.8189\n    3.21726 58.8059 3.65764 58.78 4.11097C58.7671 4.55134 58.7477 4.93991\n    58.7218 5.27667H59.052C59.1038 5.17305 59.1621 5.06296 59.2269\n    4.94639C59.3046 4.81687 59.3758 4.7003 59.4406 4.59668C59.5183 4.48011\n    59.5896 4.38297 59.6543 4.30525C59.7191 4.21459 59.7644 4.14982 59.7903\n    4.11097L62.8794 0.167013H66.6097L61.4612 6.77265L67.0565 14H63.2486L59.7903\n    9.57034C59.7644 9.53148 59.7191 9.47319 59.6543 9.39548C59.5896 9.30481\n    59.5183 9.20767 59.4406 9.10406C59.3758 8.98748 59.3046 8.87091 59.2269\n    8.75434C59.1621 8.62482 59.1038 8.50825 59.052 8.40463H58.7218C58.7477\n    8.74139 58.7671 9.07167 58.78 9.39548C58.8059 9.70633 58.8189 10.0884\n    58.8189 10.5418V14H55.7686V10.5418ZM74.362 14H71.2924V0.167013H73.41L78.0146\n    4.77153L82.6191 0.167013H84.7368V14H81.6671V7.88007C81.6671 7.41379 81.68\n    6.94751 81.706 6.48123C81.7319 6.00199 81.7707 5.51629 81.8225\n    5.0241H81.6865C81.026 5.96961 80.482 6.65608 80.0545 7.08351L78.0146\n    9.10406L75.994 7.08351C75.5666 6.65608 75.0226 5.96961 74.362\n    5.0241H74.2066C74.2584 5.51629 74.2973 6.00199 74.3232 6.48123C74.3491\n    6.94751 74.362 7.41379 74.362 7.88007V14ZM90.3906\n    0.167013H93.4797V6.26752C93.4797 6.39704 93.4732 6.5978 93.4603\n    6.86979C93.4473 7.12884 93.4279 7.4656 93.402 7.88007H93.4991C93.7064\n    7.54331 93.8812 7.26484 94.0237 7.04465C94.1662 6.81151 94.2827 6.63018\n    94.3734 6.50066L98.9391 0.167013H101.445V14H98.3756V7.43322C98.3756 7.07055\n    98.3951 6.57837 98.4339 5.95666H98.3562C98.1749 6.25456 98.0065 6.52008\n    97.8511 6.75322C97.7086 6.98636 97.5661 7.1936 97.4236 7.37493L92.6248\n    14H90.3906V0.167013ZM105.505 17.1668C106.217 15.0556 106.703 13.1387 106.962\n    11.416L110.265 11.1829C109.462 13.6568 108.529 15.755 107.467\n    17.4777L105.505 17.1668ZM8.07143\n    29.2378V24.167H11.1411V38H8.07143V32.2881H3.81662V38H0.746944V24.167H3.81662V29.2378H8.07143ZM16.8095\n    38V24.167H25.2025V27.1201H19.8792V29.5681H25.0471V32.4241H19.8792V35.0469H25.2025V38H16.8095ZM43.6021\n    23.9339C45.5579 23.9339 47.2093 24.6268 48.5563 26.0127C49.9163 27.3986\n    50.5963 29.0889 50.5963 31.0835C50.5963 33.0911 49.9163 34.7814 48.5563\n    36.1543C47.2222 37.5272 45.5708 38.2137 43.6021 38.2137C41.6593 38.2137\n    40.0078 37.5272 38.6479 36.1543C37.2879 34.7814 36.6079 33.0911 36.6079\n    31.0835C36.6079 29.0889 37.2879 27.3986 38.6479 26.0127C40.0078 24.6268\n    41.6593 23.9339 43.6021 23.9339ZM43.6021 26.9841C42.4623 26.9841 41.5297\n    27.3856 40.8044 28.1887C40.0791 28.9917 39.7164 29.9567 39.7164\n    31.0835C39.7164 32.2104 40.0791 33.1753 40.8044 33.9783C41.5427 34.7684\n    42.4753 35.1635 43.6021 35.1635C44.7419 35.1635 45.668 34.7684 46.3803\n    33.9783C47.1057 33.1753 47.4683 32.2104 47.4683 31.0835C47.4683 29.9567\n    47.1057 28.9917 46.3803 28.1887C45.655 27.3856 44.7289 26.9841 43.6021\n    26.9841ZM60.108\n    27.1978V38H57.0189V27.1978H53.9492V24.167H63.1777V27.1978H60.108ZM67.7975\n    24.167H78.1917V38H75.122V27.1978H70.8672V38H67.7975V24.167ZM88.7366\n    28.9852C88.8402 29.1925 88.9179 29.3803 88.9697 29.5487C89.0215 29.717\n    89.0798 29.8984 89.1446 30.0927H89.3C89.3648 29.8984 89.4231 29.717 89.4749\n    29.5487C89.5267 29.3803 89.6044 29.1925 89.708 28.9852L91.9423\n    24.167H95.5559L89.028 38H85.5115L87.532 33.6675L82.5778\n    24.167H86.2692L88.7366 28.9852ZM105.386 26.9841C104.233 26.9841 103.301\n    27.3792 102.589 28.1693C101.863 28.9723 101.501 29.9372 101.501\n    31.0641C101.501 32.2039 101.863 33.1753 102.589 33.9783C103.301 34.7684\n    104.233 35.1635 105.386 35.1635C106.435 35.1635 107.536 34.6518 108.689\n    33.6286V37.32C107.51 37.9158 106.409 38.2137 105.386 38.2137C103.43 38.2137\n    101.773 37.5208 100.413 36.1349C99.0526 34.7619 98.3726 33.0717 98.3726\n    31.0641C98.3726 29.0824 99.0526 27.3986 100.413 26.0127C101.773 24.6268\n    103.43 23.9339 105.386 23.9339C106.409 23.9339 107.51 24.2318 108.689\n    24.8276V28.519C107.51 27.4957 106.409 26.9841 105.386 26.9841ZM117.669\n    33.1818C117.643 33.1429 117.597 33.0846 117.533 33.0069C117.468 32.9162\n    117.397 32.8191 117.319 32.7155C117.254 32.5989 117.183 32.4823 117.105\n    32.3658C117.04 32.2363 116.982 32.1197 116.93 32.0161H116.794C116.82 32.3528\n    116.84 32.6831 116.853 33.0069C116.879 33.3178 116.891 33.6999 116.891\n    34.1532V38H113.841V24.167H116.891V27.1978C116.891 27.6253 116.879 28.0656\n    116.853 28.519C116.84 28.9593 116.82 29.3479 116.794 29.6847H116.93C116.982\n    29.581 117.04 29.471 117.105 29.3544C117.183 29.2249 117.254 29.1083 117.319\n    29.0047C117.397 28.8881 117.468 28.791 117.533 28.7132C117.597 28.6226\n    117.643 28.5578 117.669 28.519L121.146 24.167H124.935L119.611\n    30.7727L125.382 38H121.515L117.669 33.1818ZM136.73 24.167L142.733\n    38H139.294L138.187 35.1829H132.961L131.853 38H128.415L134.418\n    24.167H136.73ZM134.049 32.3463H137.06L136.341 30.3841C136.29 30.2546 136.205\n    30.0149 136.089 29.6652C135.972 29.3026 135.823 28.8104 135.642\n    28.1887H135.467C135.467 28.2275 135.383 28.5254 135.215 29.0824C135.124\n    29.3803 135.046 29.6393 134.981 29.8595C134.917 30.0797 134.852 30.261\n    134.787 30.4035L134.049 32.3463ZM147.2 24.167H150.289V30.2675C150.289 30.397\n    150.282 30.5978 150.269 30.8698C150.256 31.1288 150.237 31.4656 150.211\n    31.8801H150.308C150.515 31.5433 150.69 31.2648 150.833 31.0446C150.975\n    30.8115 151.092 30.6302 151.182 30.5007L155.748\n    24.167H158.254V38H155.185V31.4332C155.185 31.0706 155.204 30.5784 155.243\n    29.9567H155.165C154.984 30.2546 154.815 30.5201 154.66 30.7532C154.518\n    30.9864 154.375 31.1936 154.233 31.3749L149.434 38H147.2V24.167ZM156.467\n    20.5922C156.052 21.5248 155.515 22.1983 154.854 22.6127C154.194 23.0272\n    153.507 23.2345 152.795 23.2345C152.083 23.2345 151.396 23.0272 150.736\n    22.6127C150.075 22.1983 149.537 21.5248 149.123 20.5922L150.852\n    19.7762C151.137 20.3979 151.435 20.7994 151.746 20.9808C152.057 21.1621\n    152.406 21.2528 152.795 21.2528C152.963 21.2528 153.125 21.2398 153.281\n    21.2139C153.449 21.188 153.611 21.1297 153.766 21.039C153.935 20.9354\n    154.097 20.7865 154.252 20.5922C154.42 20.385 154.595 20.1065 154.777\n    19.7568L156.467 20.5922Z");
        attr_dev(path, "fill", "white");
        add_location(path, file$p, 11, 2, 184);
        attr_dev(svg, "width", svg_width_value = /*height*/ctx[0] * 4.077);
        attr_dev(svg, "height", /*height*/ctx[0]);
        attr_dev(svg, "viewBox", "0 0 159 39");
        attr_dev(svg, "fill", "none");
        attr_dev(svg, "xmlns", "http://www.w3.org/2000/svg");
        attr_dev(svg, "class", "pulsated svelte-1shx6ec");
        add_location(svg, file$p, 4, 0, 47);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, svg, anchor);
        append_dev(svg, path);
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if (dirty & /*height*/1 && svg_width_value !== (svg_width_value = /*height*/ctx[0] * 4.077)) {
          attr_dev(svg, "width", svg_width_value);
        }
        if (dirty & /*height*/1) {
          attr_dev(svg, "height", /*height*/ctx[0]);
        }
      },
      i: noop,
      o: noop,
      d: function destroy(detaching) {
        if (detaching) detach_dev(svg);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$p.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$p($$self, $$props, $$invalidate) {
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('PressButton', slots, []);
    var _$$props$height = $$props.height,
      height = _$$props$height === void 0 ? 159 : _$$props$height;
    var writable_props = ['height'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<PressButton> was created with unknown prop '".concat(key, "'"));
    });
    $$self.$$set = function ($$props) {
      if ('height' in $$props) $$invalidate(0, height = $$props.height);
    };
    $$self.$capture_state = function () {
      return {
        height: height
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('height' in $$props) $$invalidate(0, height = $$props.height);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    return [height];
  }
  var PressButton = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(PressButton, _SvelteComponentDev);
    var _super = _createSuper$p(PressButton);
    function PressButton(options) {
      var _this;
      _classCallCheck(this, PressButton);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$p, create_fragment$p, safe_not_equal, {
        height: 0
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "PressButton",
        options: options,
        id: create_fragment$p.name
      });
      return _this;
    }
    _createClass(PressButton, [{
      key: "height",
      get: function get() {
        throw new Error("<PressButton>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<PressButton>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return PressButton;
  }(SvelteComponentDev);

  function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
    try {
      var info = gen[key](arg);
      var value = info.value;
    } catch (error) {
      reject(error);
      return;
    }
    if (info.done) {
      resolve(value);
    } else {
      Promise.resolve(value).then(_next, _throw);
    }
  }
  function _asyncToGenerator(fn) {
    return function () {
      var self = this,
        args = arguments;
      return new Promise(function (resolve, reject) {
        var gen = fn.apply(self, args);
        function _next(value) {
          asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
        }
        function _throw(err) {
          asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
        }
        _next(undefined);
      });
    };
  }

  var _typeof_1 = createCommonjsModule(function (module) {
  function _typeof(obj) {
    "@babel/helpers - typeof";

    return (module.exports = _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) {
      return typeof obj;
    } : function (obj) {
      return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    }, module.exports.__esModule = true, module.exports["default"] = module.exports), _typeof(obj);
  }
  module.exports = _typeof, module.exports.__esModule = true, module.exports["default"] = module.exports;
  });

  unwrapExports(_typeof_1);

  var regeneratorRuntime$1 = createCommonjsModule(function (module) {
  var _typeof = _typeof_1["default"];
  function _regeneratorRuntime() {
    module.exports = _regeneratorRuntime = function _regeneratorRuntime() {
      return exports;
    }, module.exports.__esModule = true, module.exports["default"] = module.exports;
    var exports = {},
      Op = Object.prototype,
      hasOwn = Op.hasOwnProperty,
      defineProperty = Object.defineProperty || function (obj, key, desc) {
        obj[key] = desc.value;
      },
      $Symbol = "function" == typeof Symbol ? Symbol : {},
      iteratorSymbol = $Symbol.iterator || "@@iterator",
      asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator",
      toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";
    function define(obj, key, value) {
      return Object.defineProperty(obj, key, {
        value: value,
        enumerable: !0,
        configurable: !0,
        writable: !0
      }), obj[key];
    }
    try {
      define({}, "");
    } catch (err) {
      define = function define(obj, key, value) {
        return obj[key] = value;
      };
    }
    function wrap(innerFn, outerFn, self, tryLocsList) {
      var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator,
        generator = Object.create(protoGenerator.prototype),
        context = new Context(tryLocsList || []);
      return defineProperty(generator, "_invoke", {
        value: makeInvokeMethod(innerFn, self, context)
      }), generator;
    }
    function tryCatch(fn, obj, arg) {
      try {
        return {
          type: "normal",
          arg: fn.call(obj, arg)
        };
      } catch (err) {
        return {
          type: "throw",
          arg: err
        };
      }
    }
    exports.wrap = wrap;
    var ContinueSentinel = {};
    function Generator() {}
    function GeneratorFunction() {}
    function GeneratorFunctionPrototype() {}
    var IteratorPrototype = {};
    define(IteratorPrototype, iteratorSymbol, function () {
      return this;
    });
    var getProto = Object.getPrototypeOf,
      NativeIteratorPrototype = getProto && getProto(getProto(values([])));
    NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype);
    var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype);
    function defineIteratorMethods(prototype) {
      ["next", "throw", "return"].forEach(function (method) {
        define(prototype, method, function (arg) {
          return this._invoke(method, arg);
        });
      });
    }
    function AsyncIterator(generator, PromiseImpl) {
      function invoke(method, arg, resolve, reject) {
        var record = tryCatch(generator[method], generator, arg);
        if ("throw" !== record.type) {
          var result = record.arg,
            value = result.value;
          return value && "object" == _typeof(value) && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) {
            invoke("next", value, resolve, reject);
          }, function (err) {
            invoke("throw", err, resolve, reject);
          }) : PromiseImpl.resolve(value).then(function (unwrapped) {
            result.value = unwrapped, resolve(result);
          }, function (error) {
            return invoke("throw", error, resolve, reject);
          });
        }
        reject(record.arg);
      }
      var previousPromise;
      defineProperty(this, "_invoke", {
        value: function value(method, arg) {
          function callInvokeWithMethodAndArg() {
            return new PromiseImpl(function (resolve, reject) {
              invoke(method, arg, resolve, reject);
            });
          }
          return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg();
        }
      });
    }
    function makeInvokeMethod(innerFn, self, context) {
      var state = "suspendedStart";
      return function (method, arg) {
        if ("executing" === state) throw new Error("Generator is already running");
        if ("completed" === state) {
          if ("throw" === method) throw arg;
          return doneResult();
        }
        for (context.method = method, context.arg = arg;;) {
          var delegate = context.delegate;
          if (delegate) {
            var delegateResult = maybeInvokeDelegate(delegate, context);
            if (delegateResult) {
              if (delegateResult === ContinueSentinel) continue;
              return delegateResult;
            }
          }
          if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) {
            if ("suspendedStart" === state) throw state = "completed", context.arg;
            context.dispatchException(context.arg);
          } else "return" === context.method && context.abrupt("return", context.arg);
          state = "executing";
          var record = tryCatch(innerFn, self, context);
          if ("normal" === record.type) {
            if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue;
            return {
              value: record.arg,
              done: context.done
            };
          }
          "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg);
        }
      };
    }
    function maybeInvokeDelegate(delegate, context) {
      var methodName = context.method,
        method = delegate.iterator[methodName];
      if (undefined === method) return context.delegate = null, "throw" === methodName && delegate.iterator["return"] && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method) || "return" !== methodName && (context.method = "throw", context.arg = new TypeError("The iterator does not provide a '" + methodName + "' method")), ContinueSentinel;
      var record = tryCatch(method, delegate.iterator, context.arg);
      if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel;
      var info = record.arg;
      return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel);
    }
    function pushTryEntry(locs) {
      var entry = {
        tryLoc: locs[0]
      };
      1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry);
    }
    function resetTryEntry(entry) {
      var record = entry.completion || {};
      record.type = "normal", delete record.arg, entry.completion = record;
    }
    function Context(tryLocsList) {
      this.tryEntries = [{
        tryLoc: "root"
      }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0);
    }
    function values(iterable) {
      if (iterable) {
        var iteratorMethod = iterable[iteratorSymbol];
        if (iteratorMethod) return iteratorMethod.call(iterable);
        if ("function" == typeof iterable.next) return iterable;
        if (!isNaN(iterable.length)) {
          var i = -1,
            next = function next() {
              for (; ++i < iterable.length;) if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next;
              return next.value = undefined, next.done = !0, next;
            };
          return next.next = next;
        }
      }
      return {
        next: doneResult
      };
    }
    function doneResult() {
      return {
        value: undefined,
        done: !0
      };
    }
    return GeneratorFunction.prototype = GeneratorFunctionPrototype, defineProperty(Gp, "constructor", {
      value: GeneratorFunctionPrototype,
      configurable: !0
    }), defineProperty(GeneratorFunctionPrototype, "constructor", {
      value: GeneratorFunction,
      configurable: !0
    }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports.isGeneratorFunction = function (genFun) {
      var ctor = "function" == typeof genFun && genFun.constructor;
      return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name));
    }, exports.mark = function (genFun) {
      return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun;
    }, exports.awrap = function (arg) {
      return {
        __await: arg
      };
    }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () {
      return this;
    }), exports.AsyncIterator = AsyncIterator, exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) {
      void 0 === PromiseImpl && (PromiseImpl = Promise);
      var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl);
      return exports.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) {
        return result.done ? result.value : iter.next();
      });
    }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () {
      return this;
    }), define(Gp, "toString", function () {
      return "[object Generator]";
    }), exports.keys = function (val) {
      var object = Object(val),
        keys = [];
      for (var key in object) keys.push(key);
      return keys.reverse(), function next() {
        for (; keys.length;) {
          var key = keys.pop();
          if (key in object) return next.value = key, next.done = !1, next;
        }
        return next.done = !0, next;
      };
    }, exports.values = values, Context.prototype = {
      constructor: Context,
      reset: function reset(skipTempReset) {
        if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined);
      },
      stop: function stop() {
        this.done = !0;
        var rootRecord = this.tryEntries[0].completion;
        if ("throw" === rootRecord.type) throw rootRecord.arg;
        return this.rval;
      },
      dispatchException: function dispatchException(exception) {
        if (this.done) throw exception;
        var context = this;
        function handle(loc, caught) {
          return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught;
        }
        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
          var entry = this.tryEntries[i],
            record = entry.completion;
          if ("root" === entry.tryLoc) return handle("end");
          if (entry.tryLoc <= this.prev) {
            var hasCatch = hasOwn.call(entry, "catchLoc"),
              hasFinally = hasOwn.call(entry, "finallyLoc");
            if (hasCatch && hasFinally) {
              if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0);
              if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc);
            } else if (hasCatch) {
              if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0);
            } else {
              if (!hasFinally) throw new Error("try statement without catch or finally");
              if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc);
            }
          }
        }
      },
      abrupt: function abrupt(type, arg) {
        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
          var entry = this.tryEntries[i];
          if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) {
            var finallyEntry = entry;
            break;
          }
        }
        finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null);
        var record = finallyEntry ? finallyEntry.completion : {};
        return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record);
      },
      complete: function complete(record, afterLoc) {
        if ("throw" === record.type) throw record.arg;
        return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel;
      },
      finish: function finish(finallyLoc) {
        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
          var entry = this.tryEntries[i];
          if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel;
        }
      },
      "catch": function _catch(tryLoc) {
        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
          var entry = this.tryEntries[i];
          if (entry.tryLoc === tryLoc) {
            var record = entry.completion;
            if ("throw" === record.type) {
              var thrown = record.arg;
              resetTryEntry(entry);
            }
            return thrown;
          }
        }
        throw new Error("illegal catch attempt");
      },
      delegateYield: function delegateYield(iterable, resultName, nextLoc) {
        return this.delegate = {
          iterator: values(iterable),
          resultName: resultName,
          nextLoc: nextLoc
        }, "next" === this.method && (this.arg = undefined), ContinueSentinel;
      }
    }, exports;
  }
  module.exports = _regeneratorRuntime, module.exports.__esModule = true, module.exports["default"] = module.exports;
  });

  unwrapExports(regeneratorRuntime$1);

  // TODO(Babel 8): Remove this file.

  var runtime = regeneratorRuntime$1();
  var regenerator = runtime;

  // Copied from https://github.com/facebook/regenerator/blob/main/packages/runtime/runtime.js#L736=
  try {
    regeneratorRuntime = runtime;
  } catch (accidentalStrictMode) {
    if (typeof globalThis === "object") {
      globalThis.regeneratorRuntime = runtime;
    } else {
      Function("r", "regeneratorRuntime = r")(runtime);
    }
  }

  // (() => {
  //   Object.keys(audioFiles).forEach(type => {
  //     audioFiles[type].forEach((item, i) => {
  //       fetch(`https://az67128.gitlab.io/svelte-sunflower/assets/${item}.mp3`)
  //         .then(res => res.blob())
  //         .then(blob => window.URL.createObjectURL(blob))
  //         .then(blob => {
  //           if (!audioBlobs[type]) audioBlobs[type] = [];
  //           audioBlobs[type].push(blob);
  //         });
  //     });
  //   });
  // })();

  var isSunflower = false;
  var IS_TEST = /(localhost|192\.168)/.test(window.location.href);
  var sendMetrik = function sendMetrik(name, payload) {
    var consoleMetrik = function consoleMetrik(id, type, name, payload) {
      return console.log(name, payload);
    };
    var ym = IS_TEST ? consoleMetrik : window.ym || consoleMetrik;
    // const ym = window.ym || consoleMetrik;

    ym(70641691, "reachGoal", "Gambler.".concat(name), payload);
  };
  var payByCard = function payByCard(amount) {
    var onSuccess = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};
    sendMetrik("PaymentCard");
    setTimeout(function () {
      var widget = new cp.CloudPayments();
      widget.charge({
        publicId: "pk_2f76d1f816a38a3b14fd7b38a5718",
        description: "Пожертвование в пользу благотворительного фонда Жизнь с ДЦП",
        amount: amount,
        currency: "RUB",
        skin: "mini"
      }, function () {
        sendMetrik("PaymentCardSuccess", {
          "Gambler.ButtonPaymentAmount": amount
        });
        onSuccess();
      }, function (reason, options) {
        sendMetrik("PaymentCardFailed");
        console.log("payment fail", reason);
      });
    }, 100);
  };
  var playSound$1 = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regenerator.mark(function _callee(type) {
      var audioElement, auidoItem;
      return regenerator.wrap(function _callee$(_context) {
        while (1) switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return");
          case 6:
            audioElement.src = auidoItem;
            audioElement.play();
          case 8:
          case "end":
            return _context.stop();
        }
      }, _callee);
    }));
    return function playSound(_x) {
      return _ref.apply(this, arguments);
    };
  }();

  var amount = writable(0);
  var intervalId = writable(null);
  var minimalAmount = 20; //75

  var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
  var showPressMore = derived([intervalId, amount], function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),
      $intervalId = _ref2[0],
      $amount = _ref2[1];
    return !$intervalId && $amount < minimalAmount && $amount > 0;
  });
  var addTime = function addTime() {
    var currentAmount = get_store_value(amount);
    if (currentAmount && currentAmount % 150 === 0) playSound$1("hold");
    amount.update(function (state) {
      return state += 0.25;
    });
  };
  var buttonStart = function buttonStart() {
    sendMetrik("ButtonDown");
    playSound$1("down");
    clearInterval(get_store_value(intervalId));
    intervalId.set(setInterval(addTime, 20));
  };
  var buttonEnd = function buttonEnd() {
    isCharityButtonPressed.set(true);
    if (!get_store_value(intervalId)) return;
    clearInterval(get_store_value(intervalId));
    var currentAmount = get_store_value(amount);
    intervalId.set(null);
    if (currentAmount >= minimalAmount) {
      sendMetrik("ButtonUp", {
        'Gambeler.ButtonValue': Math.floor(currentAmount)
      });
      playSound$1("success");
      if (document.fullscreenElement || document.webkitCurrentFullScreenElement) {
        document.exitFullscreen();
      }
      // if (isMobile) {
      //   payBySms(Math.floor(currentAmount));
      // } else {
      payByCard(Math.floor(currentAmount));
      // }
    } else {
      playSound$1("error");
      sendMetrik("ButtonFailed");
    }
  };

  function _createSuper$o(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$o(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$o() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$o = "src/Charity/components/PulseButton.svelte";

  // (40:4) {#if !$intervalId}
  function create_if_block$d(ctx) {
    var div;
    var pressbutton;
    var div_transition;
    var current;
    pressbutton = new PressButton({
      props: {
        height: /*clientWidth*/ctx[0] * 0.03
      },
      $$inline: true
    });
    var block = {
      c: function create() {
        div = element("div");
        create_component(pressbutton.$$.fragment);
        attr_dev(div, "class", "wrapper pressButton svelte-25tm0t");
        add_location(div, file$o, 40, 6, 948);
      },
      m: function mount(target, anchor) {
        insert_dev(target, div, anchor);
        mount_component(pressbutton, div, null);
        current = true;
      },
      p: function update(ctx, dirty) {
        var pressbutton_changes = {};
        if (dirty & /*clientWidth*/1) pressbutton_changes.height = /*clientWidth*/ctx[0] * 0.03;
        pressbutton.$set(pressbutton_changes);
      },
      i: function intro(local) {
        if (current) return;
        transition_in(pressbutton.$$.fragment, local);
        if (local) {
          add_render_callback(function () {
            if (!current) return;
            if (!div_transition) div_transition = create_bidirectional_transition(div, fade, {}, true);
            div_transition.run(1);
          });
        }
        current = true;
      },
      o: function outro(local) {
        transition_out(pressbutton.$$.fragment, local);
        if (local) {
          if (!div_transition) div_transition = create_bidirectional_transition(div, fade, {}, false);
          div_transition.run(0);
        }
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div);
        destroy_component(pressbutton);
        if (detaching && div_transition) div_transition.end();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block$d.name,
      type: "if",
      source: "(40:4) {#if !$intervalId}",
      ctx: ctx
    });
    return block;
  }
  function create_fragment$o(ctx) {
    var dic;
    var button;
    var div1;
    var div0;
    var t0;
    var div3;
    var div2;
    var t1;
    var div5;
    var div4;
    var t2;
    var div7;
    var div6;
    var t3;
    var current;
    var mounted;
    var dispose;
    var if_block = ! /*$intervalId*/ctx[1] && create_if_block$d(ctx);
    var block = {
      c: function create() {
        dic = element("dic");
        button = element("button");
        div1 = element("div");
        div0 = element("div");
        t0 = space();
        div3 = element("div");
        div2 = element("div");
        t1 = space();
        div5 = element("div");
        div4 = element("div");
        t2 = space();
        div7 = element("div");
        div6 = element("div");
        t3 = space();
        if (if_block) if_block.c();
        attr_dev(div0, "class", "radius1 svelte-25tm0t");
        add_location(div0, file$o, 28, 6, 683);
        attr_dev(div1, "class", "wrapper svelte-25tm0t");
        add_location(div1, file$o, 27, 4, 655);
        attr_dev(div2, "class", "radius2 svelte-25tm0t");
        add_location(div2, file$o, 31, 6, 750);
        attr_dev(div3, "class", "wrapper svelte-25tm0t");
        add_location(div3, file$o, 30, 4, 722);
        attr_dev(div4, "class", "radius3 svelte-25tm0t");
        add_location(div4, file$o, 34, 6, 817);
        attr_dev(div5, "class", "wrapper svelte-25tm0t");
        add_location(div5, file$o, 33, 4, 789);
        attr_dev(div6, "class", "radius4 svelte-25tm0t");
        add_location(div6, file$o, 37, 6, 884);
        attr_dev(div7, "class", "wrapper svelte-25tm0t");
        add_location(div7, file$o, 36, 4, 856);
        attr_dev(button, "class", "button wrapper container svelte-25tm0t");
        add_location(button, file$o, 18, 2, 424);
        attr_dev(dic, "class", "container svelte-25tm0t");
        toggle_class(dic, "isAnimated", /*$intervalId*/ctx[1]);
        add_location(dic, file$o, 17, 0, 367);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, dic, anchor);
        append_dev(dic, button);
        append_dev(button, div1);
        append_dev(div1, div0);
        append_dev(button, t0);
        append_dev(button, div3);
        append_dev(div3, div2);
        append_dev(button, t1);
        append_dev(button, div5);
        append_dev(div5, div4);
        append_dev(button, t2);
        append_dev(button, div7);
        append_dev(div7, div6);
        append_dev(button, t3);
        if (if_block) if_block.m(button, null);
        current = true;
        if (!mounted) {
          dispose = [listen_dev(button, "contextmenu", contextmenu_handler, false, false, false, false), listen_dev(button, "pointerdown", buttonStart, false, false, false, false), listen_dev(button, "pointerup", buttonEnd, false, false, false, false), listen_dev(button, "pointerleave", buttonEnd, false, false, false, false)];
          mounted = true;
        }
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if (! /*$intervalId*/ctx[1]) {
          if (if_block) {
            if_block.p(ctx, dirty);
            if (dirty & /*$intervalId*/2) {
              transition_in(if_block, 1);
            }
          } else {
            if_block = create_if_block$d(ctx);
            if_block.c();
            transition_in(if_block, 1);
            if_block.m(button, null);
          }
        } else if (if_block) {
          group_outros();
          transition_out(if_block, 1, 1, function () {
            if_block = null;
          });
          check_outros();
        }
        if (!current || dirty & /*$intervalId*/2) {
          toggle_class(dic, "isAnimated", /*$intervalId*/ctx[1]);
        }
      },
      i: function intro(local) {
        if (current) return;
        transition_in(if_block);
        current = true;
      },
      o: function outro(local) {
        transition_out(if_block);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(dic);
        if (if_block) if_block.d();
        mounted = false;
        run_all(dispose);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$o.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  var contextmenu_handler = function contextmenu_handler(e) {
    e.preventDefault();
    e.stopPropagation();
  };
  function instance$o($$self, $$props, $$invalidate) {
    var $intervalId;
    validate_store(intervalId, 'intervalId');
    component_subscribe($$self, intervalId, function ($$value) {
      return $$invalidate(1, $intervalId = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('PulseButton', slots, []);
    window.addEventListener("scroll", function () {
      if ($intervalId) buttonEnd();
    });
    var _$$props$clientWidth = $$props.clientWidth,
      clientWidth = _$$props$clientWidth === void 0 ? 500 : _$$props$clientWidth;
    var writable_props = ['clientWidth'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<PulseButton> was created with unknown prop '".concat(key, "'"));
    });
    $$self.$$set = function ($$props) {
      if ('clientWidth' in $$props) $$invalidate(0, clientWidth = $$props.clientWidth);
    };
    $$self.$capture_state = function () {
      return {
        PressButton: PressButton,
        fade: fade,
        amount: amount,
        minimalAmount: minimalAmount,
        intervalId: intervalId,
        buttonStart: buttonStart,
        buttonEnd: buttonEnd,
        isMobile: isMobile,
        clientWidth: clientWidth,
        $intervalId: $intervalId
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('clientWidth' in $$props) $$invalidate(0, clientWidth = $$props.clientWidth);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    return [clientWidth, $intervalId];
  }
  var PulseButton = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(PulseButton, _SvelteComponentDev);
    var _super = _createSuper$o(PulseButton);
    function PulseButton(options) {
      var _this;
      _classCallCheck(this, PulseButton);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$o, create_fragment$o, safe_not_equal, {
        clientWidth: 0
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "PulseButton",
        options: options,
        id: create_fragment$o.name
      });
      return _this;
    }
    _createClass(PulseButton, [{
      key: "clientWidth",
      get: function get() {
        throw new Error("<PulseButton>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<PulseButton>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return PulseButton;
  }(SvelteComponentDev);

  function _createSuper$n(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$n(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$n() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$n = "src/Charity/components/Amount.svelte";

  // (9:2) {#if $amount}
  function create_if_block_1$5(ctx) {
    var span;
    var t0_value = Math.ceil( /*$amount*/ctx[1]) + "";
    var t0;
    var t1;
    var span_style_value;
    var span_intro;
    var block = {
      c: function create() {
        span = element("span");
        t0 = text(t0_value);
        t1 = text("₽");
        attr_dev(span, "class", "money svelte-1w38vbz");
        attr_dev(span, "style", span_style_value = "font-size:".concat( /*clientWidth*/ctx[0] * 0.033, "px"));
        add_location(span, file$n, 9, 4, 270);
      },
      m: function mount(target, anchor) {
        insert_dev(target, span, anchor);
        append_dev(span, t0);
        append_dev(span, t1);
      },
      p: function update(ctx, dirty) {
        if (dirty & /*$amount*/2 && t0_value !== (t0_value = Math.ceil( /*$amount*/ctx[1]) + "")) set_data_dev(t0, t0_value);
        if (dirty & /*clientWidth*/1 && span_style_value !== (span_style_value = "font-size:".concat( /*clientWidth*/ctx[0] * 0.033, "px"))) {
          attr_dev(span, "style", span_style_value);
        }
      },
      i: function intro(local) {
        if (local) {
          if (!span_intro) {
            add_render_callback(function () {
              span_intro = create_in_transition(span, fade, {});
              span_intro.start();
            });
          }
        }
      },
      o: noop,
      d: function destroy(detaching) {
        if (detaching) detach_dev(span);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block_1$5.name,
      type: "if",
      source: "(9:2) {#if $amount}",
      ctx: ctx
    });
    return block;
  }

  // (12:2) {#if $showPressMore}
  function create_if_block$c(ctx) {
    var div1;
    var div0;
    var t;
    var div0_style_value;
    var div1_transition;
    var current;
    var block = {
      c: function create() {
        div1 = element("div");
        div0 = element("div");
        t = text("Удерживайте кнопку");
        attr_dev(div0, "class", "holdPress svelte-1w38vbz");
        attr_dev(div0, "style", div0_style_value = "font-size:".concat( /*clientWidth*/ctx[0] * 0.015, "px"));
        toggle_class(div0, "isSunflower", isSunflower);
        add_location(div0, file$n, 13, 6, 461);
        attr_dev(div1, "class", "wrapper");
        add_location(div1, file$n, 12, 4, 411);
      },
      m: function mount(target, anchor) {
        insert_dev(target, div1, anchor);
        append_dev(div1, div0);
        append_dev(div0, t);
        current = true;
      },
      p: function update(ctx, dirty) {
        if (!current || dirty & /*clientWidth*/1 && div0_style_value !== (div0_style_value = "font-size:".concat( /*clientWidth*/ctx[0] * 0.015, "px"))) {
          attr_dev(div0, "style", div0_style_value);
        }
      },
      i: function intro(local) {
        if (current) return;
        if (local) {
          add_render_callback(function () {
            if (!current) return;
            if (!div1_transition) div1_transition = create_bidirectional_transition(div1, fade, {}, true);
            div1_transition.run(1);
          });
        }
        current = true;
      },
      o: function outro(local) {
        if (local) {
          if (!div1_transition) div1_transition = create_bidirectional_transition(div1, fade, {}, false);
          div1_transition.run(0);
        }
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div1);
        if (detaching && div1_transition) div1_transition.end();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block$c.name,
      type: "if",
      source: "(12:2) {#if $showPressMore}",
      ctx: ctx
    });
    return block;
  }
  function create_fragment$n(ctx) {
    var div;
    var t;
    var div_style_value;
    var if_block0 = /*$amount*/ctx[1] && create_if_block_1$5(ctx);
    var if_block1 = /*$showPressMore*/ctx[2] && create_if_block$c(ctx);
    var block = {
      c: function create() {
        div = element("div");
        if (if_block0) if_block0.c();
        t = space();
        if (if_block1) if_block1.c();
        attr_dev(div, "class", "amount svelte-1w38vbz");
        attr_dev(div, "style", div_style_value = "min-height:".concat( /*clientWidth*/ctx[0] * 0.06, "px"));
        add_location(div, file$n, 7, 0, 186);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, div, anchor);
        if (if_block0) if_block0.m(div, null);
        append_dev(div, t);
        if (if_block1) if_block1.m(div, null);
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if ( /*$amount*/ctx[1]) {
          if (if_block0) {
            if_block0.p(ctx, dirty);
            if (dirty & /*$amount*/2) {
              transition_in(if_block0, 1);
            }
          } else {
            if_block0 = create_if_block_1$5(ctx);
            if_block0.c();
            transition_in(if_block0, 1);
            if_block0.m(div, t);
          }
        } else if (if_block0) {
          if_block0.d(1);
          if_block0 = null;
        }
        if ( /*$showPressMore*/ctx[2]) {
          if (if_block1) {
            if_block1.p(ctx, dirty);
            if (dirty & /*$showPressMore*/4) {
              transition_in(if_block1, 1);
            }
          } else {
            if_block1 = create_if_block$c(ctx);
            if_block1.c();
            transition_in(if_block1, 1);
            if_block1.m(div, null);
          }
        } else if (if_block1) {
          group_outros();
          transition_out(if_block1, 1, 1, function () {
            if_block1 = null;
          });
          check_outros();
        }
        if (dirty & /*clientWidth*/1 && div_style_value !== (div_style_value = "min-height:".concat( /*clientWidth*/ctx[0] * 0.06, "px"))) {
          attr_dev(div, "style", div_style_value);
        }
      },
      i: function intro(local) {
        transition_in(if_block0);
        transition_in(if_block1);
      },
      o: function outro(local) {
        transition_out(if_block1);
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div);
        if (if_block0) if_block0.d();
        if (if_block1) if_block1.d();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$n.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$n($$self, $$props, $$invalidate) {
    var $amount;
    var $showPressMore;
    validate_store(amount, 'amount');
    component_subscribe($$self, amount, function ($$value) {
      return $$invalidate(1, $amount = $$value);
    });
    validate_store(showPressMore, 'showPressMore');
    component_subscribe($$self, showPressMore, function ($$value) {
      return $$invalidate(2, $showPressMore = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('Amount', slots, []);
    var clientWidth = $$props.clientWidth;
    $$self.$$.on_mount.push(function () {
      if (clientWidth === undefined && !('clientWidth' in $$props || $$self.$$.bound[$$self.$$.props['clientWidth']])) {
        console.warn("<Amount> was created without expected prop 'clientWidth'");
      }
    });
    var writable_props = ['clientWidth'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<Amount> was created with unknown prop '".concat(key, "'"));
    });
    $$self.$$set = function ($$props) {
      if ('clientWidth' in $$props) $$invalidate(0, clientWidth = $$props.clientWidth);
    };
    $$self.$capture_state = function () {
      return {
        fade: fade,
        amount: amount,
        showPressMore: showPressMore,
        isSunflower: isSunflower,
        clientWidth: clientWidth,
        $amount: $amount,
        $showPressMore: $showPressMore
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('clientWidth' in $$props) $$invalidate(0, clientWidth = $$props.clientWidth);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    return [clientWidth, $amount, $showPressMore];
  }
  var Amount = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(Amount, _SvelteComponentDev);
    var _super = _createSuper$n(Amount);
    function Amount(options) {
      var _this;
      _classCallCheck(this, Amount);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$n, create_fragment$n, safe_not_equal, {
        clientWidth: 0
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "Amount",
        options: options,
        id: create_fragment$n.name
      });
      return _this;
    }
    _createClass(Amount, [{
      key: "clientWidth",
      get: function get() {
        throw new Error("<Amount>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Amount>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return Amount;
  }(SvelteComponentDev);

  function _createSuper$m(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$m(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$m() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$m = "src/Charity/components/HeadFiles.svelte";
  function create_fragment$m(ctx) {
    var script;
    var script_src_value;
    var style;
    var block = {
      c: function create() {
        script = element("script");
        style = element("style");
        style.textContent = "@font-face {\n      font-family: \"corsica\";\n      src: url(\"https://az67128.gitlab.io/svelte-sunflower/assets/CorsicaRamblerLXnumber.ttf\")\n        format(\"truetype\");\n    }\n    .sunflowerApp {\n      overflow: visible;\n      opacity: 0;\n      transition: opacity 0.3s;\n    }\n    .credentials {\n      opacity: 0;\n      transition: opacity 0.3s;\n    }\n    #sunflowerCharity {\n      display: flex;\n      flex-direction: column;\n      align-items: center;\n      justify-content: center;\n    }";
        if (!src_url_equal(script.src, script_src_value = "https://widget.cloudpayments.ru/bundles/cloudpayments")) attr_dev(script, "src", script_src_value);
        script.async = true;
        add_location(script, file$m, 1, 2, 16);
        add_location(style, file$m, 4, 2, 106);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        append_dev(document.head, script);
        append_dev(document.head, style);
      },
      p: noop,
      i: noop,
      o: noop,
      d: function destroy(detaching) {
        detach_dev(script);
        detach_dev(style);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$m.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$m($$self, $$props) {
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('HeadFiles', slots, []);
    var writable_props = [];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<HeadFiles> was created with unknown prop '".concat(key, "'"));
    });
    return [];
  }
  var HeadFiles = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(HeadFiles, _SvelteComponentDev);
    var _super = _createSuper$m(HeadFiles);
    function HeadFiles(options) {
      var _this;
      _classCallCheck(this, HeadFiles);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$m, create_fragment$m, safe_not_equal, {});
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "HeadFiles",
        options: options,
        id: create_fragment$m.name
      });
      return _this;
    }
    return _createClass(HeadFiles);
  }(SvelteComponentDev);

  /* Points - v0.1.1 - 2013-07-11
   * Another Pointer Events polyfill
   * http://rich-harris.github.io/Points
   * Copyright (c) 2013 Rich Harris; Released under the MIT License */

  (function () {

    var activePointers, numActivePointers, recentTouchStarts, i, setUpMouseEvent, createUIEvent, createEvent, createMouseProxyEvent, mouseEventIsSimulated, createTouchProxyEvent, buttonsMap, pointerEventProperties;

    // Pointer events supported? Great, nothing to do, let's go home
    if (window.onpointerdown !== undefined) {
      return;
    }
    pointerEventProperties = 'screenX screenY clientX clientY ctrlKey shiftKey altKey metaKey relatedTarget detail button buttons pointerId pointerType width height pressure tiltX tiltY isPrimary'.split(' ');

    // Can we create events using the MouseEvent constructor? If so, gravy
    try {
      i = new UIEvent('test');
      createUIEvent = function createUIEvent(type, bubbles) {
        return new UIEvent(type, {
          view: window,
          bubbles: bubbles
        });
      };

      // otherwise we need to do things oldschool
    } catch (err) {
      if (document.createEvent) {
        createUIEvent = function createUIEvent(type, bubbles) {
          var pointerEvent = document.createEvent('UIEvents');
          pointerEvent.initUIEvent(type, bubbles, true, window);
          return pointerEvent;
        };
      }
    }
    if (!createUIEvent) {
      throw new Error('Cannot create events. You may be using an unsupported browser.');
    }
    createEvent = function createEvent(type, originalEvent, params, noBubble) {
      var pointerEvent, i;
      pointerEvent = createUIEvent(type, !noBubble);
      i = pointerEventProperties.length;
      while (i--) {
        Object.defineProperty(pointerEvent, pointerEventProperties[i], {
          value: params[pointerEventProperties[i]],
          writable: false
        });
      }
      Object.defineProperty(pointerEvent, 'originalEvent', {
        value: originalEvent,
        writable: false
      });
      Object.defineProperty(pointerEvent, 'preventDefault', {
        value: preventDefault,
        writable: false
      });
      return pointerEvent;
    };

    // add pointerEnabled property to navigator
    navigator.pointerEnabled = true;

    // If we're in IE10, these events are already supported, except prefixed
    if (window.onmspointerdown !== undefined) {
      ['MSPointerDown', 'MSPointerUp', 'MSPointerCancel', 'MSPointerMove', 'MSPointerOver', 'MSPointerOut'].forEach(function (prefixed) {
        var unprefixed;
        unprefixed = prefixed.toLowerCase().substring(2);

        // pointerenter and pointerleave are special cases
        if (unprefixed === 'pointerover' || unprefixed === 'pointerout') {
          window.addEventListener(prefixed, function (originalEvent) {
            var unprefixedEvent = createEvent(unprefixed, originalEvent, originalEvent, false);
            originalEvent.target.dispatchEvent(unprefixedEvent);
            if (!originalEvent.target.contains(originalEvent.relatedTarget)) {
              unprefixedEvent = createEvent(unprefixed === 'pointerover' ? 'pointerenter' : 'pointerleave', originalEvent, originalEvent, true);
              originalEvent.target.dispatchEvent(unprefixedEvent);
            }
          }, true);
        } else {
          window.addEventListener(prefixed, function (originalEvent) {
            var unprefixedEvent = createEvent(unprefixed, originalEvent, originalEvent, false);
            originalEvent.target.dispatchEvent(unprefixedEvent);
          }, true);
        }
      });
      navigator.maxTouchPoints = navigator.msMaxTouchPoints;

      // Nothing more to do.
      return;
    }

    // https://dvcs.w3.org/hg/pointerevents/raw-file/tip/pointerEvents.html#dfn-chorded-buttons
    buttonsMap = {
      0: 1,
      1: 4,
      2: 2
    };
    createMouseProxyEvent = function createMouseProxyEvent(type, originalEvent, noBubble) {
      var button, buttons, pressure, params;

      // normalise button and buttons
      if (originalEvent.buttons !== undefined) {
        buttons = originalEvent.buttons;
        button = !originalEvent.buttons ? -1 : originalEvent.button;
      } else {
        if (event.button === 0 && event.which === 0) {
          button = -1;
          buttons = 0;
        } else {
          button = originalEvent.button;
          buttons = buttonsMap[button];
        }
      }

      // Pressure is 0.5 for buttons down, 0 for no buttons down (unless pressure is
      // reported, obvs)
      pressure = originalEvent.pressure || originalEvent.mozPressure || (buttons ? 0.5 : 0);

      // This is the quickest way to copy event parameters. You can't enumerate
      // over event properties in Firefox (possibly elsewhere), so a traditional
      // extend function won't work
      params = {
        screenX: originalEvent.screenX,
        screenY: originalEvent.screenY,
        clientX: originalEvent.clientX,
        clientY: originalEvent.clientY,
        ctrlKey: originalEvent.ctrlKey,
        shiftKey: originalEvent.shiftKey,
        altKey: originalEvent.altKey,
        metaKey: originalEvent.metaKey,
        relatedTarget: originalEvent.relatedTarget,
        detail: originalEvent.detail,
        button: button,
        buttons: buttons,
        pointerId: 1,
        pointerType: 'mouse',
        width: 0,
        height: 0,
        pressure: pressure,
        tiltX: 0,
        tiltY: 0,
        isPrimary: true,
        preventDefault: preventDefault
      };
      return createEvent(type, originalEvent, params, noBubble);
    };

    // Some mouse events are real, others are simulated based on touch events.
    // We only want the real ones, or we'll end up firing our load at
    // inappropriate moments.
    //
    // Surprisingly, the coordinates of the mouse event won't exactly correspond
    // with the touchstart that originated them, so we need to be a bit fuzzy.
    if (window.ontouchstart !== undefined) {
      mouseEventIsSimulated = function mouseEventIsSimulated(event) {
        var i = recentTouchStarts.length,
          threshold = 10,
          touch;
        while (i--) {
          touch = recentTouchStarts[i];
          if (Math.abs(event.clientX - touch.clientX) < threshold && Math.abs(event.clientY - touch.clientY) < threshold) {
            return true;
          }
        }
      };
    } else {
      mouseEventIsSimulated = function mouseEventIsSimulated() {
        return false;
      };
    }
    setUpMouseEvent = function setUpMouseEvent(type) {
      if (type === 'over' || type === 'out') {
        window.addEventListener('mouse' + type, function (originalEvent) {
          var pointerEvent;
          if (mouseEventIsSimulated(originalEvent)) {
            return;
          }
          pointerEvent = createMouseProxyEvent('pointer' + type, originalEvent);
          originalEvent.target.dispatchEvent(pointerEvent);
          if (!originalEvent.target.contains(originalEvent.relatedTarget)) {
            pointerEvent = createMouseProxyEvent(type === 'over' ? 'pointerenter' : 'pointerleave', originalEvent, true);
            originalEvent.target.dispatchEvent(pointerEvent);
          }
        });
      } else {
        window.addEventListener('mouse' + type, function (originalEvent) {
          var pointerEvent;
          if (mouseEventIsSimulated(originalEvent)) {
            return;
          }
          pointerEvent = createMouseProxyEvent('pointer' + type, originalEvent);
          originalEvent.target.dispatchEvent(pointerEvent);
        });
      }
    };
    ['down', 'up', 'over', 'out', 'move'].forEach(function (eventType) {
      setUpMouseEvent(eventType);
    });

    // Touch events:
    if (window.ontouchstart !== undefined) {
      // Set up a registry of current touches
      activePointers = {};
      numActivePointers = 0;

      // Maintain a list of recent touchstarts, so we can eliminate simulate
      // mouse events later
      recentTouchStarts = [];
      createTouchProxyEvent = function createTouchProxyEvent(type, originalEvent, touch, noBubble, relatedTarget) {
        var params;
        params = {
          screenX: originalEvent.screenX,
          screenY: originalEvent.screenY,
          clientX: touch.clientX,
          clientY: touch.clientY,
          ctrlKey: originalEvent.ctrlKey,
          shiftKey: originalEvent.shiftKey,
          altKey: originalEvent.altKey,
          metaKey: originalEvent.metaKey,
          relatedTarget: relatedTarget || originalEvent.relatedTarget,
          // TODO is this right? also: mouseenter/leave?
          detail: originalEvent.detail,
          button: 0,
          buttons: 1,
          pointerId: touch.identifier + 2,
          // ensure no collisions between touch and mouse pointer IDs
          pointerType: 'touch',
          width: 20,
          // roughly how fat people's fingers are
          height: 20,
          pressure: 0.5,
          tiltX: 0,
          tiltY: 0,
          isPrimary: activePointers[touch.identifier].isPrimary,
          preventDefault: preventDefault
        };
        return createEvent(type, originalEvent, params, noBubble);
      };

      // touchstart
      window.addEventListener('touchstart', function (event) {
        var touches, processTouch;
        touches = event.changedTouches;
        processTouch = function processTouch(touch) {
          var pointerdownEvent, pointeroverEvent, pointerenterEvent, pointer;
          pointer = {
            target: touch.target,
            isPrimary: numActivePointers ? false : true
          };
          activePointers[touch.identifier] = pointer;
          numActivePointers += 1;
          pointerdownEvent = createTouchProxyEvent('pointerdown', event, touch);
          pointeroverEvent = createTouchProxyEvent('pointerover', event, touch);
          pointerenterEvent = createTouchProxyEvent('pointerenter', event, touch, true);
          touch.target.dispatchEvent(pointeroverEvent);
          touch.target.dispatchEvent(pointerenterEvent);
          touch.target.dispatchEvent(pointerdownEvent);

          // we need to keep track of recent touchstart events, so we can test
          // whether later mouse events are simulated
          recentTouchStarts.push(touch);
          setTimeout(function () {
            var index = recentTouchStarts.indexOf(touch);
            if (index !== -1) {
              recentTouchStarts.splice(index, 1);
            }
          }, 1500);
        };
        for (i = 0; i < touches.length; i += 1) {
          processTouch(touches[i]);
        }
      });

      // touchmove
      window.addEventListener('touchmove', function (event) {
        var touches, processTouch;
        touches = event.changedTouches;
        processTouch = function processTouch(touch) {
          var pointermoveEvent, pointeroverEvent, pointeroutEvent, pointerenterEvent, pointerleaveEvent, pointer, previousTarget, actualTarget;
          pointer = activePointers[touch.identifier];
          actualTarget = document.elementFromPoint(touch.clientX, touch.clientY);
          if (pointer.target === actualTarget) {
            // just fire a touchmove event
            pointermoveEvent = createTouchProxyEvent('pointermove', event, touch);
            actualTarget.dispatchEvent(pointermoveEvent);
            return;
          }

          // target has changed - we need to fire a pointerout (and possibly pointerleave)
          // event on the previous target, and a pointerover (and possibly pointerenter)
          // event on the current target. Then we fire the pointermove event on the current
          // target

          previousTarget = pointer.target;
          pointer.target = actualTarget;

          // pointerleave
          if (!previousTarget.contains(actualTarget)) {
            // new target is not a child of previous target, so fire pointerleave on previous
            pointerleaveEvent = createTouchProxyEvent('pointerleave', event, touch, true, actualTarget);
            previousTarget.dispatchEvent(pointerleaveEvent);
          }

          // pointerout
          pointeroutEvent = createTouchProxyEvent('pointerout', event, touch, false);
          previousTarget.dispatchEvent(pointeroutEvent);

          // pointermove
          pointermoveEvent = createTouchProxyEvent('pointermove', event, touch, false);
          actualTarget.dispatchEvent(pointermoveEvent);

          // pointerover
          pointeroverEvent = createTouchProxyEvent('pointerover', event, touch, false);
          actualTarget.dispatchEvent(pointeroverEvent);

          // pointerenter
          if (!actualTarget.contains(previousTarget)) {
            // previous target is not a child of current target, so fire pointerenter on current
            pointerenterEvent = createTouchProxyEvent('pointerenter', event, touch, true, previousTarget);
            actualTarget.dispatchEvent(pointerenterEvent);
          }
        };
        for (i = 0; i < touches.length; i += 1) {
          processTouch(touches[i]);
        }
      });

      // touchend
      window.addEventListener('touchend', function (event) {
        var touches, processTouch;
        touches = event.changedTouches;
        processTouch = function processTouch(touch) {
          var pointerupEvent, pointeroutEvent, pointerleaveEvent, actualTarget;
          actualTarget = document.elementFromPoint(touch.clientX, touch.clientY);
          pointerupEvent = createTouchProxyEvent('pointerup', event, touch, false);
          pointeroutEvent = createTouchProxyEvent('pointerout', event, touch, false);
          pointerleaveEvent = createTouchProxyEvent('pointerleave', event, touch, true);
          delete activePointers[touch.identifier];
          numActivePointers -= 1;
          actualTarget.dispatchEvent(pointerupEvent);
          actualTarget.dispatchEvent(pointeroutEvent);
          actualTarget.dispatchEvent(pointerleaveEvent);
        };
        for (i = 0; i < touches.length; i += 1) {
          processTouch(touches[i]);
        }
      });

      // touchcancel
      window.addEventListener('touchcancel', function (event) {
        var touches, processTouch;
        touches = event.changedTouches;
        processTouch = function processTouch(touch) {
          var pointercancelEvent, pointeroutEvent, pointerleaveEvent;
          pointercancelEvent = createTouchProxyEvent('pointercancel', event, touch);
          pointeroutEvent = createTouchProxyEvent('pointerout', event, touch);
          pointerleaveEvent = createTouchProxyEvent('pointerleave', event, touch);
          touch.target.dispatchEvent(pointercancelEvent);
          touch.target.dispatchEvent(pointeroutEvent);
          touch.target.dispatchEvent(pointerleaveEvent);
          delete activePointers[touch.identifier];
          numActivePointers -= 1;
        };
        for (i = 0; i < touches.length; i += 1) {
          processTouch(touches[i]);
        }
      });
    }

    // Single preventDefault function - no point recreating it over and over
    function preventDefault() {
      this.originalEvent.preventDefault();
    }

    // TODO stopPropagation?
  })();

  function _createSuper$l(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$l(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$l() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$l = "src/Charity/App.svelte";
  function create_fragment$l(ctx) {
    var div;
    var amount_1;
    var t0;
    var pulsebutton;
    var t1;
    var headfiles;
    var current;
    amount_1 = new Amount({
      props: {
        clientWidth: /*clientWidth*/ctx[0]
      },
      $$inline: true
    });
    pulsebutton = new PulseButton({
      props: {
        clientWidth: /*clientWidth*/ctx[0]
      },
      $$inline: true
    });
    headfiles = new HeadFiles({
      $$inline: true
    });
    var block = {
      c: function create() {
        div = element("div");
        create_component(amount_1.$$.fragment);
        t0 = space();
        create_component(pulsebutton.$$.fragment);
        t1 = space();
        create_component(headfiles.$$.fragment);
        attr_dev(div, "class", "sunflowerApp svelte-17al34h");
        add_location(div, file$l, 27, 0, 782);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, div, anchor);
        mount_component(amount_1, div, null);
        append_dev(div, t0);
        mount_component(pulsebutton, div, null);
        /*div_binding*/
        ctx[2](div);
        insert_dev(target, t1, anchor);
        mount_component(headfiles, target, anchor);
        current = true;
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        var amount_1_changes = {};
        if (dirty & /*clientWidth*/1) amount_1_changes.clientWidth = /*clientWidth*/ctx[0];
        amount_1.$set(amount_1_changes);
        var pulsebutton_changes = {};
        if (dirty & /*clientWidth*/1) pulsebutton_changes.clientWidth = /*clientWidth*/ctx[0];
        pulsebutton.$set(pulsebutton_changes);
      },
      i: function intro(local) {
        if (current) return;
        transition_in(amount_1.$$.fragment, local);
        transition_in(pulsebutton.$$.fragment, local);
        transition_in(headfiles.$$.fragment, local);
        current = true;
      },
      o: function outro(local) {
        transition_out(amount_1.$$.fragment, local);
        transition_out(pulsebutton.$$.fragment, local);
        transition_out(headfiles.$$.fragment, local);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div);
        destroy_component(amount_1);
        destroy_component(pulsebutton);
        /*div_binding*/
        ctx[2](null);
        if (detaching) detach_dev(t1);
        destroy_component(headfiles, detaching);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$l.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$l($$self, $$props, $$invalidate) {
    var $amount;
    validate_store(amount, 'amount');
    component_subscribe($$self, amount, function ($$value) {
      return $$invalidate(3, $amount = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('App', slots, []);
    var appRef = null;
    var clientWidth = $$props.clientWidth;

    // $: {
    //   if (appRef) sayHelloIfVisible(appRef, y);
    // }
    var handleVisibilityChange = function handleVisibilityChange() {
      set_store_value(amount, $amount = 0, $amount);
    };
    window.addEventListener("blur", handleVisibilityChange, false);
    $$self.$$.on_mount.push(function () {
      if (clientWidth === undefined && !('clientWidth' in $$props || $$self.$$.bound[$$self.$$.props['clientWidth']])) {
        console.warn("<App> was created without expected prop 'clientWidth'");
      }
    });
    var writable_props = ['clientWidth'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<App> was created with unknown prop '".concat(key, "'"));
    });
    function div_binding($$value) {
      binding_callbacks[$$value ? 'unshift' : 'push'](function () {
        appRef = $$value;
        $$invalidate(1, appRef);
      });
    }
    $$self.$$set = function ($$props) {
      if ('clientWidth' in $$props) $$invalidate(0, clientWidth = $$props.clientWidth);
    };
    $$self.$capture_state = function () {
      return {
        PulseButton: PulseButton,
        Amount: Amount,
        HeadFiles: HeadFiles,
        amount: amount,
        appRef: appRef,
        clientWidth: clientWidth,
        handleVisibilityChange: handleVisibilityChange,
        $amount: $amount
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('appRef' in $$props) $$invalidate(1, appRef = $$props.appRef);
      if ('clientWidth' in $$props) $$invalidate(0, clientWidth = $$props.clientWidth);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    return [clientWidth, appRef, div_binding];
  }
  var App$1 = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(App, _SvelteComponentDev);
    var _super = _createSuper$l(App);
    function App(options) {
      var _this;
      _classCallCheck(this, App);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$l, create_fragment$l, safe_not_equal, {
        clientWidth: 0
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "App",
        options: options,
        id: create_fragment$l.name
      });
      return _this;
    }
    _createClass(App, [{
      key: "clientWidth",
      get: function get() {
        throw new Error("<App>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<App>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return App;
  }(SvelteComponentDev);

  function _createSuper$k(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$k(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$k() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$k = "src/Interactive/Social.svelte";
  function get_each_context$5(ctx, list, i) {
    var child_ctx = ctx.slice();
    child_ctx[3] = list[i];
    return child_ctx;
  }

  // (32:2) {#each socialButtons as social}
  function create_each_block$5(ctx) {
    var a;
    var img;
    var img_src_value;
    var t;
    var block = {
      c: function create() {
        a = element("a");
        img = element("img");
        t = space();
        if (!src_url_equal(img.src, img_src_value = BASE_URL + /*social*/ctx[3].src)) attr_dev(img, "src", img_src_value);
        attr_dev(img, "alt", "social share");
        attr_dev(img, "class", "socialImage svelte-1u9qsji");
        add_location(img, file$k, 33, 6, 1174);
        attr_dev(a, "href", /*social*/ctx[3].href);
        attr_dev(a, "target", "_blank");
        attr_dev(a, "class", "socialLink");
        add_location(a, file$k, 32, 4, 1110);
      },
      m: function mount(target, anchor) {
        insert_dev(target, a, anchor);
        append_dev(a, img);
        append_dev(a, t);
      },
      p: noop,
      d: function destroy(detaching) {
        if (detaching) detach_dev(a);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_each_block$5.name,
      type: "each",
      source: "(32:2) {#each socialButtons as social}",
      ctx: ctx
    });
    return block;
  }
  function create_fragment$k(ctx) {
    var div;
    var div_transition;
    var current;
    var each_value = /*socialButtons*/ctx[0];
    validate_each_argument(each_value);
    var each_blocks = [];
    for (var i = 0; i < each_value.length; i += 1) {
      each_blocks[i] = create_each_block$5(get_each_context$5(ctx, each_value, i));
    }
    var block = {
      c: function create() {
        div = element("div");
        for (var _i = 0; _i < each_blocks.length; _i += 1) {
          each_blocks[_i].c();
        }
        attr_dev(div, "class", "social svelte-1u9qsji");
        add_location(div, file$k, 30, 0, 1029);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, div, anchor);
        for (var _i2 = 0; _i2 < each_blocks.length; _i2 += 1) {
          if (each_blocks[_i2]) {
            each_blocks[_i2].m(div, null);
          }
        }
        current = true;
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if (dirty & /*socialButtons, BASE_URL*/1) {
          each_value = /*socialButtons*/ctx[0];
          validate_each_argument(each_value);
          var _i3;
          for (_i3 = 0; _i3 < each_value.length; _i3 += 1) {
            var child_ctx = get_each_context$5(ctx, each_value, _i3);
            if (each_blocks[_i3]) {
              each_blocks[_i3].p(child_ctx, dirty);
            } else {
              each_blocks[_i3] = create_each_block$5(child_ctx);
              each_blocks[_i3].c();
              each_blocks[_i3].m(div, null);
            }
          }
          for (; _i3 < each_blocks.length; _i3 += 1) {
            each_blocks[_i3].d(1);
          }
          each_blocks.length = each_value.length;
        }
      },
      i: function intro(local) {
        if (current) return;
        if (local) {
          add_render_callback(function () {
            if (!current) return;
            if (!div_transition) div_transition = create_bidirectional_transition(div, fade, {}, true);
            div_transition.run(1);
          });
        }
        current = true;
      },
      o: function outro(local) {
        if (local) {
          if (!div_transition) div_transition = create_bidirectional_transition(div, fade, {}, false);
          div_transition.run(0);
        }
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div);
        destroy_each(each_blocks, detaching);
        if (detaching && div_transition) div_transition.end();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$k.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  var utm = "playerShare";
  function instance$k($$self, $$props, $$invalidate) {
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('Social', slots, []);
    var shareUrl = encodeURIComponent("https://igra.film/");
    var title = encodeURIComponent("Игрок - Первая интерактивная комедия!");
    var socialButtons = [{
      src: "/images/social/vk.svg",
      href: "https://vk.com/share.php?url=".concat(shareUrl, "&title=").concat(title, "&utm_source=").concat(utm)
    }, {
      src: "/images/social/facebook.svg",
      href: "https://www.facebook.com/sharer.php?src=sp&u=".concat(shareUrl, "&title=").concat(title, "&utm_source=").concat(utm)
    }, {
      src: "/images/social/ok.svg",
      href: "https://connect.ok.ru/offer?url=".concat(shareUrl, "&title=").concat(title, "&utm_source=").concat(utm)
    }, {
      src: "/images/social/twitter.svg",
      href: "https://twitter.com/intent/tweet?text=".concat(title, "&url=").concat(shareUrl, "&utm_source=").concat(utm)
    }, {
      src: "/images/social/telegram.svg",
      href: "https://t.me/share/url?url=".concat(shareUrl, "&text=").concat(title, "&utm_source=").concat(utm)
    }];
    var writable_props = [];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<Social> was created with unknown prop '".concat(key, "'"));
    });
    $$self.$capture_state = function () {
      return {
        fade: fade,
        BASE_URL: BASE_URL,
        shareUrl: shareUrl,
        title: title,
        utm: utm,
        socialButtons: socialButtons
      };
    };
    return [socialButtons];
  }
  var Social = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(Social, _SvelteComponentDev);
    var _super = _createSuper$k(Social);
    function Social(options) {
      var _this;
      _classCallCheck(this, Social);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$k, create_fragment$k, safe_not_equal, {});
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "Social",
        options: options,
        id: create_fragment$k.name
      });
      return _this;
    }
    return _createClass(Social);
  }(SvelteComponentDev);

  function _createSuper$j(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$j(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$j() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$j = "src/Interactive/Credits.svelte";

  // (32:2) {#if currentTime > 49}
  function create_if_block_1$4(ctx) {
    var social;
    var current;
    social = new Social({
      $$inline: true
    });
    var block = {
      c: function create() {
        create_component(social.$$.fragment);
      },
      m: function mount(target, anchor) {
        mount_component(social, target, anchor);
        current = true;
      },
      i: function intro(local) {
        if (current) return;
        transition_in(social.$$.fragment, local);
        current = true;
      },
      o: function outro(local) {
        transition_out(social.$$.fragment, local);
        current = false;
      },
      d: function destroy(detaching) {
        destroy_component(social, detaching);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block_1$4.name,
      type: "if",
      source: "(32:2) {#if currentTime > 49}",
      ctx: ctx
    });
    return block;
  }

  // (35:2) {#if $isCharityButtonPressed}
  function create_if_block$b(ctx) {
    var a0;
    var img0;
    var img0_src_value;
    var a0_transition;
    var t;
    var a1;
    var img1;
    var img1_src_value;
    var a1_transition;
    var current;
    var mounted;
    var dispose;
    var block = {
      c: function create() {
        a0 = element("a");
        img0 = element("img");
        t = space();
        a1 = element("a");
        img1 = element("img");
        if (!src_url_equal(img0.src, img0_src_value = "".concat(BASE_URL, "/images/appstore.png"))) attr_dev(img0, "src", img0_src_value);
        attr_dev(img0, "alt", "appstore");
        attr_dev(img0, "class", "appstore svelte-1cyhcdp");
        add_location(img0, file$j, 40, 5, 1204);
        attr_dev(a0, "href", "https://apps.apple.com/ru/app/%D0%B4%D0%BE%D0%B1%D1%80%D0%BE%D0%BC%D0%B8%D0%BD-%D0%BA%D0%BD%D0%BE%D0%BF%D0%BA%D0%B0-%D1%81%D1%87%D0%B0%D1%81%D1%82%D1%8C%D1%8F/id1543294085");
        attr_dev(a0, "target", "_blank");
        add_location(a0, file$j, 35, 2, 879);
        if (!src_url_equal(img1.src, img1_src_value = "".concat(BASE_URL, "/images/googleplay.png"))) attr_dev(img1, "src", img1_src_value);
        attr_dev(img1, "alt", "googleplay");
        attr_dev(img1, "class", "googleplay svelte-1cyhcdp");
        add_location(img1, file$j, 49, 5, 1531);
        attr_dev(a1, "href", "https://play.google.com/store/apps/details?id=tech.screenlife.dbtn");
        attr_dev(a1, "target", "_blank");
        add_location(a1, file$j, 44, 2, 1308);
      },
      m: function mount(target, anchor) {
        insert_dev(target, a0, anchor);
        append_dev(a0, img0);
        insert_dev(target, t, anchor);
        insert_dev(target, a1, anchor);
        append_dev(a1, img1);
        current = true;
        if (!mounted) {
          dispose = [listen_dev(a0, "click", /*click_handler*/ctx[3], false, false, false, false), listen_dev(a1, "click", /*click_handler_1*/ctx[4], false, false, false, false)];
          mounted = true;
        }
      },
      p: noop,
      i: function intro(local) {
        if (current) return;
        if (local) {
          add_render_callback(function () {
            if (!current) return;
            if (!a0_transition) a0_transition = create_bidirectional_transition(a0, fade, {}, true);
            a0_transition.run(1);
          });
        }
        if (local) {
          add_render_callback(function () {
            if (!current) return;
            if (!a1_transition) a1_transition = create_bidirectional_transition(a1, fade, {}, true);
            a1_transition.run(1);
          });
        }
        current = true;
      },
      o: function outro(local) {
        if (local) {
          if (!a0_transition) a0_transition = create_bidirectional_transition(a0, fade, {}, false);
          a0_transition.run(0);
        }
        if (local) {
          if (!a1_transition) a1_transition = create_bidirectional_transition(a1, fade, {}, false);
          a1_transition.run(0);
        }
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(a0);
        if (detaching && a0_transition) a0_transition.end();
        if (detaching) detach_dev(t);
        if (detaching) detach_dev(a1);
        if (detaching && a1_transition) a1_transition.end();
        mounted = false;
        run_all(dispose);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block$b.name,
      type: "if",
      source: "(35:2) {#if $isCharityButtonPressed}",
      ctx: ctx
    });
    return block;
  }
  function create_fragment$j(ctx) {
    var div2;
    var div0;
    var t0;
    var div1;
    var t1;
    var img0;
    var img0_src_value;
    var t2;
    var charity;
    var t3;
    var img1;
    var img1_src_value;
    var t4;
    var t5;
    var div2_resize_listener;
    var div2_transition;
    var current;
    charity = new App$1({
      props: {
        clientWidth: /*clientWidth*/ctx[1]
      },
      $$inline: true
    });
    var if_block0 = /*currentTime*/ctx[0] > 49 && create_if_block_1$4(ctx);
    var if_block1 = /*$isCharityButtonPressed*/ctx[2] && create_if_block$b(ctx);
    var block = {
      c: function create() {
        div2 = element("div");
        div0 = element("div");
        t0 = space();
        div1 = element("div");
        t1 = space();
        img0 = element("img");
        t2 = space();
        create_component(charity.$$.fragment);
        t3 = space();
        img1 = element("img");
        t4 = space();
        if (if_block0) if_block0.c();
        t5 = space();
        if (if_block1) if_block1.c();
        attr_dev(div0, "class", "background svelte-1cyhcdp");
        attr_dev(div0, "style", "background-image: url(".concat(BASE_URL, "/images/charitybg.jpg)"));
        add_location(div0, file$j, 19, 2, 400);
        attr_dev(div1, "class", "background2 svelte-1cyhcdp");
        attr_dev(div1, "style", "background-image: url(".concat(BASE_URL, "/images/charitybg.jpg)"));
        add_location(div1, file$j, 22, 2, 503);
        if (!src_url_equal(img0.src, img0_src_value = "".concat(BASE_URL, "/images/dobromin.png"))) attr_dev(img0, "src", img0_src_value);
        attr_dev(img0, "alt", "dobromin");
        attr_dev(img0, "class", "dobromin svelte-1cyhcdp");
        add_location(img0, file$j, 25, 2, 607);
        if (!src_url_equal(img1.src, img1_src_value = "".concat(BASE_URL, "/images/live.png"))) attr_dev(img1, "src", img1_src_value);
        attr_dev(img1, "alt", "live");
        attr_dev(img1, "class", "live svelte-1cyhcdp");
        add_location(img1, file$j, 30, 2, 729);
        attr_dev(div2, "class", "container  svelte-1cyhcdp");
        add_render_callback(function () {
          return (/*div2_elementresize_handler*/ctx[5].call(div2)
          );
        });
        add_location(div2, file$j, 15, 0, 307);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, div2, anchor);
        append_dev(div2, div0);
        append_dev(div2, t0);
        append_dev(div2, div1);
        append_dev(div2, t1);
        append_dev(div2, img0);
        append_dev(div2, t2);
        mount_component(charity, div2, null);
        append_dev(div2, t3);
        append_dev(div2, img1);
        append_dev(div2, t4);
        if (if_block0) if_block0.m(div2, null);
        append_dev(div2, t5);
        if (if_block1) if_block1.m(div2, null);
        div2_resize_listener = add_iframe_resize_listener(div2, /*div2_elementresize_handler*/ctx[5].bind(div2));
        current = true;
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        var charity_changes = {};
        if (dirty & /*clientWidth*/2) charity_changes.clientWidth = /*clientWidth*/ctx[1];
        charity.$set(charity_changes);
        if ( /*currentTime*/ctx[0] > 49) {
          if (if_block0) {
            if (dirty & /*currentTime*/1) {
              transition_in(if_block0, 1);
            }
          } else {
            if_block0 = create_if_block_1$4(ctx);
            if_block0.c();
            transition_in(if_block0, 1);
            if_block0.m(div2, t5);
          }
        } else if (if_block0) {
          group_outros();
          transition_out(if_block0, 1, 1, function () {
            if_block0 = null;
          });
          check_outros();
        }
        if ( /*$isCharityButtonPressed*/ctx[2]) {
          if (if_block1) {
            if_block1.p(ctx, dirty);
            if (dirty & /*$isCharityButtonPressed*/4) {
              transition_in(if_block1, 1);
            }
          } else {
            if_block1 = create_if_block$b(ctx);
            if_block1.c();
            transition_in(if_block1, 1);
            if_block1.m(div2, null);
          }
        } else if (if_block1) {
          group_outros();
          transition_out(if_block1, 1, 1, function () {
            if_block1 = null;
          });
          check_outros();
        }
      },
      i: function intro(local) {
        if (current) return;
        transition_in(charity.$$.fragment, local);
        transition_in(if_block0);
        transition_in(if_block1);
        if (local) {
          add_render_callback(function () {
            if (!current) return;
            if (!div2_transition) div2_transition = create_bidirectional_transition(div2, fade, {
              duration: 2000
            }, true);
            div2_transition.run(1);
          });
        }
        current = true;
      },
      o: function outro(local) {
        transition_out(charity.$$.fragment, local);
        transition_out(if_block0);
        transition_out(if_block1);
        if (local) {
          if (!div2_transition) div2_transition = create_bidirectional_transition(div2, fade, {
            duration: 2000
          }, false);
          div2_transition.run(0);
        }
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div2);
        destroy_component(charity);
        if (if_block0) if_block0.d();
        if (if_block1) if_block1.d();
        div2_resize_listener();
        if (detaching && div2_transition) div2_transition.end();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$j.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$j($$self, $$props, $$invalidate) {
    var $isCharityButtonPressed;
    validate_store(isCharityButtonPressed, 'isCharityButtonPressed');
    component_subscribe($$self, isCharityButtonPressed, function ($$value) {
      return $$invalidate(2, $isCharityButtonPressed = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('Credits', slots, []);
    var currentTime = $$props.currentTime;
    var clientWidth;
    $$self.$$.on_mount.push(function () {
      if (currentTime === undefined && !('currentTime' in $$props || $$self.$$.bound[$$self.$$.props['currentTime']])) {
        console.warn("<Credits> was created without expected prop 'currentTime'");
      }
    });
    var writable_props = ['currentTime'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<Credits> was created with unknown prop '".concat(key, "'"));
    });
    var click_handler = function click_handler() {
      return sendMetrik$1('CharityAppClick', {
        "Gambler.CharityAppStore": 'appstore'
      });
    };
    var click_handler_1 = function click_handler_1() {
      return sendMetrik$1('CharityAppClick', {
        "Gambler.CharityAppStore": 'google play'
      });
    };
    function div2_elementresize_handler() {
      clientWidth = this.clientWidth;
      $$invalidate(1, clientWidth);
    }
    $$self.$$set = function ($$props) {
      if ('currentTime' in $$props) $$invalidate(0, currentTime = $$props.currentTime);
    };
    $$self.$capture_state = function () {
      return {
        fade: fade,
        BASE_URL: BASE_URL,
        sendMetrik: sendMetrik$1,
        externalLink: externalLink,
        isCharityButtonPressed: isCharityButtonPressed,
        Charity: App$1,
        Social: Social,
        currentTime: currentTime,
        clientWidth: clientWidth,
        $isCharityButtonPressed: $isCharityButtonPressed
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('currentTime' in $$props) $$invalidate(0, currentTime = $$props.currentTime);
      if ('clientWidth' in $$props) $$invalidate(1, clientWidth = $$props.clientWidth);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    return [currentTime, clientWidth, $isCharityButtonPressed, click_handler, click_handler_1, div2_elementresize_handler];
  }
  var Credits = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(Credits, _SvelteComponentDev);
    var _super = _createSuper$j(Credits);
    function Credits(options) {
      var _this;
      _classCallCheck(this, Credits);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$j, create_fragment$j, safe_not_equal, {
        currentTime: 0
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "Credits",
        options: options,
        id: create_fragment$j.name
      });
      return _this;
    }
    _createClass(Credits, [{
      key: "currentTime",
      get: function get() {
        throw new Error("<Credits>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Credits>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return Credits;
  }(SvelteComponentDev);

  var ITERATOR$1 = wellKnownSymbol('iterator');

  var urlConstructorDetection = !fails(function () {
    // eslint-disable-next-line unicorn/relative-url-style -- required for testing
    var url = new URL('b?a=1&b=2&c=3', 'http://a');
    var params = url.searchParams;
    var params2 = new URLSearchParams('a=1&a=2&b=3');
    var result = '';
    url.pathname = 'c%20d';
    params.forEach(function (value, key) {
      params['delete']('b');
      result += key + value;
    });
    params2['delete']('a', 2);
    // `undefined` case is a Chromium 117 bug
    // https://bugs.chromium.org/p/v8/issues/detail?id=14222
    params2['delete']('b', undefined);
    return (isPure && (!url.toJSON || !params2.has('a', 1) || params2.has('a', 2) || !params2.has('a', undefined) || params2.has('b')))
      || (!params.size && (isPure || !descriptors))
      || !params.sort
      || url.href !== 'http://a/c%20d?a=1&c=3'
      || params.get('c') !== '3'
      || String(new URLSearchParams('?a=1')) !== 'a=1'
      || !params[ITERATOR$1]
      // throws in Edge
      || new URL('https://a@b').username !== 'a'
      || new URLSearchParams(new URLSearchParams('a=b')).get('a') !== 'b'
      // not punycoded in Edge
      || new URL('http://тест').host !== 'xn--e1aybc'
      // not escaped in Chrome 62-
      || new URL('http://a#б').hash !== '#%D0%B1'
      // fails in Chrome 66-
      || result !== 'a1c3'
      // throws in Safari
      || new URL('http://x', undefined).host !== 'x';
  });

  // based on https://github.com/bestiejs/punycode.js/blob/master/punycode.js


  var maxInt = 2147483647; // aka. 0x7FFFFFFF or 2^31-1
  var base = 36;
  var tMin = 1;
  var tMax = 26;
  var skew = 38;
  var damp = 700;
  var initialBias = 72;
  var initialN = 128; // 0x80
  var delimiter = '-'; // '\x2D'
  var regexNonASCII = /[^\0-\u007E]/; // non-ASCII chars
  var regexSeparators = /[.\u3002\uFF0E\uFF61]/g; // RFC 3490 separators
  var OVERFLOW_ERROR = 'Overflow: input needs wider integers to process';
  var baseMinusTMin = base - tMin;

  var $RangeError = RangeError;
  var exec$1 = functionUncurryThis(regexSeparators.exec);
  var floor$2 = Math.floor;
  var fromCharCode = String.fromCharCode;
  var charCodeAt$1 = functionUncurryThis(''.charCodeAt);
  var join$2 = functionUncurryThis([].join);
  var push$3 = functionUncurryThis([].push);
  var replace$3 = functionUncurryThis(''.replace);
  var split$2 = functionUncurryThis(''.split);
  var toLowerCase$1 = functionUncurryThis(''.toLowerCase);

  /**
   * Creates an array containing the numeric code points of each Unicode
   * character in the string. While JavaScript uses UCS-2 internally,
   * this function will convert a pair of surrogate halves (each of which
   * UCS-2 exposes as separate characters) into a single code point,
   * matching UTF-16.
   */
  var ucs2decode = function (string) {
    var output = [];
    var counter = 0;
    var length = string.length;
    while (counter < length) {
      var value = charCodeAt$1(string, counter++);
      if (value >= 0xD800 && value <= 0xDBFF && counter < length) {
        // It's a high surrogate, and there is a next character.
        var extra = charCodeAt$1(string, counter++);
        if ((extra & 0xFC00) == 0xDC00) { // Low surrogate.
          push$3(output, ((value & 0x3FF) << 10) + (extra & 0x3FF) + 0x10000);
        } else {
          // It's an unmatched surrogate; only append this code unit, in case the
          // next code unit is the high surrogate of a surrogate pair.
          push$3(output, value);
          counter--;
        }
      } else {
        push$3(output, value);
      }
    }
    return output;
  };

  /**
   * Converts a digit/integer into a basic code point.
   */
  var digitToBasic = function (digit) {
    //  0..25 map to ASCII a..z or A..Z
    // 26..35 map to ASCII 0..9
    return digit + 22 + 75 * (digit < 26);
  };

  /**
   * Bias adaptation function as per section 3.4 of RFC 3492.
   * https://tools.ietf.org/html/rfc3492#section-3.4
   */
  var adapt = function (delta, numPoints, firstTime) {
    var k = 0;
    delta = firstTime ? floor$2(delta / damp) : delta >> 1;
    delta += floor$2(delta / numPoints);
    while (delta > baseMinusTMin * tMax >> 1) {
      delta = floor$2(delta / baseMinusTMin);
      k += base;
    }
    return floor$2(k + (baseMinusTMin + 1) * delta / (delta + skew));
  };

  /**
   * Converts a string of Unicode symbols (e.g. a domain name label) to a
   * Punycode string of ASCII-only symbols.
   */
  var encode = function (input) {
    var output = [];

    // Convert the input in UCS-2 to an array of Unicode code points.
    input = ucs2decode(input);

    // Cache the length.
    var inputLength = input.length;

    // Initialize the state.
    var n = initialN;
    var delta = 0;
    var bias = initialBias;
    var i, currentValue;

    // Handle the basic code points.
    for (i = 0; i < input.length; i++) {
      currentValue = input[i];
      if (currentValue < 0x80) {
        push$3(output, fromCharCode(currentValue));
      }
    }

    var basicLength = output.length; // number of basic code points.
    var handledCPCount = basicLength; // number of code points that have been handled;

    // Finish the basic string with a delimiter unless it's empty.
    if (basicLength) {
      push$3(output, delimiter);
    }

    // Main encoding loop:
    while (handledCPCount < inputLength) {
      // All non-basic code points < n have been handled already. Find the next larger one:
      var m = maxInt;
      for (i = 0; i < input.length; i++) {
        currentValue = input[i];
        if (currentValue >= n && currentValue < m) {
          m = currentValue;
        }
      }

      // Increase `delta` enough to advance the decoder's <n,i> state to <m,0>, but guard against overflow.
      var handledCPCountPlusOne = handledCPCount + 1;
      if (m - n > floor$2((maxInt - delta) / handledCPCountPlusOne)) {
        throw $RangeError(OVERFLOW_ERROR);
      }

      delta += (m - n) * handledCPCountPlusOne;
      n = m;

      for (i = 0; i < input.length; i++) {
        currentValue = input[i];
        if (currentValue < n && ++delta > maxInt) {
          throw $RangeError(OVERFLOW_ERROR);
        }
        if (currentValue == n) {
          // Represent delta as a generalized variable-length integer.
          var q = delta;
          var k = base;
          while (true) {
            var t = k <= bias ? tMin : (k >= bias + tMax ? tMax : k - bias);
            if (q < t) break;
            var qMinusT = q - t;
            var baseMinusT = base - t;
            push$3(output, fromCharCode(digitToBasic(t + qMinusT % baseMinusT)));
            q = floor$2(qMinusT / baseMinusT);
            k += base;
          }

          push$3(output, fromCharCode(digitToBasic(q)));
          bias = adapt(delta, handledCPCountPlusOne, handledCPCount == basicLength);
          delta = 0;
          handledCPCount++;
        }
      }

      delta++;
      n++;
    }
    return join$2(output, '');
  };

  var stringPunycodeToAscii = function (input) {
    var encoded = [];
    var labels = split$2(replace$3(toLowerCase$1(input), regexSeparators, '\u002E'), '.');
    var i, label;
    for (i = 0; i < labels.length; i++) {
      label = labels[i];
      push$3(encoded, exec$1(regexNonASCII, label) ? 'xn--' + encode(label) : label);
    }
    return join$2(encoded, '.');
  };

  // TODO: in core-js@4, move /modules/ dependencies to public entries for better optimization by tools like `preset-env`





























  var ITERATOR = wellKnownSymbol('iterator');
  var URL_SEARCH_PARAMS = 'URLSearchParams';
  var URL_SEARCH_PARAMS_ITERATOR = URL_SEARCH_PARAMS + 'Iterator';
  var setInternalState$1 = internalState.set;
  var getInternalParamsState = internalState.getterFor(URL_SEARCH_PARAMS);
  var getInternalIteratorState = internalState.getterFor(URL_SEARCH_PARAMS_ITERATOR);
  // eslint-disable-next-line es/no-object-getownpropertydescriptor -- safe
  var getOwnPropertyDescriptor$1 = Object.getOwnPropertyDescriptor;

  // Avoid NodeJS experimental warning
  var safeGetBuiltIn = function (name) {
    if (!descriptors) return global_1[name];
    var descriptor = getOwnPropertyDescriptor$1(global_1, name);
    return descriptor && descriptor.value;
  };

  var nativeFetch = safeGetBuiltIn('fetch');
  var NativeRequest = safeGetBuiltIn('Request');
  var Headers$1 = safeGetBuiltIn('Headers');
  var RequestPrototype = NativeRequest && NativeRequest.prototype;
  var HeadersPrototype = Headers$1 && Headers$1.prototype;
  var RegExp$1 = global_1.RegExp;
  var TypeError$3 = global_1.TypeError;
  var decodeURIComponent$1 = global_1.decodeURIComponent;
  var encodeURIComponent$1 = global_1.encodeURIComponent;
  var charAt$2 = functionUncurryThis(''.charAt);
  var join$1 = functionUncurryThis([].join);
  var push$2 = functionUncurryThis([].push);
  var replace$2 = functionUncurryThis(''.replace);
  var shift$1 = functionUncurryThis([].shift);
  var splice = functionUncurryThis([].splice);
  var split$1 = functionUncurryThis(''.split);
  var stringSlice$4 = functionUncurryThis(''.slice);

  var plus = /\+/g;
  var sequences = Array(4);

  var percentSequence = function (bytes) {
    return sequences[bytes - 1] || (sequences[bytes - 1] = RegExp$1('((?:%[\\da-f]{2}){' + bytes + '})', 'gi'));
  };

  var percentDecode = function (sequence) {
    try {
      return decodeURIComponent$1(sequence);
    } catch (error) {
      return sequence;
    }
  };

  var deserialize = function (it) {
    var result = replace$2(it, plus, ' ');
    var bytes = 4;
    try {
      return decodeURIComponent$1(result);
    } catch (error) {
      while (bytes) {
        result = replace$2(result, percentSequence(bytes--), percentDecode);
      }
      return result;
    }
  };

  var find = /[!'()~]|%20/g;

  var replacements = {
    '!': '%21',
    "'": '%27',
    '(': '%28',
    ')': '%29',
    '~': '%7E',
    '%20': '+'
  };

  var replacer = function (match) {
    return replacements[match];
  };

  var serialize = function (it) {
    return replace$2(encodeURIComponent$1(it), find, replacer);
  };

  var URLSearchParamsIterator = iteratorCreateConstructor(function Iterator(params, kind) {
    setInternalState$1(this, {
      type: URL_SEARCH_PARAMS_ITERATOR,
      iterator: getIterator(getInternalParamsState(params).entries),
      kind: kind
    });
  }, 'Iterator', function next() {
    var state = getInternalIteratorState(this);
    var kind = state.kind;
    var step = state.iterator.next();
    var entry = step.value;
    if (!step.done) {
      step.value = kind === 'keys' ? entry.key : kind === 'values' ? entry.value : [entry.key, entry.value];
    } return step;
  }, true);

  var URLSearchParamsState = function (init) {
    this.entries = [];
    this.url = null;

    if (init !== undefined) {
      if (isObject(init)) this.parseObject(init);
      else this.parseQuery(typeof init == 'string' ? charAt$2(init, 0) === '?' ? stringSlice$4(init, 1) : init : toString_1(init));
    }
  };

  URLSearchParamsState.prototype = {
    type: URL_SEARCH_PARAMS,
    bindURL: function (url) {
      this.url = url;
      this.update();
    },
    parseObject: function (object) {
      var iteratorMethod = getIteratorMethod(object);
      var iterator, next, step, entryIterator, entryNext, first, second;

      if (iteratorMethod) {
        iterator = getIterator(object, iteratorMethod);
        next = iterator.next;
        while (!(step = functionCall(next, iterator)).done) {
          entryIterator = getIterator(anObject(step.value));
          entryNext = entryIterator.next;
          if (
            (first = functionCall(entryNext, entryIterator)).done ||
            (second = functionCall(entryNext, entryIterator)).done ||
            !functionCall(entryNext, entryIterator).done
          ) throw TypeError$3('Expected sequence with length 2');
          push$2(this.entries, { key: toString_1(first.value), value: toString_1(second.value) });
        }
      } else for (var key in object) if (hasOwnProperty_1(object, key)) {
        push$2(this.entries, { key: key, value: toString_1(object[key]) });
      }
    },
    parseQuery: function (query) {
      if (query) {
        var attributes = split$1(query, '&');
        var index = 0;
        var attribute, entry;
        while (index < attributes.length) {
          attribute = attributes[index++];
          if (attribute.length) {
            entry = split$1(attribute, '=');
            push$2(this.entries, {
              key: deserialize(shift$1(entry)),
              value: deserialize(join$1(entry, '='))
            });
          }
        }
      }
    },
    serialize: function () {
      var entries = this.entries;
      var result = [];
      var index = 0;
      var entry;
      while (index < entries.length) {
        entry = entries[index++];
        push$2(result, serialize(entry.key) + '=' + serialize(entry.value));
      } return join$1(result, '&');
    },
    update: function () {
      this.entries.length = 0;
      this.parseQuery(this.url.query);
    },
    updateURL: function () {
      if (this.url) this.url.update();
    }
  };

  // `URLSearchParams` constructor
  // https://url.spec.whatwg.org/#interface-urlsearchparams
  var URLSearchParamsConstructor = function URLSearchParams(/* init */) {
    anInstance(this, URLSearchParamsPrototype);
    var init = arguments.length > 0 ? arguments[0] : undefined;
    var state = setInternalState$1(this, new URLSearchParamsState(init));
    if (!descriptors) this.size = state.entries.length;
  };

  var URLSearchParamsPrototype = URLSearchParamsConstructor.prototype;

  defineBuiltIns(URLSearchParamsPrototype, {
    // `URLSearchParams.prototype.append` method
    // https://url.spec.whatwg.org/#dom-urlsearchparams-append
    append: function append(name, value) {
      var state = getInternalParamsState(this);
      validateArgumentsLength(arguments.length, 2);
      push$2(state.entries, { key: toString_1(name), value: toString_1(value) });
      if (!descriptors) this.length++;
      state.updateURL();
    },
    // `URLSearchParams.prototype.delete` method
    // https://url.spec.whatwg.org/#dom-urlsearchparams-delete
    'delete': function (name /* , value */) {
      var state = getInternalParamsState(this);
      var length = validateArgumentsLength(arguments.length, 1);
      var entries = state.entries;
      var key = toString_1(name);
      var $value = length < 2 ? undefined : arguments[1];
      var value = $value === undefined ? $value : toString_1($value);
      var index = 0;
      while (index < entries.length) {
        var entry = entries[index];
        if (entry.key === key && (value === undefined || entry.value === value)) {
          splice(entries, index, 1);
          if (value !== undefined) break;
        } else index++;
      }
      if (!descriptors) this.size = entries.length;
      state.updateURL();
    },
    // `URLSearchParams.prototype.get` method
    // https://url.spec.whatwg.org/#dom-urlsearchparams-get
    get: function get(name) {
      var entries = getInternalParamsState(this).entries;
      validateArgumentsLength(arguments.length, 1);
      var key = toString_1(name);
      var index = 0;
      for (; index < entries.length; index++) {
        if (entries[index].key === key) return entries[index].value;
      }
      return null;
    },
    // `URLSearchParams.prototype.getAll` method
    // https://url.spec.whatwg.org/#dom-urlsearchparams-getall
    getAll: function getAll(name) {
      var entries = getInternalParamsState(this).entries;
      validateArgumentsLength(arguments.length, 1);
      var key = toString_1(name);
      var result = [];
      var index = 0;
      for (; index < entries.length; index++) {
        if (entries[index].key === key) push$2(result, entries[index].value);
      }
      return result;
    },
    // `URLSearchParams.prototype.has` method
    // https://url.spec.whatwg.org/#dom-urlsearchparams-has
    has: function has(name /* , value */) {
      var entries = getInternalParamsState(this).entries;
      var length = validateArgumentsLength(arguments.length, 1);
      var key = toString_1(name);
      var $value = length < 2 ? undefined : arguments[1];
      var value = $value === undefined ? $value : toString_1($value);
      var index = 0;
      while (index < entries.length) {
        var entry = entries[index++];
        if (entry.key === key && (value === undefined || entry.value === value)) return true;
      }
      return false;
    },
    // `URLSearchParams.prototype.set` method
    // https://url.spec.whatwg.org/#dom-urlsearchparams-set
    set: function set(name, value) {
      var state = getInternalParamsState(this);
      validateArgumentsLength(arguments.length, 1);
      var entries = state.entries;
      var found = false;
      var key = toString_1(name);
      var val = toString_1(value);
      var index = 0;
      var entry;
      for (; index < entries.length; index++) {
        entry = entries[index];
        if (entry.key === key) {
          if (found) splice(entries, index--, 1);
          else {
            found = true;
            entry.value = val;
          }
        }
      }
      if (!found) push$2(entries, { key: key, value: val });
      if (!descriptors) this.size = entries.length;
      state.updateURL();
    },
    // `URLSearchParams.prototype.sort` method
    // https://url.spec.whatwg.org/#dom-urlsearchparams-sort
    sort: function sort() {
      var state = getInternalParamsState(this);
      arraySort(state.entries, function (a, b) {
        return a.key > b.key ? 1 : -1;
      });
      state.updateURL();
    },
    // `URLSearchParams.prototype.forEach` method
    forEach: function forEach(callback /* , thisArg */) {
      var entries = getInternalParamsState(this).entries;
      var boundFunction = functionBindContext(callback, arguments.length > 1 ? arguments[1] : undefined);
      var index = 0;
      var entry;
      while (index < entries.length) {
        entry = entries[index++];
        boundFunction(entry.value, entry.key, this);
      }
    },
    // `URLSearchParams.prototype.keys` method
    keys: function keys() {
      return new URLSearchParamsIterator(this, 'keys');
    },
    // `URLSearchParams.prototype.values` method
    values: function values() {
      return new URLSearchParamsIterator(this, 'values');
    },
    // `URLSearchParams.prototype.entries` method
    entries: function entries() {
      return new URLSearchParamsIterator(this, 'entries');
    }
  }, { enumerable: true });

  // `URLSearchParams.prototype[@@iterator]` method
  defineBuiltIn(URLSearchParamsPrototype, ITERATOR, URLSearchParamsPrototype.entries, { name: 'entries' });

  // `URLSearchParams.prototype.toString` method
  // https://url.spec.whatwg.org/#urlsearchparams-stringification-behavior
  defineBuiltIn(URLSearchParamsPrototype, 'toString', function toString() {
    return getInternalParamsState(this).serialize();
  }, { enumerable: true });

  // `URLSearchParams.prototype.size` getter
  // https://github.com/whatwg/url/pull/734
  if (descriptors) defineBuiltInAccessor(URLSearchParamsPrototype, 'size', {
    get: function size() {
      return getInternalParamsState(this).entries.length;
    },
    configurable: true,
    enumerable: true
  });

  setToStringTag(URLSearchParamsConstructor, URL_SEARCH_PARAMS);

  _export({ global: true, constructor: true, forced: !urlConstructorDetection }, {
    URLSearchParams: URLSearchParamsConstructor
  });

  // Wrap `fetch` and `Request` for correct work with polyfilled `URLSearchParams`
  if (!urlConstructorDetection && isCallable(Headers$1)) {
    var headersHas = functionUncurryThis(HeadersPrototype.has);
    var headersSet = functionUncurryThis(HeadersPrototype.set);

    var wrapRequestOptions = function (init) {
      if (isObject(init)) {
        var body = init.body;
        var headers;
        if (classof(body) === URL_SEARCH_PARAMS) {
          headers = init.headers ? new Headers$1(init.headers) : new Headers$1();
          if (!headersHas(headers, 'content-type')) {
            headersSet(headers, 'content-type', 'application/x-www-form-urlencoded;charset=UTF-8');
          }
          return objectCreate(init, {
            body: createPropertyDescriptor(0, toString_1(body)),
            headers: createPropertyDescriptor(0, headers)
          });
        }
      } return init;
    };

    if (isCallable(nativeFetch)) {
      _export({ global: true, enumerable: true, dontCallGetSet: true, forced: true }, {
        fetch: function fetch(input /* , init */) {
          return nativeFetch(input, arguments.length > 1 ? wrapRequestOptions(arguments[1]) : {});
        }
      });
    }

    if (isCallable(NativeRequest)) {
      var RequestConstructor = function Request(input /* , init */) {
        anInstance(this, RequestPrototype);
        return new NativeRequest(input, arguments.length > 1 ? wrapRequestOptions(arguments[1]) : {});
      };

      RequestPrototype.constructor = RequestConstructor;
      RequestConstructor.prototype = RequestPrototype;

      _export({ global: true, constructor: true, dontCallGetSet: true, forced: true }, {
        Request: RequestConstructor
      });
    }
  }

  var web_urlSearchParams_constructor = {
    URLSearchParams: URLSearchParamsConstructor,
    getState: getInternalParamsState
  };

  // TODO: in core-js@4, move /modules/ dependencies to public entries for better optimization by tools like `preset-env`














  var codeAt = stringMultibyte.codeAt;







  var setInternalState = internalState.set;
  var getInternalURLState = internalState.getterFor('URL');
  var URLSearchParams$1 = web_urlSearchParams_constructor.URLSearchParams;
  var getInternalSearchParamsState = web_urlSearchParams_constructor.getState;

  var NativeURL = global_1.URL;
  var TypeError$2 = global_1.TypeError;
  var parseInt$1 = global_1.parseInt;
  var floor$1 = Math.floor;
  var pow = Math.pow;
  var charAt$1 = functionUncurryThis(''.charAt);
  var exec = functionUncurryThis(/./.exec);
  var join = functionUncurryThis([].join);
  var numberToString = functionUncurryThis(1.0.toString);
  var pop = functionUncurryThis([].pop);
  var push$1 = functionUncurryThis([].push);
  var replace$1 = functionUncurryThis(''.replace);
  var shift = functionUncurryThis([].shift);
  var split = functionUncurryThis(''.split);
  var stringSlice$3 = functionUncurryThis(''.slice);
  var toLowerCase = functionUncurryThis(''.toLowerCase);
  var unshift = functionUncurryThis([].unshift);

  var INVALID_AUTHORITY = 'Invalid authority';
  var INVALID_SCHEME = 'Invalid scheme';
  var INVALID_HOST = 'Invalid host';
  var INVALID_PORT = 'Invalid port';

  var ALPHA = /[a-z]/i;
  // eslint-disable-next-line regexp/no-obscure-range -- safe
  var ALPHANUMERIC = /[\d+-.a-z]/i;
  var DIGIT = /\d/;
  var HEX_START = /^0x/i;
  var OCT = /^[0-7]+$/;
  var DEC = /^\d+$/;
  var HEX = /^[\da-f]+$/i;
  /* eslint-disable regexp/no-control-character -- safe */
  var FORBIDDEN_HOST_CODE_POINT = /[\0\t\n\r #%/:<>?@[\\\]^|]/;
  var FORBIDDEN_HOST_CODE_POINT_EXCLUDING_PERCENT = /[\0\t\n\r #/:<>?@[\\\]^|]/;
  var LEADING_C0_CONTROL_OR_SPACE = /^[\u0000-\u0020]+/;
  var TRAILING_C0_CONTROL_OR_SPACE = /(^|[^\u0000-\u0020])[\u0000-\u0020]+$/;
  var TAB_AND_NEW_LINE = /[\t\n\r]/g;
  /* eslint-enable regexp/no-control-character -- safe */
  var EOF;

  // https://url.spec.whatwg.org/#ipv4-number-parser
  var parseIPv4 = function (input) {
    var parts = split(input, '.');
    var partsLength, numbers, index, part, radix, number, ipv4;
    if (parts.length && parts[parts.length - 1] == '') {
      parts.length--;
    }
    partsLength = parts.length;
    if (partsLength > 4) return input;
    numbers = [];
    for (index = 0; index < partsLength; index++) {
      part = parts[index];
      if (part == '') return input;
      radix = 10;
      if (part.length > 1 && charAt$1(part, 0) == '0') {
        radix = exec(HEX_START, part) ? 16 : 8;
        part = stringSlice$3(part, radix == 8 ? 1 : 2);
      }
      if (part === '') {
        number = 0;
      } else {
        if (!exec(radix == 10 ? DEC : radix == 8 ? OCT : HEX, part)) return input;
        number = parseInt$1(part, radix);
      }
      push$1(numbers, number);
    }
    for (index = 0; index < partsLength; index++) {
      number = numbers[index];
      if (index == partsLength - 1) {
        if (number >= pow(256, 5 - partsLength)) return null;
      } else if (number > 255) return null;
    }
    ipv4 = pop(numbers);
    for (index = 0; index < numbers.length; index++) {
      ipv4 += numbers[index] * pow(256, 3 - index);
    }
    return ipv4;
  };

  // https://url.spec.whatwg.org/#concept-ipv6-parser
  // eslint-disable-next-line max-statements -- TODO
  var parseIPv6 = function (input) {
    var address = [0, 0, 0, 0, 0, 0, 0, 0];
    var pieceIndex = 0;
    var compress = null;
    var pointer = 0;
    var value, length, numbersSeen, ipv4Piece, number, swaps, swap;

    var chr = function () {
      return charAt$1(input, pointer);
    };

    if (chr() == ':') {
      if (charAt$1(input, 1) != ':') return;
      pointer += 2;
      pieceIndex++;
      compress = pieceIndex;
    }
    while (chr()) {
      if (pieceIndex == 8) return;
      if (chr() == ':') {
        if (compress !== null) return;
        pointer++;
        pieceIndex++;
        compress = pieceIndex;
        continue;
      }
      value = length = 0;
      while (length < 4 && exec(HEX, chr())) {
        value = value * 16 + parseInt$1(chr(), 16);
        pointer++;
        length++;
      }
      if (chr() == '.') {
        if (length == 0) return;
        pointer -= length;
        if (pieceIndex > 6) return;
        numbersSeen = 0;
        while (chr()) {
          ipv4Piece = null;
          if (numbersSeen > 0) {
            if (chr() == '.' && numbersSeen < 4) pointer++;
            else return;
          }
          if (!exec(DIGIT, chr())) return;
          while (exec(DIGIT, chr())) {
            number = parseInt$1(chr(), 10);
            if (ipv4Piece === null) ipv4Piece = number;
            else if (ipv4Piece == 0) return;
            else ipv4Piece = ipv4Piece * 10 + number;
            if (ipv4Piece > 255) return;
            pointer++;
          }
          address[pieceIndex] = address[pieceIndex] * 256 + ipv4Piece;
          numbersSeen++;
          if (numbersSeen == 2 || numbersSeen == 4) pieceIndex++;
        }
        if (numbersSeen != 4) return;
        break;
      } else if (chr() == ':') {
        pointer++;
        if (!chr()) return;
      } else if (chr()) return;
      address[pieceIndex++] = value;
    }
    if (compress !== null) {
      swaps = pieceIndex - compress;
      pieceIndex = 7;
      while (pieceIndex != 0 && swaps > 0) {
        swap = address[pieceIndex];
        address[pieceIndex--] = address[compress + swaps - 1];
        address[compress + --swaps] = swap;
      }
    } else if (pieceIndex != 8) return;
    return address;
  };

  var findLongestZeroSequence = function (ipv6) {
    var maxIndex = null;
    var maxLength = 1;
    var currStart = null;
    var currLength = 0;
    var index = 0;
    for (; index < 8; index++) {
      if (ipv6[index] !== 0) {
        if (currLength > maxLength) {
          maxIndex = currStart;
          maxLength = currLength;
        }
        currStart = null;
        currLength = 0;
      } else {
        if (currStart === null) currStart = index;
        ++currLength;
      }
    }
    if (currLength > maxLength) {
      maxIndex = currStart;
      maxLength = currLength;
    }
    return maxIndex;
  };

  // https://url.spec.whatwg.org/#host-serializing
  var serializeHost = function (host) {
    var result, index, compress, ignore0;
    // ipv4
    if (typeof host == 'number') {
      result = [];
      for (index = 0; index < 4; index++) {
        unshift(result, host % 256);
        host = floor$1(host / 256);
      } return join(result, '.');
    // ipv6
    } else if (typeof host == 'object') {
      result = '';
      compress = findLongestZeroSequence(host);
      for (index = 0; index < 8; index++) {
        if (ignore0 && host[index] === 0) continue;
        if (ignore0) ignore0 = false;
        if (compress === index) {
          result += index ? ':' : '::';
          ignore0 = true;
        } else {
          result += numberToString(host[index], 16);
          if (index < 7) result += ':';
        }
      }
      return '[' + result + ']';
    } return host;
  };

  var C0ControlPercentEncodeSet = {};
  var fragmentPercentEncodeSet = objectAssign({}, C0ControlPercentEncodeSet, {
    ' ': 1, '"': 1, '<': 1, '>': 1, '`': 1
  });
  var pathPercentEncodeSet = objectAssign({}, fragmentPercentEncodeSet, {
    '#': 1, '?': 1, '{': 1, '}': 1
  });
  var userinfoPercentEncodeSet = objectAssign({}, pathPercentEncodeSet, {
    '/': 1, ':': 1, ';': 1, '=': 1, '@': 1, '[': 1, '\\': 1, ']': 1, '^': 1, '|': 1
  });

  var percentEncode = function (chr, set) {
    var code = codeAt(chr, 0);
    return code > 0x20 && code < 0x7F && !hasOwnProperty_1(set, chr) ? chr : encodeURIComponent(chr);
  };

  // https://url.spec.whatwg.org/#special-scheme
  var specialSchemes = {
    ftp: 21,
    file: null,
    http: 80,
    https: 443,
    ws: 80,
    wss: 443
  };

  // https://url.spec.whatwg.org/#windows-drive-letter
  var isWindowsDriveLetter = function (string, normalized) {
    var second;
    return string.length == 2 && exec(ALPHA, charAt$1(string, 0))
      && ((second = charAt$1(string, 1)) == ':' || (!normalized && second == '|'));
  };

  // https://url.spec.whatwg.org/#start-with-a-windows-drive-letter
  var startsWithWindowsDriveLetter = function (string) {
    var third;
    return string.length > 1 && isWindowsDriveLetter(stringSlice$3(string, 0, 2)) && (
      string.length == 2 ||
      ((third = charAt$1(string, 2)) === '/' || third === '\\' || third === '?' || third === '#')
    );
  };

  // https://url.spec.whatwg.org/#single-dot-path-segment
  var isSingleDot = function (segment) {
    return segment === '.' || toLowerCase(segment) === '%2e';
  };

  // https://url.spec.whatwg.org/#double-dot-path-segment
  var isDoubleDot = function (segment) {
    segment = toLowerCase(segment);
    return segment === '..' || segment === '%2e.' || segment === '.%2e' || segment === '%2e%2e';
  };

  // States:
  var SCHEME_START = {};
  var SCHEME = {};
  var NO_SCHEME = {};
  var SPECIAL_RELATIVE_OR_AUTHORITY = {};
  var PATH_OR_AUTHORITY = {};
  var RELATIVE = {};
  var RELATIVE_SLASH = {};
  var SPECIAL_AUTHORITY_SLASHES = {};
  var SPECIAL_AUTHORITY_IGNORE_SLASHES = {};
  var AUTHORITY = {};
  var HOST = {};
  var HOSTNAME = {};
  var PORT = {};
  var FILE = {};
  var FILE_SLASH = {};
  var FILE_HOST = {};
  var PATH_START = {};
  var PATH = {};
  var CANNOT_BE_A_BASE_URL_PATH = {};
  var QUERY = {};
  var FRAGMENT = {};

  var URLState = function (url, isBase, base) {
    var urlString = toString_1(url);
    var baseState, failure, searchParams;
    if (isBase) {
      failure = this.parse(urlString);
      if (failure) throw TypeError$2(failure);
      this.searchParams = null;
    } else {
      if (base !== undefined) baseState = new URLState(base, true);
      failure = this.parse(urlString, null, baseState);
      if (failure) throw TypeError$2(failure);
      searchParams = getInternalSearchParamsState(new URLSearchParams$1());
      searchParams.bindURL(this);
      this.searchParams = searchParams;
    }
  };

  URLState.prototype = {
    type: 'URL',
    // https://url.spec.whatwg.org/#url-parsing
    // eslint-disable-next-line max-statements -- TODO
    parse: function (input, stateOverride, base) {
      var url = this;
      var state = stateOverride || SCHEME_START;
      var pointer = 0;
      var buffer = '';
      var seenAt = false;
      var seenBracket = false;
      var seenPasswordToken = false;
      var codePoints, chr, bufferCodePoints, failure;

      input = toString_1(input);

      if (!stateOverride) {
        url.scheme = '';
        url.username = '';
        url.password = '';
        url.host = null;
        url.port = null;
        url.path = [];
        url.query = null;
        url.fragment = null;
        url.cannotBeABaseURL = false;
        input = replace$1(input, LEADING_C0_CONTROL_OR_SPACE, '');
        input = replace$1(input, TRAILING_C0_CONTROL_OR_SPACE, '$1');
      }

      input = replace$1(input, TAB_AND_NEW_LINE, '');

      codePoints = arrayFrom(input);

      while (pointer <= codePoints.length) {
        chr = codePoints[pointer];
        switch (state) {
          case SCHEME_START:
            if (chr && exec(ALPHA, chr)) {
              buffer += toLowerCase(chr);
              state = SCHEME;
            } else if (!stateOverride) {
              state = NO_SCHEME;
              continue;
            } else return INVALID_SCHEME;
            break;

          case SCHEME:
            if (chr && (exec(ALPHANUMERIC, chr) || chr == '+' || chr == '-' || chr == '.')) {
              buffer += toLowerCase(chr);
            } else if (chr == ':') {
              if (stateOverride && (
                (url.isSpecial() != hasOwnProperty_1(specialSchemes, buffer)) ||
                (buffer == 'file' && (url.includesCredentials() || url.port !== null)) ||
                (url.scheme == 'file' && !url.host)
              )) return;
              url.scheme = buffer;
              if (stateOverride) {
                if (url.isSpecial() && specialSchemes[url.scheme] == url.port) url.port = null;
                return;
              }
              buffer = '';
              if (url.scheme == 'file') {
                state = FILE;
              } else if (url.isSpecial() && base && base.scheme == url.scheme) {
                state = SPECIAL_RELATIVE_OR_AUTHORITY;
              } else if (url.isSpecial()) {
                state = SPECIAL_AUTHORITY_SLASHES;
              } else if (codePoints[pointer + 1] == '/') {
                state = PATH_OR_AUTHORITY;
                pointer++;
              } else {
                url.cannotBeABaseURL = true;
                push$1(url.path, '');
                state = CANNOT_BE_A_BASE_URL_PATH;
              }
            } else if (!stateOverride) {
              buffer = '';
              state = NO_SCHEME;
              pointer = 0;
              continue;
            } else return INVALID_SCHEME;
            break;

          case NO_SCHEME:
            if (!base || (base.cannotBeABaseURL && chr != '#')) return INVALID_SCHEME;
            if (base.cannotBeABaseURL && chr == '#') {
              url.scheme = base.scheme;
              url.path = arraySliceSimple(base.path);
              url.query = base.query;
              url.fragment = '';
              url.cannotBeABaseURL = true;
              state = FRAGMENT;
              break;
            }
            state = base.scheme == 'file' ? FILE : RELATIVE;
            continue;

          case SPECIAL_RELATIVE_OR_AUTHORITY:
            if (chr == '/' && codePoints[pointer + 1] == '/') {
              state = SPECIAL_AUTHORITY_IGNORE_SLASHES;
              pointer++;
            } else {
              state = RELATIVE;
              continue;
            } break;

          case PATH_OR_AUTHORITY:
            if (chr == '/') {
              state = AUTHORITY;
              break;
            } else {
              state = PATH;
              continue;
            }

          case RELATIVE:
            url.scheme = base.scheme;
            if (chr == EOF) {
              url.username = base.username;
              url.password = base.password;
              url.host = base.host;
              url.port = base.port;
              url.path = arraySliceSimple(base.path);
              url.query = base.query;
            } else if (chr == '/' || (chr == '\\' && url.isSpecial())) {
              state = RELATIVE_SLASH;
            } else if (chr == '?') {
              url.username = base.username;
              url.password = base.password;
              url.host = base.host;
              url.port = base.port;
              url.path = arraySliceSimple(base.path);
              url.query = '';
              state = QUERY;
            } else if (chr == '#') {
              url.username = base.username;
              url.password = base.password;
              url.host = base.host;
              url.port = base.port;
              url.path = arraySliceSimple(base.path);
              url.query = base.query;
              url.fragment = '';
              state = FRAGMENT;
            } else {
              url.username = base.username;
              url.password = base.password;
              url.host = base.host;
              url.port = base.port;
              url.path = arraySliceSimple(base.path);
              url.path.length--;
              state = PATH;
              continue;
            } break;

          case RELATIVE_SLASH:
            if (url.isSpecial() && (chr == '/' || chr == '\\')) {
              state = SPECIAL_AUTHORITY_IGNORE_SLASHES;
            } else if (chr == '/') {
              state = AUTHORITY;
            } else {
              url.username = base.username;
              url.password = base.password;
              url.host = base.host;
              url.port = base.port;
              state = PATH;
              continue;
            } break;

          case SPECIAL_AUTHORITY_SLASHES:
            state = SPECIAL_AUTHORITY_IGNORE_SLASHES;
            if (chr != '/' || charAt$1(buffer, pointer + 1) != '/') continue;
            pointer++;
            break;

          case SPECIAL_AUTHORITY_IGNORE_SLASHES:
            if (chr != '/' && chr != '\\') {
              state = AUTHORITY;
              continue;
            } break;

          case AUTHORITY:
            if (chr == '@') {
              if (seenAt) buffer = '%40' + buffer;
              seenAt = true;
              bufferCodePoints = arrayFrom(buffer);
              for (var i = 0; i < bufferCodePoints.length; i++) {
                var codePoint = bufferCodePoints[i];
                if (codePoint == ':' && !seenPasswordToken) {
                  seenPasswordToken = true;
                  continue;
                }
                var encodedCodePoints = percentEncode(codePoint, userinfoPercentEncodeSet);
                if (seenPasswordToken) url.password += encodedCodePoints;
                else url.username += encodedCodePoints;
              }
              buffer = '';
            } else if (
              chr == EOF || chr == '/' || chr == '?' || chr == '#' ||
              (chr == '\\' && url.isSpecial())
            ) {
              if (seenAt && buffer == '') return INVALID_AUTHORITY;
              pointer -= arrayFrom(buffer).length + 1;
              buffer = '';
              state = HOST;
            } else buffer += chr;
            break;

          case HOST:
          case HOSTNAME:
            if (stateOverride && url.scheme == 'file') {
              state = FILE_HOST;
              continue;
            } else if (chr == ':' && !seenBracket) {
              if (buffer == '') return INVALID_HOST;
              failure = url.parseHost(buffer);
              if (failure) return failure;
              buffer = '';
              state = PORT;
              if (stateOverride == HOSTNAME) return;
            } else if (
              chr == EOF || chr == '/' || chr == '?' || chr == '#' ||
              (chr == '\\' && url.isSpecial())
            ) {
              if (url.isSpecial() && buffer == '') return INVALID_HOST;
              if (stateOverride && buffer == '' && (url.includesCredentials() || url.port !== null)) return;
              failure = url.parseHost(buffer);
              if (failure) return failure;
              buffer = '';
              state = PATH_START;
              if (stateOverride) return;
              continue;
            } else {
              if (chr == '[') seenBracket = true;
              else if (chr == ']') seenBracket = false;
              buffer += chr;
            } break;

          case PORT:
            if (exec(DIGIT, chr)) {
              buffer += chr;
            } else if (
              chr == EOF || chr == '/' || chr == '?' || chr == '#' ||
              (chr == '\\' && url.isSpecial()) ||
              stateOverride
            ) {
              if (buffer != '') {
                var port = parseInt$1(buffer, 10);
                if (port > 0xFFFF) return INVALID_PORT;
                url.port = (url.isSpecial() && port === specialSchemes[url.scheme]) ? null : port;
                buffer = '';
              }
              if (stateOverride) return;
              state = PATH_START;
              continue;
            } else return INVALID_PORT;
            break;

          case FILE:
            url.scheme = 'file';
            if (chr == '/' || chr == '\\') state = FILE_SLASH;
            else if (base && base.scheme == 'file') {
              if (chr == EOF) {
                url.host = base.host;
                url.path = arraySliceSimple(base.path);
                url.query = base.query;
              } else if (chr == '?') {
                url.host = base.host;
                url.path = arraySliceSimple(base.path);
                url.query = '';
                state = QUERY;
              } else if (chr == '#') {
                url.host = base.host;
                url.path = arraySliceSimple(base.path);
                url.query = base.query;
                url.fragment = '';
                state = FRAGMENT;
              } else {
                if (!startsWithWindowsDriveLetter(join(arraySliceSimple(codePoints, pointer), ''))) {
                  url.host = base.host;
                  url.path = arraySliceSimple(base.path);
                  url.shortenPath();
                }
                state = PATH;
                continue;
              }
            } else {
              state = PATH;
              continue;
            } break;

          case FILE_SLASH:
            if (chr == '/' || chr == '\\') {
              state = FILE_HOST;
              break;
            }
            if (base && base.scheme == 'file' && !startsWithWindowsDriveLetter(join(arraySliceSimple(codePoints, pointer), ''))) {
              if (isWindowsDriveLetter(base.path[0], true)) push$1(url.path, base.path[0]);
              else url.host = base.host;
            }
            state = PATH;
            continue;

          case FILE_HOST:
            if (chr == EOF || chr == '/' || chr == '\\' || chr == '?' || chr == '#') {
              if (!stateOverride && isWindowsDriveLetter(buffer)) {
                state = PATH;
              } else if (buffer == '') {
                url.host = '';
                if (stateOverride) return;
                state = PATH_START;
              } else {
                failure = url.parseHost(buffer);
                if (failure) return failure;
                if (url.host == 'localhost') url.host = '';
                if (stateOverride) return;
                buffer = '';
                state = PATH_START;
              } continue;
            } else buffer += chr;
            break;

          case PATH_START:
            if (url.isSpecial()) {
              state = PATH;
              if (chr != '/' && chr != '\\') continue;
            } else if (!stateOverride && chr == '?') {
              url.query = '';
              state = QUERY;
            } else if (!stateOverride && chr == '#') {
              url.fragment = '';
              state = FRAGMENT;
            } else if (chr != EOF) {
              state = PATH;
              if (chr != '/') continue;
            } break;

          case PATH:
            if (
              chr == EOF || chr == '/' ||
              (chr == '\\' && url.isSpecial()) ||
              (!stateOverride && (chr == '?' || chr == '#'))
            ) {
              if (isDoubleDot(buffer)) {
                url.shortenPath();
                if (chr != '/' && !(chr == '\\' && url.isSpecial())) {
                  push$1(url.path, '');
                }
              } else if (isSingleDot(buffer)) {
                if (chr != '/' && !(chr == '\\' && url.isSpecial())) {
                  push$1(url.path, '');
                }
              } else {
                if (url.scheme == 'file' && !url.path.length && isWindowsDriveLetter(buffer)) {
                  if (url.host) url.host = '';
                  buffer = charAt$1(buffer, 0) + ':'; // normalize windows drive letter
                }
                push$1(url.path, buffer);
              }
              buffer = '';
              if (url.scheme == 'file' && (chr == EOF || chr == '?' || chr == '#')) {
                while (url.path.length > 1 && url.path[0] === '') {
                  shift(url.path);
                }
              }
              if (chr == '?') {
                url.query = '';
                state = QUERY;
              } else if (chr == '#') {
                url.fragment = '';
                state = FRAGMENT;
              }
            } else {
              buffer += percentEncode(chr, pathPercentEncodeSet);
            } break;

          case CANNOT_BE_A_BASE_URL_PATH:
            if (chr == '?') {
              url.query = '';
              state = QUERY;
            } else if (chr == '#') {
              url.fragment = '';
              state = FRAGMENT;
            } else if (chr != EOF) {
              url.path[0] += percentEncode(chr, C0ControlPercentEncodeSet);
            } break;

          case QUERY:
            if (!stateOverride && chr == '#') {
              url.fragment = '';
              state = FRAGMENT;
            } else if (chr != EOF) {
              if (chr == "'" && url.isSpecial()) url.query += '%27';
              else if (chr == '#') url.query += '%23';
              else url.query += percentEncode(chr, C0ControlPercentEncodeSet);
            } break;

          case FRAGMENT:
            if (chr != EOF) url.fragment += percentEncode(chr, fragmentPercentEncodeSet);
            break;
        }

        pointer++;
      }
    },
    // https://url.spec.whatwg.org/#host-parsing
    parseHost: function (input) {
      var result, codePoints, index;
      if (charAt$1(input, 0) == '[') {
        if (charAt$1(input, input.length - 1) != ']') return INVALID_HOST;
        result = parseIPv6(stringSlice$3(input, 1, -1));
        if (!result) return INVALID_HOST;
        this.host = result;
      // opaque host
      } else if (!this.isSpecial()) {
        if (exec(FORBIDDEN_HOST_CODE_POINT_EXCLUDING_PERCENT, input)) return INVALID_HOST;
        result = '';
        codePoints = arrayFrom(input);
        for (index = 0; index < codePoints.length; index++) {
          result += percentEncode(codePoints[index], C0ControlPercentEncodeSet);
        }
        this.host = result;
      } else {
        input = stringPunycodeToAscii(input);
        if (exec(FORBIDDEN_HOST_CODE_POINT, input)) return INVALID_HOST;
        result = parseIPv4(input);
        if (result === null) return INVALID_HOST;
        this.host = result;
      }
    },
    // https://url.spec.whatwg.org/#cannot-have-a-username-password-port
    cannotHaveUsernamePasswordPort: function () {
      return !this.host || this.cannotBeABaseURL || this.scheme == 'file';
    },
    // https://url.spec.whatwg.org/#include-credentials
    includesCredentials: function () {
      return this.username != '' || this.password != '';
    },
    // https://url.spec.whatwg.org/#is-special
    isSpecial: function () {
      return hasOwnProperty_1(specialSchemes, this.scheme);
    },
    // https://url.spec.whatwg.org/#shorten-a-urls-path
    shortenPath: function () {
      var path = this.path;
      var pathSize = path.length;
      if (pathSize && (this.scheme != 'file' || pathSize != 1 || !isWindowsDriveLetter(path[0], true))) {
        path.length--;
      }
    },
    // https://url.spec.whatwg.org/#concept-url-serializer
    serialize: function () {
      var url = this;
      var scheme = url.scheme;
      var username = url.username;
      var password = url.password;
      var host = url.host;
      var port = url.port;
      var path = url.path;
      var query = url.query;
      var fragment = url.fragment;
      var output = scheme + ':';
      if (host !== null) {
        output += '//';
        if (url.includesCredentials()) {
          output += username + (password ? ':' + password : '') + '@';
        }
        output += serializeHost(host);
        if (port !== null) output += ':' + port;
      } else if (scheme == 'file') output += '//';
      output += url.cannotBeABaseURL ? path[0] : path.length ? '/' + join(path, '/') : '';
      if (query !== null) output += '?' + query;
      if (fragment !== null) output += '#' + fragment;
      return output;
    },
    // https://url.spec.whatwg.org/#dom-url-href
    setHref: function (href) {
      var failure = this.parse(href);
      if (failure) throw TypeError$2(failure);
      this.searchParams.update();
    },
    // https://url.spec.whatwg.org/#dom-url-origin
    getOrigin: function () {
      var scheme = this.scheme;
      var port = this.port;
      if (scheme == 'blob') try {
        return new URLConstructor(scheme.path[0]).origin;
      } catch (error) {
        return 'null';
      }
      if (scheme == 'file' || !this.isSpecial()) return 'null';
      return scheme + '://' + serializeHost(this.host) + (port !== null ? ':' + port : '');
    },
    // https://url.spec.whatwg.org/#dom-url-protocol
    getProtocol: function () {
      return this.scheme + ':';
    },
    setProtocol: function (protocol) {
      this.parse(toString_1(protocol) + ':', SCHEME_START);
    },
    // https://url.spec.whatwg.org/#dom-url-username
    getUsername: function () {
      return this.username;
    },
    setUsername: function (username) {
      var codePoints = arrayFrom(toString_1(username));
      if (this.cannotHaveUsernamePasswordPort()) return;
      this.username = '';
      for (var i = 0; i < codePoints.length; i++) {
        this.username += percentEncode(codePoints[i], userinfoPercentEncodeSet);
      }
    },
    // https://url.spec.whatwg.org/#dom-url-password
    getPassword: function () {
      return this.password;
    },
    setPassword: function (password) {
      var codePoints = arrayFrom(toString_1(password));
      if (this.cannotHaveUsernamePasswordPort()) return;
      this.password = '';
      for (var i = 0; i < codePoints.length; i++) {
        this.password += percentEncode(codePoints[i], userinfoPercentEncodeSet);
      }
    },
    // https://url.spec.whatwg.org/#dom-url-host
    getHost: function () {
      var host = this.host;
      var port = this.port;
      return host === null ? ''
        : port === null ? serializeHost(host)
        : serializeHost(host) + ':' + port;
    },
    setHost: function (host) {
      if (this.cannotBeABaseURL) return;
      this.parse(host, HOST);
    },
    // https://url.spec.whatwg.org/#dom-url-hostname
    getHostname: function () {
      var host = this.host;
      return host === null ? '' : serializeHost(host);
    },
    setHostname: function (hostname) {
      if (this.cannotBeABaseURL) return;
      this.parse(hostname, HOSTNAME);
    },
    // https://url.spec.whatwg.org/#dom-url-port
    getPort: function () {
      var port = this.port;
      return port === null ? '' : toString_1(port);
    },
    setPort: function (port) {
      if (this.cannotHaveUsernamePasswordPort()) return;
      port = toString_1(port);
      if (port == '') this.port = null;
      else this.parse(port, PORT);
    },
    // https://url.spec.whatwg.org/#dom-url-pathname
    getPathname: function () {
      var path = this.path;
      return this.cannotBeABaseURL ? path[0] : path.length ? '/' + join(path, '/') : '';
    },
    setPathname: function (pathname) {
      if (this.cannotBeABaseURL) return;
      this.path = [];
      this.parse(pathname, PATH_START);
    },
    // https://url.spec.whatwg.org/#dom-url-search
    getSearch: function () {
      var query = this.query;
      return query ? '?' + query : '';
    },
    setSearch: function (search) {
      search = toString_1(search);
      if (search == '') {
        this.query = null;
      } else {
        if ('?' == charAt$1(search, 0)) search = stringSlice$3(search, 1);
        this.query = '';
        this.parse(search, QUERY);
      }
      this.searchParams.update();
    },
    // https://url.spec.whatwg.org/#dom-url-searchparams
    getSearchParams: function () {
      return this.searchParams.facade;
    },
    // https://url.spec.whatwg.org/#dom-url-hash
    getHash: function () {
      var fragment = this.fragment;
      return fragment ? '#' + fragment : '';
    },
    setHash: function (hash) {
      hash = toString_1(hash);
      if (hash == '') {
        this.fragment = null;
        return;
      }
      if ('#' == charAt$1(hash, 0)) hash = stringSlice$3(hash, 1);
      this.fragment = '';
      this.parse(hash, FRAGMENT);
    },
    update: function () {
      this.query = this.searchParams.serialize() || null;
    }
  };

  // `URL` constructor
  // https://url.spec.whatwg.org/#url-class
  var URLConstructor = function URL(url /* , base */) {
    var that = anInstance(this, URLPrototype);
    var base = validateArgumentsLength(arguments.length, 1) > 1 ? arguments[1] : undefined;
    var state = setInternalState(that, new URLState(url, false, base));
    if (!descriptors) {
      that.href = state.serialize();
      that.origin = state.getOrigin();
      that.protocol = state.getProtocol();
      that.username = state.getUsername();
      that.password = state.getPassword();
      that.host = state.getHost();
      that.hostname = state.getHostname();
      that.port = state.getPort();
      that.pathname = state.getPathname();
      that.search = state.getSearch();
      that.searchParams = state.getSearchParams();
      that.hash = state.getHash();
    }
  };

  var URLPrototype = URLConstructor.prototype;

  var accessorDescriptor = function (getter, setter) {
    return {
      get: function () {
        return getInternalURLState(this)[getter]();
      },
      set: setter && function (value) {
        return getInternalURLState(this)[setter](value);
      },
      configurable: true,
      enumerable: true
    };
  };

  if (descriptors) {
    // `URL.prototype.href` accessors pair
    // https://url.spec.whatwg.org/#dom-url-href
    defineBuiltInAccessor(URLPrototype, 'href', accessorDescriptor('serialize', 'setHref'));
    // `URL.prototype.origin` getter
    // https://url.spec.whatwg.org/#dom-url-origin
    defineBuiltInAccessor(URLPrototype, 'origin', accessorDescriptor('getOrigin'));
    // `URL.prototype.protocol` accessors pair
    // https://url.spec.whatwg.org/#dom-url-protocol
    defineBuiltInAccessor(URLPrototype, 'protocol', accessorDescriptor('getProtocol', 'setProtocol'));
    // `URL.prototype.username` accessors pair
    // https://url.spec.whatwg.org/#dom-url-username
    defineBuiltInAccessor(URLPrototype, 'username', accessorDescriptor('getUsername', 'setUsername'));
    // `URL.prototype.password` accessors pair
    // https://url.spec.whatwg.org/#dom-url-password
    defineBuiltInAccessor(URLPrototype, 'password', accessorDescriptor('getPassword', 'setPassword'));
    // `URL.prototype.host` accessors pair
    // https://url.spec.whatwg.org/#dom-url-host
    defineBuiltInAccessor(URLPrototype, 'host', accessorDescriptor('getHost', 'setHost'));
    // `URL.prototype.hostname` accessors pair
    // https://url.spec.whatwg.org/#dom-url-hostname
    defineBuiltInAccessor(URLPrototype, 'hostname', accessorDescriptor('getHostname', 'setHostname'));
    // `URL.prototype.port` accessors pair
    // https://url.spec.whatwg.org/#dom-url-port
    defineBuiltInAccessor(URLPrototype, 'port', accessorDescriptor('getPort', 'setPort'));
    // `URL.prototype.pathname` accessors pair
    // https://url.spec.whatwg.org/#dom-url-pathname
    defineBuiltInAccessor(URLPrototype, 'pathname', accessorDescriptor('getPathname', 'setPathname'));
    // `URL.prototype.search` accessors pair
    // https://url.spec.whatwg.org/#dom-url-search
    defineBuiltInAccessor(URLPrototype, 'search', accessorDescriptor('getSearch', 'setSearch'));
    // `URL.prototype.searchParams` getter
    // https://url.spec.whatwg.org/#dom-url-searchparams
    defineBuiltInAccessor(URLPrototype, 'searchParams', accessorDescriptor('getSearchParams'));
    // `URL.prototype.hash` accessors pair
    // https://url.spec.whatwg.org/#dom-url-hash
    defineBuiltInAccessor(URLPrototype, 'hash', accessorDescriptor('getHash', 'setHash'));
  }

  // `URL.prototype.toJSON` method
  // https://url.spec.whatwg.org/#dom-url-tojson
  defineBuiltIn(URLPrototype, 'toJSON', function toJSON() {
    return getInternalURLState(this).serialize();
  }, { enumerable: true });

  // `URL.prototype.toString` method
  // https://url.spec.whatwg.org/#URL-stringification-behavior
  defineBuiltIn(URLPrototype, 'toString', function toString() {
    return getInternalURLState(this).serialize();
  }, { enumerable: true });

  if (NativeURL) {
    var nativeCreateObjectURL = NativeURL.createObjectURL;
    var nativeRevokeObjectURL = NativeURL.revokeObjectURL;
    // `URL.createObjectURL` method
    // https://developer.mozilla.org/en-US/docs/Web/API/URL/createObjectURL
    if (nativeCreateObjectURL) defineBuiltIn(URLConstructor, 'createObjectURL', functionBindContext(nativeCreateObjectURL, NativeURL));
    // `URL.revokeObjectURL` method
    // https://developer.mozilla.org/en-US/docs/Web/API/URL/revokeObjectURL
    if (nativeRevokeObjectURL) defineBuiltIn(URLConstructor, 'revokeObjectURL', functionBindContext(nativeRevokeObjectURL, NativeURL));
  }

  setToStringTag(URLConstructor, 'URL');

  _export({ global: true, constructor: true, forced: !urlConstructorDetection, sham: !descriptors }, {
    URL: URLConstructor
  });

  var ARRAY_BUFFER = 'ArrayBuffer';
  var ArrayBuffer$1 = arrayBuffer[ARRAY_BUFFER];
  var NativeArrayBuffer = global_1[ARRAY_BUFFER];

  // `ArrayBuffer` constructor
  // https://tc39.es/ecma262/#sec-arraybuffer-constructor
  _export({ global: true, constructor: true, forced: NativeArrayBuffer !== ArrayBuffer$1 }, {
    ArrayBuffer: ArrayBuffer$1
  });

  setSpecies(ARRAY_BUFFER);

  var getOwnPropertyNames$1 = objectGetOwnPropertyNamesExternal.f;

  // eslint-disable-next-line es/no-object-getownpropertynames -- required for testing
  var FAILS_ON_PRIMITIVES = fails(function () { return !Object.getOwnPropertyNames(1); });

  // `Object.getOwnPropertyNames` method
  // https://tc39.es/ecma262/#sec-object.getownpropertynames
  _export({ target: 'Object', stat: true, forced: FAILS_ON_PRIMITIVES }, {
    getOwnPropertyNames: getOwnPropertyNames$1
  });

  // `Uint8Array` constructor
  // https://tc39.es/ecma262/#sec-typedarray-objects
  typedArrayConstructor('Uint8', function (init) {
    return function Uint8Array(data, byteOffset, length) {
      return init(this, data, byteOffset, length);
    };
  });

  var floor = Math.floor;
  var charAt = functionUncurryThis(''.charAt);
  var replace = functionUncurryThis(''.replace);
  var stringSlice$2 = functionUncurryThis(''.slice);
  // eslint-disable-next-line redos/no-vulnerable -- safe
  var SUBSTITUTION_SYMBOLS = /\$([$&'`]|\d{1,2}|<[^>]*>)/g;
  var SUBSTITUTION_SYMBOLS_NO_NAMED = /\$([$&'`]|\d{1,2})/g;

  // `GetSubstitution` abstract operation
  // https://tc39.es/ecma262/#sec-getsubstitution
  var getSubstitution = function (matched, str, position, captures, namedCaptures, replacement) {
    var tailPos = position + matched.length;
    var m = captures.length;
    var symbols = SUBSTITUTION_SYMBOLS_NO_NAMED;
    if (namedCaptures !== undefined) {
      namedCaptures = toObject(namedCaptures);
      symbols = SUBSTITUTION_SYMBOLS;
    }
    return replace(replacement, symbols, function (match, ch) {
      var capture;
      switch (charAt(ch, 0)) {
        case '$': return '$';
        case '&': return matched;
        case '`': return stringSlice$2(str, 0, position);
        case "'": return stringSlice$2(str, tailPos);
        case '<':
          capture = namedCaptures[stringSlice$2(ch, 1, -1)];
          break;
        default: // \d\d?
          var n = +ch;
          if (n === 0) return match;
          if (n > m) {
            var f = floor(n / 10);
            if (f === 0) return match;
            if (f <= m) return captures[f - 1] === undefined ? charAt(ch, 1) : captures[f - 1] + charAt(ch, 1);
            return match;
          }
          capture = captures[n - 1];
      }
      return capture === undefined ? '' : capture;
    });
  };

  var REPLACE = wellKnownSymbol('replace');
  var max = Math.max;
  var min = Math.min;
  var concat = functionUncurryThis([].concat);
  var push = functionUncurryThis([].push);
  var stringIndexOf = functionUncurryThis(''.indexOf);
  var stringSlice$1 = functionUncurryThis(''.slice);

  var maybeToString = function (it) {
    return it === undefined ? it : String(it);
  };

  // IE <= 11 replaces $0 with the whole match, as if it was $&
  // https://stackoverflow.com/questions/6024666/getting-ie-to-replace-a-regex-with-the-literal-string-0
  var REPLACE_KEEPS_$0 = (function () {
    // eslint-disable-next-line regexp/prefer-escape-replacement-dollar-char -- required for testing
    return 'a'.replace(/./, '$0') === '$0';
  })();

  // Safari <= 13.0.3(?) substitutes nth capture where n>m with an empty string
  var REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE = (function () {
    if (/./[REPLACE]) {
      return /./[REPLACE]('a', '$0') === '';
    }
    return false;
  })();

  var REPLACE_SUPPORTS_NAMED_GROUPS = !fails(function () {
    var re = /./;
    re.exec = function () {
      var result = [];
      result.groups = { a: '7' };
      return result;
    };
    // eslint-disable-next-line regexp/no-useless-dollar-replacements -- false positive
    return ''.replace(re, '$<a>') !== '7';
  });

  // @@replace logic
  fixRegexpWellKnownSymbolLogic('replace', function (_, nativeReplace, maybeCallNative) {
    var UNSAFE_SUBSTITUTE = REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE ? '$' : '$0';

    return [
      // `String.prototype.replace` method
      // https://tc39.es/ecma262/#sec-string.prototype.replace
      function replace(searchValue, replaceValue) {
        var O = requireObjectCoercible(this);
        var replacer = isNullOrUndefined(searchValue) ? undefined : getMethod(searchValue, REPLACE);
        return replacer
          ? functionCall(replacer, searchValue, O, replaceValue)
          : functionCall(nativeReplace, toString_1(O), searchValue, replaceValue);
      },
      // `RegExp.prototype[@@replace]` method
      // https://tc39.es/ecma262/#sec-regexp.prototype-@@replace
      function (string, replaceValue) {
        var rx = anObject(this);
        var S = toString_1(string);

        if (
          typeof replaceValue == 'string' &&
          stringIndexOf(replaceValue, UNSAFE_SUBSTITUTE) === -1 &&
          stringIndexOf(replaceValue, '$<') === -1
        ) {
          var res = maybeCallNative(nativeReplace, rx, S, replaceValue);
          if (res.done) return res.value;
        }

        var functionalReplace = isCallable(replaceValue);
        if (!functionalReplace) replaceValue = toString_1(replaceValue);

        var global = rx.global;
        if (global) {
          var fullUnicode = rx.unicode;
          rx.lastIndex = 0;
        }
        var results = [];
        while (true) {
          var result = regexpExecAbstract(rx, S);
          if (result === null) break;

          push(results, result);
          if (!global) break;

          var matchStr = toString_1(result[0]);
          if (matchStr === '') rx.lastIndex = advanceStringIndex(S, toLength(rx.lastIndex), fullUnicode);
        }

        var accumulatedResult = '';
        var nextSourcePosition = 0;
        for (var i = 0; i < results.length; i++) {
          result = results[i];

          var matched = toString_1(result[0]);
          var position = max(min(toIntegerOrInfinity(result.index), S.length), 0);
          var captures = [];
          // NOTE: This is equivalent to
          //   captures = result.slice(1).map(maybeToString)
          // but for some reason `nativeSlice.call(result, 1, result.length)` (called in
          // the slice polyfill when slicing native arrays) "doesn't work" in safari 9 and
          // causes a crash (https://pastebin.com/N21QzeQA) when trying to debug it.
          for (var j = 1; j < result.length; j++) push(captures, maybeToString(result[j]));
          var namedCaptures = result.groups;
          if (functionalReplace) {
            var replacerArgs = concat([matched], captures, position, S);
            if (namedCaptures !== undefined) push(replacerArgs, namedCaptures);
            var replacement = toString_1(functionApply(replaceValue, undefined, replacerArgs));
          } else {
            replacement = getSubstitution(matched, S, position, captures, namedCaptures, replaceValue);
          }
          if (position >= nextSourcePosition) {
            accumulatedResult += stringSlice$1(S, nextSourcePosition, position) + replacement;
            nextSourcePosition = position + matched.length;
          }
        }
        return accumulatedResult + stringSlice$1(S, nextSourcePosition);
      }
    ];
  }, !REPLACE_SUPPORTS_NAMED_GROUPS || !REPLACE_KEEPS_$0 || REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE);

  /* eslint-disable no-prototype-builtins */
  var g = typeof globalThis !== 'undefined' && globalThis || typeof self !== 'undefined' && self ||
  // eslint-disable-next-line no-undef
  typeof global !== 'undefined' && global || {};
  var support = {
    searchParams: 'URLSearchParams' in g,
    iterable: 'Symbol' in g && 'iterator' in Symbol,
    blob: 'FileReader' in g && 'Blob' in g && function () {
      try {
        new Blob();
        return true;
      } catch (e) {
        return false;
      }
    }(),
    formData: 'FormData' in g,
    arrayBuffer: 'ArrayBuffer' in g
  };
  function isDataView(obj) {
    return obj && DataView.prototype.isPrototypeOf(obj);
  }
  if (support.arrayBuffer) {
    var viewClasses = ['[object Int8Array]', '[object Uint8Array]', '[object Uint8ClampedArray]', '[object Int16Array]', '[object Uint16Array]', '[object Int32Array]', '[object Uint32Array]', '[object Float32Array]', '[object Float64Array]'];
    var isArrayBufferView = ArrayBuffer.isView || function (obj) {
      return obj && viewClasses.indexOf(Object.prototype.toString.call(obj)) > -1;
    };
  }
  function normalizeName(name) {
    if (typeof name !== 'string') {
      name = String(name);
    }
    if (/[^a-z0-9\-#$%&'*+.^_`|~!]/i.test(name) || name === '') {
      throw new TypeError('Invalid character in header field name: "' + name + '"');
    }
    return name.toLowerCase();
  }
  function normalizeValue(value) {
    if (typeof value !== 'string') {
      value = String(value);
    }
    return value;
  }

  // Build a destructive iterator for the value list
  function iteratorFor(items) {
    var iterator = {
      next: function next() {
        var value = items.shift();
        return {
          done: value === undefined,
          value: value
        };
      }
    };
    if (support.iterable) {
      iterator[Symbol.iterator] = function () {
        return iterator;
      };
    }
    return iterator;
  }
  function Headers(headers) {
    this.map = {};
    if (headers instanceof Headers) {
      headers.forEach(function (value, name) {
        this.append(name, value);
      }, this);
    } else if (Array.isArray(headers)) {
      headers.forEach(function (header) {
        if (header.length != 2) {
          throw new TypeError('Headers constructor: expected name/value pair to be length 2, found' + header.length);
        }
        this.append(header[0], header[1]);
      }, this);
    } else if (headers) {
      Object.getOwnPropertyNames(headers).forEach(function (name) {
        this.append(name, headers[name]);
      }, this);
    }
  }
  Headers.prototype.append = function (name, value) {
    name = normalizeName(name);
    value = normalizeValue(value);
    var oldValue = this.map[name];
    this.map[name] = oldValue ? oldValue + ', ' + value : value;
  };
  Headers.prototype['delete'] = function (name) {
    delete this.map[normalizeName(name)];
  };
  Headers.prototype.get = function (name) {
    name = normalizeName(name);
    return this.has(name) ? this.map[name] : null;
  };
  Headers.prototype.has = function (name) {
    return this.map.hasOwnProperty(normalizeName(name));
  };
  Headers.prototype.set = function (name, value) {
    this.map[normalizeName(name)] = normalizeValue(value);
  };
  Headers.prototype.forEach = function (callback, thisArg) {
    for (var name in this.map) {
      if (this.map.hasOwnProperty(name)) {
        callback.call(thisArg, this.map[name], name, this);
      }
    }
  };
  Headers.prototype.keys = function () {
    var items = [];
    this.forEach(function (value, name) {
      items.push(name);
    });
    return iteratorFor(items);
  };
  Headers.prototype.values = function () {
    var items = [];
    this.forEach(function (value) {
      items.push(value);
    });
    return iteratorFor(items);
  };
  Headers.prototype.entries = function () {
    var items = [];
    this.forEach(function (value, name) {
      items.push([name, value]);
    });
    return iteratorFor(items);
  };
  if (support.iterable) {
    Headers.prototype[Symbol.iterator] = Headers.prototype.entries;
  }
  function consumed(body) {
    if (body._noBody) return;
    if (body.bodyUsed) {
      return Promise.reject(new TypeError('Already read'));
    }
    body.bodyUsed = true;
  }
  function fileReaderReady(reader) {
    return new Promise(function (resolve, reject) {
      reader.onload = function () {
        resolve(reader.result);
      };
      reader.onerror = function () {
        reject(reader.error);
      };
    });
  }
  function readBlobAsArrayBuffer(blob) {
    var reader = new FileReader();
    var promise = fileReaderReady(reader);
    reader.readAsArrayBuffer(blob);
    return promise;
  }
  function readBlobAsText(blob) {
    var reader = new FileReader();
    var promise = fileReaderReady(reader);
    var match = /charset=([A-Za-z0-9_-]+)/.exec(blob.type);
    var encoding = match ? match[1] : 'utf-8';
    reader.readAsText(blob, encoding);
    return promise;
  }
  function readArrayBufferAsText(buf) {
    var view = new Uint8Array(buf);
    var chars = new Array(view.length);
    for (var i = 0; i < view.length; i++) {
      chars[i] = String.fromCharCode(view[i]);
    }
    return chars.join('');
  }
  function bufferClone(buf) {
    if (buf.slice) {
      return buf.slice(0);
    } else {
      var view = new Uint8Array(buf.byteLength);
      view.set(new Uint8Array(buf));
      return view.buffer;
    }
  }
  function Body() {
    this.bodyUsed = false;
    this._initBody = function (body) {
      /*
        fetch-mock wraps the Response object in an ES6 Proxy to
        provide useful test harness features such as flush. However, on
        ES5 browsers without fetch or Proxy support pollyfills must be used;
        the proxy-pollyfill is unable to proxy an attribute unless it exists
        on the object before the Proxy is created. This change ensures
        Response.bodyUsed exists on the instance, while maintaining the
        semantic of setting Request.bodyUsed in the constructor before
        _initBody is called.
      */
      // eslint-disable-next-line no-self-assign
      this.bodyUsed = this.bodyUsed;
      this._bodyInit = body;
      if (!body) {
        this._noBody = true;
        this._bodyText = '';
      } else if (typeof body === 'string') {
        this._bodyText = body;
      } else if (support.blob && Blob.prototype.isPrototypeOf(body)) {
        this._bodyBlob = body;
      } else if (support.formData && FormData.prototype.isPrototypeOf(body)) {
        this._bodyFormData = body;
      } else if (support.searchParams && URLSearchParams.prototype.isPrototypeOf(body)) {
        this._bodyText = body.toString();
      } else if (support.arrayBuffer && support.blob && isDataView(body)) {
        this._bodyArrayBuffer = bufferClone(body.buffer);
        // IE 10-11 can't handle a DataView body.
        this._bodyInit = new Blob([this._bodyArrayBuffer]);
      } else if (support.arrayBuffer && (ArrayBuffer.prototype.isPrototypeOf(body) || isArrayBufferView(body))) {
        this._bodyArrayBuffer = bufferClone(body);
      } else {
        this._bodyText = body = Object.prototype.toString.call(body);
      }
      if (!this.headers.get('content-type')) {
        if (typeof body === 'string') {
          this.headers.set('content-type', 'text/plain;charset=UTF-8');
        } else if (this._bodyBlob && this._bodyBlob.type) {
          this.headers.set('content-type', this._bodyBlob.type);
        } else if (support.searchParams && URLSearchParams.prototype.isPrototypeOf(body)) {
          this.headers.set('content-type', 'application/x-www-form-urlencoded;charset=UTF-8');
        }
      }
    };
    if (support.blob) {
      this.blob = function () {
        var rejected = consumed(this);
        if (rejected) {
          return rejected;
        }
        if (this._bodyBlob) {
          return Promise.resolve(this._bodyBlob);
        } else if (this._bodyArrayBuffer) {
          return Promise.resolve(new Blob([this._bodyArrayBuffer]));
        } else if (this._bodyFormData) {
          throw new Error('could not read FormData body as blob');
        } else {
          return Promise.resolve(new Blob([this._bodyText]));
        }
      };
    }
    this.arrayBuffer = function () {
      if (this._bodyArrayBuffer) {
        var isConsumed = consumed(this);
        if (isConsumed) {
          return isConsumed;
        } else if (ArrayBuffer.isView(this._bodyArrayBuffer)) {
          return Promise.resolve(this._bodyArrayBuffer.buffer.slice(this._bodyArrayBuffer.byteOffset, this._bodyArrayBuffer.byteOffset + this._bodyArrayBuffer.byteLength));
        } else {
          return Promise.resolve(this._bodyArrayBuffer);
        }
      } else if (support.blob) {
        return this.blob().then(readBlobAsArrayBuffer);
      } else {
        throw new Error('could not read as ArrayBuffer');
      }
    };
    this.text = function () {
      var rejected = consumed(this);
      if (rejected) {
        return rejected;
      }
      if (this._bodyBlob) {
        return readBlobAsText(this._bodyBlob);
      } else if (this._bodyArrayBuffer) {
        return Promise.resolve(readArrayBufferAsText(this._bodyArrayBuffer));
      } else if (this._bodyFormData) {
        throw new Error('could not read FormData body as text');
      } else {
        return Promise.resolve(this._bodyText);
      }
    };
    if (support.formData) {
      this.formData = function () {
        return this.text().then(decode);
      };
    }
    this.json = function () {
      return this.text().then(JSON.parse);
    };
    return this;
  }

  // HTTP methods whose capitalization should be normalized
  var methods = ['CONNECT', 'DELETE', 'GET', 'HEAD', 'OPTIONS', 'PATCH', 'POST', 'PUT', 'TRACE'];
  function normalizeMethod(method) {
    var upcased = method.toUpperCase();
    return methods.indexOf(upcased) > -1 ? upcased : method;
  }
  function Request(input, options) {
    if (!(this instanceof Request)) {
      throw new TypeError('Please use the "new" operator, this DOM object constructor cannot be called as a function.');
    }
    options = options || {};
    var body = options.body;
    if (input instanceof Request) {
      if (input.bodyUsed) {
        throw new TypeError('Already read');
      }
      this.url = input.url;
      this.credentials = input.credentials;
      if (!options.headers) {
        this.headers = new Headers(input.headers);
      }
      this.method = input.method;
      this.mode = input.mode;
      this.signal = input.signal;
      if (!body && input._bodyInit != null) {
        body = input._bodyInit;
        input.bodyUsed = true;
      }
    } else {
      this.url = String(input);
    }
    this.credentials = options.credentials || this.credentials || 'same-origin';
    if (options.headers || !this.headers) {
      this.headers = new Headers(options.headers);
    }
    this.method = normalizeMethod(options.method || this.method || 'GET');
    this.mode = options.mode || this.mode || null;
    this.signal = options.signal || this.signal || function () {
      if ('AbortController' in g) {
        var ctrl = new AbortController();
        return ctrl.signal;
      }
    }();
    this.referrer = null;
    if ((this.method === 'GET' || this.method === 'HEAD') && body) {
      throw new TypeError('Body not allowed for GET or HEAD requests');
    }
    this._initBody(body);
    if (this.method === 'GET' || this.method === 'HEAD') {
      if (options.cache === 'no-store' || options.cache === 'no-cache') {
        // Search for a '_' parameter in the query string
        var reParamSearch = /([?&])_=[^&]*/;
        if (reParamSearch.test(this.url)) {
          // If it already exists then set the value with the current time
          this.url = this.url.replace(reParamSearch, '$1_=' + new Date().getTime());
        } else {
          // Otherwise add a new '_' parameter to the end with the current time
          var reQueryString = /\?/;
          this.url += (reQueryString.test(this.url) ? '&' : '?') + '_=' + new Date().getTime();
        }
      }
    }
  }
  Request.prototype.clone = function () {
    return new Request(this, {
      body: this._bodyInit
    });
  };
  function decode(body) {
    var form = new FormData();
    body.trim().split('&').forEach(function (bytes) {
      if (bytes) {
        var split = bytes.split('=');
        var name = split.shift().replace(/\+/g, ' ');
        var value = split.join('=').replace(/\+/g, ' ');
        form.append(decodeURIComponent(name), decodeURIComponent(value));
      }
    });
    return form;
  }
  function parseHeaders(rawHeaders) {
    var headers = new Headers();
    // Replace instances of \r\n and \n followed by at least one space or horizontal tab with a space
    // https://tools.ietf.org/html/rfc7230#section-3.2
    var preProcessedHeaders = rawHeaders.replace(/\r?\n[\t ]+/g, ' ');
    // Avoiding split via regex to work around a common IE11 bug with the core-js 3.6.0 regex polyfill
    // https://github.com/github/fetch/issues/748
    // https://github.com/zloirock/core-js/issues/751
    preProcessedHeaders.split('\r').map(function (header) {
      return header.indexOf('\n') === 0 ? header.substr(1, header.length) : header;
    }).forEach(function (line) {
      var parts = line.split(':');
      var key = parts.shift().trim();
      if (key) {
        var value = parts.join(':').trim();
        try {
          headers.append(key, value);
        } catch (error) {
          console.warn('Response ' + error.message);
        }
      }
    });
    return headers;
  }
  Body.call(Request.prototype);
  function Response(bodyInit, options) {
    if (!(this instanceof Response)) {
      throw new TypeError('Please use the "new" operator, this DOM object constructor cannot be called as a function.');
    }
    if (!options) {
      options = {};
    }
    this.type = 'default';
    this.status = options.status === undefined ? 200 : options.status;
    if (this.status < 200 || this.status > 599) {
      throw new RangeError("Failed to construct 'Response': The status provided (0) is outside the range [200, 599].");
    }
    this.ok = this.status >= 200 && this.status < 300;
    this.statusText = options.statusText === undefined ? '' : '' + options.statusText;
    this.headers = new Headers(options.headers);
    this.url = options.url || '';
    this._initBody(bodyInit);
  }
  Body.call(Response.prototype);
  Response.prototype.clone = function () {
    return new Response(this._bodyInit, {
      status: this.status,
      statusText: this.statusText,
      headers: new Headers(this.headers),
      url: this.url
    });
  };
  Response.error = function () {
    var response = new Response(null, {
      status: 200,
      statusText: ''
    });
    response.status = 0;
    response.type = 'error';
    return response;
  };
  var redirectStatuses = [301, 302, 303, 307, 308];
  Response.redirect = function (url, status) {
    if (redirectStatuses.indexOf(status) === -1) {
      throw new RangeError('Invalid status code');
    }
    return new Response(null, {
      status: status,
      headers: {
        location: url
      }
    });
  };
  var DOMException = g.DOMException;
  try {
    new DOMException();
  } catch (err) {
    DOMException = function DOMException(message, name) {
      this.message = message;
      this.name = name;
      var error = Error(message);
      this.stack = error.stack;
    };
    DOMException.prototype = Object.create(Error.prototype);
    DOMException.prototype.constructor = DOMException;
  }
  function fetch$1(input, init) {
    return new Promise(function (resolve, reject) {
      var request = new Request(input, init);
      if (request.signal && request.signal.aborted) {
        return reject(new DOMException('Aborted', 'AbortError'));
      }
      var xhr = new XMLHttpRequest();
      function abortXhr() {
        xhr.abort();
      }
      xhr.onload = function () {
        var options = {
          status: xhr.status,
          statusText: xhr.statusText,
          headers: parseHeaders(xhr.getAllResponseHeaders() || '')
        };
        options.url = 'responseURL' in xhr ? xhr.responseURL : options.headers.get('X-Request-URL');
        var body = 'response' in xhr ? xhr.response : xhr.responseText;
        setTimeout(function () {
          resolve(new Response(body, options));
        }, 0);
      };
      xhr.onerror = function () {
        setTimeout(function () {
          reject(new TypeError('Network request failed'));
        }, 0);
      };
      xhr.ontimeout = function () {
        setTimeout(function () {
          reject(new TypeError('Network request failed'));
        }, 0);
      };
      xhr.onabort = function () {
        setTimeout(function () {
          reject(new DOMException('Aborted', 'AbortError'));
        }, 0);
      };
      function fixUrl(url) {
        try {
          return url === '' && g.location.href ? g.location.href : url;
        } catch (e) {
          return url;
        }
      }
      xhr.open(request.method, fixUrl(request.url), true);
      if (request.credentials === 'include') {
        xhr.withCredentials = true;
      } else if (request.credentials === 'omit') {
        xhr.withCredentials = false;
      }
      if ('responseType' in xhr) {
        if (support.blob) {
          xhr.responseType = 'blob';
        } else if (support.arrayBuffer) {
          xhr.responseType = 'arraybuffer';
        }
      }
      if (init && _typeof(init.headers) === 'object' && !(init.headers instanceof Headers || g.Headers && init.headers instanceof g.Headers)) {
        var names = [];
        Object.getOwnPropertyNames(init.headers).forEach(function (name) {
          names.push(normalizeName(name));
          xhr.setRequestHeader(name, normalizeValue(init.headers[name]));
        });
        request.headers.forEach(function (value, name) {
          if (names.indexOf(name) === -1) {
            xhr.setRequestHeader(name, value);
          }
        });
      } else {
        request.headers.forEach(function (value, name) {
          xhr.setRequestHeader(name, value);
        });
      }
      if (request.signal) {
        request.signal.addEventListener('abort', abortXhr);
        xhr.onreadystatechange = function () {
          // DONE (success or failure)
          if (xhr.readyState === 4) {
            request.signal.removeEventListener('abort', abortXhr);
          }
        };
      }
      xhr.send(typeof request._bodyInit === 'undefined' ? null : request._bodyInit);
    });
  }
  fetch$1.polyfill = true;
  if (!g.fetch) {
    g.fetch = fetch$1;
    g.Headers = Headers;
    g.Request = Request;
    g.Response = Response;
  }

  function _createSuper$i(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$i(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$i() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var Object_1$1 = globals.Object;
  var file$i = "src/Audio.svelte";
  function create_fragment$i(ctx) {
    var audio;
    var source;
    var source_src_value;
    var block = {
      c: function create() {
        audio = element("audio");
        source = element("source");
        if (!src_url_equal(source.src, source_src_value = "".concat(BASE_URL, "/assets/GAMBLER Narrator 201226 to Player 01_1.mp3"))) attr_dev(source, "src", source_src_value);
        attr_dev(source, "type", "audio/mpeg");
        add_location(source, file$i, 77, 2, 2312);
        attr_dev(audio, "preload", "");
        add_location(audio, file$i, 76, 0, 2269);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, audio, anchor);
        append_dev(audio, source);
        /*audio_binding*/
        ctx[0](audio);
      },
      p: noop,
      i: noop,
      o: noop,
      d: function destroy(detaching) {
        if (detaching) detach_dev(audio);
        /*audio_binding*/
        ctx[0](null);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$i.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  var audioFiles = {
    vk: ["GAMBLER Narrator 201226 to Player 01_1"],
    promo1: ["GAMBLER Narrator 201226 to Player 02_1"],
    promo2: ["GAMBLER Narrator 201226 to Player 03_1"],
    gameWon: ["GAMBLER Narrator 201226 to Player 04_1"]
  };
  var audioBlobs = {};
  (function () {
    Object.keys(audioFiles).forEach(function (type) {
      audioFiles[type].forEach(function (item, i) {
        fetch("".concat(BASE_URL, "/assets/").concat(item, ".mp3")).then(function (res) {
          return res.blob();
        }).then(function (blob) {
          return window.URL.createObjectURL(blob);
        }).then(function (blob) {
          if (!audioBlobs[type]) audioBlobs[type] = [];
          audioBlobs[type].push(blob);
        });
      });
    });
  })();
  var audioElement;
  var playSound = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regenerator.mark(function _callee(type) {
      var callback,
        onEnd,
        auidoItem,
        _args = arguments;
      return regenerator.wrap(function _callee$(_context) {
        while (1) switch (_context.prev = _context.next) {
          case 0:
            callback = _args.length > 1 && _args[1] !== undefined ? _args[1] : function () {};
            onEnd = function onEnd() {
              audioElement.removeEventListener("ended", onEnd);
              if (typeof callback === "function") callback();
            };
            audioElement.addEventListener("ended", onEnd);
            try {
              audioElement.muted = true;
              audioElement.play();
              audioElement.pause();
              audioElement.muted = false;
              audioElement.currentTime = 0;
            } catch (err) {} // nothing to do
            auidoItem = audioBlobs[type] ? audioBlobs[type][Math.floor(Math.random() * audioBlobs[type].length)] : null;
            if (auidoItem) {
              _context.next = 7;
              break;
            }
            return _context.abrupt("return");
          case 7:
            audioElement.src = auidoItem;
            audioElement.play();
          case 9:
          case "end":
            return _context.stop();
        }
      }, _callee);
    }));
    return function playSound(_x) {
      return _ref.apply(this, arguments);
    };
  }();
  var stopSound = function stopSound() {
    audioElement.pause();
  };
  function instance$i($$self, $$props, $$invalidate) {
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('Audio', slots, []);
    var isAudioUnlocked = false;
    var unlockAudio = function unlockAudio() {
      if (isAudioUnlocked) return;
      playSound(null);
      isAudioUnlocked = true;
      document.body.removeEventListener("click", unlockAudio);
      document.body.removeEventListener("touchstart", unlockAudio);
    };
    onMount(function () {
      document.body.addEventListener("click", unlockAudio, true);
      document.body.addEventListener("touchstart", unlockAudio, true);
      return function () {
        document.body.removeEventListener("click", unlockAudio);
        document.body.removeEventListener("touchstart", unlockAudio);
      };
    });
    var writable_props = [];
    Object_1$1.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<Audio> was created with unknown prop '".concat(key, "'"));
    });
    function audio_binding($$value) {
      binding_callbacks[$$value ? 'unshift' : 'push'](function () {
        audioElement = $$value;
      });
    }
    $$self.$capture_state = function () {
      return {
        audioFiles: audioFiles,
        audioBlobs: audioBlobs,
        audioElement: audioElement,
        playSound: playSound,
        stopSound: stopSound,
        onMount: onMount,
        BASE_URL: BASE_URL,
        isAudioUnlocked: isAudioUnlocked,
        unlockAudio: unlockAudio
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('isAudioUnlocked' in $$props) isAudioUnlocked = $$props.isAudioUnlocked;
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    return [audio_binding];
  }
  var Audio = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(Audio, _SvelteComponentDev);
    var _super = _createSuper$i(Audio);
    function Audio(options) {
      var _this;
      _classCallCheck(this, Audio);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$i, create_fragment$i, safe_not_equal, {});
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "Audio",
        options: options,
        id: create_fragment$i.name
      });
      return _this;
    }
    return _createClass(Audio);
  }(SvelteComponentDev);

  var promoData = [{
    src: "/images/promo/eldorado.png",
    href: "https://ad.admitad.com/coupon/pf9klv6my56a099e8870824224cf5a/",
    showInIframe: true
  }, {
    src: "/images/promo/market.png",
    href: "https://ad.admitad.com/g/27u6ol7uc26a099e8870b62ed2b196/?i=3",
    showInIframe: false
  }, {
    src: "/images/promo/aliexpress.png",
    href: "https://alitems.com/coupon/94wrvygpn76a099e88707a660ebfae/",
    showInIframe: true
  }, {
    src: "/images/promo/geekbrains.png",
    href: "https://ad.admitad.com/g/3cgo37g1gg6a099e887065a37ca03d/?i=3",
    showInIframe: false
  }, {
    src: "/images/promo/dominos.png",
    href: "https://ad.admitad.com/coupon/ok3jg5c2o76a099e887035e8fc368c/",
    showInIframe: true
  }];

  function _createSuper$h(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$h(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$h() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$h = "src/Interactive/Promo.svelte";
  function get_each_context$4(ctx, list, i) {
    var child_ctx = ctx.slice();
    child_ctx[14] = list[i];
    child_ctx[16] = i;
    return child_ctx;
  }

  // (74:4) {#each promoData as item, i (item.src)}
  function create_each_block$4(key_1, ctx) {
    var a;
    var img;
    var img_src_value;
    var t;
    var mounted;
    var dispose;
    function click_handler() {
      var _ctx;
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }
      return (/*click_handler*/(_ctx = ctx)[8].apply(_ctx, [/*item*/ctx[14]].concat(args))
      );
    }
    var block = {
      key: key_1,
      first: null,
      c: function create() {
        a = element("a");
        img = element("img");
        t = space();
        if (!src_url_equal(img.src, img_src_value = "".concat(BASE_URL).concat( /*item*/ctx[14].src))) attr_dev(img, "src", img_src_value);
        attr_dev(img, "alt", "logo");
        attr_dev(img, "class", "promo svelte-w572jg");
        toggle_class(img, "invisible", /*selectedPromo*/ctx[1] && /*selectedPromo*/ctx[1] !== /*item*/ctx[14].href);
        add_location(img, file$h, 78, 8, 2291);
        attr_dev(a, "href", /*item*/ctx[14].href);
        attr_dev(a, "target", "_blank");
        attr_dev(a, "class", "svelte-w572jg");
        add_location(a, file$h, 74, 6, 2187);
        this.first = a;
      },
      m: function mount(target, anchor) {
        insert_dev(target, a, anchor);
        append_dev(a, img);
        append_dev(a, t);
        if (!mounted) {
          dispose = listen_dev(a, "click", click_handler, false, false, false, false);
          mounted = true;
        }
      },
      p: function update(new_ctx, dirty) {
        ctx = new_ctx;
        if (dirty & /*selectedPromo, promoData*/2) {
          toggle_class(img, "invisible", /*selectedPromo*/ctx[1] && /*selectedPromo*/ctx[1] !== /*item*/ctx[14].href);
        }
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(a);
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_each_block$4.name,
      type: "each",
      source: "(74:4) {#each promoData as item, i (item.src)}",
      ctx: ctx
    });
    return block;
  }

  // (87:2) {#if !isTimerVisible}
  function create_if_block_1$3(ctx) {
    var img;
    var img_src_value;
    var mounted;
    var dispose;
    var block = {
      c: function create() {
        img = element("img");
        if (!src_url_equal(img.src, img_src_value = "".concat(BASE_URL, "/images/watch.png"))) attr_dev(img, "src", img_src_value);
        attr_dev(img, "alt", "cards");
        attr_dev(img, "class", "button svelte-w572jg");
        add_location(img, file$h, 87, 4, 2518);
      },
      m: function mount(target, anchor) {
        insert_dev(target, img, anchor);
        if (!mounted) {
          dispose = listen_dev(img, "click", /*onSkip*/ctx[3], false, false, false, false);
          mounted = true;
        }
      },
      p: noop,
      d: function destroy(detaching) {
        if (detaching) detach_dev(img);
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block_1$3.name,
      type: "if",
      source: "(87:2) {#if !isTimerVisible}",
      ctx: ctx
    });
    return block;
  }

  // (94:2) {#if duration && isTimerVisible}
  function create_if_block$a(ctx) {
    var div;
    var timer;
    var t;
    var img;
    var img_src_value;
    var current;
    var mounted;
    var dispose;
    timer = new Timer({
      props: {
        duration: /*duration*/ctx[0]
      },
      $$inline: true
    });
    var block = {
      c: function create() {
        div = element("div");
        create_component(timer.$$.fragment);
        t = space();
        img = element("img");
        attr_dev(div, "class", "timer svelte-w572jg");
        add_location(div, file$h, 94, 4, 2679);
        if (!src_url_equal(img.src, img_src_value = "".concat(BASE_URL, "/images/continue.png"))) attr_dev(img, "src", img_src_value);
        attr_dev(img, "alt", "cards");
        attr_dev(img, "class", "continue appear svelte-w572jg");
        add_location(img, file$h, 97, 4, 2741);
      },
      m: function mount(target, anchor) {
        insert_dev(target, div, anchor);
        mount_component(timer, div, null);
        insert_dev(target, t, anchor);
        insert_dev(target, img, anchor);
        current = true;
        if (!mounted) {
          dispose = listen_dev(img, "click", /*onSkip*/ctx[3], false, false, false, false);
          mounted = true;
        }
      },
      p: function update(ctx, dirty) {
        var timer_changes = {};
        if (dirty & /*duration*/1) timer_changes.duration = /*duration*/ctx[0];
        timer.$set(timer_changes);
      },
      i: function intro(local) {
        if (current) return;
        transition_in(timer.$$.fragment, local);
        current = true;
      },
      o: function outro(local) {
        transition_out(timer.$$.fragment, local);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div);
        destroy_component(timer);
        if (detaching) detach_dev(t);
        if (detaching) detach_dev(img);
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block$a.name,
      type: "if",
      source: "(94:2) {#if duration && isTimerVisible}",
      ctx: ctx
    });
    return block;
  }
  function create_fragment$h(ctx) {
    var div1;
    var img0;
    var img0_src_value;
    var t0;
    var img1;
    var img1_src_value;
    var t1;
    var img2;
    var img2_src_value;
    var t2;
    var img3;
    var img3_src_value;
    var t3;
    var img4;
    var img4_src_value;
    var t4;
    var div0;
    var each_blocks = [];
    var each_1_lookup = new Map();
    var t5;
    var t6;
    var current;
    var each_value = promoData;
    validate_each_argument(each_value);
    var get_key = function get_key(ctx) {
      return (/*item*/ctx[14].src
      );
    };
    validate_each_keys(ctx, each_value, get_each_context$4, get_key);
    for (var i = 0; i < each_value.length; i += 1) {
      var child_ctx = get_each_context$4(ctx, each_value, i);
      var key = get_key(child_ctx);
      each_1_lookup.set(key, each_blocks[i] = create_each_block$4(key, child_ctx));
    }
    var if_block0 = ! /*isTimerVisible*/ctx[2] && create_if_block_1$3(ctx);
    var if_block1 = /*duration*/ctx[0] && /*isTimerVisible*/ctx[2] && create_if_block$a(ctx);
    var block = {
      c: function create() {
        div1 = element("div");
        img0 = element("img");
        t0 = space();
        img1 = element("img");
        t1 = space();
        img2 = element("img");
        t2 = space();
        img3 = element("img");
        t3 = space();
        img4 = element("img");
        t4 = space();
        div0 = element("div");
        for (var _i = 0; _i < each_blocks.length; _i += 1) {
          each_blocks[_i].c();
        }
        t5 = space();
        if (if_block0) if_block0.c();
        t6 = space();
        if (if_block1) if_block1.c();
        if (!src_url_equal(img0.src, img0_src_value = "".concat(BASE_URL, "/images/chip3.png"))) attr_dev(img0, "src", img0_src_value);
        attr_dev(img0, "alt", "chip1");
        attr_dev(img0, "class", "chip3 svelte-w572jg");
        add_location(img0, file$h, 67, 2, 1746);
        if (!src_url_equal(img1.src, img1_src_value = "".concat(BASE_URL, "/images/chip1.png"))) attr_dev(img1, "src", img1_src_value);
        attr_dev(img1, "alt", "chip1");
        attr_dev(img1, "class", "chip1 svelte-w572jg");
        add_location(img1, file$h, 68, 2, 1819);
        if (!src_url_equal(img2.src, img2_src_value = "".concat(BASE_URL, "/images/chip4.png"))) attr_dev(img2, "src", img2_src_value);
        attr_dev(img2, "alt", "chip1");
        attr_dev(img2, "class", "chip4 svelte-w572jg");
        add_location(img2, file$h, 69, 2, 1892);
        if (!src_url_equal(img3.src, img3_src_value = "".concat(BASE_URL, "/images/chip2.png"))) attr_dev(img3, "src", img3_src_value);
        attr_dev(img3, "alt", "chip1");
        attr_dev(img3, "class", "chip2 svelte-w572jg");
        add_location(img3, file$h, 70, 2, 1965);
        if (!src_url_equal(img4.src, img4_src_value = "".concat(BASE_URL, "/images/logo.png"))) attr_dev(img4, "src", img4_src_value);
        attr_dev(img4, "alt", "logo");
        attr_dev(img4, "class", "logo svelte-w572jg");
        add_location(img4, file$h, 71, 2, 2038);
        attr_dev(div0, "class", "promoContainer svelte-w572jg");
        add_location(div0, file$h, 72, 2, 2108);
        attr_dev(div1, "class", "container appear svelte-w572jg");
        add_location(div1, file$h, 66, 0, 1713);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, div1, anchor);
        append_dev(div1, img0);
        append_dev(div1, t0);
        append_dev(div1, img1);
        append_dev(div1, t1);
        append_dev(div1, img2);
        append_dev(div1, t2);
        append_dev(div1, img3);
        append_dev(div1, t3);
        append_dev(div1, img4);
        append_dev(div1, t4);
        append_dev(div1, div0);
        for (var _i2 = 0; _i2 < each_blocks.length; _i2 += 1) {
          if (each_blocks[_i2]) {
            each_blocks[_i2].m(div0, null);
          }
        }
        append_dev(div1, t5);
        if (if_block0) if_block0.m(div1, null);
        append_dev(div1, t6);
        if (if_block1) if_block1.m(div1, null);
        current = true;
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if (dirty & /*promoData, onClick, BASE_URL, selectedPromo*/18) {
          each_value = promoData;
          validate_each_argument(each_value);
          validate_each_keys(ctx, each_value, get_each_context$4, get_key);
          each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value, each_1_lookup, div0, destroy_block, create_each_block$4, null, get_each_context$4);
        }
        if (! /*isTimerVisible*/ctx[2]) {
          if (if_block0) {
            if_block0.p(ctx, dirty);
          } else {
            if_block0 = create_if_block_1$3(ctx);
            if_block0.c();
            if_block0.m(div1, t6);
          }
        } else if (if_block0) {
          if_block0.d(1);
          if_block0 = null;
        }
        if ( /*duration*/ctx[0] && /*isTimerVisible*/ctx[2]) {
          if (if_block1) {
            if_block1.p(ctx, dirty);
            if (dirty & /*duration, isTimerVisible*/5) {
              transition_in(if_block1, 1);
            }
          } else {
            if_block1 = create_if_block$a(ctx);
            if_block1.c();
            transition_in(if_block1, 1);
            if_block1.m(div1, null);
          }
        } else if (if_block1) {
          group_outros();
          transition_out(if_block1, 1, 1, function () {
            if_block1 = null;
          });
          check_outros();
        }
      },
      i: function intro(local) {
        if (current) return;
        transition_in(if_block1);
        current = true;
      },
      o: function outro(local) {
        transition_out(if_block1);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div1);
        for (var _i3 = 0; _i3 < each_blocks.length; _i3 += 1) {
          each_blocks[_i3].d();
        }
        if (if_block0) if_block0.d();
        if (if_block1) if_block1.d();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$h.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$h($$self, $$props, $$invalidate) {
    var $gameSkipped;
    var $gameClosed;
    var $gameWon;
    var $externalLink;
    var $iframeLink;
    validate_store(gameSkipped, 'gameSkipped');
    component_subscribe($$self, gameSkipped, function ($$value) {
      return $$invalidate(9, $gameSkipped = $$value);
    });
    validate_store(gameClosed, 'gameClosed');
    component_subscribe($$self, gameClosed, function ($$value) {
      return $$invalidate(10, $gameClosed = $$value);
    });
    validate_store(gameWon, 'gameWon');
    component_subscribe($$self, gameWon, function ($$value) {
      return $$invalidate(11, $gameWon = $$value);
    });
    validate_store(externalLink, 'externalLink');
    component_subscribe($$self, externalLink, function ($$value) {
      return $$invalidate(12, $externalLink = $$value);
    });
    validate_store(iframeLink, 'iframeLink');
    component_subscribe($$self, iframeLink, function ($$value) {
      return $$invalidate(13, $iframeLink = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('Promo', slots, []);
    var myPlayer = $$props.myPlayer;
    var duration = $$props.duration;
    var episode = $$props.episode;
    var selectedPromo;
    var isTimerVisible = $gameClosed && !$gameSkipped;
    var nextVideo = $$props.nextVideo;

    // sendMetrik("GameAdShow", { "Gambler.GameAdEpisode": episode });
    var onSkip = function onSkip() {
      if (!selectedPromo) {
        sendMetrik$1("PromoSkip");
      }
      stopSound();
      nextVideo();
    };
    var onClick = function onClick(event, item) {
      stopSound();
      $$invalidate(2, isTimerVisible = false);
      if (selectedPromo) {
        event.preventDefault();
        return;
      }
      if (item.showInIframe) {
        event.preventDefault();
        set_store_value(iframeLink, $iframeLink = item.href, $iframeLink);
      } else {
        set_store_value(externalLink, $externalLink = item.href, $externalLink);
      }
      sendMetrik$1('PromoClick', {
        'Gambler.PromoClickUrl': item.href
      });
      $$invalidate(1, selectedPromo = item.href);
      myPlayer.pause();
    };
    onMount(function () {
      if ($gameWon) {
        sendMetrik$1("PromoShow");
        playSound('gameWon');
      } else if (episode === '22') {
        if ($gameSkipped) {
          sendMetrik$1("PromoShow");
          playSound('promo2');
        } else if ($gameClosed && !$gameSkipped) {
          sendMetrik$1("PromoShow");
          myPlayer.muted(true);
          playSound('promo1');
        }
      }
      $$invalidate(5, myPlayer.controlBar.el_.style.display = "none", myPlayer);
      return function () {
        stopSound();
        $$invalidate(5, myPlayer.controlBar.el_.style.display = "", myPlayer);
      };
    });
    $$self.$$.on_mount.push(function () {
      if (myPlayer === undefined && !('myPlayer' in $$props || $$self.$$.bound[$$self.$$.props['myPlayer']])) {
        console.warn("<Promo> was created without expected prop 'myPlayer'");
      }
      if (duration === undefined && !('duration' in $$props || $$self.$$.bound[$$self.$$.props['duration']])) {
        console.warn("<Promo> was created without expected prop 'duration'");
      }
      if (episode === undefined && !('episode' in $$props || $$self.$$.bound[$$self.$$.props['episode']])) {
        console.warn("<Promo> was created without expected prop 'episode'");
      }
      if (nextVideo === undefined && !('nextVideo' in $$props || $$self.$$.bound[$$self.$$.props['nextVideo']])) {
        console.warn("<Promo> was created without expected prop 'nextVideo'");
      }
    });
    var writable_props = ['myPlayer', 'duration', 'episode', 'nextVideo'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<Promo> was created with unknown prop '".concat(key, "'"));
    });
    var click_handler = function click_handler(item, e) {
      return onClick(e, item);
    };
    $$self.$$set = function ($$props) {
      if ('myPlayer' in $$props) $$invalidate(5, myPlayer = $$props.myPlayer);
      if ('duration' in $$props) $$invalidate(0, duration = $$props.duration);
      if ('episode' in $$props) $$invalidate(6, episode = $$props.episode);
      if ('nextVideo' in $$props) $$invalidate(7, nextVideo = $$props.nextVideo);
    };
    $$self.$capture_state = function () {
      return {
        playSound: playSound,
        stopSound: stopSound,
        promoData: promoData,
        onMount: onMount,
        BASE_URL: BASE_URL,
        gameClosed: gameClosed,
        gameWon: gameWon,
        gameSkipped: gameSkipped,
        sendMetrik: sendMetrik$1,
        externalLink: externalLink,
        iframeLink: iframeLink,
        Timer: Timer,
        myPlayer: myPlayer,
        duration: duration,
        episode: episode,
        selectedPromo: selectedPromo,
        isTimerVisible: isTimerVisible,
        nextVideo: nextVideo,
        onSkip: onSkip,
        onClick: onClick,
        $gameSkipped: $gameSkipped,
        $gameClosed: $gameClosed,
        $gameWon: $gameWon,
        $externalLink: $externalLink,
        $iframeLink: $iframeLink
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('myPlayer' in $$props) $$invalidate(5, myPlayer = $$props.myPlayer);
      if ('duration' in $$props) $$invalidate(0, duration = $$props.duration);
      if ('episode' in $$props) $$invalidate(6, episode = $$props.episode);
      if ('selectedPromo' in $$props) $$invalidate(1, selectedPromo = $$props.selectedPromo);
      if ('isTimerVisible' in $$props) $$invalidate(2, isTimerVisible = $$props.isTimerVisible);
      if ('nextVideo' in $$props) $$invalidate(7, nextVideo = $$props.nextVideo);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    return [duration, selectedPromo, isTimerVisible, onSkip, onClick, myPlayer, episode, nextVideo, click_handler];
  }
  var Promo = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(Promo, _SvelteComponentDev);
    var _super = _createSuper$h(Promo);
    function Promo(options) {
      var _this;
      _classCallCheck(this, Promo);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$h, create_fragment$h, safe_not_equal, {
        myPlayer: 5,
        duration: 0,
        episode: 6,
        nextVideo: 7
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "Promo",
        options: options,
        id: create_fragment$h.name
      });
      return _this;
    }
    _createClass(Promo, [{
      key: "myPlayer",
      get: function get() {
        throw new Error("<Promo>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Promo>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "duration",
      get: function get() {
        throw new Error("<Promo>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Promo>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "episode",
      get: function get() {
        throw new Error("<Promo>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Promo>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "nextVideo",
      get: function get() {
        throw new Error("<Promo>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Promo>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return Promo;
  }(SvelteComponentDev);

  function _createSuper$g(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$g(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$g() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$g = "src/Interactive/Promo1.svelte";
  function get_each_context$3(ctx, list, i) {
    var child_ctx = ctx.slice();
    child_ctx[14] = list[i];
    child_ctx[16] = i;
    return child_ctx;
  }

  // (77:6) {#each promoData as item, i (item.src)}
  function create_each_block$3(key_1, ctx) {
    var a;
    var img;
    var img_src_value;
    var t;
    var mounted;
    var dispose;
    function click_handler() {
      var _ctx;
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }
      return (/*click_handler*/(_ctx = ctx)[9].apply(_ctx, [/*item*/ctx[14]].concat(args))
      );
    }
    var block = {
      key: key_1,
      first: null,
      c: function create() {
        a = element("a");
        img = element("img");
        t = space();
        if (!src_url_equal(img.src, img_src_value = "".concat(BASE_URL).concat( /*item*/ctx[14].src))) attr_dev(img, "src", img_src_value);
        attr_dev(img, "alt", "logo");
        attr_dev(img, "class", "promo svelte-852yzg");
        toggle_class(img, "invisible", /*selectedPromo*/ctx[1] && /*selectedPromo*/ctx[1] !== /*item*/ctx[14].href);
        add_location(img, file$g, 81, 10, 2429);
        attr_dev(a, "href", /*item*/ctx[14].href);
        attr_dev(a, "target", "_blank");
        attr_dev(a, "class", "svelte-852yzg");
        add_location(a, file$g, 77, 8, 2317);
        this.first = a;
      },
      m: function mount(target, anchor) {
        insert_dev(target, a, anchor);
        append_dev(a, img);
        append_dev(a, t);
        if (!mounted) {
          dispose = listen_dev(a, "click", click_handler, false, false, false, false);
          mounted = true;
        }
      },
      p: function update(new_ctx, dirty) {
        ctx = new_ctx;
        if (dirty & /*selectedPromo, promoData*/2) {
          toggle_class(img, "invisible", /*selectedPromo*/ctx[1] && /*selectedPromo*/ctx[1] !== /*item*/ctx[14].href);
        }
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(a);
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_each_block$3.name,
      type: "each",
      source: "(77:6) {#each promoData as item, i (item.src)}",
      ctx: ctx
    });
    return block;
  }

  // (90:4) {#if !isTimerVisible}
  function create_if_block_1$2(ctx) {
    var img;
    var img_src_value;
    var mounted;
    var dispose;
    var block = {
      c: function create() {
        img = element("img");
        if (!src_url_equal(img.src, img_src_value = "".concat(BASE_URL, "/images/watch.png"))) attr_dev(img, "src", img_src_value);
        attr_dev(img, "alt", "cards");
        attr_dev(img, "class", "button svelte-852yzg");
        add_location(img, file$g, 90, 6, 2674);
      },
      m: function mount(target, anchor) {
        insert_dev(target, img, anchor);
        if (!mounted) {
          dispose = listen_dev(img, "click", /*onSkip*/ctx[3], false, false, false, false);
          mounted = true;
        }
      },
      p: noop,
      d: function destroy(detaching) {
        if (detaching) detach_dev(img);
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block_1$2.name,
      type: "if",
      source: "(90:4) {#if !isTimerVisible}",
      ctx: ctx
    });
    return block;
  }

  // (97:4) {#if duration && isTimerVisible }
  function create_if_block$9(ctx) {
    var div;
    var timer;
    var t;
    var img;
    var img_src_value;
    var current;
    var mounted;
    var dispose;
    timer = new Timer({
      props: {
        duration: /*duration*/ctx[0]
      },
      $$inline: true
    });
    var block = {
      c: function create() {
        div = element("div");
        create_component(timer.$$.fragment);
        t = space();
        img = element("img");
        attr_dev(div, "class", "timer svelte-852yzg");
        add_location(div, file$g, 97, 6, 2850);
        if (!src_url_equal(img.src, img_src_value = "".concat(BASE_URL, "/images/continue.png"))) attr_dev(img, "src", img_src_value);
        attr_dev(img, "alt", "cards");
        attr_dev(img, "class", "continue appear svelte-852yzg");
        add_location(img, file$g, 100, 6, 2918);
      },
      m: function mount(target, anchor) {
        insert_dev(target, div, anchor);
        mount_component(timer, div, null);
        insert_dev(target, t, anchor);
        insert_dev(target, img, anchor);
        current = true;
        if (!mounted) {
          dispose = listen_dev(img, "click", /*onSkip*/ctx[3], false, false, false, false);
          mounted = true;
        }
      },
      p: function update(ctx, dirty) {
        var timer_changes = {};
        if (dirty & /*duration*/1) timer_changes.duration = /*duration*/ctx[0];
        timer.$set(timer_changes);
      },
      i: function intro(local) {
        if (current) return;
        transition_in(timer.$$.fragment, local);
        current = true;
      },
      o: function outro(local) {
        transition_out(timer.$$.fragment, local);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div);
        destroy_component(timer);
        if (detaching) detach_dev(t);
        if (detaching) detach_dev(img);
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block$9.name,
      type: "if",
      source: "(97:4) {#if duration && isTimerVisible }",
      ctx: ctx
    });
    return block;
  }
  function create_fragment$g(ctx) {
    var div1;
    var img0;
    var img0_src_value;
    var t0;
    var img1;
    var img1_src_value;
    var t1;
    var img2;
    var img2_src_value;
    var t2;
    var img3;
    var img3_src_value;
    var t3;
    var img4;
    var img4_src_value;
    var t4;
    var div0;
    var each_blocks = [];
    var each_1_lookup = new Map();
    var t5;
    var t6;
    var current;
    var each_value = promoData;
    validate_each_argument(each_value);
    var get_key = function get_key(ctx) {
      return (/*item*/ctx[14].src
      );
    };
    validate_each_keys(ctx, each_value, get_each_context$3, get_key);
    for (var i = 0; i < each_value.length; i += 1) {
      var child_ctx = get_each_context$3(ctx, each_value, i);
      var key = get_key(child_ctx);
      each_1_lookup.set(key, each_blocks[i] = create_each_block$3(key, child_ctx));
    }
    var if_block0 = ! /*isTimerVisible*/ctx[2] && create_if_block_1$2(ctx);
    var if_block1 = /*duration*/ctx[0] && /*isTimerVisible*/ctx[2] && create_if_block$9(ctx);
    var block = {
      c: function create() {
        div1 = element("div");
        img0 = element("img");
        t0 = space();
        img1 = element("img");
        t1 = space();
        img2 = element("img");
        t2 = space();
        img3 = element("img");
        t3 = space();
        img4 = element("img");
        t4 = space();
        div0 = element("div");
        for (var _i = 0; _i < each_blocks.length; _i += 1) {
          each_blocks[_i].c();
        }
        t5 = space();
        if (if_block0) if_block0.c();
        t6 = space();
        if (if_block1) if_block1.c();
        if (!src_url_equal(img0.src, img0_src_value = "".concat(BASE_URL, "/images/chip3.png"))) attr_dev(img0, "src", img0_src_value);
        attr_dev(img0, "alt", "chip1");
        attr_dev(img0, "class", "chip3 svelte-852yzg");
        add_location(img0, file$g, 70, 4, 1862);
        if (!src_url_equal(img1.src, img1_src_value = "".concat(BASE_URL, "/images/chip1.png"))) attr_dev(img1, "src", img1_src_value);
        attr_dev(img1, "alt", "chip1");
        attr_dev(img1, "class", "chip1 svelte-852yzg");
        add_location(img1, file$g, 71, 4, 1937);
        if (!src_url_equal(img2.src, img2_src_value = "".concat(BASE_URL, "/images/chip4.png"))) attr_dev(img2, "src", img2_src_value);
        attr_dev(img2, "alt", "chip1");
        attr_dev(img2, "class", "chip4 svelte-852yzg");
        add_location(img2, file$g, 72, 4, 2012);
        if (!src_url_equal(img3.src, img3_src_value = "".concat(BASE_URL, "/images/chip2.png"))) attr_dev(img3, "src", img3_src_value);
        attr_dev(img3, "alt", "chip1");
        attr_dev(img3, "class", "chip2 svelte-852yzg");
        add_location(img3, file$g, 73, 4, 2087);
        if (!src_url_equal(img4.src, img4_src_value = "".concat(BASE_URL, "/images/logo.png"))) attr_dev(img4, "src", img4_src_value);
        attr_dev(img4, "alt", "logo");
        attr_dev(img4, "class", "logo svelte-852yzg");
        add_location(img4, file$g, 74, 4, 2162);
        attr_dev(div0, "class", "promoContainer svelte-852yzg");
        add_location(div0, file$g, 75, 4, 2234);
        attr_dev(div1, "class", "container appear svelte-852yzg");
        add_location(div1, file$g, 69, 2, 1827);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, div1, anchor);
        append_dev(div1, img0);
        append_dev(div1, t0);
        append_dev(div1, img1);
        append_dev(div1, t1);
        append_dev(div1, img2);
        append_dev(div1, t2);
        append_dev(div1, img3);
        append_dev(div1, t3);
        append_dev(div1, img4);
        append_dev(div1, t4);
        append_dev(div1, div0);
        for (var _i2 = 0; _i2 < each_blocks.length; _i2 += 1) {
          if (each_blocks[_i2]) {
            each_blocks[_i2].m(div0, null);
          }
        }
        append_dev(div1, t5);
        if (if_block0) if_block0.m(div1, null);
        append_dev(div1, t6);
        if (if_block1) if_block1.m(div1, null);
        current = true;
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if (dirty & /*promoData, onClick, BASE_URL, selectedPromo*/18) {
          each_value = promoData;
          validate_each_argument(each_value);
          validate_each_keys(ctx, each_value, get_each_context$3, get_key);
          each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value, each_1_lookup, div0, destroy_block, create_each_block$3, null, get_each_context$3);
        }
        if (! /*isTimerVisible*/ctx[2]) {
          if (if_block0) {
            if_block0.p(ctx, dirty);
          } else {
            if_block0 = create_if_block_1$2(ctx);
            if_block0.c();
            if_block0.m(div1, t6);
          }
        } else if (if_block0) {
          if_block0.d(1);
          if_block0 = null;
        }
        if ( /*duration*/ctx[0] && /*isTimerVisible*/ctx[2]) {
          if (if_block1) {
            if_block1.p(ctx, dirty);
            if (dirty & /*duration, isTimerVisible*/5) {
              transition_in(if_block1, 1);
            }
          } else {
            if_block1 = create_if_block$9(ctx);
            if_block1.c();
            transition_in(if_block1, 1);
            if_block1.m(div1, null);
          }
        } else if (if_block1) {
          group_outros();
          transition_out(if_block1, 1, 1, function () {
            if_block1 = null;
          });
          check_outros();
        }
      },
      i: function intro(local) {
        if (current) return;
        transition_in(if_block1);
        current = true;
      },
      o: function outro(local) {
        transition_out(if_block1);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div1);
        for (var _i3 = 0; _i3 < each_blocks.length; _i3 += 1) {
          each_blocks[_i3].d();
        }
        if (if_block0) if_block0.d();
        if (if_block1) if_block1.d();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$g.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$g($$self, $$props, $$invalidate) {
    var $adDisabled;
    var $externalLink;
    var $iframeLink;
    validate_store(adDisabled, 'adDisabled');
    component_subscribe($$self, adDisabled, function ($$value) {
      return $$invalidate(11, $adDisabled = $$value);
    });
    validate_store(externalLink, 'externalLink');
    component_subscribe($$self, externalLink, function ($$value) {
      return $$invalidate(12, $externalLink = $$value);
    });
    validate_store(iframeLink, 'iframeLink');
    component_subscribe($$self, iframeLink, function ($$value) {
      return $$invalidate(13, $iframeLink = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('Promo1', slots, []);
    var myPlayer = $$props.myPlayer;
    var duration = $$props.duration;
    var currentTime = $$props.currentTime;
    var isSkipped = false;
    var selectedPromo;
    var isTimerVisible = true;
    var musicPlayed = false;
    var nextVideo = $$props.nextVideo;

    // sendMetrik("GameAdShow", { "Gambler.GameAdEpisode": episode });
    var onSkip = function onSkip() {
      isSkipped = true;
      if (!selectedPromo) {
        sendMetrik$1("PromoSkip");
      }
      stopSound();
      nextVideo();
    };
    var onClick = function onClick(event, item) {
      $$invalidate(2, isTimerVisible = false);
      stopSound();
      $$invalidate(2, isTimerVisible = false);
      if (selectedPromo) {
        event.preventDefault();
        return;
      }
      if (item.showInIframe) {
        event.preventDefault();
        set_store_value(iframeLink, $iframeLink = item.href, $iframeLink);
      } else {
        set_store_value(externalLink, $externalLink = item.href, $externalLink);
      }
      sendMetrik$1('PromoClick', {
        'Gambler.PromoClickUrl': item.href
      });
      $$invalidate(1, selectedPromo = item.href);
      myPlayer.pause();
    };
    onMount(function () {
      sendMetrik$1("PromoShow");
      $$invalidate(5, myPlayer.controlBar.el_.style.display = "none", myPlayer);
      return function () {
        stopSound();
        $$invalidate(5, myPlayer.controlBar.el_.style.display = "", myPlayer);
        if (!selectedPromo && !isSkipped) {
          sendMetrik$1('PromoTimerSkip');
        }
      };
    });
    if ($adDisabled) {
      nextVideo();
    }
    $$self.$$.on_mount.push(function () {
      if (myPlayer === undefined && !('myPlayer' in $$props || $$self.$$.bound[$$self.$$.props['myPlayer']])) {
        console.warn("<Promo1> was created without expected prop 'myPlayer'");
      }
      if (duration === undefined && !('duration' in $$props || $$self.$$.bound[$$self.$$.props['duration']])) {
        console.warn("<Promo1> was created without expected prop 'duration'");
      }
      if (currentTime === undefined && !('currentTime' in $$props || $$self.$$.bound[$$self.$$.props['currentTime']])) {
        console.warn("<Promo1> was created without expected prop 'currentTime'");
      }
      if (nextVideo === undefined && !('nextVideo' in $$props || $$self.$$.bound[$$self.$$.props['nextVideo']])) {
        console.warn("<Promo1> was created without expected prop 'nextVideo'");
      }
    });
    var writable_props = ['myPlayer', 'duration', 'currentTime', 'nextVideo'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<Promo1> was created with unknown prop '".concat(key, "'"));
    });
    var click_handler = function click_handler(item, e) {
      return onClick(e, item);
    };
    $$self.$$set = function ($$props) {
      if ('myPlayer' in $$props) $$invalidate(5, myPlayer = $$props.myPlayer);
      if ('duration' in $$props) $$invalidate(0, duration = $$props.duration);
      if ('currentTime' in $$props) $$invalidate(6, currentTime = $$props.currentTime);
      if ('nextVideo' in $$props) $$invalidate(7, nextVideo = $$props.nextVideo);
    };
    $$self.$capture_state = function () {
      return {
        playSound: playSound,
        stopSound: stopSound,
        promoData: promoData,
        onMount: onMount,
        BASE_URL: BASE_URL,
        gameClosed: gameClosed,
        gameWon: gameWon,
        gameSkipped: gameSkipped,
        sendMetrik: sendMetrik$1,
        externalLink: externalLink,
        iframeLink: iframeLink,
        adDisabled: adDisabled,
        Timer: Timer,
        myPlayer: myPlayer,
        duration: duration,
        currentTime: currentTime,
        isSkipped: isSkipped,
        selectedPromo: selectedPromo,
        isTimerVisible: isTimerVisible,
        musicPlayed: musicPlayed,
        nextVideo: nextVideo,
        onSkip: onSkip,
        onClick: onClick,
        $adDisabled: $adDisabled,
        $externalLink: $externalLink,
        $iframeLink: $iframeLink
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('myPlayer' in $$props) $$invalidate(5, myPlayer = $$props.myPlayer);
      if ('duration' in $$props) $$invalidate(0, duration = $$props.duration);
      if ('currentTime' in $$props) $$invalidate(6, currentTime = $$props.currentTime);
      if ('isSkipped' in $$props) isSkipped = $$props.isSkipped;
      if ('selectedPromo' in $$props) $$invalidate(1, selectedPromo = $$props.selectedPromo);
      if ('isTimerVisible' in $$props) $$invalidate(2, isTimerVisible = $$props.isTimerVisible);
      if ('musicPlayed' in $$props) $$invalidate(8, musicPlayed = $$props.musicPlayed);
      if ('nextVideo' in $$props) $$invalidate(7, nextVideo = $$props.nextVideo);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    $$self.$$.update = function () {
      if ($$self.$$.dirty & /*currentTime, musicPlayed*/320) {
        if (currentTime > 2.332599 && !musicPlayed) {
          $$invalidate(8, musicPlayed = true);
          azure.muted(true);
          playSound('promo1');
        }
      }
    };
    return [duration, selectedPromo, isTimerVisible, onSkip, onClick, myPlayer, currentTime, nextVideo, musicPlayed, click_handler];
  }
  var Promo1 = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(Promo1, _SvelteComponentDev);
    var _super = _createSuper$g(Promo1);
    function Promo1(options) {
      var _this;
      _classCallCheck(this, Promo1);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$g, create_fragment$g, safe_not_equal, {
        myPlayer: 5,
        duration: 0,
        currentTime: 6,
        nextVideo: 7
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "Promo1",
        options: options,
        id: create_fragment$g.name
      });
      return _this;
    }
    _createClass(Promo1, [{
      key: "myPlayer",
      get: function get() {
        throw new Error("<Promo1>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Promo1>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "duration",
      get: function get() {
        throw new Error("<Promo1>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Promo1>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "currentTime",
      get: function get() {
        throw new Error("<Promo1>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Promo1>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "nextVideo",
      get: function get() {
        throw new Error("<Promo1>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Promo1>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return Promo1;
  }(SvelteComponentDev);

  var interactive = {
    'ONE': One,
    'TWO': Two,
    'THREE': Three,
    'CREDITS': Credits,
    'PROMO': Promo,
    'PROMO1': Promo1
  };

  // `String.prototype.link` method
  // https://tc39.es/ecma262/#sec-string.prototype.link
  _export({ target: 'String', proto: true, forced: stringHtmlForced('link') }, {
    link: function link(url) {
      return createHtml(this, 'a', 'href', url);
    }
  });

  function _createSuper$f(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$f(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$f() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$f = "src/ActiveArea/Rss.svelte";
  function create_fragment$f(ctx) {
    var div3;
    var a;
    var img;
    var img_src_value;
    var img_style_value;
    var t0;
    var div2;
    var div0;
    var t1_value = ( /*item*/ctx[1].rssType === 'tele' ? 'Teleprogramma.pro' : '7days.ru') + "";
    var t1;
    var div0_style_value;
    var t2;
    var div1;
    var t3_value = /*news*/ctx[3][0].title + "";
    var t3;
    var div1_style_value;
    var a_href_value;
    var div3_transition;
    var current;
    var mounted;
    var dispose;
    var block = {
      c: function create() {
        div3 = element("div");
        a = element("a");
        img = element("img");
        t0 = space();
        div2 = element("div");
        div0 = element("div");
        t1 = text(t1_value);
        t2 = space();
        div1 = element("div");
        t3 = text(t3_value);
        if (!src_url_equal(img.src, img_src_value = "".concat(BASE_URL, "/images/").concat( /*item*/ctx[1].rssType === 'tele' ? 'teleprogramma.png' : '7days.png'))) attr_dev(img, "src", img_src_value);
        attr_dev(img, "alt", "logo");
        attr_dev(img, "style", img_style_value = "width:".concat( /*containerWidth*/ctx[0] * 0.045, "px; margin-right:").concat( /*containerWidth*/ctx[0] * 0.02, "px; margin-top:").concat( /*containerWidth*/ctx[0] * 0.009, "px"));
        add_location(img, file$f, 57, 4, 1856);
        attr_dev(div0, "style", div0_style_value = "font-size:".concat( /*containerWidth*/ctx[0] * 0.015, "px;margin-bottom:").concat( /*containerWidth*/ctx[0] * 0.009, "px"));
        attr_dev(div0, "class", "title svelte-15wz0nl");
        add_location(div0, file$f, 62, 6, 2138);
        attr_dev(div1, "style", div1_style_value = "font-size:".concat( /*containerWidth*/ctx[0] * 0.015, "px"));
        attr_dev(div1, "class", "text svelte-15wz0nl");
        add_location(div1, file$f, 68, 6, 2351);
        attr_dev(div2, "class", "details");
        add_location(div2, file$f, 61, 4, 2110);
        attr_dev(a, "href", a_href_value = /*news*/ctx[3][0].link);
        attr_dev(a, "target", "_blank");
        attr_dev(a, "class", "link svelte-15wz0nl");
        add_location(a, file$f, 56, 2, 1780);
        attr_dev(div3, "class", "rss svelte-15wz0nl");
        attr_dev(div3, "style", /*blockStyle*/ctx[2]);
        toggle_class(div3, "white", /*item*/ctx[1].rssType === '7days');
        add_location(div3, file$f, 50, 0, 1626);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, div3, anchor);
        append_dev(div3, a);
        append_dev(a, img);
        append_dev(a, t0);
        append_dev(a, div2);
        append_dev(div2, div0);
        append_dev(div0, t1);
        append_dev(div2, t2);
        append_dev(div2, div1);
        append_dev(div1, t3);
        current = true;
        if (!mounted) {
          dispose = [listen_dev(a, "click", /*onClick*/ctx[4], false, false, false, false), listen_dev(div3, "outroend", /*outroend*/ctx[5], false, false, false, false)];
          mounted = true;
        }
      },
      p: function update(new_ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        ctx = new_ctx;
        if (!current || dirty & /*item*/2 && !src_url_equal(img.src, img_src_value = "".concat(BASE_URL, "/images/").concat( /*item*/ctx[1].rssType === 'tele' ? 'teleprogramma.png' : '7days.png'))) {
          attr_dev(img, "src", img_src_value);
        }
        if (!current || dirty & /*containerWidth*/1 && img_style_value !== (img_style_value = "width:".concat( /*containerWidth*/ctx[0] * 0.045, "px; margin-right:").concat( /*containerWidth*/ctx[0] * 0.02, "px; margin-top:").concat( /*containerWidth*/ctx[0] * 0.009, "px"))) {
          attr_dev(img, "style", img_style_value);
        }
        if ((!current || dirty & /*item*/2) && t1_value !== (t1_value = ( /*item*/ctx[1].rssType === 'tele' ? 'Teleprogramma.pro' : '7days.ru') + "")) set_data_dev(t1, t1_value);
        if (!current || dirty & /*containerWidth*/1 && div0_style_value !== (div0_style_value = "font-size:".concat( /*containerWidth*/ctx[0] * 0.015, "px;margin-bottom:").concat( /*containerWidth*/ctx[0] * 0.009, "px"))) {
          attr_dev(div0, "style", div0_style_value);
        }
        if ((!current || dirty & /*news*/8) && t3_value !== (t3_value = /*news*/ctx[3][0].title + "")) set_data_dev(t3, t3_value);
        if (!current || dirty & /*containerWidth*/1 && div1_style_value !== (div1_style_value = "font-size:".concat( /*containerWidth*/ctx[0] * 0.015, "px"))) {
          attr_dev(div1, "style", div1_style_value);
        }
        if (!current || dirty & /*news*/8 && a_href_value !== (a_href_value = /*news*/ctx[3][0].link)) {
          attr_dev(a, "href", a_href_value);
        }
        if (!current || dirty & /*blockStyle*/4) {
          attr_dev(div3, "style", /*blockStyle*/ctx[2]);
        }
        if (!current || dirty & /*item*/2) {
          toggle_class(div3, "white", /*item*/ctx[1].rssType === '7days');
        }
      },
      i: function intro(local) {
        if (current) return;
        if (local) {
          add_render_callback(function () {
            if (!current) return;
            if (!div3_transition) div3_transition = create_bidirectional_transition(div3, fly, {
              x: /*containerWidth*/ctx[0]
            }, true);
            div3_transition.run(1);
          });
        }
        current = true;
      },
      o: function outro(local) {
        if (local) {
          if (!div3_transition) div3_transition = create_bidirectional_transition(div3, fly, {
            x: /*containerWidth*/ctx[0]
          }, false);
          div3_transition.run(0);
        }
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div3);
        if (detaching && div3_transition) div3_transition.end();
        mounted = false;
        run_all(dispose);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$f.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  var getFeed = function getFeed(url) {
    return fetch("https://screenlife.tech/gambler/rss.php?url=" + url).then(function (response) {
      return response.text();
    }).then(function (str) {
      return new window.DOMParser().parseFromString(str, "text/xml");
    }).then(function (data) {
      return _toConsumableArray(data.querySelectorAll("channel item")).map(function (item) {
        return {
          title: item.querySelector("title").textContent,
          description: item.querySelector("description").textContent,
          link: item.querySelector("link").textContent,
          pubDate: item.querySelector("pubDate").textContent
        };
      });
    });
  };
  var newsTele = [];
  var news7days = [];
  getFeed("https://teleprogramma.pro/edition-rss/").then(function (data) {
    return newsTele = data;
  });
  getFeed("https://7days.ru/rss/yandex/news").then(function (data) {
    return news7days = data;
  });
  function instance$f($$self, $$props, $$invalidate) {
    var news;
    var blockStyle;
    var $iframeLink;
    validate_store(iframeLink, 'iframeLink');
    component_subscribe($$self, iframeLink, function ($$value) {
      return $$invalidate(7, $iframeLink = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('Rss', slots, []);
    var containerWidth = $$props.containerWidth;
    var myPlayer = $$props.myPlayer;
    var item = $$props.item;
    sendMetrik$1("RssShow");
    var onClick = function onClick(e) {
      e.preventDefault();
      set_store_value(iframeLink, $iframeLink = news[0].link, $iframeLink);
      sendMetrik$1("RssClick");
      myPlayer.pause();
    };
    var outroend = function outroend() {
      if (item.rssType === "tele") {
        newsTele = newsTele.slice(1);
      } else {
        news7days = news7days.slice(1);
      }
    };
    $$self.$$.on_mount.push(function () {
      if (containerWidth === undefined && !('containerWidth' in $$props || $$self.$$.bound[$$self.$$.props['containerWidth']])) {
        console.warn("<Rss> was created without expected prop 'containerWidth'");
      }
      if (myPlayer === undefined && !('myPlayer' in $$props || $$self.$$.bound[$$self.$$.props['myPlayer']])) {
        console.warn("<Rss> was created without expected prop 'myPlayer'");
      }
      if (item === undefined && !('item' in $$props || $$self.$$.bound[$$self.$$.props['item']])) {
        console.warn("<Rss> was created without expected prop 'item'");
      }
    });
    var writable_props = ['containerWidth', 'myPlayer', 'item'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<Rss> was created with unknown prop '".concat(key, "'"));
    });
    $$self.$$set = function ($$props) {
      if ('containerWidth' in $$props) $$invalidate(0, containerWidth = $$props.containerWidth);
      if ('myPlayer' in $$props) $$invalidate(6, myPlayer = $$props.myPlayer);
      if ('item' in $$props) $$invalidate(1, item = $$props.item);
    };
    $$self.$capture_state = function () {
      return {
        getFeed: getFeed,
        newsTele: newsTele,
        news7days: news7days,
        fly: fly,
        sendMetrik: sendMetrik$1,
        externalLink: externalLink,
        iframeLink: iframeLink,
        BASE_URL: BASE_URL,
        containerWidth: containerWidth,
        myPlayer: myPlayer,
        item: item,
        onClick: onClick,
        outroend: outroend,
        blockStyle: blockStyle,
        news: news,
        $iframeLink: $iframeLink
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('containerWidth' in $$props) $$invalidate(0, containerWidth = $$props.containerWidth);
      if ('myPlayer' in $$props) $$invalidate(6, myPlayer = $$props.myPlayer);
      if ('item' in $$props) $$invalidate(1, item = $$props.item);
      if ('blockStyle' in $$props) $$invalidate(2, blockStyle = $$props.blockStyle);
      if ('news' in $$props) $$invalidate(3, news = $$props.news);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    $$self.$$.update = function () {
      if ($$self.$$.dirty & /*item*/2) {
        $$invalidate(3, news = item.rssType === "tele" ? newsTele : news7days);
      }
      if ($$self.$$.dirty & /*containerWidth*/1) {
        $$invalidate(2, blockStyle = "width: ".concat(containerWidth * 0.5, "px; padding: ").concat(containerWidth * 0.022, "px ").concat(containerWidth * 0.017, "px;border-radius:").concat(containerWidth * 0.0122, "px"));
      }
    };
    return [containerWidth, item, blockStyle, news, onClick, outroend, myPlayer];
  }
  var Rss = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(Rss, _SvelteComponentDev);
    var _super = _createSuper$f(Rss);
    function Rss(options) {
      var _this;
      _classCallCheck(this, Rss);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$f, create_fragment$f, safe_not_equal, {
        containerWidth: 0,
        myPlayer: 6,
        item: 1
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "Rss",
        options: options,
        id: create_fragment$f.name
      });
      return _this;
    }
    _createClass(Rss, [{
      key: "containerWidth",
      get: function get() {
        throw new Error("<Rss>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Rss>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "myPlayer",
      get: function get() {
        throw new Error("<Rss>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Rss>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "item",
      get: function get() {
        throw new Error("<Rss>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Rss>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return Rss;
  }(SvelteComponentDev);

  function _createSuper$e(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$e(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$e() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$e = "src/ActiveArea/Image.svelte";

  // (60:2) {#if isLoaded}
  function create_if_block$8(ctx) {
    var div;
    var block = {
      c: function create() {
        div = element("div");
        attr_dev(div, "style", /*shiteStyle*/ctx[3]);
        attr_dev(div, "class", "shine svelte-hncxbg");
        add_location(div, file$e, 60, 4, 1624);
      },
      m: function mount(target, anchor) {
        insert_dev(target, div, anchor);
      },
      p: function update(ctx, dirty) {
        if (dirty & /*shiteStyle*/8) {
          attr_dev(div, "style", /*shiteStyle*/ctx[3]);
        }
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block$8.name,
      type: "if",
      source: "(60:2) {#if isLoaded}",
      ctx: ctx
    });
    return block;
  }
  function create_fragment$e(ctx) {
    var a;
    var img;
    var img_src_value;
    var img_style_value;
    var t;
    var a_href_value;
    var mounted;
    var dispose;
    var if_block = /*isLoaded*/ctx[2] && create_if_block$8(ctx);
    var block = {
      c: function create() {
        a = element("a");
        img = element("img");
        t = space();
        if (if_block) if_block.c();
        if (!src_url_equal(img.src, img_src_value = /*item*/ctx[0].image)) attr_dev(img, "src", img_src_value);
        attr_dev(img, "alt", "interactive");
        attr_dev(img, "class", "interactive svelte-hncxbg");
        attr_dev(img, "style", img_style_value = "width:".concat( /*item*/ctx[0].width, "%; left:").concat( /*item*/ctx[0].left, "%; top: ").concat( /*item*/ctx[0].top, "%;"));
        toggle_class(img, "isLoaded", /*isLoaded*/ctx[2]);
        add_location(img, file$e, 50, 2, 1332);
        attr_dev(a, "target", "_blank");
        attr_dev(a, "rel", "nofollow");
        attr_dev(a, "href", a_href_value = /*item*/ctx[0].href);
        toggle_class(a, "isLoaded", /*isLoaded*/ctx[2]);
        add_location(a, file$e, 44, 0, 1234);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, a, anchor);
        append_dev(a, img);
        /*img_binding*/
        ctx[8](img);
        append_dev(a, t);
        if (if_block) if_block.m(a, null);
        if (!mounted) {
          dispose = [listen_dev(img, "load", /*onLoad*/ctx[4], false, false, false, false), listen_dev(img, "error", error_handler, false, false, false, false), listen_dev(a, "click", /*onClick*/ctx[5], false, false, false, false)];
          mounted = true;
        }
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if (dirty & /*item*/1 && !src_url_equal(img.src, img_src_value = /*item*/ctx[0].image)) {
          attr_dev(img, "src", img_src_value);
        }
        if (dirty & /*item*/1 && img_style_value !== (img_style_value = "width:".concat( /*item*/ctx[0].width, "%; left:").concat( /*item*/ctx[0].left, "%; top: ").concat( /*item*/ctx[0].top, "%;"))) {
          attr_dev(img, "style", img_style_value);
        }
        if (dirty & /*isLoaded*/4) {
          toggle_class(img, "isLoaded", /*isLoaded*/ctx[2]);
        }
        if ( /*isLoaded*/ctx[2]) {
          if (if_block) {
            if_block.p(ctx, dirty);
          } else {
            if_block = create_if_block$8(ctx);
            if_block.c();
            if_block.m(a, null);
          }
        } else if (if_block) {
          if_block.d(1);
          if_block = null;
        }
        if (dirty & /*item*/1 && a_href_value !== (a_href_value = /*item*/ctx[0].href)) {
          attr_dev(a, "href", a_href_value);
        }
        if (dirty & /*isLoaded*/4) {
          toggle_class(a, "isLoaded", /*isLoaded*/ctx[2]);
        }
      },
      i: noop,
      o: noop,
      d: function destroy(detaching) {
        if (detaching) detach_dev(a);
        /*img_binding*/
        ctx[8](null);
        if (if_block) if_block.d();
        mounted = false;
        run_all(dispose);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$e.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  var error_handler = function error_handler(e) {
    return e.target.style.display = 'none';
  };
  function instance$e($$self, $$props, $$invalidate) {
    var shiteStyle;
    var $externalLink;
    var $iframeLink;
    validate_store(externalLink, 'externalLink');
    component_subscribe($$self, externalLink, function ($$value) {
      return $$invalidate(9, $externalLink = $$value);
    });
    validate_store(iframeLink, 'iframeLink');
    component_subscribe($$self, iframeLink, function ($$value) {
      return $$invalidate(10, $iframeLink = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('Image', slots, []);
    var item = $$props.item;
    var myPlayer = $$props.myPlayer;
    var containerWidth = $$props.containerWidth;
    var imageRef;

    // $: whiteShadow = `box-shadow: 0 0 ${containerWidth * 0.05}px ${
    //   containerWidth * 0.01
    // }px `;
    var isLoaded = false;
    var onLoad = function onLoad() {
      $$invalidate(2, isLoaded = true);
      sendMetrik$1("BannerShow", {
        "Gambler.BannerShowLink": item.href
      });
    };
    var onClick = function onClick(e) {
      myPlayer.pause();
      $$invalidate(6, myPlayer.controlBar.el_.style.display = "", myPlayer);
      sendMetrik$1("BannerClick", {
        "Gambler.BannerClickLink": item.href
      });
      if (item.showInIframe) {
        e.preventDefault();
        set_store_value(iframeLink, $iframeLink = item.href, $iframeLink);
      } else {
        set_store_value(externalLink, $externalLink = item.href, $externalLink);
      }
    };
    onMount(function () {
      if (item.hideControls) {
        $$invalidate(6, myPlayer.controlBar.el_.style.display = "none", myPlayer);
      }
      return function () {
        $$invalidate(6, myPlayer.controlBar.el_.style.display = "", myPlayer);
      };
    });
    $$self.$$.on_mount.push(function () {
      if (item === undefined && !('item' in $$props || $$self.$$.bound[$$self.$$.props['item']])) {
        console.warn("<Image> was created without expected prop 'item'");
      }
      if (myPlayer === undefined && !('myPlayer' in $$props || $$self.$$.bound[$$self.$$.props['myPlayer']])) {
        console.warn("<Image> was created without expected prop 'myPlayer'");
      }
      if (containerWidth === undefined && !('containerWidth' in $$props || $$self.$$.bound[$$self.$$.props['containerWidth']])) {
        console.warn("<Image> was created without expected prop 'containerWidth'");
      }
    });
    var writable_props = ['item', 'myPlayer', 'containerWidth'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<Image> was created with unknown prop '".concat(key, "'"));
    });
    function img_binding($$value) {
      binding_callbacks[$$value ? 'unshift' : 'push'](function () {
        imageRef = $$value;
        $$invalidate(1, imageRef);
      });
    }
    $$self.$$set = function ($$props) {
      if ('item' in $$props) $$invalidate(0, item = $$props.item);
      if ('myPlayer' in $$props) $$invalidate(6, myPlayer = $$props.myPlayer);
      if ('containerWidth' in $$props) $$invalidate(7, containerWidth = $$props.containerWidth);
    };
    $$self.$capture_state = function () {
      return {
        onMount: onMount,
        item: item,
        myPlayer: myPlayer,
        containerWidth: containerWidth,
        imageRef: imageRef,
        sendMetrik: sendMetrik$1,
        externalLink: externalLink,
        iframeLink: iframeLink,
        isLoaded: isLoaded,
        onLoad: onLoad,
        onClick: onClick,
        shiteStyle: shiteStyle,
        $externalLink: $externalLink,
        $iframeLink: $iframeLink
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('item' in $$props) $$invalidate(0, item = $$props.item);
      if ('myPlayer' in $$props) $$invalidate(6, myPlayer = $$props.myPlayer);
      if ('containerWidth' in $$props) $$invalidate(7, containerWidth = $$props.containerWidth);
      if ('imageRef' in $$props) $$invalidate(1, imageRef = $$props.imageRef);
      if ('isLoaded' in $$props) $$invalidate(2, isLoaded = $$props.isLoaded);
      if ('shiteStyle' in $$props) $$invalidate(3, shiteStyle = $$props.shiteStyle);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    $$self.$$.update = function () {
      if ($$self.$$.dirty & /*containerWidth, imageRef, isLoaded, item*/135) {
        $$invalidate(3, shiteStyle = containerWidth && imageRef && isLoaded ? "width:".concat(item.width, "%; left:").concat(item.left, "%; top: ").concat(item.top, "%;height:").concat(imageRef.getBoundingClientRect().height, "px") : "");
      }
    };
    return [item, imageRef, isLoaded, shiteStyle, onLoad, onClick, myPlayer, containerWidth, img_binding];
  }
  var Image = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(Image, _SvelteComponentDev);
    var _super = _createSuper$e(Image);
    function Image(options) {
      var _this;
      _classCallCheck(this, Image);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$e, create_fragment$e, safe_not_equal, {
        item: 0,
        myPlayer: 6,
        containerWidth: 7
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "Image",
        options: options,
        id: create_fragment$e.name
      });
      return _this;
    }
    _createClass(Image, [{
      key: "item",
      get: function get() {
        throw new Error("<Image>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Image>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "myPlayer",
      get: function get() {
        throw new Error("<Image>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Image>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "containerWidth",
      get: function get() {
        throw new Error("<Image>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Image>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return Image;
  }(SvelteComponentDev);

  function _defineProperty(obj, key, value) {
    key = _toPropertyKey(key);
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }
    return obj;
  }

  function _createSuper$d(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$d(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$d() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$d = "src/ActiveArea/Zone.svelte";
  function create_fragment$d(ctx) {
    var a;
    var div;
    var a_href_value;
    var mounted;
    var dispose;
    var block = {
      c: function create() {
        a = element("a");
        div = element("div");
        attr_dev(div, "class", "zone shine svelte-11l9k4l");
        attr_dev(div, "style", /*style*/ctx[1]);
        toggle_class(div, "whiteShadow", /*item*/ctx[0].color === 'white');
        add_location(div, file$d, 52, 2, 1654);
        attr_dev(a, "target", "_blank");
        attr_dev(a, "rel", "nofollow");
        attr_dev(a, "href", a_href_value = /*item*/ctx[0].href);
        add_location(a, file$d, 51, 0, 1581);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, a, anchor);
        append_dev(a, div);
        if (!mounted) {
          dispose = listen_dev(a, "click", /*onClick*/ctx[2], false, false, false, false);
          mounted = true;
        }
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if (dirty & /*style*/2) {
          attr_dev(div, "style", /*style*/ctx[1]);
        }
        if (dirty & /*item*/1) {
          toggle_class(div, "whiteShadow", /*item*/ctx[0].color === 'white');
        }
        if (dirty & /*item*/1 && a_href_value !== (a_href_value = /*item*/ctx[0].href)) {
          attr_dev(a, "href", a_href_value);
        }
      },
      i: noop,
      o: noop,
      d: function destroy(detaching) {
        if (detaching) detach_dev(a);
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$d.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$d($$self, $$props, $$invalidate) {
    var borderRadius;
    var style;
    var $externalLink;
    var $iframeLink;
    validate_store(externalLink, 'externalLink');
    component_subscribe($$self, externalLink, function ($$value) {
      return $$invalidate(6, $externalLink = $$value);
    });
    validate_store(iframeLink, 'iframeLink');
    component_subscribe($$self, iframeLink, function ($$value) {
      return $$invalidate(7, $iframeLink = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('Zone', slots, []);
    var item = $$props.item;
    var myPlayer = $$props.myPlayer;
    var containerWidth = $$props.containerWidth;
    var isVkLink = item.href.startsWith("https://vk.com");
    sendMetrik$1(isVkLink ? "VkShow" : "BannerShow", _defineProperty({}, isVkLink ? "Gambler.VkShowLink" : "Gambler.BannerShowLink", item.href));
    var onClick = function onClick(e) {
      if (item.showInIframe) {
        e.preventDefault();
        set_store_value(iframeLink, $iframeLink = item.href, $iframeLink);
      } else {
        set_store_value(externalLink, $externalLink = item.href, $externalLink);
      }
      sendMetrik$1(isVkLink ? "VkClick" : "BannerClick", _defineProperty({}, isVkLink ? "Gambler.VkClickLink" : "Gambler.BannerClickLink", item.href));
      myPlayer.pause();
      $$invalidate(3, myPlayer.controlBar.el_.style.display = "", myPlayer);
    };
    onMount(function () {
      if (item.needPause) {
        // document.querySelector(".vjs-control-bar").style.opacity = 0;
        var onPlay = function onPlay() {
          myPlayer.pause();
        };
        playSound("vk", function () {
          myPlayer.removeEventListener("play", onPlay);
          myPlayer.play();
        });
        myPlayer.addEventListener("play", onPlay);
        myPlayer.pause();
      }
      if (item.hideControls) {
        $$invalidate(3, myPlayer.controlBar.el_.style.display = "none", myPlayer);
      }
      return function () {
        $$invalidate(3, myPlayer.controlBar.el_.style.display = "", myPlayer);
      };
    });
    $$self.$$.on_mount.push(function () {
      if (item === undefined && !('item' in $$props || $$self.$$.bound[$$self.$$.props['item']])) {
        console.warn("<Zone> was created without expected prop 'item'");
      }
      if (myPlayer === undefined && !('myPlayer' in $$props || $$self.$$.bound[$$self.$$.props['myPlayer']])) {
        console.warn("<Zone> was created without expected prop 'myPlayer'");
      }
      if (containerWidth === undefined && !('containerWidth' in $$props || $$self.$$.bound[$$self.$$.props['containerWidth']])) {
        console.warn("<Zone> was created without expected prop 'containerWidth'");
      }
    });
    var writable_props = ['item', 'myPlayer', 'containerWidth'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<Zone> was created with unknown prop '".concat(key, "'"));
    });
    $$self.$$set = function ($$props) {
      if ('item' in $$props) $$invalidate(0, item = $$props.item);
      if ('myPlayer' in $$props) $$invalidate(3, myPlayer = $$props.myPlayer);
      if ('containerWidth' in $$props) $$invalidate(4, containerWidth = $$props.containerWidth);
    };
    $$self.$capture_state = function () {
      return {
        onMount: onMount,
        sendMetrik: sendMetrik$1,
        externalLink: externalLink,
        iframeLink: iframeLink,
        playSound: playSound,
        item: item,
        myPlayer: myPlayer,
        containerWidth: containerWidth,
        isVkLink: isVkLink,
        onClick: onClick,
        borderRadius: borderRadius,
        style: style,
        $externalLink: $externalLink,
        $iframeLink: $iframeLink
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('item' in $$props) $$invalidate(0, item = $$props.item);
      if ('myPlayer' in $$props) $$invalidate(3, myPlayer = $$props.myPlayer);
      if ('containerWidth' in $$props) $$invalidate(4, containerWidth = $$props.containerWidth);
      if ('borderRadius' in $$props) $$invalidate(5, borderRadius = $$props.borderRadius);
      if ('style' in $$props) $$invalidate(1, style = $$props.style);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    $$self.$$.update = function () {
      if ($$self.$$.dirty & /*item, containerWidth*/17) {
        $$invalidate(5, borderRadius = item.withoutBorder ? 0 : containerWidth * 0.05);
      }
      if ($$self.$$.dirty & /*item, borderRadius*/33) {
        $$invalidate(1, style = "width:".concat(item.width, "%; left:").concat(item.left, "%; top: ").concat(item.top, "%; padding-top: ").concat(item.paddingTop, "%; border-radius:").concat(borderRadius, "px;"));
      }
    };
    return [item, style, onClick, myPlayer, containerWidth, borderRadius];
  }
  var Zone = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(Zone, _SvelteComponentDev);
    var _super = _createSuper$d(Zone);
    function Zone(options) {
      var _this;
      _classCallCheck(this, Zone);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$d, create_fragment$d, safe_not_equal, {
        item: 0,
        myPlayer: 3,
        containerWidth: 4
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "Zone",
        options: options,
        id: create_fragment$d.name
      });
      return _this;
    }
    _createClass(Zone, [{
      key: "item",
      get: function get() {
        throw new Error("<Zone>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Zone>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "myPlayer",
      get: function get() {
        throw new Error("<Zone>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Zone>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "containerWidth",
      get: function get() {
        throw new Error("<Zone>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Zone>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return Zone;
  }(SvelteComponentDev);

  function _createSuper$c(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$c(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$c() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$c = "src/ActiveArea/Push.svelte";
  function create_fragment$c(ctx) {
    var a;
    var img;
    var img_src_value;
    var img_style_value;
    var t0;
    var div2;
    var div0;
    var t1_value = /*item*/ctx[1].title + "";
    var t1;
    var div0_style_value;
    var t2;
    var div1;
    var t3_value = /*item*/ctx[1].text + "";
    var t3;
    var div1_style_value;
    var a_href_value;
    var a_intro;
    var mounted;
    var dispose;
    var block = {
      c: function create() {
        a = element("a");
        img = element("img");
        t0 = space();
        div2 = element("div");
        div0 = element("div");
        t1 = text(t1_value);
        t2 = space();
        div1 = element("div");
        t3 = text(t3_value);
        if (!src_url_equal(img.src, img_src_value = /*item*/ctx[1].src)) attr_dev(img, "src", img_src_value);
        attr_dev(img, "alt", "logo");
        attr_dev(img, "style", img_style_value = "width:".concat( /*containerWidth*/ctx[0] * /*item*/ctx[1].imageWidth, "px; margin-right:").concat( /*containerWidth*/ctx[0] * 0.035, "px;"));
        add_location(img, file$c, 40, 4, 1169);
        attr_dev(div0, "style", div0_style_value = "font-size:".concat( /*containerWidth*/ctx[0] * /*item*/ctx[1].fontSize, "px;margin-bottom:").concat( /*containerWidth*/ctx[0] * 0.009, "px"));
        attr_dev(div0, "class", "title svelte-v524pc");
        add_location(div0, file$c, 45, 6, 1350);
        attr_dev(div1, "style", div1_style_value = "font-size:".concat( /*containerWidth*/ctx[0] * /*item*/ctx[1].fontSize, "px"));
        attr_dev(div1, "class", "text svelte-v524pc");
        add_location(div1, file$c, 51, 6, 1525);
        attr_dev(div2, "class", "details");
        add_location(div2, file$c, 44, 4, 1322);
        attr_dev(a, "class", "rss shine svelte-v524pc");
        attr_dev(a, "style", /*blockStyle*/ctx[2]);
        attr_dev(a, "href", a_href_value = /*item*/ctx[1].href);
        attr_dev(a, "target", "_blank");
        add_location(a, file$c, 38, 0, 1034);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, a, anchor);
        append_dev(a, img);
        append_dev(a, t0);
        append_dev(a, div2);
        append_dev(div2, div0);
        append_dev(div0, t1);
        append_dev(div2, t2);
        append_dev(div2, div1);
        append_dev(div1, t3);
        if (!mounted) {
          dispose = listen_dev(a, "click", /*onClick*/ctx[3], false, false, false, false);
          mounted = true;
        }
      },
      p: function update(new_ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        ctx = new_ctx;
        if (dirty & /*item*/2 && !src_url_equal(img.src, img_src_value = /*item*/ctx[1].src)) {
          attr_dev(img, "src", img_src_value);
        }
        if (dirty & /*containerWidth, item*/3 && img_style_value !== (img_style_value = "width:".concat( /*containerWidth*/ctx[0] * /*item*/ctx[1].imageWidth, "px; margin-right:").concat( /*containerWidth*/ctx[0] * 0.035, "px;"))) {
          attr_dev(img, "style", img_style_value);
        }
        if (dirty & /*item*/2 && t1_value !== (t1_value = /*item*/ctx[1].title + "")) set_data_dev(t1, t1_value);
        if (dirty & /*containerWidth, item*/3 && div0_style_value !== (div0_style_value = "font-size:".concat( /*containerWidth*/ctx[0] * /*item*/ctx[1].fontSize, "px;margin-bottom:").concat( /*containerWidth*/ctx[0] * 0.009, "px"))) {
          attr_dev(div0, "style", div0_style_value);
        }
        if (dirty & /*item*/2 && t3_value !== (t3_value = /*item*/ctx[1].text + "")) set_data_dev(t3, t3_value);
        if (dirty & /*containerWidth, item*/3 && div1_style_value !== (div1_style_value = "font-size:".concat( /*containerWidth*/ctx[0] * /*item*/ctx[1].fontSize, "px"))) {
          attr_dev(div1, "style", div1_style_value);
        }
        if (dirty & /*blockStyle*/4) {
          attr_dev(a, "style", /*blockStyle*/ctx[2]);
        }
        if (dirty & /*item*/2 && a_href_value !== (a_href_value = /*item*/ctx[1].href)) {
          attr_dev(a, "href", a_href_value);
        }
      },
      i: function intro(local) {
        if (local) {
          if (!a_intro) {
            add_render_callback(function () {
              a_intro = create_in_transition(a, fly, {
                x: /*containerWidth*/ctx[0]
              });
              a_intro.start();
            });
          }
        }
      },
      o: noop,
      d: function destroy(detaching) {
        if (detaching) detach_dev(a);
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$c.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$c($$self, $$props, $$invalidate) {
    var blockStyle;
    var $externalLink;
    var $iframeLink;
    validate_store(externalLink, 'externalLink');
    component_subscribe($$self, externalLink, function ($$value) {
      return $$invalidate(6, $externalLink = $$value);
    });
    validate_store(iframeLink, 'iframeLink');
    component_subscribe($$self, iframeLink, function ($$value) {
      return $$invalidate(7, $iframeLink = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('Push', slots, []);
    var containerWidth = $$props.containerWidth;
    var myPlayer = $$props.myPlayer;
    var item = $$props.item;
    sendMetrik$1("BannerShow", {
      "Gambler.BannerShowLink": item.href
    });
    var onClick = function onClick() {
      myPlayer.pause();
      $$invalidate(4, myPlayer.controlBar.el_.style.display = "", myPlayer);
      sendMetrik$1("BannerClick", {
        "Gambler.BannerClickLink": item.href
      });
      if (item.showInIframe) {
        e.preventDefault();
        set_store_value(iframeLink, $iframeLink = item.href, $iframeLink);
      } else {
        set_store_value(externalLink, $externalLink = item.href, $externalLink);
      }
    };
    var timeoutId;
    onMount(function () {
      if (item.needPause) {
        myPlayer.pause();
        timeoutId = setTimeout(function () {
          return myPlayer.play();
        }, 2000);
      }
      return function () {
        clearTimeout(timeoutId);
      };
    });
    $$self.$$.on_mount.push(function () {
      if (containerWidth === undefined && !('containerWidth' in $$props || $$self.$$.bound[$$self.$$.props['containerWidth']])) {
        console.warn("<Push> was created without expected prop 'containerWidth'");
      }
      if (myPlayer === undefined && !('myPlayer' in $$props || $$self.$$.bound[$$self.$$.props['myPlayer']])) {
        console.warn("<Push> was created without expected prop 'myPlayer'");
      }
      if (item === undefined && !('item' in $$props || $$self.$$.bound[$$self.$$.props['item']])) {
        console.warn("<Push> was created without expected prop 'item'");
      }
    });
    var writable_props = ['containerWidth', 'myPlayer', 'item'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<Push> was created with unknown prop '".concat(key, "'"));
    });
    $$self.$$set = function ($$props) {
      if ('containerWidth' in $$props) $$invalidate(0, containerWidth = $$props.containerWidth);
      if ('myPlayer' in $$props) $$invalidate(4, myPlayer = $$props.myPlayer);
      if ('item' in $$props) $$invalidate(1, item = $$props.item);
    };
    $$self.$capture_state = function () {
      return {
        onMount: onMount,
        fly: fly,
        sendMetrik: sendMetrik$1,
        externalLink: externalLink,
        iframeLink: iframeLink,
        containerWidth: containerWidth,
        myPlayer: myPlayer,
        item: item,
        onClick: onClick,
        timeoutId: timeoutId,
        blockStyle: blockStyle,
        $externalLink: $externalLink,
        $iframeLink: $iframeLink
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('containerWidth' in $$props) $$invalidate(0, containerWidth = $$props.containerWidth);
      if ('myPlayer' in $$props) $$invalidate(4, myPlayer = $$props.myPlayer);
      if ('item' in $$props) $$invalidate(1, item = $$props.item);
      if ('timeoutId' in $$props) timeoutId = $$props.timeoutId;
      if ('blockStyle' in $$props) $$invalidate(2, blockStyle = $$props.blockStyle);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    $$self.$$.update = function () {
      if ($$self.$$.dirty & /*item, containerWidth*/3) {
        $$invalidate(2, blockStyle = "width: ".concat(item.width, "%; padding: ").concat(containerWidth * item.paddingVertical, "px ").concat(containerWidth * item.paddingHorizontal, "px; top:").concat(item.top, "%; right: ").concat(item.right, "%;"));
      }
    };
    return [containerWidth, item, blockStyle, onClick, myPlayer];
  }
  var Push = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(Push, _SvelteComponentDev);
    var _super = _createSuper$c(Push);
    function Push(options) {
      var _this;
      _classCallCheck(this, Push);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$c, create_fragment$c, safe_not_equal, {
        containerWidth: 0,
        myPlayer: 4,
        item: 1
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "Push",
        options: options,
        id: create_fragment$c.name
      });
      return _this;
    }
    _createClass(Push, [{
      key: "containerWidth",
      get: function get() {
        throw new Error("<Push>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Push>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "myPlayer",
      get: function get() {
        throw new Error("<Push>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Push>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "item",
      get: function get() {
        throw new Error("<Push>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Push>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return Push;
  }(SvelteComponentDev);

  function _createSuper$b(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$b(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$b() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$b = "src/ActiveArea/MidRoll.svelte";

  // (95:0) {#if isAdVisible}
  function create_if_block$7(ctx) {
    var a;
    var video;
    var t;
    var current_block_type_index;
    var if_block;
    var a_href_value;
    var current;
    var mounted;
    var dispose;
    var if_block_creators = [create_if_block_1$1, create_else_block];
    var if_blocks = [];
    function select_block_type(ctx, dirty) {
      if ( /*isTimerVisible*/ctx[4]) return 0;
      return 1;
    }
    current_block_type_index = select_block_type(ctx);
    if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    var block = {
      c: function create() {
        a = element("a");
        video = element("video");
        t = space();
        if_block.c();
        attr_dev(video, "id", "midroll");
        attr_dev(video, "class", "azuremediaplayer amp-default-skin");
        video.playsInline = true;
        add_location(video, file$b, 102, 4, 2742);
        attr_dev(a, "class", "overlay svelte-5kg4vc");
        attr_dev(a, "href", a_href_value = /*item*/ctx[0].href);
        attr_dev(a, "target", "_blank");
        add_location(a, file$b, 95, 2, 2573);
      },
      m: function mount(target, anchor) {
        insert_dev(target, a, anchor);
        append_dev(a, video);
        append_dev(a, t);
        if_blocks[current_block_type_index].m(a, null);
        /*a_binding*/
        ctx[10](a);
        current = true;
        if (!mounted) {
          dispose = listen_dev(a, "click", /*onClick*/ctx[6], false, false, false, false);
          mounted = true;
        }
      },
      p: function update(ctx, dirty) {
        var previous_block_index = current_block_type_index;
        current_block_type_index = select_block_type(ctx);
        if (current_block_type_index === previous_block_index) {
          if_blocks[current_block_type_index].p(ctx, dirty);
        } else {
          group_outros();
          transition_out(if_blocks[previous_block_index], 1, 1, function () {
            if_blocks[previous_block_index] = null;
          });
          check_outros();
          if_block = if_blocks[current_block_type_index];
          if (!if_block) {
            if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
            if_block.c();
          } else {
            if_block.p(ctx, dirty);
          }
          transition_in(if_block, 1);
          if_block.m(a, null);
        }
        if (!current || dirty & /*item*/1 && a_href_value !== (a_href_value = /*item*/ctx[0].href)) {
          attr_dev(a, "href", a_href_value);
        }
      },
      i: function intro(local) {
        if (current) return;
        transition_in(if_block);
        current = true;
      },
      o: function outro(local) {
        transition_out(if_block);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(a);
        if_blocks[current_block_type_index].d();
        /*a_binding*/
        ctx[10](null);
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block$7.name,
      type: "if",
      source: "(95:0) {#if isAdVisible}",
      ctx: ctx
    });
    return block;
  }

  // (119:4) {:else}
  function create_else_block(ctx) {
    var div1;
    var img;
    var img_src_value;
    var t;
    var div0;
    var div0_style_value;
    var mounted;
    var dispose;
    var block = {
      c: function create() {
        div1 = element("div");
        img = element("img");
        t = space();
        div0 = element("div");
        if (!src_url_equal(img.src, img_src_value = "".concat(BASE_URL, "/images/continuewhite.png"))) attr_dev(img, "src", img_src_value);
        attr_dev(img, "alt", "continue");
        attr_dev(img, "class", "closeImage svelte-5kg4vc");
        add_location(img, file$b, 120, 8, 3447);
        attr_dev(div0, "class", "shine svelte-5kg4vc");
        attr_dev(div0, "style", div0_style_value = "border-radius:".concat( /*containerWidth*/ctx[1] * 0.05, "px"));
        add_location(div0, file$b, 124, 8, 3572);
        attr_dev(div1, "class", "closeButton svelte-5kg4vc");
        add_location(div1, file$b, 119, 6, 3376);
      },
      m: function mount(target, anchor) {
        insert_dev(target, div1, anchor);
        append_dev(div1, img);
        append_dev(div1, t);
        append_dev(div1, div0);
        if (!mounted) {
          dispose = listen_dev(div1, "click", prevent_default( /*onVideoEnd*/ctx[7]), false, true, false, false);
          mounted = true;
        }
      },
      p: function update(ctx, dirty) {
        if (dirty & /*containerWidth*/2 && div0_style_value !== (div0_style_value = "border-radius:".concat( /*containerWidth*/ctx[1] * 0.05, "px"))) {
          attr_dev(div0, "style", div0_style_value);
        }
      },
      i: noop,
      o: noop,
      d: function destroy(detaching) {
        if (detaching) detach_dev(div1);
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_else_block.name,
      type: "else",
      source: "(119:4) {:else}",
      ctx: ctx
    });
    return block;
  }

  // (105:4) {#if isTimerVisible}
  function create_if_block_1$1(ctx) {
    var img;
    var img_src_value;
    var t0;
    var div;
    var timer;
    var t1;
    var if_block_anchor;
    var current;
    timer = new Timer({
      props: {
        duration: 5
      },
      $$inline: true
    });
    timer.$on("timerend", /*timerEnd*/ctx[8]);
    var if_block = /*isRssVisible*/ctx[5] && create_if_block_2(ctx);
    var block = {
      c: function create() {
        img = element("img");
        t0 = space();
        div = element("div");
        create_component(timer.$$.fragment);
        t1 = space();
        if (if_block) if_block.c();
        if_block_anchor = empty();
        if (!src_url_equal(img.src, img_src_value = "".concat(BASE_URL, "/images/skip.png"))) attr_dev(img, "src", img_src_value);
        attr_dev(img, "alt", "cards");
        attr_dev(img, "class", "continue svelte-5kg4vc");
        add_location(img, file$b, 105, 6, 2851);
        attr_dev(div, "class", "timer svelte-5kg4vc");
        add_location(div, file$b, 106, 6, 2930);
      },
      m: function mount(target, anchor) {
        insert_dev(target, img, anchor);
        insert_dev(target, t0, anchor);
        insert_dev(target, div, anchor);
        mount_component(timer, div, null);
        insert_dev(target, t1, anchor);
        if (if_block) if_block.m(target, anchor);
        insert_dev(target, if_block_anchor, anchor);
        current = true;
      },
      p: function update(ctx, dirty) {
        if ( /*isRssVisible*/ctx[5]) {
          if (if_block) {
            if_block.p(ctx, dirty);
            if (dirty & /*isRssVisible*/32) {
              transition_in(if_block, 1);
            }
          } else {
            if_block = create_if_block_2(ctx);
            if_block.c();
            transition_in(if_block, 1);
            if_block.m(if_block_anchor.parentNode, if_block_anchor);
          }
        } else if (if_block) {
          group_outros();
          transition_out(if_block, 1, 1, function () {
            if_block = null;
          });
          check_outros();
        }
      },
      i: function intro(local) {
        if (current) return;
        transition_in(timer.$$.fragment, local);
        transition_in(if_block);
        current = true;
      },
      o: function outro(local) {
        transition_out(timer.$$.fragment, local);
        transition_out(if_block);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(img);
        if (detaching) detach_dev(t0);
        if (detaching) detach_dev(div);
        destroy_component(timer);
        if (detaching) detach_dev(t1);
        if (if_block) if_block.d(detaching);
        if (detaching) detach_dev(if_block_anchor);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block_1$1.name,
      type: "if",
      source: "(105:4) {#if isTimerVisible}",
      ctx: ctx
    });
    return block;
  }

  // (110:6) {#if isRssVisible}
  function create_if_block_2(ctx) {
    var div;
    var t;
    var div_style_value;
    var div_intro;
    var div_outro;
    var current;
    var block = {
      c: function create() {
        div = element("div");
        t = text("Кликните для перехода на сайт рекламодателя");
        attr_dev(div, "class", "rss svelte-5kg4vc");
        attr_dev(div, "style", div_style_value = "font-size:".concat( /*containerWidth*/ctx[1] * 0.015, "px;margin-bottom:").concat( /*containerWidth*/ctx[1] * 0.009, "px"));
        add_location(div, file$b, 110, 8, 3050);
      },
      m: function mount(target, anchor) {
        insert_dev(target, div, anchor);
        append_dev(div, t);
        current = true;
      },
      p: function update(new_ctx, dirty) {
        ctx = new_ctx;
        if (!current || dirty & /*containerWidth*/2 && div_style_value !== (div_style_value = "font-size:".concat( /*containerWidth*/ctx[1] * 0.015, "px;margin-bottom:").concat( /*containerWidth*/ctx[1] * 0.009, "px"))) {
          attr_dev(div, "style", div_style_value);
        }
      },
      i: function intro(local) {
        if (current) return;
        if (local) {
          add_render_callback(function () {
            if (!current) return;
            if (div_outro) div_outro.end(1);
            div_intro = create_in_transition(div, fly, {
              x: /*containerWidth*/ctx[1]
            });
            div_intro.start();
          });
        }
        current = true;
      },
      o: function outro(local) {
        if (div_intro) div_intro.invalidate();
        if (local) {
          div_outro = create_out_transition(div, fly, {
            x: /*containerWidth*/ctx[1] * 0.5
          });
        }
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div);
        if (detaching && div_outro) div_outro.end();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block_2.name,
      type: "if",
      source: "(110:6) {#if isRssVisible}",
      ctx: ctx
    });
    return block;
  }
  function create_fragment$b(ctx) {
    var if_block_anchor;
    var current;
    var if_block = /*isAdVisible*/ctx[2] && create_if_block$7(ctx);
    var block = {
      c: function create() {
        if (if_block) if_block.c();
        if_block_anchor = empty();
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        if (if_block) if_block.m(target, anchor);
        insert_dev(target, if_block_anchor, anchor);
        current = true;
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if ( /*isAdVisible*/ctx[2]) {
          if (if_block) {
            if_block.p(ctx, dirty);
            if (dirty & /*isAdVisible*/4) {
              transition_in(if_block, 1);
            }
          } else {
            if_block = create_if_block$7(ctx);
            if_block.c();
            transition_in(if_block, 1);
            if_block.m(if_block_anchor.parentNode, if_block_anchor);
          }
        } else if (if_block) {
          group_outros();
          transition_out(if_block, 1, 1, function () {
            if_block = null;
          });
          check_outros();
        }
      },
      i: function intro(local) {
        if (current) return;
        transition_in(if_block);
        current = true;
      },
      o: function outro(local) {
        transition_out(if_block);
        current = false;
      },
      d: function destroy(detaching) {
        if (if_block) if_block.d(detaching);
        if (detaching) detach_dev(if_block_anchor);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$b.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$b($$self, $$props, $$invalidate) {
    var $externalLink;
    var $iframeLink;
    validate_store(externalLink, 'externalLink');
    component_subscribe($$self, externalLink, function ($$value) {
      return $$invalidate(14, $externalLink = $$value);
    });
    validate_store(iframeLink, 'iframeLink');
    component_subscribe($$self, iframeLink, function ($$value) {
      return $$invalidate(15, $iframeLink = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('MidRoll', slots, []);
    var item = $$props.item;
    var myPlayer = $$props.myPlayer;
    var containerWidth = $$props.containerWidth;
    var midrollPlayer;
    var isAdVisible = true;
    var overlayRef;
    var currentTime;
    var previousMetrikSecond;
    var isTimerVisible = true;
    var isRssVisible = false;
    setTimeout(function () {
      return $$invalidate(5, isRssVisible = true);
    }, 1000);
    setTimeout(function () {
      return $$invalidate(5, isRssVisible = false);
    }, 6000);
    var onClick = function onClick(e) {
      $$invalidate(2, isAdVisible = false);
      myPlayer.pause();
      $$invalidate(9, myPlayer.controlBar.el_.style.display = "", myPlayer);
      sendMetrik$1("MidrollClick", {
        "Gambler.BannerClickLink": item.href
      });
      if (item.showInIframe) {
        e.preventDefault();
        set_store_value(iframeLink, $iframeLink = item.href, $iframeLink);
      } else {
        set_store_value(externalLink, $externalLink = item.href, $externalLink);
      }
    };
    var onVideoEnd = function onVideoEnd(e) {
      sendMetrik$1("MidrollSkip", {
        "Gambler.MidrollSkipSecond": Math.floor(currentTime)
      });
      e.stopPropagation();
      $$invalidate(2, isAdVisible = false);
      myPlayer.play();
      $$invalidate(9, myPlayer.controlBar.el_.style.display = "", myPlayer);
    };
    var timerEnd = function timerEnd() {
      $$invalidate(4, isTimerVisible = false);
    };
    var onTimeUpdate = function onTimeUpdate() {
      currentTime = midrollPlayer.currentTime();
      var newMetrikSecond = Math.floor(currentTime);
      if (newMetrikSecond !== previousMetrikSecond) {
        previousMetrikSecond = newMetrikSecond;
        sendMetrik$1("MidrollWatch", {
          "Gambler.MidrollWatchSecond": newMetrikSecond
        });
      }
    };
    var init = function init() {
      if (!window.amp || !overlayRef) {
        return setTimeout(init, 10);
      }
      midrollPlayer = window.amp("midroll", {
        nativeControlsForTouch: false,
        autoplay: true,
        controls: false,
        fluid: true,
        poster: false,
        width: "640",
        logo: {
          enabled: false
        }
      }, function () {
        myPlayer.pause();
        $$invalidate(9, myPlayer.controlBar.el_.style.display = "none", myPlayer);
        window.midrollPlayer = midrollPlayer;
        this.removeEventListener("ended", onVideoEnd);
        this.addEventListener("ended", onVideoEnd);
        this.removeEventListener("timeupdate", onTimeUpdate);
        this.addEventListener("timeupdate", onTimeUpdate);
      });
      midrollPlayer.src([{
        src: item.src,
        type: "application/vnd.ms-sstr+xml"
      }]);
    };
    init();
    sendMetrik$1("MidrollStart");
    onMount(function () {
      return function () {
        midrollPlayer.dispose();
      };
    });
    $$self.$$.on_mount.push(function () {
      if (item === undefined && !('item' in $$props || $$self.$$.bound[$$self.$$.props['item']])) {
        console.warn("<MidRoll> was created without expected prop 'item'");
      }
      if (myPlayer === undefined && !('myPlayer' in $$props || $$self.$$.bound[$$self.$$.props['myPlayer']])) {
        console.warn("<MidRoll> was created without expected prop 'myPlayer'");
      }
      if (containerWidth === undefined && !('containerWidth' in $$props || $$self.$$.bound[$$self.$$.props['containerWidth']])) {
        console.warn("<MidRoll> was created without expected prop 'containerWidth'");
      }
    });
    var writable_props = ['item', 'myPlayer', 'containerWidth'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<MidRoll> was created with unknown prop '".concat(key, "'"));
    });
    function a_binding($$value) {
      binding_callbacks[$$value ? 'unshift' : 'push'](function () {
        overlayRef = $$value;
        $$invalidate(3, overlayRef);
      });
    }
    $$self.$$set = function ($$props) {
      if ('item' in $$props) $$invalidate(0, item = $$props.item);
      if ('myPlayer' in $$props) $$invalidate(9, myPlayer = $$props.myPlayer);
      if ('containerWidth' in $$props) $$invalidate(1, containerWidth = $$props.containerWidth);
    };
    $$self.$capture_state = function () {
      return {
        onMount: onMount,
        BASE_URL: BASE_URL,
        iframeLink: iframeLink,
        externalLink: externalLink,
        sendMetrik: sendMetrik$1,
        fly: fly,
        Timer: Timer,
        item: item,
        myPlayer: myPlayer,
        containerWidth: containerWidth,
        midrollPlayer: midrollPlayer,
        isAdVisible: isAdVisible,
        overlayRef: overlayRef,
        currentTime: currentTime,
        previousMetrikSecond: previousMetrikSecond,
        isTimerVisible: isTimerVisible,
        isRssVisible: isRssVisible,
        onClick: onClick,
        onVideoEnd: onVideoEnd,
        timerEnd: timerEnd,
        onTimeUpdate: onTimeUpdate,
        init: init,
        $externalLink: $externalLink,
        $iframeLink: $iframeLink
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('item' in $$props) $$invalidate(0, item = $$props.item);
      if ('myPlayer' in $$props) $$invalidate(9, myPlayer = $$props.myPlayer);
      if ('containerWidth' in $$props) $$invalidate(1, containerWidth = $$props.containerWidth);
      if ('midrollPlayer' in $$props) midrollPlayer = $$props.midrollPlayer;
      if ('isAdVisible' in $$props) $$invalidate(2, isAdVisible = $$props.isAdVisible);
      if ('overlayRef' in $$props) $$invalidate(3, overlayRef = $$props.overlayRef);
      if ('currentTime' in $$props) currentTime = $$props.currentTime;
      if ('previousMetrikSecond' in $$props) previousMetrikSecond = $$props.previousMetrikSecond;
      if ('isTimerVisible' in $$props) $$invalidate(4, isTimerVisible = $$props.isTimerVisible);
      if ('isRssVisible' in $$props) $$invalidate(5, isRssVisible = $$props.isRssVisible);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    return [item, containerWidth, isAdVisible, overlayRef, isTimerVisible, isRssVisible, onClick, onVideoEnd, timerEnd, myPlayer, a_binding];
  }
  var MidRoll = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(MidRoll, _SvelteComponentDev);
    var _super = _createSuper$b(MidRoll);
    function MidRoll(options) {
      var _this;
      _classCallCheck(this, MidRoll);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$b, create_fragment$b, safe_not_equal, {
        item: 0,
        myPlayer: 9,
        containerWidth: 1
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "MidRoll",
        options: options,
        id: create_fragment$b.name
      });
      return _this;
    }
    _createClass(MidRoll, [{
      key: "item",
      get: function get() {
        throw new Error("<MidRoll>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<MidRoll>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "myPlayer",
      get: function get() {
        throw new Error("<MidRoll>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<MidRoll>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "containerWidth",
      get: function get() {
        throw new Error("<MidRoll>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<MidRoll>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return MidRoll;
  }(SvelteComponentDev);

  function _createSuper$a(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$a(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$a() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$a = "src/ActiveArea/DisableAd.svelte";

  // (37:0) {#if isVisible}
  function create_if_block$6(ctx) {
    var div1;
    var img0;
    var img0_src_value;
    var t0;
    var img1;
    var img1_src_value;
    var t1;
    var img2;
    var img2_src_value;
    var t2;
    var img3;
    var img3_src_value;
    var t3;
    var img4;
    var img4_src_value;
    var t4;
    var img5;
    var img5_src_value;
    var t5;
    var img6;
    var img6_src_value;
    var t6;
    var img7;
    var img7_src_value;
    var t7;
    var img8;
    var img8_src_value;
    var t8;
    var div0;
    var timer;
    var current;
    var mounted;
    var dispose;
    timer = new Timer({
      props: {
        duration: 10
      },
      $$inline: true
    });
    timer.$on("timerend", /*onSkip*/ctx[3]);
    var block = {
      c: function create() {
        div1 = element("div");
        img0 = element("img");
        t0 = space();
        img1 = element("img");
        t1 = space();
        img2 = element("img");
        t2 = space();
        img3 = element("img");
        t3 = space();
        img4 = element("img");
        t4 = space();
        img5 = element("img");
        t5 = space();
        img6 = element("img");
        t6 = space();
        img7 = element("img");
        t7 = space();
        img8 = element("img");
        t8 = space();
        div0 = element("div");
        create_component(timer.$$.fragment);
        if (!src_url_equal(img0.src, img0_src_value = "".concat(BASE_URL, "/images/chip3.png"))) attr_dev(img0, "src", img0_src_value);
        attr_dev(img0, "alt", "chip1");
        attr_dev(img0, "class", "chip3 svelte-5ronmc");
        add_location(img0, file$a, 38, 4, 941);
        if (!src_url_equal(img1.src, img1_src_value = "".concat(BASE_URL, "/images/chip1.png"))) attr_dev(img1, "src", img1_src_value);
        attr_dev(img1, "alt", "chip1");
        attr_dev(img1, "class", "chip1 svelte-5ronmc");
        add_location(img1, file$a, 39, 4, 1016);
        if (!src_url_equal(img2.src, img2_src_value = "".concat(BASE_URL, "/images/chip4.png"))) attr_dev(img2, "src", img2_src_value);
        attr_dev(img2, "alt", "chip1");
        attr_dev(img2, "class", "chip4 svelte-5ronmc");
        add_location(img2, file$a, 40, 4, 1091);
        if (!src_url_equal(img3.src, img3_src_value = "".concat(BASE_URL, "/images/chip2.png"))) attr_dev(img3, "src", img3_src_value);
        attr_dev(img3, "alt", "chip1");
        attr_dev(img3, "class", "chip2 svelte-5ronmc");
        add_location(img3, file$a, 41, 4, 1166);
        if (!src_url_equal(img4.src, img4_src_value = "".concat(BASE_URL, "/images/logo.png"))) attr_dev(img4, "src", img4_src_value);
        attr_dev(img4, "alt", "logo");
        attr_dev(img4, "class", "logo svelte-5ronmc");
        add_location(img4, file$a, 42, 4, 1241);
        if (!src_url_equal(img5.src, img5_src_value = "".concat(BASE_URL, "/images/disable.png"))) attr_dev(img5, "src", img5_src_value);
        attr_dev(img5, "alt", "cards");
        attr_dev(img5, "class", "cards svelte-5ronmc");
        add_location(img5, file$a, 43, 4, 1313);
        if (!src_url_equal(img6.src, img6_src_value = "".concat(BASE_URL, "/images/yes.png"))) attr_dev(img6, "src", img6_src_value);
        attr_dev(img6, "alt", "cards");
        attr_dev(img6, "class", "yes svelte-5ronmc");
        add_location(img6, file$a, 45, 4, 1391);
        if (!src_url_equal(img7.src, img7_src_value = "".concat(BASE_URL, "/images/no.png"))) attr_dev(img7, "src", img7_src_value);
        attr_dev(img7, "alt", "cards");
        attr_dev(img7, "class", "no svelte-5ronmc");
        add_location(img7, file$a, 50, 4, 1507);
        if (!src_url_equal(img8.src, img8_src_value = "".concat(BASE_URL, "/images/continueblack.png"))) attr_dev(img8, "src", img8_src_value);
        attr_dev(img8, "alt", "cards");
        attr_dev(img8, "class", "continue appear svelte-5ronmc");
        add_location(img8, file$a, 55, 4, 1624);
        attr_dev(div0, "class", "timer svelte-5ronmc");
        add_location(div0, file$a, 60, 4, 1759);
        attr_dev(div1, "class", "container appear svelte-5ronmc");
        add_location(div1, file$a, 37, 2, 906);
      },
      m: function mount(target, anchor) {
        insert_dev(target, div1, anchor);
        append_dev(div1, img0);
        append_dev(div1, t0);
        append_dev(div1, img1);
        append_dev(div1, t1);
        append_dev(div1, img2);
        append_dev(div1, t2);
        append_dev(div1, img3);
        append_dev(div1, t3);
        append_dev(div1, img4);
        append_dev(div1, t4);
        append_dev(div1, img5);
        append_dev(div1, t5);
        append_dev(div1, img6);
        append_dev(div1, t6);
        append_dev(div1, img7);
        append_dev(div1, t7);
        append_dev(div1, img8);
        append_dev(div1, t8);
        append_dev(div1, div0);
        mount_component(timer, div0, null);
        current = true;
        if (!mounted) {
          dispose = [listen_dev(img6, "click", /*onDisable*/ctx[1], false, false, false, false), listen_dev(img7, "click", /*continueFilm*/ctx[2], false, false, false, false), listen_dev(img8, "click", /*onSkip*/ctx[3], false, false, false, false)];
          mounted = true;
        }
      },
      p: noop,
      i: function intro(local) {
        if (current) return;
        transition_in(timer.$$.fragment, local);
        current = true;
      },
      o: function outro(local) {
        transition_out(timer.$$.fragment, local);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div1);
        destroy_component(timer);
        mounted = false;
        run_all(dispose);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block$6.name,
      type: "if",
      source: "(37:0) {#if isVisible}",
      ctx: ctx
    });
    return block;
  }
  function create_fragment$a(ctx) {
    var if_block_anchor;
    var current;
    var if_block = /*isVisible*/ctx[0] && create_if_block$6(ctx);
    var block = {
      c: function create() {
        if (if_block) if_block.c();
        if_block_anchor = empty();
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        if (if_block) if_block.m(target, anchor);
        insert_dev(target, if_block_anchor, anchor);
        current = true;
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if ( /*isVisible*/ctx[0]) {
          if (if_block) {
            if_block.p(ctx, dirty);
            if (dirty & /*isVisible*/1) {
              transition_in(if_block, 1);
            }
          } else {
            if_block = create_if_block$6(ctx);
            if_block.c();
            transition_in(if_block, 1);
            if_block.m(if_block_anchor.parentNode, if_block_anchor);
          }
        } else if (if_block) {
          group_outros();
          transition_out(if_block, 1, 1, function () {
            if_block = null;
          });
          check_outros();
        }
      },
      i: function intro(local) {
        if (current) return;
        transition_in(if_block);
        current = true;
      },
      o: function outro(local) {
        transition_out(if_block);
        current = false;
      },
      d: function destroy(detaching) {
        if (if_block) if_block.d(detaching);
        if (detaching) detach_dev(if_block_anchor);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$a.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$a($$self, $$props, $$invalidate) {
    var $adDisabled;
    validate_store(adDisabled, 'adDisabled');
    component_subscribe($$self, adDisabled, function ($$value) {
      return $$invalidate(5, $adDisabled = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('DisableAd', slots, []);
    var myPlayer = $$props.myPlayer;
    var isVisible = true;
    sendMetrik$1("DisabledAdShow");
    var onDisable = function onDisable() {
      $$invalidate(4, myPlayer.controlBar.el_.style.display = "", myPlayer);
      set_store_value(adDisabled, $adDisabled = true, $adDisabled);
      sendMetrik$1("DisabledAdClickYes");
      $$invalidate(0, isVisible = false);
      myPlayer.play();
    };
    var continueFilm = function continueFilm() {
      $$invalidate(4, myPlayer.controlBar.el_.style.display = "", myPlayer);
      sendMetrik$1("DisabledAdClickNo");
      $$invalidate(0, isVisible = false);
      myPlayer.play();
    };
    var onSkip = function onSkip() {
      $$invalidate(4, myPlayer.controlBar.el_.style.display = "", myPlayer);
      $$invalidate(0, isVisible = false);
      myPlayer.play();
    };
    onMount(function () {
      $$invalidate(4, myPlayer.controlBar.el_.style.display = "none", myPlayer);
      myPlayer.pause();
      return function () {
        $$invalidate(4, myPlayer.controlBar.el_.style.display = "", myPlayer);
      };
    });
    $$self.$$.on_mount.push(function () {
      if (myPlayer === undefined && !('myPlayer' in $$props || $$self.$$.bound[$$self.$$.props['myPlayer']])) {
        console.warn("<DisableAd> was created without expected prop 'myPlayer'");
      }
    });
    var writable_props = ['myPlayer'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<DisableAd> was created with unknown prop '".concat(key, "'"));
    });
    $$self.$$set = function ($$props) {
      if ('myPlayer' in $$props) $$invalidate(4, myPlayer = $$props.myPlayer);
    };
    $$self.$capture_state = function () {
      return {
        BASE_URL: BASE_URL,
        sendMetrik: sendMetrik$1,
        adDisabled: adDisabled,
        onMount: onMount,
        Timer: Timer,
        myPlayer: myPlayer,
        isVisible: isVisible,
        onDisable: onDisable,
        continueFilm: continueFilm,
        onSkip: onSkip,
        $adDisabled: $adDisabled
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('myPlayer' in $$props) $$invalidate(4, myPlayer = $$props.myPlayer);
      if ('isVisible' in $$props) $$invalidate(0, isVisible = $$props.isVisible);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    return [isVisible, onDisable, continueFilm, onSkip, myPlayer];
  }
  var DisableAd = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(DisableAd, _SvelteComponentDev);
    var _super = _createSuper$a(DisableAd);
    function DisableAd(options) {
      var _this;
      _classCallCheck(this, DisableAd);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$a, create_fragment$a, safe_not_equal, {
        myPlayer: 4
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "DisableAd",
        options: options,
        id: create_fragment$a.name
      });
      return _this;
    }
    _createClass(DisableAd, [{
      key: "myPlayer",
      get: function get() {
        throw new Error("<DisableAd>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<DisableAd>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return DisableAd;
  }(SvelteComponentDev);

  var getOwnPropertyNames = objectGetOwnPropertyNames.f;
  var getOwnPropertyDescriptor = objectGetOwnPropertyDescriptor.f;
  var defineProperty = objectDefineProperty.f;

  var trim = stringTrim.trim;

  var NUMBER = 'Number';
  var NativeNumber = global_1[NUMBER];
  path[NUMBER];
  var NumberPrototype = NativeNumber.prototype;
  var TypeError$1 = global_1.TypeError;
  var stringSlice = functionUncurryThis(''.slice);
  var charCodeAt = functionUncurryThis(''.charCodeAt);

  // `ToNumeric` abstract operation
  // https://tc39.es/ecma262/#sec-tonumeric
  var toNumeric = function (value) {
    var primValue = toPrimitive(value, 'number');
    return typeof primValue == 'bigint' ? primValue : toNumber(primValue);
  };

  // `ToNumber` abstract operation
  // https://tc39.es/ecma262/#sec-tonumber
  var toNumber = function (argument) {
    var it = toPrimitive(argument, 'number');
    var first, third, radix, maxCode, digits, length, index, code;
    if (isSymbol(it)) throw TypeError$1('Cannot convert a Symbol value to a number');
    if (typeof it == 'string' && it.length > 2) {
      it = trim(it);
      first = charCodeAt(it, 0);
      if (first === 43 || first === 45) {
        third = charCodeAt(it, 2);
        if (third === 88 || third === 120) return NaN; // Number('+0x1') should be NaN, old V8 fix
      } else if (first === 48) {
        switch (charCodeAt(it, 1)) {
          case 66: case 98: radix = 2; maxCode = 49; break; // fast equal of /^0b[01]+$/i
          case 79: case 111: radix = 8; maxCode = 55; break; // fast equal of /^0o[0-7]+$/i
          default: return +it;
        }
        digits = stringSlice(it, 2);
        length = digits.length;
        for (index = 0; index < length; index++) {
          code = charCodeAt(digits, index);
          // parseInt parses a string to a first unavailable symbol
          // but ToNumber should return NaN if a string contains unavailable symbols
          if (code < 48 || code > maxCode) return NaN;
        } return parseInt(digits, radix);
      }
    } return +it;
  };

  var FORCED = isForced_1(NUMBER, !NativeNumber(' 0o1') || !NativeNumber('0b1') || NativeNumber('+0x1'));

  var calledWithNew = function (dummy) {
    // includes check on 1..constructor(foo) case
    return objectIsPrototypeOf(NumberPrototype, dummy) && fails(function () { thisNumberValue(dummy); });
  };

  // `Number` constructor
  // https://tc39.es/ecma262/#sec-number-constructor
  var NumberWrapper = function Number(value) {
    var n = arguments.length < 1 ? 0 : NativeNumber(toNumeric(value));
    return calledWithNew(this) ? inheritIfRequired(Object(n), this, NumberWrapper) : n;
  };

  NumberWrapper.prototype = NumberPrototype;
  if (FORCED && !isPure) NumberPrototype.constructor = NumberWrapper;

  _export({ global: true, constructor: true, wrap: true, forced: FORCED }, {
    Number: NumberWrapper
  });

  // Use `internal/copy-constructor-properties` helper in `core-js@4`
  var copyConstructorProperties = function (target, source) {
    for (var keys = descriptors ? getOwnPropertyNames(source) : (
      // ES3:
      'MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,' +
      // ES2015 (in case, if modules with ES2015 Number statics required before):
      'EPSILON,MAX_SAFE_INTEGER,MIN_SAFE_INTEGER,isFinite,isInteger,isNaN,isSafeInteger,parseFloat,parseInt,' +
      // ESNext
      'fromString,range'
    ).split(','), j = 0, key; keys.length > j; j++) {
      if (hasOwnProperty_1(source, key = keys[j]) && !hasOwnProperty_1(target, key)) {
        defineProperty(target, key, getOwnPropertyDescriptor(source, key));
      }
    }
  };
  if (FORCED || isPure) copyConstructorProperties(path[NUMBER], NativeNumber);

  function _createSuper$9(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$9(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$9() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$9 = "src/ActiveArea/ContinueWatch.svelte";

  // (27:0) {#if isVisible}
  function create_if_block$5(ctx) {
    var div;
    var img0;
    var img0_src_value;
    var t0;
    var img1;
    var img1_src_value;
    var t1;
    var img2;
    var img2_src_value;
    var div_intro;
    var div_outro;
    var current;
    var mounted;
    var dispose;
    var block = {
      c: function create() {
        div = element("div");
        img0 = element("img");
        t0 = space();
        img1 = element("img");
        t1 = space();
        img2 = element("img");
        if (!src_url_equal(img0.src, img0_src_value = "".concat(BASE_URL, "/images/continuewatch.png"))) attr_dev(img0, "src", img0_src_value);
        attr_dev(img0, "alt", "continuewatch");
        attr_dev(img0, "class", "continue svelte-yivuvr");
        add_location(img0, file$9, 31, 4, 693);
        if (!src_url_equal(img1.src, img1_src_value = "".concat(BASE_URL, "/images/continueyes.png"))) attr_dev(img1, "src", img1_src_value);
        attr_dev(img1, "alt", "continuewatch");
        attr_dev(img1, "class", "continueyes svelte-yivuvr");
        add_location(img1, file$9, 35, 4, 805);
        if (!src_url_equal(img2.src, img2_src_value = "".concat(BASE_URL, "/images/continueno.png"))) attr_dev(img2, "src", img2_src_value);
        attr_dev(img2, "alt", "continuewatch");
        attr_dev(img2, "class", "continueno svelte-yivuvr");
        add_location(img2, file$9, 40, 4, 949);
        attr_dev(div, "class", "container svelte-yivuvr");
        add_location(div, file$9, 27, 2, 572);
      },
      m: function mount(target, anchor) {
        insert_dev(target, div, anchor);
        append_dev(div, img0);
        append_dev(div, t0);
        append_dev(div, img1);
        append_dev(div, t1);
        append_dev(div, img2);
        current = true;
        if (!mounted) {
          dispose = [listen_dev(img1, "click", /*continueWatch*/ctx[2], false, false, false, false), listen_dev(img2, "click", /*skip*/ctx[3], false, false, false, false)];
          mounted = true;
        }
      },
      p: function update(new_ctx, dirty) {
        ctx = new_ctx;
      },
      i: function intro(local) {
        if (current) return;
        if (local) {
          add_render_callback(function () {
            if (!current) return;
            if (div_outro) div_outro.end(1);
            div_intro = create_in_transition(div, fly, {
              x: /*containerWidth*/ctx[0]
            });
            div_intro.start();
          });
        }
        current = true;
      },
      o: function outro(local) {
        if (div_intro) div_intro.invalidate();
        if (local) {
          div_outro = create_out_transition(div, fly, {
            x: /*containerWidth*/ctx[0] * 0.5
          });
        }
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div);
        if (detaching && div_outro) div_outro.end();
        mounted = false;
        run_all(dispose);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block$5.name,
      type: "if",
      source: "(27:0) {#if isVisible}",
      ctx: ctx
    });
    return block;
  }
  function create_fragment$9(ctx) {
    var if_block_anchor;
    var if_block = /*isVisible*/ctx[1] && create_if_block$5(ctx);
    var block = {
      c: function create() {
        if (if_block) if_block.c();
        if_block_anchor = empty();
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        if (if_block) if_block.m(target, anchor);
        insert_dev(target, if_block_anchor, anchor);
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if ( /*isVisible*/ctx[1]) {
          if (if_block) {
            if_block.p(ctx, dirty);
            if (dirty & /*isVisible*/2) {
              transition_in(if_block, 1);
            }
          } else {
            if_block = create_if_block$5(ctx);
            if_block.c();
            transition_in(if_block, 1);
            if_block.m(if_block_anchor.parentNode, if_block_anchor);
          }
        } else if (if_block) {
          group_outros();
          transition_out(if_block, 1, 1, function () {
            if_block = null;
          });
          check_outros();
        }
      },
      i: function intro(local) {
        transition_in(if_block);
      },
      o: function outro(local) {
        transition_out(if_block);
      },
      d: function destroy(detaching) {
        if (if_block) if_block.d(detaching);
        if (detaching) detach_dev(if_block_anchor);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$9.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$9($$self, $$props, $$invalidate) {
    var $gamblerLastTime;
    var $activeIndex;
    var $players;
    var $gamblerLastSeries;
    validate_store(gamblerLastTime, 'gamblerLastTime');
    component_subscribe($$self, gamblerLastTime, function ($$value) {
      return $$invalidate(4, $gamblerLastTime = $$value);
    });
    validate_store(activeIndex, 'activeIndex');
    component_subscribe($$self, activeIndex, function ($$value) {
      return $$invalidate(5, $activeIndex = $$value);
    });
    validate_store(players, 'players');
    component_subscribe($$self, players, function ($$value) {
      return $$invalidate(6, $players = $$value);
    });
    validate_store(gamblerLastSeries, 'gamblerLastSeries');
    component_subscribe($$self, gamblerLastSeries, function ($$value) {
      return $$invalidate(7, $gamblerLastSeries = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('ContinueWatch', slots, []);
    var containerWidth = $$props.containerWidth;
    var isVisible = Boolean($gamblerLastTime);
    var continueWatch = function continueWatch() {
      $$invalidate(1, isVisible = false);
      set_store_value(activeIndex, $activeIndex = Number($gamblerLastSeries), $activeIndex);
      $players[$activeIndex].play();
      setTimeout(function () {
        return $players[$activeIndex].currentTime(Number($gamblerLastTime));
      }, 500);
    };
    var skip = function skip() {
      $$invalidate(1, isVisible = false);
    };
    $$self.$$.on_mount.push(function () {
      if (containerWidth === undefined && !('containerWidth' in $$props || $$self.$$.bound[$$self.$$.props['containerWidth']])) {
        console.warn("<ContinueWatch> was created without expected prop 'containerWidth'");
      }
    });
    var writable_props = ['containerWidth'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<ContinueWatch> was created with unknown prop '".concat(key, "'"));
    });
    $$self.$$set = function ($$props) {
      if ('containerWidth' in $$props) $$invalidate(0, containerWidth = $$props.containerWidth);
    };
    $$self.$capture_state = function () {
      return {
        fly: fly,
        BASE_URL: BASE_URL,
        gamblerLastSeries: gamblerLastSeries,
        gamblerLastTime: gamblerLastTime,
        activeIndex: activeIndex,
        players: players,
        containerWidth: containerWidth,
        isVisible: isVisible,
        continueWatch: continueWatch,
        skip: skip,
        $gamblerLastTime: $gamblerLastTime,
        $activeIndex: $activeIndex,
        $players: $players,
        $gamblerLastSeries: $gamblerLastSeries
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('containerWidth' in $$props) $$invalidate(0, containerWidth = $$props.containerWidth);
      if ('isVisible' in $$props) $$invalidate(1, isVisible = $$props.isVisible);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    return [containerWidth, isVisible, continueWatch, skip];
  }
  var ContinueWatch = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(ContinueWatch, _SvelteComponentDev);
    var _super = _createSuper$9(ContinueWatch);
    function ContinueWatch(options) {
      var _this;
      _classCallCheck(this, ContinueWatch);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$9, create_fragment$9, safe_not_equal, {
        containerWidth: 0
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "ContinueWatch",
        options: options,
        id: create_fragment$9.name
      });
      return _this;
    }
    _createClass(ContinueWatch, [{
      key: "containerWidth",
      get: function get() {
        throw new Error("<ContinueWatch>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<ContinueWatch>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return ContinueWatch;
  }(SvelteComponentDev);

  function _createSuper$8(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$8(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$8() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$8 = "src/ActiveArea/SelectLocation1.svelte";
  function create_fragment$8(ctx) {
    var div1;
    var img0;
    var img0_src_value;
    var t0;
    var img1;
    var img1_src_value;
    var t1;
    var img2;
    var img2_src_value;
    var t2;
    var img3;
    var img3_src_value;
    var t3;
    var img4;
    var img4_src_value;
    var t4;
    var img5;
    var img5_src_value;
    var t5;
    var img6;
    var img6_src_value;
    var t6;
    var img7;
    var img7_src_value;
    var t7;
    var img8;
    var img8_src_value;
    var t8;
    var img9;
    var img9_src_value;
    var t9;
    var img10;
    var img10_src_value;
    var t10;
    var img11;
    var img11_src_value;
    var t11;
    var img12;
    var img12_src_value;
    var t12;
    var div0;
    var timer;
    var current;
    var mounted;
    var dispose;
    timer = new Timer({
      props: {
        duration: 10
      },
      $$inline: true
    });
    timer.$on("timerend", /*onSkip*/ctx[1]);
    var block = {
      c: function create() {
        div1 = element("div");
        img0 = element("img");
        t0 = space();
        img1 = element("img");
        t1 = space();
        img2 = element("img");
        t2 = space();
        img3 = element("img");
        t3 = space();
        img4 = element("img");
        t4 = space();
        img5 = element("img");
        t5 = space();
        img6 = element("img");
        t6 = space();
        img7 = element("img");
        t7 = space();
        img8 = element("img");
        t8 = space();
        img9 = element("img");
        t9 = space();
        img10 = element("img");
        t10 = space();
        img11 = element("img");
        t11 = space();
        img12 = element("img");
        t12 = space();
        div0 = element("div");
        create_component(timer.$$.fragment);
        if (!src_url_equal(img0.src, img0_src_value = "".concat(BASE_URL, "/images/chip3.png"))) attr_dev(img0, "src", img0_src_value);
        attr_dev(img0, "alt", "chip1");
        attr_dev(img0, "class", "chip3 svelte-1y4gd58");
        add_location(img0, file$8, 38, 2, 1414);
        if (!src_url_equal(img1.src, img1_src_value = "".concat(BASE_URL, "/images/chip1.png"))) attr_dev(img1, "src", img1_src_value);
        attr_dev(img1, "alt", "chip1");
        attr_dev(img1, "class", "chip1 svelte-1y4gd58");
        add_location(img1, file$8, 39, 2, 1487);
        if (!src_url_equal(img2.src, img2_src_value = "".concat(BASE_URL, "/images/chip4.png"))) attr_dev(img2, "src", img2_src_value);
        attr_dev(img2, "alt", "chip1");
        attr_dev(img2, "class", "chip4 svelte-1y4gd58");
        add_location(img2, file$8, 40, 2, 1560);
        if (!src_url_equal(img3.src, img3_src_value = "".concat(BASE_URL, "/images/chip2.png"))) attr_dev(img3, "src", img3_src_value);
        attr_dev(img3, "alt", "chip1");
        attr_dev(img3, "class", "chip2 svelte-1y4gd58");
        add_location(img3, file$8, 41, 2, 1633);
        if (!src_url_equal(img4.src, img4_src_value = "".concat(BASE_URL, "/images/logo.png"))) attr_dev(img4, "src", img4_src_value);
        attr_dev(img4, "alt", "logo");
        attr_dev(img4, "class", "logo svelte-1y4gd58");
        add_location(img4, file$8, 42, 2, 1706);
        if (!src_url_equal(img5.src, img5_src_value = "".concat(BASE_URL, "/images/wheretogo.png"))) attr_dev(img5, "src", img5_src_value);
        attr_dev(img5, "alt", "cards");
        attr_dev(img5, "class", "cards svelte-1y4gd58");
        add_location(img5, file$8, 43, 2, 1776);
        if (!src_url_equal(img6.src, img6_src_value = "".concat(BASE_URL, "/images/uralochaimg.png"))) attr_dev(img6, "src", img6_src_value);
        attr_dev(img6, "alt", "logo");
        attr_dev(img6, "class", "uralochaimg svelte-1y4gd58");
        add_location(img6, file$8, 45, 2, 1854);
        if (!src_url_equal(img7.src, img7_src_value = "".concat(BASE_URL, "/images/spbimg.png"))) attr_dev(img7, "src", img7_src_value);
        attr_dev(img7, "alt", "logo");
        attr_dev(img7, "class", "spbimg svelte-1y4gd58");
        add_location(img7, file$8, 46, 2, 1979);
        if (!src_url_equal(img8.src, img8_src_value = "".concat(BASE_URL, "/images/momimg.png"))) attr_dev(img8, "src", img8_src_value);
        attr_dev(img8, "alt", "logo");
        attr_dev(img8, "class", "momimg svelte-1y4gd58");
        add_location(img8, file$8, 47, 2, 2088);
        if (!src_url_equal(img9.src, img9_src_value = "".concat(BASE_URL, "/images/uralochka.png"))) attr_dev(img9, "src", img9_src_value);
        attr_dev(img9, "alt", "logo");
        attr_dev(img9, "class", "uralochka svelte-1y4gd58");
        add_location(img9, file$8, 49, 2, 2198);
        if (!src_url_equal(img10.src, img10_src_value = "".concat(BASE_URL, "/images/spb.png"))) attr_dev(img10, "src", img10_src_value);
        attr_dev(img10, "alt", "logo");
        attr_dev(img10, "class", "spb svelte-1y4gd58");
        add_location(img10, file$8, 50, 2, 2319);
        if (!src_url_equal(img11.src, img11_src_value = "".concat(BASE_URL, "/images/mom.png"))) attr_dev(img11, "src", img11_src_value);
        attr_dev(img11, "alt", "logo");
        attr_dev(img11, "class", "mom svelte-1y4gd58");
        add_location(img11, file$8, 51, 2, 2422);
        if (!src_url_equal(img12.src, img12_src_value = "".concat(BASE_URL, "/images/continueblack.png"))) attr_dev(img12, "src", img12_src_value);
        attr_dev(img12, "alt", "cards");
        attr_dev(img12, "class", "continue appear svelte-1y4gd58");
        add_location(img12, file$8, 53, 2, 2526);
        attr_dev(div0, "class", "timer svelte-1y4gd58");
        add_location(div0, file$8, 58, 4, 2661);
        attr_dev(div1, "class", "container appear svelte-1y4gd58");
        add_location(div1, file$8, 37, 0, 1381);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, div1, anchor);
        append_dev(div1, img0);
        append_dev(div1, t0);
        append_dev(div1, img1);
        append_dev(div1, t1);
        append_dev(div1, img2);
        append_dev(div1, t2);
        append_dev(div1, img3);
        append_dev(div1, t3);
        append_dev(div1, img4);
        append_dev(div1, t4);
        append_dev(div1, img5);
        append_dev(div1, t5);
        append_dev(div1, img6);
        append_dev(div1, t6);
        append_dev(div1, img7);
        append_dev(div1, t7);
        append_dev(div1, img8);
        append_dev(div1, t8);
        append_dev(div1, img9);
        append_dev(div1, t9);
        append_dev(div1, img10);
        append_dev(div1, t10);
        append_dev(div1, img11);
        append_dev(div1, t11);
        append_dev(div1, img12);
        append_dev(div1, t12);
        append_dev(div1, div0);
        mount_component(timer, div0, null);
        current = true;
        if (!mounted) {
          dispose = [listen_dev(img6, "click", /*click_handler*/ctx[3], false, false, false, false), listen_dev(img7, "click", /*click_handler_1*/ctx[4], false, false, false, false), listen_dev(img8, "click", /*click_handler_2*/ctx[5], false, false, false, false), listen_dev(img9, "click", /*click_handler_3*/ctx[6], false, false, false, false), listen_dev(img10, "click", /*click_handler_4*/ctx[7], false, false, false, false), listen_dev(img11, "click", /*click_handler_5*/ctx[8], false, false, false, false), listen_dev(img12, "click", /*onSkip*/ctx[1], false, false, false, false)];
          mounted = true;
        }
      },
      p: noop,
      i: function intro(local) {
        if (current) return;
        transition_in(timer.$$.fragment, local);
        current = true;
      },
      o: function outro(local) {
        transition_out(timer.$$.fragment, local);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div1);
        destroy_component(timer);
        mounted = false;
        run_all(dispose);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$8.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$8($$self, $$props, $$invalidate) {
    var $activeIndex;
    var $players;
    var $location1;
    validate_store(activeIndex, 'activeIndex');
    component_subscribe($$self, activeIndex, function ($$value) {
      return $$invalidate(9, $activeIndex = $$value);
    });
    validate_store(players, 'players');
    component_subscribe($$self, players, function ($$value) {
      return $$invalidate(10, $players = $$value);
    });
    validate_store(location1, 'location1');
    component_subscribe($$self, location1, function ($$value) {
      return $$invalidate(11, $location1 = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('SelectLocation1', slots, []);
    var myPlayer = $$props.myPlayer;
    myPlayer.pause();
    var variants = {
      uralochka: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/5855ef06-477f-4543-9145-41c14c60528b/1ural.ism/manifest',
      spb: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/6551d6c7-1e9a-477d-a3f6-8650a2f860c3/Gambler Otvet1 SaintPetersburg.ism/manifest',
      mom: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/3f9550f2-afda-4c6b-9bc0-a67708a060d6/Gambler Otvet 1 Mama.ism/manifest'
    };
    var selectChapter = function selectChapter(chapter) {
      sendMetrik$1('SelectChapterOne', {
        'Gambler.SelectChapterName': chapter
      });
      set_store_value(location1, $location1 = chapter, $location1);
      $$invalidate(2, myPlayer.controlBar.el_.style.display = "", myPlayer);
      $players[$activeIndex + 1].src({
        src: variants[chapter],
        type: "application/vnd.ms-sstr+xml"
      });
      set_store_value(activeIndex, $activeIndex += 1, $activeIndex);
      setTimeout(function () {
        return $players[$activeIndex].play();
      }, 10);
    };
    onMount(function () {
      $$invalidate(2, myPlayer.controlBar.el_.style.display = "none", myPlayer);
      return function () {
        $$invalidate(2, myPlayer.controlBar.el_.style.display = "", myPlayer);
      };
    });
    var onSkip = function onSkip() {
      $$invalidate(2, myPlayer.controlBar.el_.style.display = "", myPlayer);
      set_store_value(activeIndex, $activeIndex += 1, $activeIndex);
    };
    $$self.$$.on_mount.push(function () {
      if (myPlayer === undefined && !('myPlayer' in $$props || $$self.$$.bound[$$self.$$.props['myPlayer']])) {
        console.warn("<SelectLocation1> was created without expected prop 'myPlayer'");
      }
    });
    var writable_props = ['myPlayer'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<SelectLocation1> was created with unknown prop '".concat(key, "'"));
    });
    var click_handler = function click_handler() {
      return selectChapter('uralochka');
    };
    var click_handler_1 = function click_handler_1() {
      return selectChapter('spb');
    };
    var click_handler_2 = function click_handler_2() {
      return selectChapter('mom');
    };
    var click_handler_3 = function click_handler_3() {
      return selectChapter('uralochka');
    };
    var click_handler_4 = function click_handler_4() {
      return selectChapter('spb');
    };
    var click_handler_5 = function click_handler_5() {
      return selectChapter('mom');
    };
    $$self.$$set = function ($$props) {
      if ('myPlayer' in $$props) $$invalidate(2, myPlayer = $$props.myPlayer);
    };
    $$self.$capture_state = function () {
      return {
        BASE_URL: BASE_URL,
        sendMetrik: sendMetrik$1,
        adDisabled: adDisabled,
        activeIndex: activeIndex,
        players: players,
        location1: location1,
        onMount: onMount,
        Timer: Timer,
        myPlayer: myPlayer,
        variants: variants,
        selectChapter: selectChapter,
        onSkip: onSkip,
        $activeIndex: $activeIndex,
        $players: $players,
        $location1: $location1
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('myPlayer' in $$props) $$invalidate(2, myPlayer = $$props.myPlayer);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    return [selectChapter, onSkip, myPlayer, click_handler, click_handler_1, click_handler_2, click_handler_3, click_handler_4, click_handler_5];
  }
  var SelectLocation1 = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(SelectLocation1, _SvelteComponentDev);
    var _super = _createSuper$8(SelectLocation1);
    function SelectLocation1(options) {
      var _this;
      _classCallCheck(this, SelectLocation1);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$8, create_fragment$8, safe_not_equal, {
        myPlayer: 2
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "SelectLocation1",
        options: options,
        id: create_fragment$8.name
      });
      return _this;
    }
    _createClass(SelectLocation1, [{
      key: "myPlayer",
      get: function get() {
        throw new Error("<SelectLocation1>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<SelectLocation1>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return SelectLocation1;
  }(SvelteComponentDev);

  function _createSuper$7(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$7(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$7() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var Object_1 = globals.Object;
  var file$7 = "src/ActiveArea/SelectLocation2.svelte";
  function get_each_context$2(ctx, list, i) {
    var child_ctx = ctx.slice();
    child_ctx[8] = list[i];
    return child_ctx;
  }

  // (76:4) {#each Object.keys(variants[$location1]) as key}
  function create_each_block$2(ctx) {
    var img;
    var img_src_value;
    var mounted;
    var dispose;
    function click_handler() {
      return (/*click_handler*/ctx[5]( /*key*/ctx[8])
      );
    }
    var block = {
      c: function create() {
        img = element("img");
        if (!src_url_equal(img.src, img_src_value = "".concat(BASE_URL, "/images/").concat( /*key*/ctx[8], ".png"))) attr_dev(img, "src", img_src_value);
        attr_dev(img, "alt", "logo");
        attr_dev(img, "class", "button svelte-aoxcnb");
        add_location(img, file$7, 76, 6, 3082);
      },
      m: function mount(target, anchor) {
        insert_dev(target, img, anchor);
        if (!mounted) {
          dispose = listen_dev(img, "click", click_handler, false, false, false, false);
          mounted = true;
        }
      },
      p: function update(new_ctx, dirty) {
        ctx = new_ctx;
        if (dirty & /*$location1*/1 && !src_url_equal(img.src, img_src_value = "".concat(BASE_URL, "/images/").concat( /*key*/ctx[8], ".png"))) {
          attr_dev(img, "src", img_src_value);
        }
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(img);
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_each_block$2.name,
      type: "each",
      source: "(76:4) {#each Object.keys(variants[$location1]) as key}",
      ctx: ctx
    });
    return block;
  }
  function create_fragment$7(ctx) {
    var div2;
    var img0;
    var img0_src_value;
    var t0;
    var img1;
    var img1_src_value;
    var t1;
    var img2;
    var img2_src_value;
    var t2;
    var img3;
    var img3_src_value;
    var t3;
    var img4;
    var img4_src_value;
    var t4;
    var img5;
    var img5_src_value;
    var t5;
    var div0;
    var t6;
    var img6;
    var img6_src_value;
    var t7;
    var div1;
    var timer;
    var current;
    var mounted;
    var dispose;
    var each_value = Object.keys( /*variants*/ctx[1][/*$location1*/ctx[0]]);
    validate_each_argument(each_value);
    var each_blocks = [];
    for (var i = 0; i < each_value.length; i += 1) {
      each_blocks[i] = create_each_block$2(get_each_context$2(ctx, each_value, i));
    }
    timer = new Timer({
      props: {
        duration: 10
      },
      $$inline: true
    });
    timer.$on("timerend", /*onSkip*/ctx[3]);
    var block = {
      c: function create() {
        div2 = element("div");
        img0 = element("img");
        t0 = space();
        img1 = element("img");
        t1 = space();
        img2 = element("img");
        t2 = space();
        img3 = element("img");
        t3 = space();
        img4 = element("img");
        t4 = space();
        img5 = element("img");
        t5 = space();
        div0 = element("div");
        for (var _i = 0; _i < each_blocks.length; _i += 1) {
          each_blocks[_i].c();
        }
        t6 = space();
        img6 = element("img");
        t7 = space();
        div1 = element("div");
        create_component(timer.$$.fragment);
        if (!src_url_equal(img0.src, img0_src_value = "".concat(BASE_URL, "/images/chip3.png"))) attr_dev(img0, "src", img0_src_value);
        attr_dev(img0, "alt", "chip1");
        attr_dev(img0, "class", "chip3 svelte-aoxcnb");
        add_location(img0, file$7, 68, 2, 2554);
        if (!src_url_equal(img1.src, img1_src_value = "".concat(BASE_URL, "/images/chip1.png"))) attr_dev(img1, "src", img1_src_value);
        attr_dev(img1, "alt", "chip1");
        attr_dev(img1, "class", "chip1 svelte-aoxcnb");
        add_location(img1, file$7, 69, 2, 2627);
        if (!src_url_equal(img2.src, img2_src_value = "".concat(BASE_URL, "/images/chip4.png"))) attr_dev(img2, "src", img2_src_value);
        attr_dev(img2, "alt", "chip1");
        attr_dev(img2, "class", "chip4 svelte-aoxcnb");
        add_location(img2, file$7, 70, 2, 2700);
        if (!src_url_equal(img3.src, img3_src_value = "".concat(BASE_URL, "/images/chip2.png"))) attr_dev(img3, "src", img3_src_value);
        attr_dev(img3, "alt", "chip1");
        attr_dev(img3, "class", "chip2 svelte-aoxcnb");
        add_location(img3, file$7, 71, 2, 2773);
        if (!src_url_equal(img4.src, img4_src_value = "".concat(BASE_URL, "/images/logo.png"))) attr_dev(img4, "src", img4_src_value);
        attr_dev(img4, "alt", "logo");
        attr_dev(img4, "class", "logo svelte-aoxcnb");
        add_location(img4, file$7, 72, 2, 2846);
        if (!src_url_equal(img5.src, img5_src_value = "".concat(BASE_URL, "/images/wheretogo.png"))) attr_dev(img5, "src", img5_src_value);
        attr_dev(img5, "alt", "cards");
        attr_dev(img5, "class", "cards svelte-aoxcnb");
        add_location(img5, file$7, 73, 2, 2916);
        attr_dev(div0, "class", "buttonContainer svelte-aoxcnb");
        add_location(div0, file$7, 74, 2, 2993);
        if (!src_url_equal(img6.src, img6_src_value = "".concat(BASE_URL, "/images/continueblack.png"))) attr_dev(img6, "src", img6_src_value);
        attr_dev(img6, "alt", "cards");
        attr_dev(img6, "class", "continue appear svelte-aoxcnb");
        add_location(img6, file$7, 84, 2, 3246);
        attr_dev(div1, "class", "timer svelte-aoxcnb");
        add_location(div1, file$7, 89, 0, 3361);
        attr_dev(div2, "class", "container appear svelte-aoxcnb");
        add_location(div2, file$7, 67, 0, 2521);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, div2, anchor);
        append_dev(div2, img0);
        append_dev(div2, t0);
        append_dev(div2, img1);
        append_dev(div2, t1);
        append_dev(div2, img2);
        append_dev(div2, t2);
        append_dev(div2, img3);
        append_dev(div2, t3);
        append_dev(div2, img4);
        append_dev(div2, t4);
        append_dev(div2, img5);
        append_dev(div2, t5);
        append_dev(div2, div0);
        for (var _i2 = 0; _i2 < each_blocks.length; _i2 += 1) {
          if (each_blocks[_i2]) {
            each_blocks[_i2].m(div0, null);
          }
        }
        append_dev(div2, t6);
        append_dev(div2, img6);
        append_dev(div2, t7);
        append_dev(div2, div1);
        mount_component(timer, div1, null);
        current = true;
        if (!mounted) {
          dispose = listen_dev(img6, "click", /*onSkip*/ctx[3], false, false, false, false);
          mounted = true;
        }
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if (dirty & /*BASE_URL, Object, variants, $location1, selectChapter*/7) {
          each_value = Object.keys( /*variants*/ctx[1][/*$location1*/ctx[0]]);
          validate_each_argument(each_value);
          var _i3;
          for (_i3 = 0; _i3 < each_value.length; _i3 += 1) {
            var child_ctx = get_each_context$2(ctx, each_value, _i3);
            if (each_blocks[_i3]) {
              each_blocks[_i3].p(child_ctx, dirty);
            } else {
              each_blocks[_i3] = create_each_block$2(child_ctx);
              each_blocks[_i3].c();
              each_blocks[_i3].m(div0, null);
            }
          }
          for (; _i3 < each_blocks.length; _i3 += 1) {
            each_blocks[_i3].d(1);
          }
          each_blocks.length = each_value.length;
        }
      },
      i: function intro(local) {
        if (current) return;
        transition_in(timer.$$.fragment, local);
        current = true;
      },
      o: function outro(local) {
        transition_out(timer.$$.fragment, local);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div2);
        destroy_each(each_blocks, detaching);
        destroy_component(timer);
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$7.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$7($$self, $$props, $$invalidate) {
    var $activeIndex;
    var $players;
    var $location1;
    validate_store(activeIndex, 'activeIndex');
    component_subscribe($$self, activeIndex, function ($$value) {
      return $$invalidate(6, $activeIndex = $$value);
    });
    validate_store(players, 'players');
    component_subscribe($$self, players, function ($$value) {
      return $$invalidate(7, $players = $$value);
    });
    validate_store(location1, 'location1');
    component_subscribe($$self, location1, function ($$value) {
      return $$invalidate(0, $location1 = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('SelectLocation2', slots, []);
    var myPlayer = $$props.myPlayer;
    myPlayer.pause();
    var variants = {
      uralochka: {
        spb: "https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/f64b175b-a2b0-4248-9f54-8a96881401c1/Gambler Otvet1 Uralochka Otvet2 .ism/manifest",
        mom: "https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/0f1e695e-3d45-45e0-aac2-2a69aac86374/1ural2m.ism/manifest",
        krym: "https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/a38df08a-83c1-4f21-ab96-bbd6cdbeb4a2/Gambler Otvet1 Uralochka Otvet2 .ism/manifest"
      },
      spb: {
        mom: "https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/503152b7-f5e4-41bb-a6c3-914f4b5b8cea/Gambler Otvet1 SaintPetersburg O.ism/manifest",
        uralochka: "https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/2f29ea3c-3986-401c-8202-522d791655da/Gambler Otvet1 SaintPetersburg O.ism/manifest",
        krym: "https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/507010ae-7c54-45e5-85aa-1788e81dbacd/Gambler Otvet1 SaintPetersburg O.ism/manifest"
      },
      mom: {
        spb: "https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/7f9e9dff-47b0-41c1-8365-d4610d12b33a/Gambler Otvet1 Mama Otvet2 Saint.ism/manifest",
        uralochka: "https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/395e9576-48ce-428b-8e4c-b65bed620326/Gambler Otvet1 Mama Otvet2 Uralo.ism/manifest",
        krym: "https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/6f44c733-e2cb-4b71-8f6f-c78959600234/Gambler Otvet1 Mama Otvet2 Krym.ism/manifest"
      }
    };
    var selectChapter = function selectChapter(chapter) {
      sendMetrik$1('SelectChapterTwo', {
        'Gambler.SelectChapterName': chapter
      });
      $$invalidate(4, myPlayer.controlBar.el_.style.display = "", myPlayer);
      $players[$activeIndex + 1].src({
        src: variants[$location1][chapter],
        type: "application/vnd.ms-sstr+xml"
      });
      set_store_value(activeIndex, $activeIndex += 1, $activeIndex);
      setTimeout(function () {
        return $players[$activeIndex].play();
      }, 10);
    };
    onMount(function () {
      $$invalidate(4, myPlayer.controlBar.el_.style.display = "none", myPlayer);
      return function () {
        $$invalidate(4, myPlayer.controlBar.el_.style.display = "", myPlayer);
      };
    });
    var onSkip = function onSkip() {
      $$invalidate(4, myPlayer.controlBar.el_.style.display = "", myPlayer);
      set_store_value(activeIndex, $activeIndex += 1, $activeIndex);
    };
    $$self.$$.on_mount.push(function () {
      if (myPlayer === undefined && !('myPlayer' in $$props || $$self.$$.bound[$$self.$$.props['myPlayer']])) {
        console.warn("<SelectLocation2> was created without expected prop 'myPlayer'");
      }
    });
    var writable_props = ['myPlayer'];
    Object_1.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<SelectLocation2> was created with unknown prop '".concat(key, "'"));
    });
    var click_handler = function click_handler(key) {
      return selectChapter(key);
    };
    $$self.$$set = function ($$props) {
      if ('myPlayer' in $$props) $$invalidate(4, myPlayer = $$props.myPlayer);
    };
    $$self.$capture_state = function () {
      return {
        BASE_URL: BASE_URL,
        sendMetrik: sendMetrik$1,
        adDisabled: adDisabled,
        activeIndex: activeIndex,
        players: players,
        location1: location1,
        onMount: onMount,
        Timer: Timer,
        myPlayer: myPlayer,
        variants: variants,
        selectChapter: selectChapter,
        onSkip: onSkip,
        $activeIndex: $activeIndex,
        $players: $players,
        $location1: $location1
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('myPlayer' in $$props) $$invalidate(4, myPlayer = $$props.myPlayer);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    return [$location1, variants, selectChapter, onSkip, myPlayer, click_handler];
  }
  var SelectLocation2 = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(SelectLocation2, _SvelteComponentDev);
    var _super = _createSuper$7(SelectLocation2);
    function SelectLocation2(options) {
      var _this;
      _classCallCheck(this, SelectLocation2);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$7, create_fragment$7, safe_not_equal, {
        myPlayer: 4
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "SelectLocation2",
        options: options,
        id: create_fragment$7.name
      });
      return _this;
    }
    _createClass(SelectLocation2, [{
      key: "myPlayer",
      get: function get() {
        throw new Error("<SelectLocation2>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<SelectLocation2>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return SelectLocation2;
  }(SvelteComponentDev);

  function _createSuper$6(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$6(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$6() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$6 = "src/ActiveArea/Index.svelte";
  function get_each_context$1(ctx, list, i) {
    var child_ctx = ctx.slice();
    child_ctx[10] = list[i];
    child_ctx[12] = i;
    return child_ctx;
  }

  // (57:2) {#if !$adDisabled}
  function create_if_block$4(ctx) {
    var each_blocks = [];
    var each_1_lookup = new Map();
    var each_1_anchor;
    var current;
    var each_value = /*activeElemenets*/ctx[3];
    validate_each_argument(each_value);
    var get_key = function get_key(ctx) {
      return (/*item*/ctx[10].start + /*item*/ctx[10].type
      );
    };
    validate_each_keys(ctx, each_value, get_each_context$1, get_key);
    for (var i = 0; i < each_value.length; i += 1) {
      var child_ctx = get_each_context$1(ctx, each_value, i);
      var key = get_key(child_ctx);
      each_1_lookup.set(key, each_blocks[i] = create_each_block$1(key, child_ctx));
    }
    var block = {
      c: function create() {
        for (var _i = 0; _i < each_blocks.length; _i += 1) {
          each_blocks[_i].c();
        }
        each_1_anchor = empty();
      },
      m: function mount(target, anchor) {
        for (var _i2 = 0; _i2 < each_blocks.length; _i2 += 1) {
          if (each_blocks[_i2]) {
            each_blocks[_i2].m(target, anchor);
          }
        }
        insert_dev(target, each_1_anchor, anchor);
        current = true;
      },
      p: function update(ctx, dirty) {
        if (dirty & /*elementTypes, activeElemenets, myPlayer, containerWidth, settings*/79) {
          each_value = /*activeElemenets*/ctx[3];
          validate_each_argument(each_value);
          group_outros();
          validate_each_keys(ctx, each_value, get_each_context$1, get_key);
          each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value, each_1_lookup, each_1_anchor.parentNode, outro_and_destroy_block, create_each_block$1, each_1_anchor, get_each_context$1);
          check_outros();
        }
      },
      i: function intro(local) {
        if (current) return;
        for (var _i3 = 0; _i3 < each_value.length; _i3 += 1) {
          transition_in(each_blocks[_i3]);
        }
        current = true;
      },
      o: function outro(local) {
        for (var _i4 = 0; _i4 < each_blocks.length; _i4 += 1) {
          transition_out(each_blocks[_i4]);
        }
        current = false;
      },
      d: function destroy(detaching) {
        for (var _i5 = 0; _i5 < each_blocks.length; _i5 += 1) {
          each_blocks[_i5].d(detaching);
        }
        if (detaching) detach_dev(each_1_anchor);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block$4.name,
      type: "if",
      source: "(57:2) {#if !$adDisabled}",
      ctx: ctx
    });
    return block;
  }

  // (58:4) {#each activeElemenets as item, i (item.start + item.type)}
  function create_each_block$1(key_1, ctx) {
    var first;
    var switch_instance;
    var switch_instance_anchor;
    var current;
    var switch_value = /*elementTypes*/ctx[6][/*item*/ctx[10].type];
    function switch_props(ctx) {
      return {
        props: {
          item: /*item*/ctx[10],
          myPlayer: /*myPlayer*/ctx[0],
          containerWidth: /*containerWidth*/ctx[2],
          settings: /*settings*/ctx[1]
        },
        $$inline: true
      };
    }
    if (switch_value) {
      switch_instance = construct_svelte_component_dev(switch_value, switch_props(ctx));
    }
    var block = {
      key: key_1,
      first: null,
      c: function create() {
        first = empty();
        if (switch_instance) create_component(switch_instance.$$.fragment);
        switch_instance_anchor = empty();
        this.first = first;
      },
      m: function mount(target, anchor) {
        insert_dev(target, first, anchor);
        if (switch_instance) mount_component(switch_instance, target, anchor);
        insert_dev(target, switch_instance_anchor, anchor);
        current = true;
      },
      p: function update(new_ctx, dirty) {
        ctx = new_ctx;
        var switch_instance_changes = {};
        if (dirty & /*activeElemenets*/8) switch_instance_changes.item = /*item*/ctx[10];
        if (dirty & /*myPlayer*/1) switch_instance_changes.myPlayer = /*myPlayer*/ctx[0];
        if (dirty & /*containerWidth*/4) switch_instance_changes.containerWidth = /*containerWidth*/ctx[2];
        if (dirty & /*settings*/2) switch_instance_changes.settings = /*settings*/ctx[1];
        if (dirty & /*activeElemenets*/8 && switch_value !== (switch_value = /*elementTypes*/ctx[6][/*item*/ctx[10].type])) {
          if (switch_instance) {
            group_outros();
            var old_component = switch_instance;
            transition_out(old_component.$$.fragment, 1, 0, function () {
              destroy_component(old_component, 1);
            });
            check_outros();
          }
          if (switch_value) {
            switch_instance = construct_svelte_component_dev(switch_value, switch_props(ctx));
            create_component(switch_instance.$$.fragment);
            transition_in(switch_instance.$$.fragment, 1);
            mount_component(switch_instance, switch_instance_anchor.parentNode, switch_instance_anchor);
          } else {
            switch_instance = null;
          }
        } else if (switch_value) {
          switch_instance.$set(switch_instance_changes);
        }
      },
      i: function intro(local) {
        if (current) return;
        if (switch_instance) transition_in(switch_instance.$$.fragment, local);
        current = true;
      },
      o: function outro(local) {
        if (switch_instance) transition_out(switch_instance.$$.fragment, local);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(first);
        if (detaching) detach_dev(switch_instance_anchor);
        if (switch_instance) destroy_component(switch_instance, detaching);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_each_block$1.name,
      type: "each",
      source: "(58:4) {#each activeElemenets as item, i (item.start + item.type)}",
      ctx: ctx
    });
    return block;
  }
  function create_fragment$6(ctx) {
    var div;
    var div_resize_listener;
    var current;
    var mounted;
    var dispose;
    var if_block = ! /*$adDisabled*/ctx[4] && create_if_block$4(ctx);
    var block = {
      c: function create() {
        div = element("div");
        if (if_block) if_block.c();
        attr_dev(div, "class", "interactiveContainer svelte-uwyllo");
        add_render_callback(function () {
          return (/*div_elementresize_handler*/ctx[9].call(div)
          );
        });
        add_location(div, file$6, 48, 0, 1378);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, div, anchor);
        if (if_block) if_block.m(div, null);
        div_resize_listener = add_iframe_resize_listener(div, /*div_elementresize_handler*/ctx[9].bind(div));
        current = true;
        if (!mounted) {
          dispose = [listen_dev(div, "mousedown", /*forwardClick*/ctx[5], false, false, false, false), listen_dev(div, "mouseup", /*forwardClick*/ctx[5], false, false, false, false), listen_dev(div, "touchstart", /*forwardClick*/ctx[5], false, false, false, false), listen_dev(div, "touchend", /*forwardClick*/ctx[5], false, false, false, false), listen_dev(div, "mousemove", /*forwardClick*/ctx[5], false, false, false, false)];
          mounted = true;
        }
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if (! /*$adDisabled*/ctx[4]) {
          if (if_block) {
            if_block.p(ctx, dirty);
            if (dirty & /*$adDisabled*/16) {
              transition_in(if_block, 1);
            }
          } else {
            if_block = create_if_block$4(ctx);
            if_block.c();
            transition_in(if_block, 1);
            if_block.m(div, null);
          }
        } else if (if_block) {
          group_outros();
          transition_out(if_block, 1, 1, function () {
            if_block = null;
          });
          check_outros();
        }
      },
      i: function intro(local) {
        if (current) return;
        transition_in(if_block);
        current = true;
      },
      o: function outro(local) {
        transition_out(if_block);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div);
        if (if_block) if_block.d();
        div_resize_listener();
        mounted = false;
        run_all(dispose);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$6.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$6($$self, $$props, $$invalidate) {
    var $adDisabled;
    validate_store(adDisabled, 'adDisabled');
    component_subscribe($$self, adDisabled, function ($$value) {
      return $$invalidate(4, $adDisabled = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('Index', slots, []);
    var overlayRef = $$props.overlayRef;
    var myPlayer = $$props.myPlayer;
    var currentTime = $$props.currentTime;
    var settings = $$props.settings;
    var containerWidth;
    var forwardClick = function forwardClick(event) {
      var newEvent = event.touches ? new TouchEvent(event.type, event) : new MouseEvent(event.type, event);
      var player = overlayRef.querySelector(".vjs-player");
      if (player) player.dispatchEvent(newEvent);
    };
    var activeElemenets = [];
    var elementTypes = {
      rss: Rss,
      image: Image,
      zone: Zone,
      push: Push,
      midroll: MidRoll,
      disablead: DisableAd,
      continuewatch: ContinueWatch,
      selectLocation: SelectLocation1,
      selectLocation2: SelectLocation2
    };
    $$self.$$.on_mount.push(function () {
      if (overlayRef === undefined && !('overlayRef' in $$props || $$self.$$.bound[$$self.$$.props['overlayRef']])) {
        console.warn("<Index> was created without expected prop 'overlayRef'");
      }
      if (myPlayer === undefined && !('myPlayer' in $$props || $$self.$$.bound[$$self.$$.props['myPlayer']])) {
        console.warn("<Index> was created without expected prop 'myPlayer'");
      }
      if (currentTime === undefined && !('currentTime' in $$props || $$self.$$.bound[$$self.$$.props['currentTime']])) {
        console.warn("<Index> was created without expected prop 'currentTime'");
      }
      if (settings === undefined && !('settings' in $$props || $$self.$$.bound[$$self.$$.props['settings']])) {
        console.warn("<Index> was created without expected prop 'settings'");
      }
    });
    var writable_props = ['overlayRef', 'myPlayer', 'currentTime', 'settings'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<Index> was created with unknown prop '".concat(key, "'"));
    });
    function div_elementresize_handler() {
      containerWidth = this.clientWidth;
      $$invalidate(2, containerWidth);
    }
    $$self.$$set = function ($$props) {
      if ('overlayRef' in $$props) $$invalidate(7, overlayRef = $$props.overlayRef);
      if ('myPlayer' in $$props) $$invalidate(0, myPlayer = $$props.myPlayer);
      if ('currentTime' in $$props) $$invalidate(8, currentTime = $$props.currentTime);
      if ('settings' in $$props) $$invalidate(1, settings = $$props.settings);
    };
    $$self.$capture_state = function () {
      return {
        Rss: Rss,
        Image: Image,
        Zone: Zone,
        Push: Push,
        Midroll: MidRoll,
        DisableAd: DisableAd,
        ContinueWatch: ContinueWatch,
        SelectLocation1: SelectLocation1,
        SelectLocation2: SelectLocation2,
        adDisabled: adDisabled,
        overlayRef: overlayRef,
        myPlayer: myPlayer,
        currentTime: currentTime,
        settings: settings,
        containerWidth: containerWidth,
        forwardClick: forwardClick,
        activeElemenets: activeElemenets,
        elementTypes: elementTypes,
        $adDisabled: $adDisabled
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('overlayRef' in $$props) $$invalidate(7, overlayRef = $$props.overlayRef);
      if ('myPlayer' in $$props) $$invalidate(0, myPlayer = $$props.myPlayer);
      if ('currentTime' in $$props) $$invalidate(8, currentTime = $$props.currentTime);
      if ('settings' in $$props) $$invalidate(1, settings = $$props.settings);
      if ('containerWidth' in $$props) $$invalidate(2, containerWidth = $$props.containerWidth);
      if ('activeElemenets' in $$props) $$invalidate(3, activeElemenets = $$props.activeElemenets);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    $$self.$$.update = function () {
      if ($$self.$$.dirty & /*settings, currentTime*/258) {
        {
          $$invalidate(3, activeElemenets = settings.activeElemenets ? settings.activeElemenets.filter(function (item) {
            return item.start <= currentTime && item.end >= currentTime;
          }) : []);
        } // console.log(activeElemenets, currentTime);
      }
    };

    return [myPlayer, settings, containerWidth, activeElemenets, $adDisabled, forwardClick, elementTypes, overlayRef, currentTime, div_elementresize_handler];
  }
  var Index = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(Index, _SvelteComponentDev);
    var _super = _createSuper$6(Index);
    function Index(options) {
      var _this;
      _classCallCheck(this, Index);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$6, create_fragment$6, safe_not_equal, {
        overlayRef: 7,
        myPlayer: 0,
        currentTime: 8,
        settings: 1
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "Index",
        options: options,
        id: create_fragment$6.name
      });
      return _this;
    }
    _createClass(Index, [{
      key: "overlayRef",
      get: function get() {
        throw new Error("<Index>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Index>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "myPlayer",
      get: function get() {
        throw new Error("<Index>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Index>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "currentTime",
      get: function get() {
        throw new Error("<Index>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Index>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "settings",
      get: function get() {
        throw new Error("<Index>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Index>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return Index;
  }(SvelteComponentDev);

  function _createSuper$5(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$5(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$5() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$5 = "src/GameContainer.svelte";

  // (40:0) {#if isGameOpen && !isShowPromo}
  function create_if_block$3(ctx) {
    var div2;
    var iframe;
    var iframe_src_value;
    var t0;
    var div1;
    var img;
    var img_src_value;
    var t1;
    var div0;
    var div0_style_value;
    var div2_resize_listener;
    var mounted;
    var dispose;
    var block = {
      c: function create() {
        div2 = element("div");
        iframe = element("iframe");
        t0 = space();
        div1 = element("div");
        img = element("img");
        t1 = space();
        div0 = element("div");
        attr_dev(iframe, "frameborder", "0");
        attr_dev(iframe, "title", "poker");
        if (!src_url_equal(iframe.src, iframe_src_value = /*gameUrl*/ctx[1])) attr_dev(iframe, "src", iframe_src_value);
        attr_dev(iframe, "class", "iframe svelte-czfs3p");
        add_location(iframe, file$5, 41, 4, 1021);
        if (!src_url_equal(img.src, img_src_value = "".concat(BASE_URL, "/images/continuewhite.png"))) attr_dev(img, "src", img_src_value);
        attr_dev(img, "alt", "continue");
        attr_dev(img, "class", "closeImage svelte-czfs3p");
        add_location(img, file$5, 43, 6, 1146);
        attr_dev(div0, "class", "shine svelte-czfs3p");
        attr_dev(div0, "style", div0_style_value = "border-radius:".concat( /*clientWidth*/ctx[3] * 0.05, "px"));
        add_location(div0, file$5, 44, 6, 1238);
        attr_dev(div1, "class", "closeButton svelte-czfs3p");
        add_location(div1, file$5, 42, 4, 1095);
        attr_dev(div2, "class", "gameContainer svelte-czfs3p");
        add_render_callback(function () {
          return (/*div2_elementresize_handler*/ctx[7].call(div2)
          );
        });
        add_location(div2, file$5, 40, 2, 958);
      },
      m: function mount(target, anchor) {
        insert_dev(target, div2, anchor);
        append_dev(div2, iframe);
        append_dev(div2, t0);
        append_dev(div2, div1);
        append_dev(div1, img);
        append_dev(div1, t1);
        append_dev(div1, div0);
        div2_resize_listener = add_iframe_resize_listener(div2, /*div2_elementresize_handler*/ctx[7].bind(div2));
        if (!mounted) {
          dispose = listen_dev(div1, "click", /*onClose*/ctx[4], false, false, false, false);
          mounted = true;
        }
      },
      p: function update(ctx, dirty) {
        if (dirty & /*gameUrl*/2 && !src_url_equal(iframe.src, iframe_src_value = /*gameUrl*/ctx[1])) {
          attr_dev(iframe, "src", iframe_src_value);
        }
        if (dirty & /*clientWidth*/8 && div0_style_value !== (div0_style_value = "border-radius:".concat( /*clientWidth*/ctx[3] * 0.05, "px"))) {
          attr_dev(div0, "style", div0_style_value);
        }
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div2);
        div2_resize_listener();
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block$3.name,
      type: "if",
      source: "(40:0) {#if isGameOpen && !isShowPromo}",
      ctx: ctx
    });
    return block;
  }
  function create_fragment$5(ctx) {
    var if_block_anchor;
    var if_block = /*isGameOpen*/ctx[2] && ! /*isShowPromo*/ctx[0] && create_if_block$3(ctx);
    var block = {
      c: function create() {
        if (if_block) if_block.c();
        if_block_anchor = empty();
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        if (if_block) if_block.m(target, anchor);
        insert_dev(target, if_block_anchor, anchor);
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if ( /*isGameOpen*/ctx[2] && ! /*isShowPromo*/ctx[0]) {
          if (if_block) {
            if_block.p(ctx, dirty);
          } else {
            if_block = create_if_block$3(ctx);
            if_block.c();
            if_block.m(if_block_anchor.parentNode, if_block_anchor);
          }
        } else if (if_block) {
          if_block.d(1);
          if_block = null;
        }
      },
      i: noop,
      o: noop,
      d: function destroy(detaching) {
        if (if_block) if_block.d(detaching);
        if (detaching) detach_dev(if_block_anchor);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$5.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$5($$self, $$props, $$invalidate) {
    var $gameWon;
    var $gameSkipped;
    var $gameClosed;
    validate_store(gameWon, 'gameWon');
    component_subscribe($$self, gameWon, function ($$value) {
      return $$invalidate(9, $gameWon = $$value);
    });
    validate_store(gameSkipped, 'gameSkipped');
    component_subscribe($$self, gameSkipped, function ($$value) {
      return $$invalidate(10, $gameSkipped = $$value);
    });
    validate_store(gameClosed, 'gameClosed');
    component_subscribe($$self, gameClosed, function ($$value) {
      return $$invalidate(11, $gameClosed = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('GameContainer', slots, []);
    var gameUrl = $$props.gameUrl;
    var closeGame = $$props.closeGame;
    var isGameOpen = $$props.isGameOpen;
    var _$$props$episode = $$props.episode,
      episode = _$$props$episode === void 0 ? 1 : _$$props$episode;
    var isShowPromo = $$props.isShowPromo;
    var timerId;
    var clientWidth;
    var onClose = function onClose() {
      set_store_value(gameClosed, $gameClosed = true, $gameClosed);
      if ($gameSkipped) {
        $$invalidate(0, isShowPromo = true);
      } else {
        closeGame();
      }
      sendMetrik$1("GameAdBack", {
        "Gambler.GameAdBackEpisode": episode
      });
    };
    var onMessage = function onMessage(event) {
      if (event.data === "closeGame" && gameUrl) {
        clearTimeout(timerId);
        timerId = setTimeout(function () {
          set_store_value(gameWon, $gameWon = true, $gameWon);
          $$invalidate(0, isShowPromo = true);
        }, 500);
      }
    };
    onMount(function () {
      window.addEventListener("message", onMessage);
      return function () {
        return window.removeEventListener("message", onMessage);
      };
    });
    $$self.$$.on_mount.push(function () {
      if (gameUrl === undefined && !('gameUrl' in $$props || $$self.$$.bound[$$self.$$.props['gameUrl']])) {
        console.warn("<GameContainer> was created without expected prop 'gameUrl'");
      }
      if (closeGame === undefined && !('closeGame' in $$props || $$self.$$.bound[$$self.$$.props['closeGame']])) {
        console.warn("<GameContainer> was created without expected prop 'closeGame'");
      }
      if (isGameOpen === undefined && !('isGameOpen' in $$props || $$self.$$.bound[$$self.$$.props['isGameOpen']])) {
        console.warn("<GameContainer> was created without expected prop 'isGameOpen'");
      }
      if (isShowPromo === undefined && !('isShowPromo' in $$props || $$self.$$.bound[$$self.$$.props['isShowPromo']])) {
        console.warn("<GameContainer> was created without expected prop 'isShowPromo'");
      }
    });
    var writable_props = ['gameUrl', 'closeGame', 'isGameOpen', 'episode', 'isShowPromo'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<GameContainer> was created with unknown prop '".concat(key, "'"));
    });
    function div2_elementresize_handler() {
      clientWidth = this.clientWidth;
      $$invalidate(3, clientWidth);
    }
    $$self.$$set = function ($$props) {
      if ('gameUrl' in $$props) $$invalidate(1, gameUrl = $$props.gameUrl);
      if ('closeGame' in $$props) $$invalidate(5, closeGame = $$props.closeGame);
      if ('isGameOpen' in $$props) $$invalidate(2, isGameOpen = $$props.isGameOpen);
      if ('episode' in $$props) $$invalidate(6, episode = $$props.episode);
      if ('isShowPromo' in $$props) $$invalidate(0, isShowPromo = $$props.isShowPromo);
    };
    $$self.$capture_state = function () {
      return {
        onMount: onMount,
        gameWon: gameWon,
        gameClosed: gameClosed,
        gameSkipped: gameSkipped,
        BASE_URL: BASE_URL,
        gameUrl: gameUrl,
        closeGame: closeGame,
        isGameOpen: isGameOpen,
        episode: episode,
        isShowPromo: isShowPromo,
        timerId: timerId,
        clientWidth: clientWidth,
        sendMetrik: sendMetrik$1,
        onClose: onClose,
        onMessage: onMessage,
        $gameWon: $gameWon,
        $gameSkipped: $gameSkipped,
        $gameClosed: $gameClosed
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('gameUrl' in $$props) $$invalidate(1, gameUrl = $$props.gameUrl);
      if ('closeGame' in $$props) $$invalidate(5, closeGame = $$props.closeGame);
      if ('isGameOpen' in $$props) $$invalidate(2, isGameOpen = $$props.isGameOpen);
      if ('episode' in $$props) $$invalidate(6, episode = $$props.episode);
      if ('isShowPromo' in $$props) $$invalidate(0, isShowPromo = $$props.isShowPromo);
      if ('timerId' in $$props) timerId = $$props.timerId;
      if ('clientWidth' in $$props) $$invalidate(3, clientWidth = $$props.clientWidth);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    return [isShowPromo, gameUrl, isGameOpen, clientWidth, onClose, closeGame, episode, div2_elementresize_handler];
  }
  var GameContainer = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(GameContainer, _SvelteComponentDev);
    var _super = _createSuper$5(GameContainer);
    function GameContainer(options) {
      var _this;
      _classCallCheck(this, GameContainer);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$5, create_fragment$5, safe_not_equal, {
        gameUrl: 1,
        closeGame: 5,
        isGameOpen: 2,
        episode: 6,
        isShowPromo: 0
      });
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "GameContainer",
        options: options,
        id: create_fragment$5.name
      });
      return _this;
    }
    _createClass(GameContainer, [{
      key: "gameUrl",
      get: function get() {
        throw new Error("<GameContainer>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<GameContainer>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "closeGame",
      get: function get() {
        throw new Error("<GameContainer>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<GameContainer>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "isGameOpen",
      get: function get() {
        throw new Error("<GameContainer>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<GameContainer>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "episode",
      get: function get() {
        throw new Error("<GameContainer>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<GameContainer>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "isShowPromo",
      get: function get() {
        throw new Error("<GameContainer>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<GameContainer>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return GameContainer;
  }(SvelteComponentDev);

  function _createSuper$4(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$4(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$4() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$4 = "src/Video.svelte";

  // (245:2) {#if isReady && isActive}
  function create_if_block_1(ctx) {
    var activearea;
    var current;
    activearea = new Index({
      props: {
        overlayRef: /*overlayRef*/ctx[5],
        currentTime: /*currentTime*/ctx[9],
        settings: /*settings*/ctx[0],
        myPlayer: /*myPlayer*/ctx[3]
      },
      $$inline: true
    });
    var block = {
      c: function create() {
        create_component(activearea.$$.fragment);
      },
      m: function mount(target, anchor) {
        mount_component(activearea, target, anchor);
        current = true;
      },
      p: function update(ctx, dirty) {
        var activearea_changes = {};
        if (dirty[0] & /*overlayRef*/32) activearea_changes.overlayRef = /*overlayRef*/ctx[5];
        if (dirty[0] & /*currentTime*/512) activearea_changes.currentTime = /*currentTime*/ctx[9];
        if (dirty[0] & /*settings*/1) activearea_changes.settings = /*settings*/ctx[0];
        if (dirty[0] & /*myPlayer*/8) activearea_changes.myPlayer = /*myPlayer*/ctx[3];
        activearea.$set(activearea_changes);
      },
      i: function intro(local) {
        if (current) return;
        transition_in(activearea.$$.fragment, local);
        current = true;
      },
      o: function outro(local) {
        transition_out(activearea.$$.fragment, local);
        current = false;
      },
      d: function destroy(detaching) {
        destroy_component(activearea, detaching);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block_1.name,
      type: "if",
      source: "(245:2) {#if isReady && isActive}",
      ctx: ctx
    });
    return block;
  }

  // (246:2) {#if interactiveComponent && isActive}
  function create_if_block$2(ctx) {
    var switch_instance;
    var switch_instance_anchor;
    var current;
    var switch_value = /*interactiveComponent*/ctx[13];
    function switch_props(ctx) {
      return {
        props: {
          openGame: /*openGame*/ctx[16],
          nextVideo: /*nextVideo*/ctx[15],
          duration: /*duration*/ctx[8],
          myPlayer: /*myPlayer*/ctx[3],
          isActive: /*isActive*/ctx[1],
          currentTime: /*currentTime*/ctx[9],
          episode: /*settings*/ctx[0].episode
        },
        $$inline: true
      };
    }
    if (switch_value) {
      switch_instance = construct_svelte_component_dev(switch_value, switch_props(ctx));
    }
    var block = {
      c: function create() {
        if (switch_instance) create_component(switch_instance.$$.fragment);
        switch_instance_anchor = empty();
      },
      m: function mount(target, anchor) {
        if (switch_instance) mount_component(switch_instance, target, anchor);
        insert_dev(target, switch_instance_anchor, anchor);
        current = true;
      },
      p: function update(ctx, dirty) {
        var switch_instance_changes = {};
        if (dirty[0] & /*duration*/256) switch_instance_changes.duration = /*duration*/ctx[8];
        if (dirty[0] & /*myPlayer*/8) switch_instance_changes.myPlayer = /*myPlayer*/ctx[3];
        if (dirty[0] & /*isActive*/2) switch_instance_changes.isActive = /*isActive*/ctx[1];
        if (dirty[0] & /*currentTime*/512) switch_instance_changes.currentTime = /*currentTime*/ctx[9];
        if (dirty[0] & /*settings*/1) switch_instance_changes.episode = /*settings*/ctx[0].episode;
        if (dirty[0] & /*interactiveComponent*/8192 && switch_value !== (switch_value = /*interactiveComponent*/ctx[13])) {
          if (switch_instance) {
            group_outros();
            var old_component = switch_instance;
            transition_out(old_component.$$.fragment, 1, 0, function () {
              destroy_component(old_component, 1);
            });
            check_outros();
          }
          if (switch_value) {
            switch_instance = construct_svelte_component_dev(switch_value, switch_props(ctx));
            create_component(switch_instance.$$.fragment);
            transition_in(switch_instance.$$.fragment, 1);
            mount_component(switch_instance, switch_instance_anchor.parentNode, switch_instance_anchor);
          } else {
            switch_instance = null;
          }
        } else if (switch_value) {
          switch_instance.$set(switch_instance_changes);
        }
      },
      i: function intro(local) {
        if (current) return;
        if (switch_instance) transition_in(switch_instance.$$.fragment, local);
        current = true;
      },
      o: function outro(local) {
        if (switch_instance) transition_out(switch_instance.$$.fragment, local);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(switch_instance_anchor);
        if (switch_instance) destroy_component(switch_instance, detaching);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block$2.name,
      type: "if",
      source: "(246:2) {#if interactiveComponent && isActive}",
      ctx: ctx
    });
    return block;
  }
  function create_fragment$4(ctx) {
    var div1;
    var video;
    var t0;
    var t1;
    var t2;
    var gamecontainer;
    var updating_isShowPromo;
    var t3;
    var div0;
    var timebar;
    var current;
    var if_block0 = /*isReady*/ctx[11] && /*isActive*/ctx[1] && create_if_block_1(ctx);
    var if_block1 = /*interactiveComponent*/ctx[13] && /*isActive*/ctx[1] && create_if_block$2(ctx);
    function gamecontainer_isShowPromo_binding(value) {
      /*gamecontainer_isShowPromo_binding*/ctx[22](value);
    }
    var gamecontainer_props = {
      gameUrl: /*settings*/ctx[0].game,
      closeGame: /*closeGame*/ctx[17],
      isGameOpen: /*isGameOpen*/ctx[7],
      episode: /*settings*/ctx[0].episode
    };
    if ( /*isShowPromo*/ctx[4] !== void 0) {
      gamecontainer_props.isShowPromo = /*isShowPromo*/ctx[4];
    }
    gamecontainer = new GameContainer({
      props: gamecontainer_props,
      $$inline: true
    });
    binding_callbacks.push(function () {
      return bind(gamecontainer, 'isShowPromo', gamecontainer_isShowPromo_binding);
    });
    timebar = new TimeBar({
      $$inline: true
    });
    var block = {
      c: function create() {
        div1 = element("div");
        video = element("video");
        t0 = space();
        if (if_block0) if_block0.c();
        t1 = space();
        if (if_block1) if_block1.c();
        t2 = space();
        create_component(gamecontainer.$$.fragment);
        t3 = space();
        div0 = element("div");
        create_component(timebar.$$.fragment);
        attr_dev(video, "id", /*id*/ctx[14]);
        attr_dev(video, "class", "azuremediaplayer amp-default-skin");
        video.playsInline = true;
        add_location(video, file$4, 243, 2, 6829);
        attr_dev(div0, "class", "timeBarContainer svelte-1bsmn66");
        add_location(div0, file$4, 263, 4, 7379);
        attr_dev(div1, "class", "overlay svelte-1bsmn66");
        toggle_class(div1, "isActive", /*isActive*/ctx[1]);
        toggle_class(div1, "isFullScreen", /*isFullScreen*/ctx[6] || /*isFullScreenIos*/ctx[2]);
        toggle_class(div1, "isIos", isIos());
        toggle_class(div1, "isGameOpen", /*isGameOpen*/ctx[7]);
        toggle_class(div1, "isEnded", /*isEnded*/ctx[12]);
        add_location(div1, file$4, 235, 0, 6647);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        insert_dev(target, div1, anchor);
        append_dev(div1, video);
        append_dev(div1, t0);
        if (if_block0) if_block0.m(div1, null);
        append_dev(div1, t1);
        if (if_block1) if_block1.m(div1, null);
        append_dev(div1, t2);
        mount_component(gamecontainer, div1, null);
        append_dev(div1, t3);
        append_dev(div1, div0);
        mount_component(timebar, div0, null);
        /*div0_binding*/
        ctx[23](div0);
        /*div1_binding*/
        ctx[24](div1);
        current = true;
      },
      p: function update(ctx, dirty) {
        if ( /*isReady*/ctx[11] && /*isActive*/ctx[1]) {
          if (if_block0) {
            if_block0.p(ctx, dirty);
            if (dirty[0] & /*isReady, isActive*/2050) {
              transition_in(if_block0, 1);
            }
          } else {
            if_block0 = create_if_block_1(ctx);
            if_block0.c();
            transition_in(if_block0, 1);
            if_block0.m(div1, t1);
          }
        } else if (if_block0) {
          group_outros();
          transition_out(if_block0, 1, 1, function () {
            if_block0 = null;
          });
          check_outros();
        }
        if ( /*interactiveComponent*/ctx[13] && /*isActive*/ctx[1]) {
          if (if_block1) {
            if_block1.p(ctx, dirty);
            if (dirty[0] & /*interactiveComponent, isActive*/8194) {
              transition_in(if_block1, 1);
            }
          } else {
            if_block1 = create_if_block$2(ctx);
            if_block1.c();
            transition_in(if_block1, 1);
            if_block1.m(div1, t2);
          }
        } else if (if_block1) {
          group_outros();
          transition_out(if_block1, 1, 1, function () {
            if_block1 = null;
          });
          check_outros();
        }
        var gamecontainer_changes = {};
        if (dirty[0] & /*settings*/1) gamecontainer_changes.gameUrl = /*settings*/ctx[0].game;
        if (dirty[0] & /*isGameOpen*/128) gamecontainer_changes.isGameOpen = /*isGameOpen*/ctx[7];
        if (dirty[0] & /*settings*/1) gamecontainer_changes.episode = /*settings*/ctx[0].episode;
        if (!updating_isShowPromo && dirty[0] & /*isShowPromo*/16) {
          updating_isShowPromo = true;
          gamecontainer_changes.isShowPromo = /*isShowPromo*/ctx[4];
          add_flush_callback(function () {
            return updating_isShowPromo = false;
          });
        }
        gamecontainer.$set(gamecontainer_changes);
        if (!current || dirty[0] & /*isActive*/2) {
          toggle_class(div1, "isActive", /*isActive*/ctx[1]);
        }
        if (!current || dirty[0] & /*isFullScreen, isFullScreenIos*/68) {
          toggle_class(div1, "isFullScreen", /*isFullScreen*/ctx[6] || /*isFullScreenIos*/ctx[2]);
        }
        if (!current || dirty[0] & /*isGameOpen*/128) {
          toggle_class(div1, "isGameOpen", /*isGameOpen*/ctx[7]);
        }
        if (!current || dirty[0] & /*isEnded*/4096) {
          toggle_class(div1, "isEnded", /*isEnded*/ctx[12]);
        }
      },
      i: function intro(local) {
        if (current) return;
        transition_in(if_block0);
        transition_in(if_block1);
        transition_in(gamecontainer.$$.fragment, local);
        transition_in(timebar.$$.fragment, local);
        current = true;
      },
      o: function outro(local) {
        transition_out(if_block0);
        transition_out(if_block1);
        transition_out(gamecontainer.$$.fragment, local);
        transition_out(timebar.$$.fragment, local);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div1);
        if (if_block0) if_block0.d();
        if (if_block1) if_block1.d();
        destroy_component(gamecontainer);
        destroy_component(timebar);
        /*div0_binding*/
        ctx[23](null);
        /*div1_binding*/
        ctx[24](null);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$4.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$4($$self, $$props, $$invalidate) {
    var $players;
    var $activePlayerCurrentTime;
    var $gameClosed;
    validate_store(players, 'players');
    component_subscribe($$self, players, function ($$value) {
      return $$invalidate(28, $players = $$value);
    });
    validate_store(activePlayerCurrentTime, 'activePlayerCurrentTime');
    component_subscribe($$self, activePlayerCurrentTime, function ($$value) {
      return $$invalidate(29, $activePlayerCurrentTime = $$value);
    });
    validate_store(gameClosed, 'gameClosed');
    component_subscribe($$self, gameClosed, function ($$value) {
      return $$invalidate(21, $gameClosed = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('Video', slots, []);
    var settings = $$props.settings;
    var _$$props$isActive = $$props.isActive,
      isActive = _$$props$isActive === void 0 ? false : _$$props$isActive;
    var _$$props$onEnd = $$props.onEnd,
      onEnd = _$$props$onEnd === void 0 ? function () {} : _$$props$onEnd;
    var isFullScreenIos = $$props.isFullScreenIos;
    var index = $$props.index;
    var showPoster = $$props.showPoster;
    var overlayRef;
    var isStartEventSent = false;
    var id = "video".concat(Math.floor(Math.random() * 9999));
    var myPlayer;
    var isFullScreen = false;
    var isGameOpen = false;
    var duration;
    var currentTime = 0;
    var previousMetrikMinute = 0;
    var startWaitTime = 0;
    var isShowPromo = false;
    var sliderRef;
    var isReady = false;
    var isEnded = false;
    var interactiveComponent;
    var onTimeUpdate = function onTimeUpdate() {
      $$invalidate(9, currentTime = myPlayer.currentTime());
      if (isActive) {
        set_store_value(activePlayerCurrentTime, $activePlayerCurrentTime = currentTime, $activePlayerCurrentTime);
        localStorage.setItem('gamblerLastTime', $activePlayerCurrentTime);
      }
      var newMetrikMinute = Math.floor(currentTime / 60);
      if (newMetrikMinute !== previousMetrikMinute) {
        previousMetrikMinute = newMetrikMinute;
        sendMetrik$1("WatchMinute", {
          "Gambler.WatchMinute": "".concat(settings.episode, "-").concat(newMetrikMinute)
        });
      }
    };
    var onVideoEnd = function onVideoEnd() {
      $$invalidate(12, isEnded = true);
      onEnd("ended");
    };
    var onPlay = function onPlay() {
      if (!isStartEventSent) {
        isStartEventSent = true;
        sendMetrik$1("Start", {
          "Gambler.Episode": settings.episode
        });
      }
    };
    var onWait = function onWait() {
      startWaitTime = Date.now();
    };
    var onResume = function onResume() {
      if (startWaitTime) {
        sendMetrik$1("Waiting", {
          "Gambler.WaitingSeconds": Math.round((Date.now() - startWaitTime) / 1000)
        });
      }
      startWaitTime = 0;
    };
    var forwardEvent = function forwardEvent(event) {
      var newEvent = event.touches ? new TouchEvent(event.type, event) : new MouseEvent(event.type, event);
      overlayRef.querySelector(".vjs-slider").dispatchEvent(newEvent);
    };
    var onCanplaythrough = function onCanplaythrough() {
      $$invalidate(8, duration = myPlayer.duration());
      var element = overlayRef.querySelector(".vjs-fullscreen-control");
      var clone = element.cloneNode(true);
      element.parentNode.replaceChild(clone, element);
      [".vjs-current-time", ".vjs-time-divider", ".vjs-duration"].forEach(function (selector) {
        ["mousedown", "mouseup", "touchstart", "touchmove", "touchend", "mousemove", "click"].forEach(function (event) {
          overlayRef.querySelector(selector).addEventListener(event, forwardEvent);
        });
      });
    };
    var onVolumeChange = function onVolumeChange() {
      var volume = myPlayer.volume();
      window.azure.volume(volume);
    };
    var mountSlider = function mountSlider() {
      var target = overlayRef.querySelector('.amp-controlbaricons-middle');
      if (!target) {
        setTimeout(mountSlider, 10);
        return;
      }
      target.appendChild(sliderRef);
      $$invalidate(10, sliderRef.style.visibility = 'visible', sliderRef);
    };
    var onPause = function onPause() {
      sendMetrik$1("Pause", {
        "Gambler.PauseEpisode": settings.episode
      });
    };
    var init = function init() {
      if (!window.amp || !overlayRef) {
        return setTimeout(init, 10);
      }
      $$invalidate(3, myPlayer = window.amp(id, {
        nativeControlsForTouch: false,
        autoplay: false,
        // muted: true,
        controls: true,
        fluid: true,
        poster: showPoster ? "".concat(BASE_URL, "/images/poster.jpg") : "",
        width: "640",
        logo: {
          enabled: false
        }
      }, function () {
        mountSlider();
        $$invalidate(11, isReady = true);
        this.removeEventListener("ended", onVideoEnd);
        this.addEventListener("ended", onVideoEnd);
        this.addEventListener("durationchange", function () {
          $$invalidate(8, duration = myPlayer.duration());
        });
        this.removeEventListener("volumechange", onVolumeChange);
        this.addEventListener("volumechange", onVolumeChange);
        this.removeEventListener("pause", onPause);
        this.addEventListener("pause", onPause);
        this.removeEventListener("play", onPlay);
        this.addEventListener("play", onPlay);
        this.removeEventListener("waiting", onWait);
        this.addEventListener("waiting", onWait);
        this.removeEventListener("resume", onResume);
        this.addEventListener("resume", onResume);
        this.removeEventListener("timeupdate", onTimeUpdate);
        this.addEventListener("timeupdate", onTimeUpdate);
        this.removeEventListener("canplaythrough", onCanplaythrough);
        this.addEventListener("canplaythrough", onCanplaythrough);
        set_store_value(players, $players[index] = myPlayer, $players);
      }));
      myPlayer.src([{
        src: settings.src,
        type: "application/vnd.ms-sstr+xml"
      }]);
    };
    init();
    var fullscreenchange = function fullscreenchange() {
      $$invalidate(6, isFullScreen = Boolean(document.fullscreenElement || document.webkitCurrentFullScreenElement));
    };
    onMount(function () {
      init();
      document.addEventListener("fullscreenchange", fullscreenchange);
      document.addEventListener("webkitfullscreenchange", fullscreenchange);
      return function () {
        myPlayer.dispose();
        document.removeEventListener("fullscreenchange", fullscreenchange);
        document.removeEventListener("webkitfullscreenchange", fullscreenchange);
      };
    });
    var nextVideo = function nextVideo() {
      myPlayer.pause();
      onEnd("nextVideo");
    };
    var openGame = function openGame() {
      myPlayer.pause();
      $$invalidate(7, isGameOpen = true);
    };
    var closeGame = function closeGame() {
      $$invalidate(7, isGameOpen = false);
      onEnd("closeGame");
    };
    $$self.$$.on_mount.push(function () {
      if (settings === undefined && !('settings' in $$props || $$self.$$.bound[$$self.$$.props['settings']])) {
        console.warn("<Video> was created without expected prop 'settings'");
      }
      if (isFullScreenIos === undefined && !('isFullScreenIos' in $$props || $$self.$$.bound[$$self.$$.props['isFullScreenIos']])) {
        console.warn("<Video> was created without expected prop 'isFullScreenIos'");
      }
      if (index === undefined && !('index' in $$props || $$self.$$.bound[$$self.$$.props['index']])) {
        console.warn("<Video> was created without expected prop 'index'");
      }
      if (showPoster === undefined && !('showPoster' in $$props || $$self.$$.bound[$$self.$$.props['showPoster']])) {
        console.warn("<Video> was created without expected prop 'showPoster'");
      }
    });
    var writable_props = ['settings', 'isActive', 'onEnd', 'isFullScreenIos', 'index', 'showPoster'];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<Video> was created with unknown prop '".concat(key, "'"));
    });
    function gamecontainer_isShowPromo_binding(value) {
      isShowPromo = value;
      $$invalidate(4, isShowPromo);
    }
    function div0_binding($$value) {
      binding_callbacks[$$value ? 'unshift' : 'push'](function () {
        sliderRef = $$value;
        $$invalidate(10, sliderRef);
      });
    }
    function div1_binding($$value) {
      binding_callbacks[$$value ? 'unshift' : 'push'](function () {
        overlayRef = $$value;
        $$invalidate(5, overlayRef);
      });
    }
    $$self.$$set = function ($$props) {
      if ('settings' in $$props) $$invalidate(0, settings = $$props.settings);
      if ('isActive' in $$props) $$invalidate(1, isActive = $$props.isActive);
      if ('onEnd' in $$props) $$invalidate(18, onEnd = $$props.onEnd);
      if ('isFullScreenIos' in $$props) $$invalidate(2, isFullScreenIos = $$props.isFullScreenIos);
      if ('index' in $$props) $$invalidate(19, index = $$props.index);
      if ('showPoster' in $$props) $$invalidate(20, showPoster = $$props.showPoster);
    };
    $$self.$capture_state = function () {
      return {
        onMount: onMount,
        isIos: isIos,
        BASE_URL: BASE_URL,
        gameClosed: gameClosed,
        players: players,
        activePlayerCurrentTime: activePlayerCurrentTime,
        TimeBar: TimeBar,
        interactive: interactive,
        ActiveArea: Index,
        GameContainer: GameContainer,
        sendMetrik: sendMetrik$1,
        settings: settings,
        isActive: isActive,
        onEnd: onEnd,
        isFullScreenIos: isFullScreenIos,
        index: index,
        showPoster: showPoster,
        overlayRef: overlayRef,
        isStartEventSent: isStartEventSent,
        id: id,
        myPlayer: myPlayer,
        isFullScreen: isFullScreen,
        isGameOpen: isGameOpen,
        duration: duration,
        currentTime: currentTime,
        previousMetrikMinute: previousMetrikMinute,
        startWaitTime: startWaitTime,
        isShowPromo: isShowPromo,
        sliderRef: sliderRef,
        isReady: isReady,
        isEnded: isEnded,
        interactiveComponent: interactiveComponent,
        onTimeUpdate: onTimeUpdate,
        onVideoEnd: onVideoEnd,
        onPlay: onPlay,
        onWait: onWait,
        onResume: onResume,
        forwardEvent: forwardEvent,
        onCanplaythrough: onCanplaythrough,
        onVolumeChange: onVolumeChange,
        mountSlider: mountSlider,
        onPause: onPause,
        init: init,
        fullscreenchange: fullscreenchange,
        nextVideo: nextVideo,
        openGame: openGame,
        closeGame: closeGame,
        $players: $players,
        $activePlayerCurrentTime: $activePlayerCurrentTime,
        $gameClosed: $gameClosed
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('settings' in $$props) $$invalidate(0, settings = $$props.settings);
      if ('isActive' in $$props) $$invalidate(1, isActive = $$props.isActive);
      if ('onEnd' in $$props) $$invalidate(18, onEnd = $$props.onEnd);
      if ('isFullScreenIos' in $$props) $$invalidate(2, isFullScreenIos = $$props.isFullScreenIos);
      if ('index' in $$props) $$invalidate(19, index = $$props.index);
      if ('showPoster' in $$props) $$invalidate(20, showPoster = $$props.showPoster);
      if ('overlayRef' in $$props) $$invalidate(5, overlayRef = $$props.overlayRef);
      if ('isStartEventSent' in $$props) isStartEventSent = $$props.isStartEventSent;
      if ('myPlayer' in $$props) $$invalidate(3, myPlayer = $$props.myPlayer);
      if ('isFullScreen' in $$props) $$invalidate(6, isFullScreen = $$props.isFullScreen);
      if ('isGameOpen' in $$props) $$invalidate(7, isGameOpen = $$props.isGameOpen);
      if ('duration' in $$props) $$invalidate(8, duration = $$props.duration);
      if ('currentTime' in $$props) $$invalidate(9, currentTime = $$props.currentTime);
      if ('previousMetrikMinute' in $$props) previousMetrikMinute = $$props.previousMetrikMinute;
      if ('startWaitTime' in $$props) startWaitTime = $$props.startWaitTime;
      if ('isShowPromo' in $$props) $$invalidate(4, isShowPromo = $$props.isShowPromo);
      if ('sliderRef' in $$props) $$invalidate(10, sliderRef = $$props.sliderRef);
      if ('isReady' in $$props) $$invalidate(11, isReady = $$props.isReady);
      if ('isEnded' in $$props) $$invalidate(12, isEnded = $$props.isEnded);
      if ('interactiveComponent' in $$props) $$invalidate(13, interactiveComponent = $$props.interactiveComponent);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    $$self.$$.update = function () {
      if ($$self.$$.dirty[0] & /*$gameClosed, settings, isActive, isShowPromo*/2097171) {
        {
          if ($gameClosed && interactive[settings.interactive] && isActive && (settings.episode === "12" || settings.episode === "22")) {
            $$invalidate(13, interactiveComponent = interactive["PROMO"]);
          } else if (interactive[settings.interactive] && isShowPromo && isActive && (settings.episode === "12" || settings.episode === "22")) {
            $$invalidate(13, interactiveComponent = interactive["PROMO"]);
          } else if (isActive) {
            $$invalidate(13, interactiveComponent = interactive[settings.interactive]);
          }
        }
      }
      if ($$self.$$.dirty[0] & /*isActive, myPlayer, showPoster, settings*/1048587) {
        {
          if (isActive && myPlayer) {
            $$invalidate(8, duration = myPlayer.duration());
            window.azure = myPlayer;
            if (!showPoster) {
              sendMetrik$1("Start", {
                "Gambler.Episode": settings.episode
              });
              myPlayer.play();
            }
          }
        }
      }
    };
    return [settings, isActive, isFullScreenIos, myPlayer, isShowPromo, overlayRef, isFullScreen, isGameOpen, duration, currentTime, sliderRef, isReady, isEnded, interactiveComponent, id, nextVideo, openGame, closeGame, onEnd, index, showPoster, $gameClosed, gamecontainer_isShowPromo_binding, div0_binding, div1_binding];
  }
  var Video = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(Video, _SvelteComponentDev);
    var _super = _createSuper$4(Video);
    function Video(options) {
      var _this;
      _classCallCheck(this, Video);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$4, create_fragment$4, safe_not_equal, {
        settings: 0,
        isActive: 1,
        onEnd: 18,
        isFullScreenIos: 2,
        index: 19,
        showPoster: 20
      }, null, [-1, -1]);
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "Video",
        options: options,
        id: create_fragment$4.name
      });
      return _this;
    }
    _createClass(Video, [{
      key: "settings",
      get: function get() {
        throw new Error("<Video>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Video>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "isActive",
      get: function get() {
        throw new Error("<Video>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Video>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "onEnd",
      get: function get() {
        throw new Error("<Video>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Video>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "isFullScreenIos",
      get: function get() {
        throw new Error("<Video>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Video>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "index",
      get: function get() {
        throw new Error("<Video>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Video>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }, {
      key: "showPoster",
      get: function get() {
        throw new Error("<Video>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      },
      set: function set(value) {
        throw new Error("<Video>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
      }
    }]);
    return Video;
  }(SvelteComponentDev);

  function _createSuper$3(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$3(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$3() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$3 = "src/Header.svelte";
  function create_fragment$3(ctx) {
    var link0;
    var script0;
    var script0_src_value;
    var link1;
    var link2;
    var link3;
    var link4;
    var link5;
    var link6;
    var link7;
    var link8;
    var link9;
    var link10;
    var link11;
    var link12;
    var script1;
    var script2;
    var script2_src_value;
    var script3;
    var block = {
      c: function create() {
        link0 = element("link");
        script0 = element("script");
        link1 = element("link");
        link2 = element("link");
        link3 = element("link");
        link4 = element("link");
        link5 = element("link");
        link6 = element("link");
        link7 = element("link");
        link8 = element("link");
        link9 = element("link");
        link10 = element("link");
        link11 = element("link");
        link12 = element("link");
        script1 = element("script");
        script1.textContent = "(function (m, e, t, r, i, k, a) {\n      m[i] =\n        m[i] ||\n        function () {\n          (m[i].a = m[i].a || []).push(arguments);\n        };\n      m[i].l = 1 * new Date();\n      (k = e.createElement(t)),\n        (a = e.getElementsByTagName(t)[0]),\n        (k.async = 1),\n        (k.src = r),\n        a.parentNode.insertBefore(k, a);\n    })(window, document, \"script\", \"https://mc.yandex.ru/metrika/tag.js\", \"ym\");\n\n    ym(70641691, \"init\", {\n      clickmap: false,\n      trackLinks: false,\n      accurateTrackBounce: true,\n      webvisor: false,\n    });\n  ";
        script2 = element("script");
        script3 = element("script");
        script3.textContent = "window.dataLayer = window.dataLayer || [];\n    function gtag() {\n      dataLayer.push(arguments);\n    }\n    gtag(\"js\", new Date());\n\n    gtag(\"config\", \"G-9E56DRM245\", {\n      linker: {\n        domains: [\"igra.film\", \"screenlife.tech\"],\n      },\n    });";
        attr_dev(link0, "href", "//amp.azure.net/libs/amp/latest/skins/amp-default/azuremediaplayer.min.css");
        attr_dev(link0, "rel", "stylesheet");
        add_location(link0, file$3, 5, 2, 85);
        if (!src_url_equal(script0.src, script0_src_value = "//amp.azure.net/libs/amp/latest/azuremediaplayer.min.js")) attr_dev(script0, "src", script0_src_value);
        add_location(script0, file$3, 8, 2, 203);
        attr_dev(link1, "rel", "preload");
        attr_dev(link1, "href", "".concat(BASE_URL, "/images/chip1.png"));
        attr_dev(link1, "as", "image");
        add_location(link1, file$3, 10, 2, 288);
        attr_dev(link2, "rel", "preload");
        attr_dev(link2, "href", "".concat(BASE_URL, "/images/chip2.png"));
        attr_dev(link2, "as", "image");
        add_location(link2, file$3, 11, 2, 362);
        attr_dev(link3, "rel", "preload");
        attr_dev(link3, "href", "".concat(BASE_URL, "/images/chip3.png"));
        attr_dev(link3, "as", "image");
        add_location(link3, file$3, 12, 2, 436);
        attr_dev(link4, "rel", "preload");
        attr_dev(link4, "href", "".concat(BASE_URL, "/images/chip4.png"));
        attr_dev(link4, "as", "image");
        add_location(link4, file$3, 13, 2, 510);
        attr_dev(link5, "rel", "preload");
        attr_dev(link5, "href", "".concat(BASE_URL, "/images/button.png"));
        attr_dev(link5, "as", "image");
        add_location(link5, file$3, 14, 2, 584);
        attr_dev(link6, "rel", "preload");
        attr_dev(link6, "href", "".concat(BASE_URL, "/images/cards.png"));
        attr_dev(link6, "as", "image");
        add_location(link6, file$3, 15, 2, 659);
        attr_dev(link7, "rel", "preload");
        attr_dev(link7, "href", "".concat(BASE_URL, "/images/continue.png"));
        attr_dev(link7, "as", "image");
        add_location(link7, file$3, 16, 2, 733);
        attr_dev(link8, "rel", "preload");
        attr_dev(link8, "href", "".concat(BASE_URL, "/images/help1.png"));
        attr_dev(link8, "as", "image");
        add_location(link8, file$3, 17, 2, 810);
        attr_dev(link9, "rel", "preload");
        attr_dev(link9, "href", "".concat(BASE_URL, "/images/help2.png"));
        attr_dev(link9, "as", "image");
        add_location(link9, file$3, 18, 2, 884);
        attr_dev(link10, "rel", "preload");
        attr_dev(link10, "href", "".concat(BASE_URL, "/images/logo.png"));
        attr_dev(link10, "as", "image");
        add_location(link10, file$3, 19, 2, 958);
        attr_dev(link11, "rel", "preload");
        attr_dev(link11, "href", "".concat(BASE_URL, "/images/credits.png"));
        attr_dev(link11, "as", "image");
        add_location(link11, file$3, 20, 2, 1031);
        attr_dev(link12, "rel", "preload");
        attr_dev(link12, "href", "".concat(BASE_URL, "/images/charitybg.jpg"));
        attr_dev(link12, "as", "image");
        add_location(link12, file$3, 21, 2, 1107);
        add_location(script1, file$3, 22, 2, 1185);
        script2.async = true;
        if (!src_url_equal(script2.src, script2_src_value = "https://www.googletagmanager.com/gtag/js?id=G-9E56DRM245")) attr_dev(script2, "src", script2_src_value);
        add_location(script2, file$3, 45, 2, 1828);
        add_location(script3, file$3, 47, 2, 1920);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        append_dev(document.head, link0);
        append_dev(document.head, script0);
        append_dev(document.head, link1);
        append_dev(document.head, link2);
        append_dev(document.head, link3);
        append_dev(document.head, link4);
        append_dev(document.head, link5);
        append_dev(document.head, link6);
        append_dev(document.head, link7);
        append_dev(document.head, link8);
        append_dev(document.head, link9);
        append_dev(document.head, link10);
        append_dev(document.head, link11);
        append_dev(document.head, link12);
        append_dev(document.head, script1);
        append_dev(document.head, script2);
        append_dev(document.head, script3);
      },
      p: noop,
      i: noop,
      o: noop,
      d: function destroy(detaching) {
        detach_dev(link0);
        detach_dev(script0);
        detach_dev(link1);
        detach_dev(link2);
        detach_dev(link3);
        detach_dev(link4);
        detach_dev(link5);
        detach_dev(link6);
        detach_dev(link7);
        detach_dev(link8);
        detach_dev(link9);
        detach_dev(link10);
        detach_dev(link11);
        detach_dev(link12);
        detach_dev(script1);
        detach_dev(script2);
        detach_dev(script3);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$3.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$3($$self, $$props, $$invalidate) {
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('Header', slots, []);
    var writable_props = [];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<Header> was created with unknown prop '".concat(key, "'"));
    });
    $$self.$capture_state = function () {
      return {
        BASE_URL: BASE_URL,
        IS_TEST: IS_TEST$1
      };
    };
    return [];
  }
  var Header = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(Header, _SvelteComponentDev);
    var _super = _createSuper$3(Header);
    function Header(options) {
      var _this;
      _classCallCheck(this, Header);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$3, create_fragment$3, safe_not_equal, {});
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "Header",
        options: options,
        id: create_fragment$3.name
      });
      return _this;
    }
    return _createClass(Header);
  }(SvelteComponentDev);

  function _createSuper$2(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$2(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$2() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var file$2 = "src/AdContainer.svelte";

  // (10:0) {#if $iframeLink}
  function create_if_block$1(ctx) {
    var div3;
    var div2;
    var div1;
    var img;
    var img_src_value;
    var t0;
    var div0;
    var t1;
    var iframe;
    var iframe_src_value;
    var mounted;
    var dispose;
    var block = {
      c: function create() {
        div3 = element("div");
        div2 = element("div");
        div1 = element("div");
        img = element("img");
        t0 = space();
        div0 = element("div");
        t1 = space();
        iframe = element("iframe");
        if (!src_url_equal(img.src, img_src_value = "".concat(BASE_URL, "/images/continuewhite.png"))) attr_dev(img, "src", img_src_value);
        attr_dev(img, "alt", "continue");
        attr_dev(img, "class", "closeImage svelte-qof5mi");
        add_location(img, file$2, 13, 8, 356);
        attr_dev(div0, "class", "shine svelte-qof5mi");
        add_location(div0, file$2, 17, 8, 481);
        attr_dev(div1, "class", "closeButton svelte-qof5mi");
        add_location(div1, file$2, 12, 6, 304);
        attr_dev(div2, "class", "header svelte-qof5mi");
        add_location(div2, file$2, 11, 4, 277);
        if (!src_url_equal(iframe.src, iframe_src_value = /*$iframeLink*/ctx[0])) attr_dev(iframe, "src", iframe_src_value);
        attr_dev(iframe, "frameborder", "0");
        attr_dev(iframe, "allow", "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture");
        iframe.allowFullscreen = "";
        attr_dev(iframe, "title", "externalLink");
        attr_dev(iframe, "class", "svelte-qof5mi");
        add_location(iframe, file$2, 20, 4, 531);
        attr_dev(div3, "class", "container svelte-qof5mi");
        add_location(div3, file$2, 10, 2, 249);
      },
      m: function mount(target, anchor) {
        insert_dev(target, div3, anchor);
        append_dev(div3, div2);
        append_dev(div2, div1);
        append_dev(div1, img);
        append_dev(div1, t0);
        append_dev(div1, div0);
        append_dev(div3, t1);
        append_dev(div3, iframe);
        if (!mounted) {
          dispose = listen_dev(div1, "click", /*skipAd*/ctx[1], false, false, false, false);
          mounted = true;
        }
      },
      p: function update(ctx, dirty) {
        if (dirty & /*$iframeLink*/1 && !src_url_equal(iframe.src, iframe_src_value = /*$iframeLink*/ctx[0])) {
          attr_dev(iframe, "src", iframe_src_value);
        }
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(div3);
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block$1.name,
      type: "if",
      source: "(10:0) {#if $iframeLink}",
      ctx: ctx
    });
    return block;
  }
  function create_fragment$2(ctx) {
    var if_block_anchor;
    var if_block = /*$iframeLink*/ctx[0] && create_if_block$1(ctx);
    var block = {
      c: function create() {
        if (if_block) if_block.c();
        if_block_anchor = empty();
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        if (if_block) if_block.m(target, anchor);
        insert_dev(target, if_block_anchor, anchor);
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if ( /*$iframeLink*/ctx[0]) {
          if (if_block) {
            if_block.p(ctx, dirty);
          } else {
            if_block = create_if_block$1(ctx);
            if_block.c();
            if_block.m(if_block_anchor.parentNode, if_block_anchor);
          }
        } else if (if_block) {
          if_block.d(1);
          if_block = null;
        }
      },
      i: noop,
      o: noop,
      d: function destroy(detaching) {
        if (if_block) if_block.d(detaching);
        if (detaching) detach_dev(if_block_anchor);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$2.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$2($$self, $$props, $$invalidate) {
    var $iframeLink;
    validate_store(iframeLink, 'iframeLink');
    component_subscribe($$self, iframeLink, function ($$value) {
      return $$invalidate(0, $iframeLink = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('AdContainer', slots, []);
    var skipAd = function skipAd() {
      sendMetrik$1('AdReturn', {
        'Gambler.AdReturnLink': $iframeLink
      });
      set_store_value(iframeLink, $iframeLink = null, $iframeLink);
      window.azure.play();
    };
    var writable_props = [];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<AdContainer> was created with unknown prop '".concat(key, "'"));
    });
    $$self.$capture_state = function () {
      return {
        iframeLink: iframeLink,
        BASE_URL: BASE_URL,
        sendMetrik: sendMetrik$1,
        skipAd: skipAd,
        $iframeLink: $iframeLink
      };
    };
    return [$iframeLink, skipAd];
  }
  var AdContainer = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(AdContainer, _SvelteComponentDev);
    var _super = _createSuper$2(AdContainer);
    function AdContainer(options) {
      var _this;
      _classCallCheck(this, AdContainer);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$2, create_fragment$2, safe_not_equal, {});
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "AdContainer",
        options: options,
        id: create_fragment$2.name
      });
      return _this;
    }
    return _createClass(AdContainer);
  }(SvelteComponentDev);

  function _createSuper$1(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$1(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct$1() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var console_1 = globals.console;
  var file$1 = "src/DebugControls.svelte";

  // (1:0) {#if window.location.href === 'http://localhost:5000/'}
  function create_if_block(ctx) {
    var div;
    var button0;
    var t1;
    var button1;
    var t3;
    var button2;
    var t5;
    var button3;
    var t7;
    var button4;
    var mounted;
    var dispose;
    var block = {
      c: function create() {
        div = element("div");
        button0 = element("button");
        button0.textContent = "+1";
        t1 = space();
        button1 = element("button");
        button1.textContent = "-1";
        t3 = space();
        button2 = element("button");
        button2.textContent = "+0.1";
        t5 = space();
        button3 = element("button");
        button3.textContent = "-0.1";
        t7 = space();
        button4 = element("button");
        button4.textContent = "log";
        add_location(button0, file$1, 2, 4, 82);
        add_location(button1, file$1, 4, 4, 186);
        add_location(button2, file$1, 6, 4, 290);
        add_location(button3, file$1, 8, 4, 398);
        add_location(button4, file$1, 10, 4, 506);
        attr_dev(div, "class", "debug svelte-141yoe8");
        add_location(div, file$1, 1, 2, 58);
      },
      m: function mount(target, anchor) {
        insert_dev(target, div, anchor);
        append_dev(div, button0);
        append_dev(div, t1);
        append_dev(div, button1);
        append_dev(div, t3);
        append_dev(div, button2);
        append_dev(div, t5);
        append_dev(div, button3);
        append_dev(div, t7);
        append_dev(div, button4);
        if (!mounted) {
          dispose = [listen_dev(button0, "click", /*click_handler*/ctx[0], false, false, false, false), listen_dev(button1, "click", /*click_handler_1*/ctx[1], false, false, false, false), listen_dev(button2, "click", /*click_handler_2*/ctx[2], false, false, false, false), listen_dev(button3, "click", /*click_handler_3*/ctx[3], false, false, false, false), listen_dev(button4, "click", /*click_handler_4*/ctx[4], false, false, false, false)];
          mounted = true;
        }
      },
      p: noop,
      d: function destroy(detaching) {
        if (detaching) detach_dev(div);
        mounted = false;
        run_all(dispose);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_if_block.name,
      type: "if",
      source: "(1:0) {#if window.location.href === 'http://localhost:5000/'}",
      ctx: ctx
    });
    return block;
  }
  function create_fragment$1(ctx) {
    var if_block_anchor;
    var if_block = window.location.href === 'http://localhost:5000/' && create_if_block(ctx);
    var block = {
      c: function create() {
        if (if_block) if_block.c();
        if_block_anchor = empty();
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        if (if_block) if_block.m(target, anchor);
        insert_dev(target, if_block_anchor, anchor);
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if (window.location.href === 'http://localhost:5000/') if_block.p(ctx, dirty);
      },
      i: noop,
      o: noop,
      d: function destroy(detaching) {
        if (if_block) if_block.d(detaching);
        if (detaching) detach_dev(if_block_anchor);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment$1.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance$1($$self, $$props) {
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('DebugControls', slots, []);
    var writable_props = [];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console_1.warn("<DebugControls> was created with unknown prop '".concat(key, "'"));
    });
    var click_handler = function click_handler() {
      return window.azure.currentTime(window.azure.currentTime() + 1);
    };
    var click_handler_1 = function click_handler_1() {
      return window.azure.currentTime(window.azure.currentTime() - 1);
    };
    var click_handler_2 = function click_handler_2() {
      return window.azure.currentTime(window.azure.currentTime() + 0.1);
    };
    var click_handler_3 = function click_handler_3() {
      return window.azure.currentTime(window.azure.currentTime() - 0.1);
    };
    var click_handler_4 = function click_handler_4() {
      return console.log(window.azure.currentTime());
    };
    return [click_handler, click_handler_1, click_handler_2, click_handler_3, click_handler_4];
  }
  var DebugControls = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(DebugControls, _SvelteComponentDev);
    var _super = _createSuper$1(DebugControls);
    function DebugControls(options) {
      var _this;
      _classCallCheck(this, DebugControls);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance$1, create_fragment$1, safe_not_equal, {});
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "DebugControls",
        options: options,
        id: create_fragment$1.name
      });
      return _this;
    }
    return _createClass(DebugControls);
  }(SvelteComponentDev);

  !function (a) {

    function b(b, c) {
      var d = a.createEvent("Event");
      d.initEvent(b, !0, !1), c.dispatchEvent(d);
    }
    function c(c) {
      c.stopPropagation(), c.stopImmediatePropagation(), a[j.enabled] = a[f.enabled], a[j.element] = a[f.element], b(j.events.change, c.target);
    }
    function d(a) {
      b(j.events.error, a.target);
    }
    function e(b) {
      return function (c, d) {
        function e() {
          c(), a.removeEventListener(f.events.change, e, !1);
        }
        function g() {
          d(new TypeError()), a.removeEventListener(f.events.error, g, !1);
        }
        return b !== j.exit || a[f.element] ? (a.addEventListener(f.events.change, e, !1), void a.addEventListener(f.events.error, g, !1)) : void setTimeout(function () {
          d(new TypeError());
        }, 1);
      };
    }
    var f,
      g,
      i = {
        w3: {
          enabled: "fullscreenEnabled",
          element: "fullscreenElement",
          request: "requestFullscreen",
          exit: "exitFullscreen",
          events: {
            change: "fullscreenchange",
            error: "fullscreenerror"
          }
        },
        webkit: {
          enabled: "webkitFullscreenEnabled",
          element: "webkitCurrentFullScreenElement",
          request: "webkitRequestFullscreen",
          exit: "webkitExitFullscreen",
          events: {
            change: "webkitfullscreenchange",
            error: "webkitfullscreenerror"
          }
        },
        moz: {
          enabled: "mozFullScreenEnabled",
          element: "mozFullScreenElement",
          request: "mozRequestFullScreen",
          exit: "mozCancelFullScreen",
          events: {
            change: "mozfullscreenchange",
            error: "mozfullscreenerror"
          }
        },
        ms: {
          enabled: "msFullscreenEnabled",
          element: "msFullscreenElement",
          request: "msRequestFullscreen",
          exit: "msExitFullscreen",
          events: {
            change: "MSFullscreenChange",
            error: "MSFullscreenError"
          }
        }
      },
      j = i.w3;
    for (g in i) if (i[g].enabled in a) {
      f = i[g];
      break;
    }
    return j.enabled in a || !f || (a.addEventListener(f.events.change, c, !1), a.addEventListener(f.events.error, d, !1), a[j.enabled] = a[f.enabled], a[j.element] = a[f.element], a[j.exit] = function () {
      var b = a[f.exit]();
      return !b && Promise ? new Promise(e(j.exit)) : b;
    }, Element.prototype[j.request] = function () {
      var a = this[f.request].apply(this, arguments);
      return !a && Promise ? new Promise(e(j.request)) : a;
    }), f;
  }(document);

  function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
  function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
  var window_1 = globals.window;
  var file = "src/App.svelte";
  function get_each_context(ctx, list, i) {
    var child_ctx = ctx.slice();
    child_ctx[16] = list[i];
    child_ctx[18] = i;
    return child_ctx;
  }

  // (102:2) {#each sequence as video, i (video.src + video.episode)}
  function create_each_block(key_1, ctx) {
    var first;
    var video;
    var current;
    video = new Video({
      props: {
        index: /*i*/ctx[18],
        showPoster: /*i*/ctx[18] === 0,
        settings: /*video*/ctx[16],
        isActive: /*$activeIndex*/ctx[5] === /*i*/ctx[18],
        onEnd: /*nextVideo*/ctx[6],
        isFullScreenIos: /*isFullScreenIos*/ctx[3]
      },
      $$inline: true
    });
    var block = {
      key: key_1,
      first: null,
      c: function create() {
        first = empty();
        create_component(video.$$.fragment);
        this.first = first;
      },
      m: function mount(target, anchor) {
        insert_dev(target, first, anchor);
        mount_component(video, target, anchor);
        current = true;
      },
      p: function update(new_ctx, dirty) {
        ctx = new_ctx;
        var video_changes = {};
        if (dirty & /*$activeIndex*/32) video_changes.isActive = /*$activeIndex*/ctx[5] === /*i*/ctx[18];
        if (dirty & /*isFullScreenIos*/8) video_changes.isFullScreenIos = /*isFullScreenIos*/ctx[3];
        video.$set(video_changes);
      },
      i: function intro(local) {
        if (current) return;
        transition_in(video.$$.fragment, local);
        current = true;
      },
      o: function outro(local) {
        transition_out(video.$$.fragment, local);
        current = false;
      },
      d: function destroy(detaching) {
        if (detaching) detach_dev(first);
        destroy_component(video, detaching);
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_each_block.name,
      type: "each",
      source: "(102:2) {#each sequence as video, i (video.src + video.episode)}",
      ctx: ctx
    });
    return block;
  }
  function create_fragment(ctx) {
    var debugcontrols;
    var t0;
    var div;
    var each_blocks = [];
    var each_1_lookup = new Map();
    var t1;
    var adcontainer;
    var t2;
    var audio;
    var t3;
    var header;
    var current;
    var mounted;
    var dispose;
    add_render_callback( /*onwindowresize*/ctx[7]);
    debugcontrols = new DebugControls({
      $$inline: true
    });
    var each_value = sequence;
    validate_each_argument(each_value);
    var get_key = function get_key(ctx) {
      return (/*video*/ctx[16].src + /*video*/ctx[16].episode
      );
    };
    validate_each_keys(ctx, each_value, get_each_context, get_key);
    for (var i = 0; i < each_value.length; i += 1) {
      var child_ctx = get_each_context(ctx, each_value, i);
      var key = get_key(child_ctx);
      each_1_lookup.set(key, each_blocks[i] = create_each_block(key, child_ctx));
    }
    adcontainer = new AdContainer({
      $$inline: true
    });
    audio = new Audio({
      $$inline: true
    });
    header = new Header({
      $$inline: true
    });
    var block = {
      c: function create() {
        create_component(debugcontrols.$$.fragment);
        t0 = space();
        div = element("div");
        for (var _i = 0; _i < each_blocks.length; _i += 1) {
          each_blocks[_i].c();
        }
        t1 = space();
        create_component(adcontainer.$$.fragment);
        t2 = space();
        create_component(audio.$$.fragment);
        t3 = space();
        create_component(header.$$.fragment);
        attr_dev(div, "style", /*style*/ctx[4]);
        attr_dev(div, "class", "container svelte-1cncxen");
        toggle_class(div, "isFullScreen", /*isFullScreen*/ctx[2]);
        toggle_class(div, "isFullScreenIos", /*isFullScreenIos*/ctx[3]);
        toggle_class(div, "isIos", isIos());
        add_location(div, file, 94, 0, 3083);
      },
      l: function claim(nodes) {
        throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
      },
      m: function mount(target, anchor) {
        mount_component(debugcontrols, target, anchor);
        insert_dev(target, t0, anchor);
        insert_dev(target, div, anchor);
        for (var _i2 = 0; _i2 < each_blocks.length; _i2 += 1) {
          if (each_blocks[_i2]) {
            each_blocks[_i2].m(div, null);
          }
        }
        append_dev(div, t1);
        mount_component(adcontainer, div, null);
        /*div_binding*/
        ctx[8](div);
        insert_dev(target, t2, anchor);
        mount_component(audio, target, anchor);
        insert_dev(target, t3, anchor);
        mount_component(header, target, anchor);
        current = true;
        if (!mounted) {
          dispose = listen_dev(window_1, "resize", /*onwindowresize*/ctx[7]);
          mounted = true;
        }
      },
      p: function update(ctx, _ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];
        if (dirty & /*sequence, $activeIndex, nextVideo, isFullScreenIos*/104) {
          each_value = sequence;
          validate_each_argument(each_value);
          group_outros();
          validate_each_keys(ctx, each_value, get_each_context, get_key);
          each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value, each_1_lookup, div, outro_and_destroy_block, create_each_block, t1, get_each_context);
          check_outros();
        }
        if (!current || dirty & /*style*/16) {
          attr_dev(div, "style", /*style*/ctx[4]);
        }
        if (!current || dirty & /*isFullScreen*/4) {
          toggle_class(div, "isFullScreen", /*isFullScreen*/ctx[2]);
        }
        if (!current || dirty & /*isFullScreenIos*/8) {
          toggle_class(div, "isFullScreenIos", /*isFullScreenIos*/ctx[3]);
        }
      },
      i: function intro(local) {
        if (current) return;
        transition_in(debugcontrols.$$.fragment, local);
        for (var _i3 = 0; _i3 < each_value.length; _i3 += 1) {
          transition_in(each_blocks[_i3]);
        }
        transition_in(adcontainer.$$.fragment, local);
        transition_in(audio.$$.fragment, local);
        transition_in(header.$$.fragment, local);
        current = true;
      },
      o: function outro(local) {
        transition_out(debugcontrols.$$.fragment, local);
        for (var _i4 = 0; _i4 < each_blocks.length; _i4 += 1) {
          transition_out(each_blocks[_i4]);
        }
        transition_out(adcontainer.$$.fragment, local);
        transition_out(audio.$$.fragment, local);
        transition_out(header.$$.fragment, local);
        current = false;
      },
      d: function destroy(detaching) {
        destroy_component(debugcontrols, detaching);
        if (detaching) detach_dev(t0);
        if (detaching) detach_dev(div);
        for (var _i5 = 0; _i5 < each_blocks.length; _i5 += 1) {
          each_blocks[_i5].d();
        }
        destroy_component(adcontainer);
        /*div_binding*/
        ctx[8](null);
        if (detaching) detach_dev(t2);
        destroy_component(audio, detaching);
        if (detaching) detach_dev(t3);
        destroy_component(header, detaching);
        mounted = false;
        dispose();
      }
    };
    dispatch_dev("SvelteRegisterBlock", {
      block: block,
      id: create_fragment.name,
      type: "component",
      source: "",
      ctx: ctx
    });
    return block;
  }
  function instance($$self, $$props, $$invalidate) {
    var style;
    var $externalLink;
    var $activeIndex;
    var $gameWon;
    validate_store(externalLink, 'externalLink');
    component_subscribe($$self, externalLink, function ($$value) {
      return $$invalidate(9, $externalLink = $$value);
    });
    validate_store(activeIndex, 'activeIndex');
    component_subscribe($$self, activeIndex, function ($$value) {
      return $$invalidate(5, $activeIndex = $$value);
    });
    validate_store(gameWon, 'gameWon');
    component_subscribe($$self, gameWon, function ($$value) {
      return $$invalidate(10, $gameWon = $$value);
    });
    var _$$props$$$slots = $$props.$$slots,
      slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots;
      $$props.$$scope;
    validate_slots('App', slots, []);
    var containerRef;
    var isFullScreen = false;
    var isFullScreenIos = false;
    var nextVideo = function nextVideo() {
      if ($activeIndex === sequence.length - 1) return;
      if ($gameWon && sequence[$activeIndex + 1].skipIfPlayed) {
        set_store_value(activeIndex, $activeIndex += 2, $activeIndex);
      } else {
        set_store_value(activeIndex, $activeIndex += 1, $activeIndex);
      }
    };
    var onFullScreen = function onFullScreen(event) {
      if (event === 'payment' || event.target.classList.contains("vjs-fullscreen-control") || event.target.parentElement.classList.contains("vjs-fullscreen-control")) {
        if (isIos()) {
          $$invalidate(3, isFullScreenIos = !isFullScreenIos);
          return;
        }
        if (document.fullscreenElement || document.webkitCurrentFullScreenElement) {
          $$invalidate(2, isFullScreen = false);
          document.exitFullscreen();
        } else {
          $$invalidate(2, isFullScreen = true);
          containerRef.requestFullscreen();
        }
      }
    };
    var onExitFullScreen = function onExitFullScreen() {
      $$invalidate(2, isFullScreen = document.fullscreenElement || document.webkitCurrentFullScreenElement);
    };
    var onWindowFocus = function onWindowFocus() {
      parent.postMessage('startPlayer', "*");
      if ($externalLink) {
        sendMetrik$1('AdReturn', {
          'Gambler.AdReturnLink': $externalLink
        });
      }
      set_store_value(externalLink, $externalLink = '', $externalLink);
    };
    var onWindowBlur = function onWindowBlur() {
      return window.azure.pause();
    };
    var sendMessageToParent = function sendMessageToParent(event) {
      if (event.target.tagName === 'BODY' || event.target.id === 'gambler') {
        parent.postMessage('closePlayer', "*");
        window.azure.pause();
      } else {
        parent.postMessage('startPlayer', "*");
      }
    };
    onMount(function () {
      document.body.addEventListener('click', sendMessageToParent, true);
      document.body.addEventListener('touchstart', sendMessageToParent, true);
      window.addEventListener('focus', onWindowFocus);
      window.addEventListener('blur', onWindowBlur);
      setTimeout(function () {
        return sendMetrik$1('Visible');
      }, 2000);
      containerRef.addEventListener("click", onFullScreen, true);
      document.addEventListener("fullscreenchange", onExitFullScreen);
      document.addEventListener("webkitfullscreenchange", onExitFullScreen);

      // containerRef.addEventListener("touchstart", onFullScreen, true);
      return function () {
        window.removeEventListener('focus', onWindowFocus);
        containerRef.removeEventListener("click", onFullScreen);
        document.removeEventListener("fullscreenchange", onExitFullScreen);
        document.removeEventListener("webkitfullscreenchange", onExitFullScreen);
        window.removeEventListener('blur', onWindowBlur);
      };
    });
    var innerHeight;
    var writable_props = [];
    Object.keys($$props).forEach(function (key) {
      if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn("<App> was created with unknown prop '".concat(key, "'"));
    });
    function onwindowresize() {
      $$invalidate(0, innerHeight = window_1.innerHeight);
    }
    function div_binding($$value) {
      binding_callbacks[$$value ? 'unshift' : 'push'](function () {
        containerRef = $$value;
        $$invalidate(1, containerRef);
      });
    }
    $$self.$capture_state = function () {
      return {
        Video: Video,
        sequence: sequence,
        onMount: onMount,
        Audio: Audio,
        isIos: isIos,
        gameWon: gameWon,
        sendMetrik: sendMetrik$1,
        externalLink: externalLink,
        activeIndex: activeIndex,
        Header: Header,
        AdContainer: AdContainer,
        DebugControls: DebugControls,
        containerRef: containerRef,
        isFullScreen: isFullScreen,
        isFullScreenIos: isFullScreenIos,
        nextVideo: nextVideo,
        onFullScreen: onFullScreen,
        onExitFullScreen: onExitFullScreen,
        onWindowFocus: onWindowFocus,
        onWindowBlur: onWindowBlur,
        sendMessageToParent: sendMessageToParent,
        innerHeight: innerHeight,
        style: style,
        $externalLink: $externalLink,
        $activeIndex: $activeIndex,
        $gameWon: $gameWon
      };
    };
    $$self.$inject_state = function ($$props) {
      if ('containerRef' in $$props) $$invalidate(1, containerRef = $$props.containerRef);
      if ('isFullScreen' in $$props) $$invalidate(2, isFullScreen = $$props.isFullScreen);
      if ('isFullScreenIos' in $$props) $$invalidate(3, isFullScreenIos = $$props.isFullScreenIos);
      if ('innerHeight' in $$props) $$invalidate(0, innerHeight = $$props.innerHeight);
      if ('style' in $$props) $$invalidate(4, style = $$props.style);
    };
    if ($$props && "$$inject" in $$props) {
      $$self.$inject_state($$props.$$inject);
    }
    $$self.$$.update = function () {
      if ($$self.$$.dirty & /*innerHeight*/1) {
        $$invalidate(4, style = isIos() ? "max-width:".concat(innerHeight / 0.58, "px") : '');
      }
    };
    return [innerHeight, containerRef, isFullScreen, isFullScreenIos, style, $activeIndex, nextVideo, onwindowresize, div_binding];
  }
  var App = /*#__PURE__*/function (_SvelteComponentDev) {
    _inherits(App, _SvelteComponentDev);
    var _super = _createSuper(App);
    function App(options) {
      var _this;
      _classCallCheck(this, App);
      _this = _super.call(this, options);
      init(_assertThisInitialized(_this), options, instance, create_fragment, safe_not_equal, {});
      dispatch_dev("SvelteRegisterComponent", {
        component: _assertThisInitialized(_this),
        tagName: "App",
        options: options,
        id: create_fragment.name
      });
      return _this;
    }
    return _createClass(App);
  }(SvelteComponentDev);

  var app = new App({
    target: document.getElementById('gambler')
  });

  return app;

})();
//# sourceMappingURL=bundle.js.map
