export default [
    {
      src: "/images/promo/eldorado.png",
      href: "https://ad.admitad.com/coupon/pf9klv6my56a099e8870824224cf5a/",
      showInIframe: true,
    },
    {
      src: "/images/promo/market.png",
      href: "https://ad.admitad.com/g/27u6ol7uc26a099e8870b62ed2b196/?i=3",
      showInIframe: false,
    },
    {
      src: "/images/promo/aliexpress.png",
      href: "https://alitems.com/coupon/94wrvygpn76a099e88707a660ebfae/",
      showInIframe: true,
    },
    {
      src: "/images/promo/geekbrains.png",
      href: "https://ad.admitad.com/g/3cgo37g1gg6a099e887065a37ca03d/?i=3",
      showInIframe: false,
    },
    {
      src: "/images/promo/dominos.png",
      href: "https://ad.admitad.com/coupon/ok3jg5c2o76a099e887035e8fc368c/",
      showInIframe: true,
    },
  ];