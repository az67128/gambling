import One from './One.svelte'
import Two from './Two.svelte'
import Three from './Three.svelte'
import Credits from './Credits.svelte'
import Promo from './Promo.svelte'
import Promo1 from './Promo1.svelte'

export default {
    'ONE':One,
    'TWO':Two,
    'THREE':Three,
    'CREDITS': Credits,
    'PROMO': Promo,
    'PROMO1': Promo1,
}