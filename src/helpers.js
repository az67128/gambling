import { writable } from "svelte/store";
export function isIos() {
  return (
    [
      "iPad Simulator",
      "iPhone Simulator",
      "iPod Simulator",
      "iPad",
      "iPhone",
      "iPod",
    ].includes(navigator.platform) ||
    // iPad on iOS 13 detection
    (navigator.userAgent.includes("Mac") && "ontouchend" in document)
  );
}

export const isIpadOS = ()=> {
  return navigator.maxTouchPoints &&
    navigator.maxTouchPoints > 2 &&
    /MacIntel/.test(navigator.platform);
}

export const BASE_URL = (() => {
  if (location.href.includes("gitlab"))
    return "https://az67128.gitlab.io/gambling";
  if (location.href.includes("localhost") || location.href.includes("192.168"))
    return ".";
  return "https://az67128.gitlab.io/gambling";
})();

// export const IS_TEST = /(localhost|192\.168|az67128.gitlab.io)/.test(
export const IS_TEST = /(localhost|192\.168)/.test(
  window.location.href
);
export const sendMetrik = (name, payload) => {
  const consoleMetrik = (id, type, name, payload) => console.log(name, payload);
  const ym = IS_TEST ? consoleMetrik : window.ym || consoleMetrik;
  const dummy = ()=>{}
  const gtag = window.gtag || dummy;
  // const ym = window.ym || consoleMetrik;

  ym(70641691, "reachGoal", `Gambler.${name}`, payload);
  
  if(!IS_TEST) {
    gtag('event', `Gambler.${name}`, payload);
  }
};

export const externalLink = writable()
export const iframeLink = writable()


export const gameWon = writable(false);
export const gameClosed = writable(false);
export const gameSkipped = writable(true);

export const isCharityButtonPressed = writable(false);

export const activeIndex = writable(0);
export const players = writable([])
export const activePlayerCurrentTime = writable(0);

export const adDisabled = writable(false);

export const gamblerLastSeries = writable(localStorage.getItem('gamblerLastSeries'));
activeIndex.subscribe(state=>{
  localStorage.setItem('gamblerLastSeries', state)
})
export const gamblerLastTime = writable(localStorage.getItem('gamblerLastTime'));

export const location1 = writable('mom')