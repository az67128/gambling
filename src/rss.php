<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-Type: application/rss+xml; charset=utf-8');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    
    $ch = curl_init($_GET["url"] ?? 'https://teleprogramma.pro/edition-rss/');
    echo curl_exec($ch);
    exit;

?>