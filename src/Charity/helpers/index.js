import audioFiles from "../assets/audioFiles";

// export const isSunflower = Math.round(Math.random());
const audioBlobs = {};
// (() => {
//   Object.keys(audioFiles).forEach(type => {
//     audioFiles[type].forEach((item, i) => {
//       fetch(`https://az67128.gitlab.io/svelte-sunflower/assets/${item}.mp3`)
//         .then(res => res.blob())
//         .then(blob => window.URL.createObjectURL(blob))
//         .then(blob => {
//           if (!audioBlobs[type]) audioBlobs[type] = [];
//           audioBlobs[type].push(blob);
//         });
//     });
//   });
// })();

export const isSunflower = false;
const ym =
  window.ym || ((id, type, name, payload) => console.log(name, payload));

  export const IS_TEST = /(localhost|192\.168)/.test(
    window.location.href
  );

export const sendMetrik = (name, payload) =>{
  const consoleMetrik = (id, type, name, payload) => console.log(name, payload);
  const ym = IS_TEST ? consoleMetrik : window.ym || consoleMetrik;
  // const ym = window.ym || consoleMetrik;

  ym(70641691, "reachGoal", `Gambler.${name}`, payload);
}
  

export const payByCard = function(amount, onSuccess = () => {}) {
  sendMetrik("PaymentCard");
  setTimeout(() => {
    var widget = new cp.CloudPayments();
    widget.charge(
      {
        publicId: "pk_2f76d1f816a38a3b14fd7b38a5718",
        description: "Пожертвование в пользу благотворительного фонда Жизнь с ДЦП",
        amount: amount,
        currency: "RUB",
        skin: "mini"
      },
      function() {
        sendMetrik("PaymentCardSuccess", {"Gambler.ButtonPaymentAmount": amount});
        onSuccess();
      },
      function(reason, options) {
        sendMetrik("PaymentCardFailed");
        console.log("payment fail", reason);
      }
    );
  }, 100);
};

export const playSound = async type => {
  return;
  const audioElement = document.getElementById("sunflowerAudio");
  try {
    audioElement.muted = true;
    audioElement.play();
    audioElement.pause();
    audioElement.muted = false;
    audioElement.currentTime = 0;
  } catch (err) {
    // nothing to do
  }

  const auidoItem = audioBlobs[type]
    ? audioBlobs[type][Math.floor(Math.random() * audioBlobs[type].length)]
    : null;
  if (!auidoItem) return;
  audioElement.src = auidoItem;

  audioElement.play();
};

const isInViewport = function(el) {
  return
  var top = el.offsetTop;
  var left = el.offsetLeft;
  var width = el.offsetWidth;
  var height = el.offsetHeight;

  while (el.offsetParent) {
    el = el.offsetParent;
    top += el.offsetTop;
    left += el.offsetLeft;
  }

  return (
    top < window.pageYOffset + window.innerHeight &&
    left < window.pageXOffset + window.innerWidth &&
    top + height > window.pageYOffset &&
    left + width > window.pageXOffset
  );
};

export const sayHelloIfVisible = (node => {
  let isPlayed = false;
  return node => {
    if (isPlayed) return;
    if (isInViewport(node)) {
      sendMetrik("ButtonOnstart");
      isPlayed = true;
      playSound("hello");
    }
  };
})();

export const payBySms = amount => {
  sendMetrik("PaymentSMS");
  const isIos =
    /iPad|iPhone|iPod|Mac/.test(navigator.userAgent) && !window.MSStream;
  const href = `sms:3443${isIos ? "&" : "?"}body=подсолнух ${Math.floor(
    amount
  )}`;
  window.open(href, "_self");
};
