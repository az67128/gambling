export default {
  hello: ["hello_1"],
  down: ["button_down_2"],
  hold: [
    "button_hold_1",
    "button_hold_2",
    "button_hold_3",
  ],
  success: ["button_success_4"],
  error: [
    "button_error_2",
  ]
};
